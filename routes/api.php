<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
	Route::get('/', function () {
	    dd('test');
	});
	//Route::get('test', 'ApiController@test');

	Route::post('auth', 'AuthController@postLogin');
	//Route::post('register', 'AuthController@postRegister');

	Route::group(['prefix' => '{user_api_token}', 'middleware' => 'apiToken'], function() {
		Route::get('health-logs/stats/{date_from}/{date_to?}', 'HealthLogController@getStats');
		Route::get('health-logs/create-data', 'HealthLogController@getCreateData');
		Route::get('health-logs/{date}/{sort?}', 'HealthLogController@index');
		Route::post('health-logs', 'HealthLogController@store');
        
        Route::get('health-goals/last', 'HealthGoalController@getLastHealthGoal');
        
        //Route::post('glucometer-devices/test', 'GlucometerDeviceController@postTest');
        Route::post('glucometer-devices/sync', 'GlucometerDeviceController@syncGlucometerData');
        Route::get('glucometer-devices/glucometer', 'GlucometerDeviceController@getGlucometerForUser');
        Route::post('glucometer-devices', 'GlucometerDeviceController@store');
	});
});
