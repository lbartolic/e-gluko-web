<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::get('test', function() {
	/*$patientInfo = App\Models\Patient\PatientInfo::first();
	dd($patientInfo);*/
	$repo = new \App\Repositories\Backend\HealthLog\Glucose\GlucoseLogRepository;
	$dateFrom = '2017-06-10';
	$dateTo = '2017-07-01';
	$repo->getSummaryForPeriod(\App\Models\Access\User\User::find(2), $dateFrom, $dateTo);
});




Route::get('dashboard', 'DashboardController@index')->name('dashboard');
Route::group(['prefix' => 'patients'], function() {
    Route::group(['prefix' => '{patient}'], function() {
        Route::group(['middleware' => ['patientSetUp']], function() {
            Route::group(['namespace' => 'HealthLog'], function() {
                Route::group(['prefix' => 'health-logs', 'as' => 'health-logs.'], function() {
                    Route::get('create/{type}', [
                        'as' => 'create',
                        'uses' => 'HealthLogController@create'
                    ]);
                    Route::post('{type}', [
                        'as' => 'store',
                        'uses' => 'HealthLogController@store'
                    ]);
                    Route::get('trends/{type}/{date_from?}/{period?}', [
                        'as' => 'trends.show',
                        'uses' => 'HealthLogController@showPatientTrends'
                    ])->where('type', 'day-view|period');
                    Route::get('logs/{date?}', [
                        'as' => 'logs.show',
                        'uses' => 'HealthLogController@showPatientLogs'
                    ]);
                    Route::get('logs/{date}/pdf', [
                        'as' => 'logs.pdf',
                        'uses' => 'HealthLogController@getPatientLogsPdf'
                    ]);
                    Route::get('logs/{date}/xls', [
                        'as' => 'logs.xls',
                        'uses' => 'HealthLogController@getPatientLogsXls'
                    ]);
                    Route::get('stats/{date_from?}/{date_to?}', [
                        'as' => 'stats.show',
                        'uses' => 'HealthLogController@showPatientStats'
                    ]);
                    Route::get('stats/{date_from}/{date_to}/pdf', [
                        'as' => 'stats.pdf',
                        'uses' => 'HealthLogController@getPatientStatsPdf'
                    ]);
                    Route::group(['prefix' => 'glucose-logs', 'as' => 'glucose-logs.'], function() {
                        Route::get('summary/{date_from}/{date_to}', 'GlucoseLogController@getGlucoseSummary');
                    });
                    Route::resource('glucose-logs', 'GlucoseLogController');
                });
                Route::resource('health-logs', 'HealthLogController', ['except' => [
                    'create', 'store'
                ]]);
            });
        });
        Route::resource('health-goals', 'HealthGoal\HealthGoalController');
    });
});
Route::resource('patients', 'User\PatientController');

Route::group(['prefix' => 'fitbit', 'namespace' => 'Fitbit', 'as' => 'fitbit.'], function() {
	Route::get('authorization', [
		'as' => 'get-auth-code',
		'uses' => 'FitbitController@getAuthCode'
	]);
	Route::get('authorize', [
		'as' => 'authorize',
		'uses' => 'FitbitController@redirectToAuth'
	]);
});






Route::get('insert-dataset', function() {
	//dd();
	//die();
	// insert random extra after_meal glucose logs
	$baseDate = \Carbon\Carbon::createFromFormat('Y-m-d H:i', '2017-07-01 10:40');
	$testArr = [];
	for($i = 0; $i < 180; $i++) {
		$randMins = rand(0, 30);
		$healthLog = new App\Models\HealthLog\HealthLog;
		$glucoseLog = new App\Models\HealthLog\Glucose\GlucoseLog;
		$healthLog->user_id = 2;
		$healthLog->log_ts = $baseDate->copy()->addDays($i)->addMinutes($randMins);
		$healthLog->created_at = $healthLog->log_ts;
		$healthLog->value = number_format(rand(67, 138)/10, 1);
		$glucoseLog->health_log_label_id = 2;
		$testArr[$i]['log_ts'] = $healthLog->log_ts;
		$testArr[$i]['value'] = $healthLog->value;
		$testArr[$i]['mins'] = $randMins;
		$healthLog->log_type = 1;
		$glucoseLog->save();
		$healthLog = $glucoseLog->log()->save($healthLog);
	}
	dd($testArr);
	die();
	// insulin doses & glucose levels
	$fileContent = \Storage::get('test_datasets/Diabetes-Data/data-01');
	$delimiter = "\n";
	$lines = explode($delimiter, $fileContent);
	$partsArr = array();
	$insulinObjsArr = array();
	$glucoseObjsArr = array();
	$dateCheck = \Carbon\Carbon::createFromFormat('m-d-Y H:i', '01-22-2018 00:00');
	foreach ($lines as $key => $line) {
	    $parts = explode("\t", $line);
	    if (isset($parts[3])) {
	    	$timestamp = \Carbon\Carbon::createFromFormat('m-d-Y H:i', $parts[0] . ' ' . $parts[1]);
	    	$timestamp->addYears(26)->addMonths(4); // submonths za dodavanje 0101, za 01 bez
	    	if ($timestamp->lt($dateCheck)) {
	    		$timestamp = $timestamp->toDateTimeString();
		    	$parts[4] = $timestamp;
		    	$parts[3] = ltrim($parts[3], '0');
		    	array_push($partsArr, $parts);

		    	$code = $parts[2];
		    	$insulinCodes = [33, 34];
		    	$glucoseCodes = [48, 57, 58, 59, 60, 61, 62, 63, 64];
		    	$labelCodes = [48, 57, 58, 59, 60, 61, 62, 63, 64];

		    	$healthLog = new App\Models\HealthLog\HealthLog;
		    	$healthLog->user_id = 2;
		    	$healthLog->log_ts = $parts[4];
		    	$healthLog->created_at = $parts[4];
		    	if (in_array($code, $insulinCodes)) {
		    		$insulinLog = new App\Models\HealthLog\Insulin\InsulinLog;
		    		$insulinLog->value = $parts[3] + (rand(-2, 2));
		    		if ($code == 33) $type = 2;
		    		else $type = 1;
		    		$insulinLog->insulin_type_id = $type;
		    		$insulinLog->health_log_label_id = null;
			    	$insulinLog->user_id = 2;
			    	$insulinLog->log_ts = $parts[4];
			    	$insulinLog->created_at = $parts[4];
		    		array_push($insulinObjsArr, $insulinLog);
		    	}
		    	if (in_array($code, $glucoseCodes)) {
		    		$glucoseLog = new App\Models\HealthLog\Glucose\GlucoseLog;
		    		$healthLog->value = number_format($parts[3]/18, 1) + (rand(-20, 20)/10);
		    		$healthLog->log_type = 1;
		    		$label = 8;
		    		if ($code == 48) $label = 7;
		    		if ($code == 58) $label = 1;
		    		if ($code == 59) $label = 2;
		    		if ($code == 60) $label = 3;
		    		if ($code == 61) $label = 4;
		    		if ($code == 62) $label = 5;
		    		if ($code == 63) $label = 6;
		    		$glucoseLog->health_log_label_id = $label;

		    		$glucoseObjsArr[$parts[4]] = $glucoseLog->health_log_label_id;
		    		$glucoseLog->save();
		    		$healthLog = $glucoseLog->log()->save($healthLog);
		    	}
		    }
	    }
	}
	for ($i = 0; $i < count($insulinObjsArr); $i++) {
		if (isset($glucoseObjsArr[$insulinObjsArr[$i]->log_ts])) {
			$insulinObjsArr[$i]->health_log_label_id = $glucoseObjsArr[$insulinObjsArr[$i]->log_ts];
		}
		$healthLog = new App\Models\HealthLog\HealthLog;
    	$healthLog->user_id = $insulinObjsArr[$i]->user_id;
    	$healthLog->value = $insulinObjsArr[$i]->value;
    	$healthLog->log_ts = $insulinObjsArr[$i]->log_ts;
    	$healthLog->created_at = $insulinObjsArr[$i]->created_at;
		$healthLog->log_type = 2;
    	$insulinLogToSave = new App\Models\HealthLog\Insulin\InsulinLog;
    	$insulinLogToSave->insulin_type_id = $insulinObjsArr[$i]->insulin_type_id;
    	$insulinLogToSave->health_log_label_id = $insulinObjsArr[$i]->health_log_label_id;
		$insulinLogToSave->save();
		$healthLog = $insulinLogToSave->log()->save($healthLog);
		/*else {
			if (isset($glucoseObjsArr[$key-1]) && $glucoseObjsArr[$key-1]->log_ts == $insulinObj->log_ts) {
				$insulinObj->health_log_label_id = $glucoseObjsArr[$key-1]->health_log_label_id;
			}
			else {
				if (isset($glucoseObjsArr[$key+1]) && $glucoseObjsArr[$key+1]->log_ts == $insulinObj->log_ts) {
					$insulinObj->health_log_label_id = $glucoseObjsArr[$key+1]->health_log_label_id;
				}
			}
		}*/
	}
});