<?php

/**
 * Global Routes
 * Routes that are used between both frontend and backend.
 */

// Switch between the included languages
Route::get('lang/{lang}', 'LanguageController@swap');

/* ----------------------------------------------------------------------- */

/*
 * Frontend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    includeRouteFiles(__DIR__.'/Frontend/');
});

/* ----------------------------------------------------------------------- */

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['admin', 'dashboardGeneral']], function () {
    Route::get('/', 'DashboardController@index');
    includeRouteFiles(__DIR__.'/Backend/');
});

Route::get('test', '\App\Services\Remote\Fitbit\Fitbit@test');
Route::get('get-steps', '\App\Services\Remote\Fitbit\Fitbit@getSteps');
Route::get('get-profile', '\App\Services\Remote\Fitbit\Fitbit@getUserProfile');
Route::get('get-device', '\App\Services\Remote\Fitbit\Fitbit@getDevice');