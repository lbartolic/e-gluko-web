<?php

return [
	'diabetes_type' => [
		'1' 			=> 'orange',
		'2' 			=> 'maroon'
	],
	'glucose_level' => [
		'high' 			=> 'rgb(245, 105, 84)',
		'low' 			=> 'rgb(243, 156, 18)',
		'normal' 		=> 'rgb(0, 166, 90)'
	],
	'insulin_type' => [
		'bg_bolus' => 'rgb(52, 152, 219)',
    	'bg_basal' => 'rgb(61, 86, 112)'
	]
];
