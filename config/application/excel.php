<?php

return [
	'headings' => [
		'health_log' => [
			'all' => [
				'iznos', 'vrijeme', 'unos (0 - korisnik, 1 - uređaj)'
			],
			'models' => [
				'glucose' => [
					'vremenska oznaka', 'vremenska oznaka'
				],
				'insulin' => [
					'vremenska oznaka', 'vremenska oznaka', 'vrsta'
				],
				'calorie' => [],
				'step' => [],
				'physical_activity' => [
					'kalorije', 'koraci', 'AVG puls'
				],
				'sleep' => [
					'spavanje (min)', 'buđenje (min)', 'nemirnost (min)', 'kvaliteta sna', 
					'br. buđenja', 'br. nemirnosti'
				],
				'weight' => []
			]
		]
	]
];