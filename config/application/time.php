<?php

return [
	'now' => \Carbon\Carbon::create(2017, 8, 16, 0, 0, 0), // now()
	'date_formats' => [
		'default_date' 			=> 'd.m.Y.',
		'default_db_date' 		=> 'Y-m-d',
		'day_date'				=> 'l, d.m.Y.',
		'default_time' 			=> 'H:i',
		'default_db_time' 		=> 'H:i:s',
		'default_date_time' 	=> 'd.m.Y. H:i'
	],
	'locale_date_formats' => [
		'default_day_date'		=> '%A, %d.%m.%Y.'
	]
];