<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use Illuminate\Support\Facades\DB;

class InsulinLogTypeTableSeeder extends Seeder
{
	use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate("insulin_log_types");

        $types = [
            [
                'key'       		=> 'basal',
                'display_value'		=> 'bazalni'
            ],
            [
                'key'       		=> 'bolus',
                'display_value'		=> 'bolus'
            ]
        ];

        DB::table("insulin_log_types")->insert($types);

        $this->enableForeignKeys();
    }
}