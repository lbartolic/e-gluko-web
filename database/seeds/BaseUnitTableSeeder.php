<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use Illuminate\Support\Facades\DB;

class BaseUnitTableSeeder extends Seeder
{
	use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate("base_units");

        $units = [
            [
                'value_key'     => 'a1c',
                'value_name'	=> 'A1C',
                'unit'			=> '%'
            ],
            [
                'value_key'     => 'bp',
                'value_name'	=> 'Krvni tlak',
                'unit'			=> 'mmHg'
            ],
            [
                'value_key'     => 'chol',
                'value_name'	=> 'Kolesterol',
                'unit'			=> 'mmol/l'
            ],
            [
                'value_key'     => 'bg',
                'value_name'	=> 'Razina glukoze u krvi',
                'unit'			=> 'mmol/l'
            ],
            [
                'value_key'     => 'weight',
                'value_name'	=> 'Tjelesna masa',
                'unit'			=> 'kg'
            ],
            [
                'value_key'     => 'calories',
                'value_name'	=> 'Kalorije (energija)',
                'unit'			=> 'kcal'
            ],
            [
                'value_key'     => 'activity_length',
                'value_name'	=> 'Trajanje aktivnosti',
                'unit'			=> 'min'
            ],
            [
                'value_key'     => 'insulin',
                'value_name'	=> 'Inzulin',
                'unit'			=> 'U'
            ],
            [
                'value_key'     => 'food_ing_amt',
                'value_name'	=> 'Količina sastojka hrane',
                'unit'			=> 'g'
            ]
        ];

        DB::table("base_units")->insert($units);

        $this->enableForeignKeys();
    }
}
