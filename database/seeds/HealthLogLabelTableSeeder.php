<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use Illuminate\Support\Facades\DB;

class HealthLogLabelTableSeeder extends Seeder
{
	use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate("health_log_labels");

        $labels = [
            [
                'key'       		=> 'before-breakfast',
                'display_value'		=> 'prije doručka'
            ],
            [
                'key'       		=> 'after-breakfast',
                'display_value'		=> 'poslije doručka'
            ],
            [
                'key'       		=> 'before-lunch',
                'display_value'		=> 'prije ručka'
            ],
            [
                'key'       		=> 'after-lunch',
                'display_value'		=> 'poslije ručka'
            ],
            [
                'key'       		=> 'before-dinner',
                'display_value'		=> 'prije večere'
            ],
            [
                'key'       		=> 'after-dinner',
                'display_value'		=> 'poslije večere'
            ],
            [
                'key'       		=> 'before-bed',
                'display_value'		=> 'prije spavanja'
            ],
            [
                'key'       		=> 'other',
                'display_value'		=> 'ostalo'
            ]
        ];

        DB::table("health_log_labels")->insert($labels);

        $this->enableForeignKeys();
    }
}
