<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use Illuminate\Support\Facades\DB;

class WeightLogTypeTableSeeder extends Seeder
{
	use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate("weight_log_types");

        $types = [
            [
                'key'       		=> 'patient-input',
                'display_value'		=> 'unos pacijent'
            ],
            [
                'key'       		=> 'medical-test-input',
                'display_value'		=> 'unos med. test'
            ]
        ];

        DB::table("weight_log_types")->insert($types);

        $this->enableForeignKeys();
    }
}