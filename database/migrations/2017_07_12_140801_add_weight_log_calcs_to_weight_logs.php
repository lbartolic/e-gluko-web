<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWeightLogCalcsToWeightLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('weight_logs', function (Blueprint $table) {
            $table->integer("weight_log_calcs_id")->unsigned()->nullable();

            $table->foreign('weight_log_calcs_id')
                ->references('id')
                ->on('weight_log_calcs')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('weight_logs', function (Blueprint $table) {
            $table->dropForeign('weight_logs_weight_log_calcs_id_foreign');
            $table->dropColumn("weight_log_calcs_id");
        });
    }
}
