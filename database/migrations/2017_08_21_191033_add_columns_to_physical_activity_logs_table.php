<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToPhysicalActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('physical_activity_logs', function (Blueprint $table) {
            $table->integer('calories');
            $table->integer('steps');
            $table->integer('average_heart_rate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('physical_activity_logs', function (Blueprint $table) {
            $table->dropColumn('calories');
            $table->dropColumn('steps');
            $table->dropColumn('average_heart_rate');
        });
    }
}
