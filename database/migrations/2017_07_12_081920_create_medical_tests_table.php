<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->double('a1c')->nullable();
            $table->integer('sistolic_bp')->nullable();
            $table->integer('diastolic_bp')->nullable();
            $table->integer('weight_log_id')->unsigned()->nullable();
            $table->double('total_cholesterol')->nullable();
            $table->double('hdl_cholesterol')->nullable();
            $table->double('ldl_cholesterol')->nullable();
            $table->integer('patient_id')->unsigned()->nullable();
            $table->integer('doctor_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('weight_log_id')
                ->references('id')
                ->on('weight_logs')
                ->onDelete('restrict');
            $table->foreign('patient_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');
            $table->foreign('doctor_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_tests');
    }
}
