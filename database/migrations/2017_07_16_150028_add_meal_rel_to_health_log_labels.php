<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMealRelToHealthLogLabels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('health_log_labels', function (Blueprint $table) {
            $table->tinyInteger('meal_rel')->nullable()->comment('1: before_meal, 2: after_meal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('health_log_labels', function (Blueprint $table) {
            $table->dropColumn('meal_rel');
        });
    }
}
