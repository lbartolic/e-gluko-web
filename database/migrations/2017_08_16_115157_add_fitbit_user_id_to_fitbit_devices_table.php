<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFitbitUserIdToFitbitDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fitbit_devices', function (Blueprint $table) {
            $table->integer('fitbit_user_id')->unsigned()->nullable();

            $table->foreign('fitbit_user_id')
                ->references('id')
                ->on('fitbit_users')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fitbit_devices', function (Blueprint $table) {
            $table->dropColumn('fitbit_user_id');
        });
    }
}
