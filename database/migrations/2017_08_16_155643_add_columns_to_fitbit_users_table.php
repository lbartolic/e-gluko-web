<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToFitbitUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fitbit_users', function (Blueprint $table) {
            $table->integer('token_expires_in')->nullable();
            $table->timestamp('token_timestamp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fitbit_users', function (Blueprint $table) {
            $table->dropColumn('token_expires_in');
            $table->dropColumn('token_timestamp');
        });
    }
}
