<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBasalBolusTddToHealthGoals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('health_goals', function (Blueprint $table) {
            $table->integer('daily_basal_total')->nullable();
            $table->integer('daily_bolus_total')->nullable();
            $table->integer('tdd')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('health_goals', function (Blueprint $table) {
            $table->dropColumn('daily_basal_total');
            $table->dropColumn('daily_bolus_total');
            $table->dropColumn('tdd');
        });
    }
}
