<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlucometerDevicesLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('glucometer_devices_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('glucometer_device_id')->unsigned()->nullable();
            $table->integer('health_log_id')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('glucometer_device_id')
                ->references('id')
                ->on('glucometer_devices')
                ->onDelete('restrict');
            $table->foreign('health_log_id')
                ->references('id')
                ->on('health_logs')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('glucometer_devices_logs');
    }
}
