<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsulinLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insulin_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insulin_type_id')->unsigned()->nullable();
            $table->integer('health_log_label_id')->unsigned()->nullable();

            $table->foreign('insulin_type_id')
                ->references('id')
                ->on('insulin_log_types')
                ->onDelete('restrict');
            $table->foreign('health_log_label_id')
                ->references('id')
                ->on('health_log_labels')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insulin_logs');
    }
}
