<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsulinPrescribedDosagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::create('insulin_prescribed_dosages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insulin_type_id')->unsigned()->nullable();
            $table->string('insulin_name');
            $table->integer('bedtime_dose')->nullable();
            $table->integer('breakfast_dose')->nullable();
            $table->integer('lunch_dose')->nullable();
            $table->integer('dinner_dose')->nullable();
            $table->timestamp('prescription_date_ts')->nullable();
            $table->integer('patient_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('insulin_type_id')
                ->references('id')
                ->on('insulin_log_types')
                ->onDelete('restrict');
            $table->foreign('patient_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::dropIfExists('insulin_prescribed_dosages');*/
    }
}
