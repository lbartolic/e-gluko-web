<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToSleepLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sleep_logs', function (Blueprint $table) {
            $table->integer('asleep_mins');
            $table->integer('awake_mins');
            $table->integer('restless_mins');
            $table->integer('sleep_quality')->comment('asleep_mins/duration - percentage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sleep_logs', function (Blueprint $table) {
            $table->dropColumn('asleep_mins');
            $table->dropColumn('awake_mins');
            $table->dropColumn('restless_mins');
            $table->dropColumn('sleep_quality');
        });
    }
}
