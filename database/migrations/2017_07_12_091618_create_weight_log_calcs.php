<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeightLogCalcs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weight_log_calcs', function (Blueprint $table) {
            $table->increments('id');
            $table->double('bmi');
            $table->double('whtr');
            $table->integer('bmr');
            $table->integer('weight_log_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('weight_log_id')
                ->references('id')
                ->on('weight_logs')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weight_log_calcs');
    }
}
