<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHealthLogTypeToHealthLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('health_logs', function (Blueprint $table) {
            $table->integer('log_type')->unsigned()->nullable();
            $table->foreign('log_type')
                ->references('id')->on('health_log_types')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('health_logs', function (Blueprint $table) {
            $table->dropColumn('log_type');
        });
    }
}
