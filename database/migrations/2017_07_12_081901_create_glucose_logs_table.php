<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlucoseLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('glucose_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('health_log_label_id')->unsigned()->nullable();

            $table->foreign('health_log_label_id')
                ->references('id')
                ->on('health_log_labels')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('glucose_logs');
    }
}
