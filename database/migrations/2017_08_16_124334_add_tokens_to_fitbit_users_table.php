<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTokensToFitbitUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fitbit_users', function (Blueprint $table) {
            $table->text('auth_code')->nullable();
            $table->text('access_token')->nullable();
            $table->text('refresh_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fitbit_users', function (Blueprint $table) {
            $table->dropColumn('auth_code');
            $table->dropColumn('access_token');
            $table->dropColumn('refresh_token');
        });
    }
}
