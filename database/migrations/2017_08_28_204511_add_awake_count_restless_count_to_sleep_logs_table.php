<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAwakeCountRestlessCountToSleepLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sleep_logs', function (Blueprint $table) {
            $table->integer('awake_count');
            $table->integer('restless_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sleep_logs', function (Blueprint $table) {
            $table->dropColumn('awake_count');
            $table->dropColumn('restless_count');
        });
    }
}
