<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInsertTypeToHealthLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('health_logs', function (Blueprint $table) {
            $table->tinyInteger('insert_type')->default(0)->comment('0 - user, 1 - device');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('health_logs', function (Blueprint $table) {
            $table->dropColumn('insert_type');
        });
    }
}
