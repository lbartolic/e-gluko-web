<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHealthGolasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_goals', function (Blueprint $table) {
            $table->increments('id');
            $table->double('max_a1c')->nullable();
            $table->integer('max_sistolic_bp')->nullable();
            $table->integer('max_diastolic_bp')->nullable();
            $table->double('max_total_cholesterol')->nullable();
            $table->double('min_hdl_cholesterol')->nullable();
            $table->double('max_ldl_cholesterol')->nullable();
            $table->double('min_bg_before_meal')->nullable();
            $table->double('max_bg_before_meal')->nullable();
            $table->double('max_bg_after_meal')->nullable();
            $table->double('weight')->nullable();
            $table->integer('min_daily_steps')->nullable();
            $table->integer('min_daily_excercise')->nullable();
            $table->timestamp('goal_from_ts')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('health_goals');
    }
}
