<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('birth_date_ts');
            $table->tinyInteger('sex')->comment('male: 0, female: 1');
            $table->tinyInteger('diabetes_type')->nullable()->comment('null: no diabetes');
            $table->timestamp('diabetes_from_ts')->nullable();
            $table->integer('height')->comment('assumption that height is a constant and doesnt change');
            $table->integer('patient_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('patient_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_infos');
    }
}
