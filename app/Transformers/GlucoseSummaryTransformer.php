<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class GlucoseSummaryTransformer extends TransformerAbstract
{
    public function transform(\App\Models\Access\User\User $role)
    {
        return [
            'name' => $role->name,
            'slug' => $role->slug,
            'permissions' => $role->permissions
        ];
    }
}