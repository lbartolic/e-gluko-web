<?php

namespace App\Http\Requests\Backend\Patient;

use Illuminate\Foundation\Http\FormRequest;

class StorePatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "first_name" => "required",
            "last_name" => "required",
            "email" => "required",
            "status" => "required",
            "patientInfo.birth_date_ts" => "required",
            "patientInfo.sex" => "required",
            "patientInfo.diabetes_type" => "required",
            "patientInfo.diabetes_from_ts" => "required",
            "patientInfo.height" => "required"
        ];
        if ($this->patient == null) {
            $rules["password"] = "required";
        }
        
        return $rules;
    }
    
    public function messages()
    {
        return [
            "first_name.required"       => "Ime mora biti uneseno.",
            "last_name.required"        => "Prezime mora biti uneseno.",
            "email.required"            => "Email mora biti unesen.",
            "password.required"         => "Lozinka mora biti unesena.",
            "status.required"           => "Status mora biti unesen.",
            "patientInfo.birth_date_ts.required"    => "Datum rođenja mora biti unesen.",
            "patientInfo.sex.required"              => "Spol mora biti unesen.",
            "patientInfo.diabetes_type.required"    => "Tip dijabetesa mora biti unesen.",
            "patientInfo.diabetes_from_ts.required" => "Dijabetes od mora biti unesen.",
            "patientInfo.height.required"           => "Visina mora biti unesena."
        ];
    }
}
