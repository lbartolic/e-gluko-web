<?php

namespace App\Http\Requests\Backend\HealthGoal;

use Illuminate\Foundation\Http\FormRequest;

class StoreHealthGoalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'max_a1c' => 'required',
            'min_bg_before_meal' => 'required',
            'max_bg_before_meal' => 'required',
            'max_bg_after_meal' => 'required',
            'weight' => 'required',
            'min_daily_steps' => 'required',
            'min_daily_excercise' => 'required',
            'goal_from_ts' => 'required',
            'daily_basal_total' => 'required',
            'daily_bolus_total' => 'required',
            'tdd' => 'required'
        ];
        
        return $rules;
    }
    
    public function messages()
    {
        return [
            "max_a1c.required"                   => "MAX A1C mora biti unesen.",
            "min_bg_before_meal.required"        => "MIN prije obroka mora biti unesen.",
            "max_bg_before_meal.required"        => "MAX prije obroka mora biti unesen.",
            "max_bg_after_meal.required"         => "MAX poslije obroka mora biti unesen.",
            "weight.required"                    => "Tjelesna masa mora biti unesena.",
            "min_daily_steps.required"           => "MIN kretanje mora biti uneseno.",
            "min_daily_excercise.required"       => "MIN aktivnost mora biti unesena.",
            "goal_from_ts.required"              => "Datum od mora biti unesen.",
            "daily_basal_total.required"         => "Uk. bazalni mora biti unesen.",
            "daily_bolus_total.required"         => "Uk. bolus mora biti unesen.",
            "tdd.required"                       => "TDD mora biti unesen."
        ];
    }
}
