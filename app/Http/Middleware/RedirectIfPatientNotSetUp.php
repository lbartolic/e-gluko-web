<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Access\User\User;

class RedirectIfPatientNotSetUp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where('id', $request->route('patient'))->firstOrFail();
        $healthGoals = $user->healthGoals()->get();
        if ($healthGoals->count() == 0) {
            return redirect()->route('admin.health-goals.index', $user->id);
        }
        return $next($request);
    }
}
