<?php

namespace App\Http\Middleware;

use Closure;

class DashboardGeneral
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authUser = access()->user();
        if ($request->is('admin/patients/*')) {
            $patientId = $request->route('patient');
            if ($authUser->id != $patientId && !$authUser->hasRole(1)) {
                return redirect()->route(homeRoute());
            }
        }
        return $next($request);
    }
}
