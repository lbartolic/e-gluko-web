<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Auth;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
    	if (Auth::user()->hasRole(1)) return redirect()->route('admin.patients.index');
    	else return redirect()->route('admin.patients.show', Auth::user()->id);
    }
}
