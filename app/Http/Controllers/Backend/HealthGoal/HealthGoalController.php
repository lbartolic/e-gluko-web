<?php

namespace App\Http\Controllers\Backend\HealthGoal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Models\Goal\HealthGoal;
use Carbon\Carbon;

class HealthGoalController extends Controller
{
    public function __construct() {
        $this->middleware('access.routeNeedsRole:1')->except(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($userId)
    {
        $user = User::where('id', $userId)->first();
        $healthGoals = $user->healthGoals;
        return view('backend.layouts.health-goals.index', [
            'user' => $user,
            'goals' => $healthGoals,
            'lastGoal' => $healthGoals->sortByDesc('goal_from_ts')->first()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\Backend\HealthGoal\StoreHealthGoalRequest $request, $userId)
    {
        $patient = User::where('id', $userId)->first();
        
        $inputs = $request->except("_token");
        $inputs['goal_from_ts'] = Carbon::createFromFormat('d.m.Y.', $inputs['goal_from_ts']);
        
        $healthGoal = $patient->healthGoals()->create($inputs);
        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $userId, $id)
    {
        HealthGoal::where('id', $id)->first()->delete($id);
        
        return redirect()->back();
    }
}
