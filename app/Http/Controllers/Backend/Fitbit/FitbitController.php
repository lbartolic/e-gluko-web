<?php

namespace App\Http\Controllers\Backend\Fitbit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Services\Remote\Fitbit\Fitbit;
use Auth;

class FitbitController extends Controller
{
    private $fitbit;

    public function __construct(Fitbit $fitbit) {
        $this->middleware('access.routeNeedsRole:2');
        $this->fitbit = $fitbit;
    }

    public function getAuthCode(Request $request) {
        $authCode = $request->input('code');
        if ($authCode) {
            $user = Auth::user();
            $fitbitUser = $this->fitbit->initialFitbitUserSetup($user, $authCode);

            return redirect()->route('admin.patients.show', ['patient' => $user->id])
                ->with('fitbit_success', 'success');
        }
        return redirect()->route('admin.dashboard');
    }

    public function redirectToAuth() {
        $clientId = $this->fitbit::CLIENT_ID;

        $authUrl = "https://www.fitbit.com/oauth2/authorize?response_type=code&client_id={$clientId}&scope=activity%20nutrition%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight";

        return redirect()->to($authUrl);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
