<?php

namespace App\Http\Controllers\Backend\HealthLog;

use Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\Access\User\User;
use App\Repositories\Backend\HealthGoal\HealthGoalRepository;
use App\Repositories\Backend\HealthLog\HealthLogRepository;
use App\Repositories\Backend\HealthLog\Glucose\GlucoseLogRepository;
use App\Repositories\Backend\HealthLog\Insulin\InsulinLogRepository;
use App\Repositories\Backend\HealthLog\Step\StepLogRepository;
use App\Repositories\Backend\HealthLog\Calorie\CalorieLogRepository;
use App\Repositories\Backend\HealthLog\PhysicalActivity\PhysicalActivityLogRepository;
use App\Repositories\Backend\HealthLog\Sleep\SleepLogRepository;
use App\Models\HealthLog\HealthLogType;
use App\Models\HealthLog\HealthLogLabel;
use App\Models\HealthLog\Glucose\GlucoseLog;
use App\Models\HealthLog\HealthLog;
use App\Models\HealthLog\Weight\WeightLog;
use App\Models\HealthLog\Weight\WeightLogCalc;
use PDF;
use Excel;

class HealthLogController extends Controller
{
    private $healthGoalRepo;
    private $glucoseLogRepo;
    private $insulinLogRepo;
    protected $allowedPeriods = [
        'day-view' => ['1w', '1m', '3m'],
        'period' => ['1w', '1m', '3m']
    ];

    public function __construct(HealthGoalRepository $healthGoalRepo, HealthLogRepository $healthLogRepo, GlucoseLogRepository $glucoseLogRepo, InsulinLogRepository $insulinLogRepo, StepLogRepository $stepLogRepo, CalorieLogRepository $calorieLogRepo, PhysicalActivityLogRepository $physicalActivityLogRepo, SleepLogRepository $sleepLogRepo) {
        $this->now = Carbon::instance(config('application.time.now'));
        $this->healthGoalRepo = $healthGoalRepo;
        $this->healthLogRepo = $healthLogRepo;
        $this->glucoseLogRepo = $glucoseLogRepo;
        $this->insulinLogRepo = $insulinLogRepo;
        $this->stepLogRepo = $stepLogRepo;
        $this->calorieLogRepo = $calorieLogRepo;
        $this->physicalActivityLogRepo = $physicalActivityLogRepo;
        $this->sleepLogRepo = $sleepLogRepo;
    }

    public function showPatientLogs(Request $request, $userId, $date = null) {
        $user = User::findOrFail($userId);

        if ($date == null) $date = $this->healthLogRepo->getLastLogDate($user);
        else $date = Carbon::parse($date);

        $dateFrom = $date->copy()->startOfWeek();
        $dateTo = $date->copy()->endOfWeek();
        $healthGoals = $this->healthGoalRepo->getPatientGoalsBetweenDates($user, $dateFrom, $dateTo);
        $healthLogs = $this->healthLogRepo->getPatientLogsList($user, $healthGoals, $dateFrom, $dateTo, true);
        return view('backend.layouts.health-logs.logs', [
            'logs' => $healthLogs,
            'user' => $user,
            'next' => $date->copy()->addWeeks(1)->toDateString(),
            'prev' => $date->copy()->subWeeks(1)->toDateString()
        ]);
    }

    public function getPatientLogsPdf(Request $request, $userId, $date) {
        $dateFrom = Carbon::parse($date)->startOfMonth();
        $dateTo = Carbon::parse($date)->endOfMonth();
        $user = User::findOrFail($userId);
        $healthGoals = $this->healthGoalRepo->getPatientGoalsBetweenDates($user, $dateFrom, $dateTo);
        $healthLogs = $this->healthLogRepo->getPatientLogsList($user, $healthGoals, $dateFrom, $dateTo, true);
        $pdf = PDF::loadView('pdf.health-logs.logs', [
            'logs' => $healthLogs,
            'user' => $user
        ]);
        $fileName = 'logs-' . str_slug($user->name) . '-' . $dateFrom->toDateString() . '-' . $dateTo->toDateString();
        return $pdf->download("{$fileName}.pdf");
    }

    public function getPatientStatsPdf(Request $request, $userId, $dateFrom, $dateTo) {
        $dateFrom = Carbon::parse($dateFrom);
        $dateTo = Carbon::parse($dateTo);
        $daysDiff = $dateTo->diffInDays($dateFrom);
        $user = User::findOrFail($userId);
        $healthGoals = $this->healthGoalRepo->getPatientGoalsBetweenDates($user, $dateFrom, $dateTo);
        $stats = $this->healthLogRepo->getSeparatedLogStats($user, $dateFrom, $dateTo);
        $pdf = PDF::loadView('pdf.health-logs.stats', [
            'stats' => $stats,
            'user' => $user,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo
        ]);
        $fileName = 'stats-' . str_slug($user->name) . '-' . $dateFrom->toDateString() . '-' . $dateTo->toDateString();
        return $pdf->download("{$fileName}.pdf");
    }

    public function getPatientLogsXls(Request $request, $userId, $date) {
        $dateFrom = Carbon::parse($date)->startOfMonth();
        $dateTo = Carbon::parse($date)->endOfMonth();
        $user = User::findOrFail($userId);
        $fileName = 'logs-' . str_slug($user->name) . '-' . $dateFrom->toDateString() . '-' . $dateTo->toDateString();
        return $this->healthLogRepo->generateXlsExport($userId, $dateFrom, $dateTo, $fileName);
    }

    public function showPatientStats(Request $request, $userId, $dateFrom = null, $dateTo = null) {
        $user = User::findOrFail($userId);

        if ($dateTo == null) $dateTo = $this->healthLogRepo->getLastLogDate($user);
        else $dateTo = Carbon::parse($dateTo);

        if ($dateFrom == null) $dateFrom = $dateTo->copy()->subMonths(1);
        else $dateFrom = Carbon::parse($dateFrom);
        $daysDiff = $dateTo->diffInDays($dateFrom);

        $healthGoals = $this->healthGoalRepo->getPatientGoalsBetweenDates($user, $dateFrom, $dateTo);
        
        $stats = $this->healthLogRepo->getSeparatedLogStats($user, $dateFrom, $dateTo);

        return view('backend.layouts.health-logs.stats', [
            'stats' => $stats,
            'user' => $user,
            'healthGoals' => $healthGoals,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'next_to' => $dateTo->copy()->addDays($daysDiff)->toDateString(),
            'prev_to' => $dateTo->copy()->subDays($daysDiff)->toDateString(),
            'next_from' => $dateFrom->copy()->addDays($daysDiff)->toDateString(),
            'prev_from' => $dateFrom->copy()->subDays($daysDiff)->toDateString()
        ]);
    }

    public function showPatientTrends(Request $request, $userId, $type, $dateFrom = null, $period = null) {
        $user = User::findOrFail($userId);
        
        if ($period == null || !in_array($period, $this->allowedPeriods[$type])) {
            ($type == 'day-view') ? $period = '1m' : $period = '1w';
        }
        
        if ($dateFrom == null) {
            $refDate = $this->healthLogRepo->getLastLogDate($user);
            $dateFrom = $this->healthLogRepo->getOtherDateForPeriod($refDate, $period, 0);
        }
        else {
            $dateFrom = Carbon::parse($dateFrom);
        }
        $dateTo = $this->healthLogRepo->getOtherDateForPeriod($dateFrom, $period, 1);
        $prevDateFrom = $this->healthLogRepo->getOtherDateForPeriod($dateFrom, $period, 0);
        $nextDateFrom = $dateTo;

        $healthGoals = $this->healthGoalRepo->getPatientGoalsBetweenDates($user, $dateFrom, $dateTo);

        $stdWeekDays = false;
        if ($type == 'day-view') $stdWeekDays = true;
        
        $separatedLogLists = $this->healthLogRepo->getSeparatedLogLists($user, $healthGoals, $dateFrom, $dateTo, $stdWeekDays);

        return view('backend.layouts.health-logs.trends', [
            'type' => $type,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'nextDateFrom' => $nextDateFrom->toDateString(),
            'prevDateFrom' => $prevDateFrom->toDateString(),
            'period' => $period,
            'user' => $user,
            'glucoseLogsList' => $separatedLogLists['glucose'],
            'insulinLogsList' => $separatedLogLists['insulin'],
            'stepLogsList' => $separatedLogLists['step'],
            'calorieLogsList' => $separatedLogLists['calorie'],
            'physicalActivityLogsList' => $separatedLogLists['physical_activity'],
            'sleepLogsList' => $separatedLogLists['sleep']
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $userId, $type)
    {
        if ($request->ajax()) {
            // return data needed to create new log
            //$logType = HealthLogType::where('type', $type)->firstOrFail();
            $units = config()->get('base_units');
            $data = null;
            switch($type) {
                case 'glucose':
                    $labels = HealthLogLabel::get();
                    $data['labels'] = $labels;
                    break;
                case 'weight':
                    break;
                case 'insulin':

                    break;
            }

            return response()->json(['units' => $units, 'data' => $data]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $userId, $type)
    {
        // pretvoriti u repository metode (za pojedini tip ili sve skupa)
        if ($request->ajax()) {
            $user = User::findOrFail($userId);
            $type = HealthLogType::where('type', $type)->firstOrFail();
            $healthLog = new HealthLog;
            $logValue = $request->input('value');
            $logTs = $request->input('log_ts');
            $healthLog->value = $logValue;
            $healthLog->log_ts = $logTs;
            $healthLog->logType()->associate($type);
            $healthLog->patient()->associate($user);
            if ($type->type == 'glucose') {
                $log = new GlucoseLog;
                $label = HealthLogLabel::findOrFail($request->input('label_id'));
                $log->label()->associate($label);
                $log->save();
            }
            if ($type->type == 'insulin') {
                $log = new InsulinLog;
                $label = HealthLogLabel::findOrFail($request->input('label_id'));
                $insulinType = InsulinLogType::findOrFail($request->input('insulin_type_id'));
                $log->label()->associate($label);
                $log->insulinType()->associate($insulinType);
                $log->save();
            }
            if ($type->type == 'weight') {
                $log = new WeightLog;
                $log->waist = $request->input('waist');
                $log->save();

                $waistValue = $log->waist;
                $weightCalcs = new WeightLogCalc;
                $patientInfo = $user->patientInfo;
                $height = $patientInfo->height;
                // $this->weightLogRepo->calculateWeightCalcsValues($weight, $height, $years)
                $bmi = $logValue/(pow($height/100, 2));
                $bmr = (10*$logValue) + (6.25*$height) - (5*$patientInfo->years) + 5;
                $whtr = $waistValue/$height;
                $weightCalcs->bmi = $bmi;
                $weightCalcs->bmr = $bmr;
                $weightCalcs->whtr = $whtr;
                $weightCalcs->log()->associate($log);
                $weightCalcs = $weightCalcs->save();
            }

            $healthLog = $log->log()->save($healthLog);
            return response()->json($healthLog);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
