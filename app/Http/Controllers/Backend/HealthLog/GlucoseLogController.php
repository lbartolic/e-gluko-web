<?php

namespace App\Http\Controllers\Backend\HealthLog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\HealthLog\Glucose\GlucoseLogRepository;
use App\Repositories\Backend\HealthGoal\HealthGoalRepository;
use App\Models\Access\User\User;

class GlucoseLogController extends Controller
{
    private $glucoseLogRepo;

    public function __construct(GlucoseLogRepository $glucoseLogRepo, HealthGoalRepository $healthGoalRepo) {
        $this->glucoseLogRepo = $glucoseLogRepo;
        $this->healthGoalRepo = $healthGoalRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getGlucoseSummary(Request $request, $userId, $dateFrom, $dateTo) {
        if ($request->ajax()) {
            $user = User::find($userId);
            $summary = $this->glucoseLogRepo->getPatientGlucoseSummary($user, $dateFrom, $dateTo);
            return response()->json($summary);
        }
    }

    //public function getLogsList(Request $request, )
}
