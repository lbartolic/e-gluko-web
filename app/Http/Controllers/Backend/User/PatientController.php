<?php

namespace App\Http\Controllers\Backend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Patient\PatientRepository;
use App\Repositories\Backend\HealthLog\Glucose\GlucoseLogRepository;
use App\Repositories\Backend\HealthLog\Insulin\InsulinLogRepository;
use App\Models\Access\User\User;
use App\Models\Patient\PatientInfo;
use App\Repositories\Backend\HealthGoal\HealthGoalRepository;
use App\Repositories\Backend\HealthLog\HealthLogRepository;
use Carbon\Carbon;
use Auth;

class PatientController extends Controller
{
    private $patientRepo;
    private $glucoseLogRepo;

    public function __construct(PatientRepository $patientRepo, HealthGoalRepository $healthGoalRepo, HealthLogRepository $healthLogRepo) {
        $this->now = Carbon::instance(config('application.time.now'));
        $this->patientRepo = $patientRepo;
        $this->healthGoalRepo = $healthGoalRepo;
        $this->healthLogRepo = $healthLogRepo;
        
        $this->middleware('patientSetUp')->except(['index', 'create', 'store']);
        $this->middleware('access.routeNeedsRole:1')->except(['show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dateTo = $this->now->copy();
        $dateFrom = $dateTo->copy()->subDays(7);   
        $user = User::where('id', 2)->first();
        
        $patients = $this->patientRepo->get(true);
        $patients->map(function($patient) use($dateTo, $dateFrom) {
            $stats = $this->healthLogRepo->getSeparatedLogStats($patient, $dateFrom, $dateTo);
            $patient["stats"] = $stats;
        });
        
        return view('backend.layouts.patients.index', [
            'patients' => $patients
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.layouts.patients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\Backend\Patient\StorePatientRequest $request)
    {
        $user = $this->patientRepo->storeOrUpdate($request);
        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // TODO: handle patient check!
        $patient = User::with('patientMedicalTests', 'healthGoals', 'fitbitUser.device', 'glucometerDevice')->find($id);
        $lastGoal = $patient->healthGoals->sortByDesc('goal_from_ts')->first();
        
        $lastLog = $this->healthLogRepo->getLastPatientLog($patient);
        if ($lastLog) $date = $lastLog->log_ts;
        else $date = $this->now;
        $dateFrom = $date->copy()->startOfDay();
        $dateTo = $date->copy()->endOfDay();

        $healthGoals = $this->healthGoalRepo->getPatientGoalsBetweenDates($patient, $dateFrom, $dateTo);
        $healthLogs = $this->healthLogRepo->getPatientLogsList($patient, $healthGoals, $dateFrom, $dateTo, true);
        
        if ($patient->fitbitUser) $fitbitDevice = $patient->fitbitUser->device;
        else $fitbitDevice = null;

        return view('backend.layouts.patients.show', [
            'patient' => $patient,
            'lastGoal' => $lastGoal,
            'glucometer' => $patient->glucometerDevice,
            'fitbit' => $fitbitDevice,
            'logs' => $healthLogs
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('backend.layouts.patients.create', ['patient' => User::with('patientInfo')->where('id', $id)->first()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\Backend\Patient\StorePatientRequest $request, $id)
    {
        $user = $this->patientRepo->storeOrUpdate($request, $id);
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
