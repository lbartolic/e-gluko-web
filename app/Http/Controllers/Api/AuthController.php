<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User\User;

class AuthController extends Controller
{
    public function postLogin(Request $request) {
    	$email = $request->input('email');
    	$password = $request->input('password');
    	return $this->authenticate($email, $password);
    }

    protected function authenticate($email, $password) {
    	if (Auth::once(['email' => $email, 'password' => $password])) {
            $user = Auth::user();
            return $user->toJson();
        }
        return response('Unauthorized.', 401);
    }

    public function postRegister(Request $request) {
    	$firstName = $request->input("first_name");
    	$lastName = $request->input("last_name");
    	$email = $request->input("email");
    	$password = $request->input("password");
    	$passwordRepeat = $request->input("password_repeat");

    	if ($password == $passwordRepeat) {
    		$emailCheck = User::where('email', $email)->first();
    		if (!$emailCheck) {
	    		$user = new User();
	    		$user->first_name = $firstName;
	    		$user->last_name = $lastName;
	    		$user->email = $email;
	    		$user->password = bcrypt($password);

	    		$hash = \Carbon\Carbon::now() . $email;
	    		$user->api_token = md5($hash);
	    		$user->save();

	    		return $user;
	    	}
	    	return response('Forbidden.', 403);
    	}
    	return response('Unauthorized.', 401);
    }
}
