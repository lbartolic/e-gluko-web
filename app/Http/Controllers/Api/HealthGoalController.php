<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\HealthLog\HealthLogLabel;
use App\Models\HealthLog\Insulin\InsulinLogType;
use App\Models\Access\User\User;

use App\Repositories\Backend\HealthGoal\HealthGoalRepository;
use App\Repositories\Backend\HealthLog\HealthLogRepository;

class HealthGoalController extends Controller
{
    public function __construct(HealthGoalRepository $healthGoalRepo, HealthLogRepository $healthLogRepo) {
        $this->now = Carbon::instance(config('application.time.now'));
        $this->healthGoalRepo = $healthGoalRepo;
    }

    public function getLastHealthGoal(Request $request, User $user) {
        $lastGoal = $user->healthGoals()->get()->sortByDesc('goal_from_ts')->first();
        return response()->json($lastGoal, 200);
    }
}
