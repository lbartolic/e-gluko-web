<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\GlucometerDevice\GlucometerDevice;
use App\Models\Access\User\User;
use App\Models\HealthLog\HealthLog;
use App\Models\HealthLog\HealthLogType;
use Carbon\Carbon;
use App\Models\HealthLog\Glucose\GlucoseLog;
use App\Models\HealthLog\HealthLogLabel;

class GlucometerDeviceController extends Controller
{
    public function postTest(Request $request) {
        $data = $request->json()->all();
        $txtTest = "";
        foreach($data as $record) {
            $txtTest .= $record['timestamp'] . ": " . $record['glucoseConcentration'] . " " . $record['meal'] . "\n";
        }
        $dataCount = count($data);
        Storage::append('api/glucometer-device-controller-tests.txt', $txtTest);
        $newDataCount = $dataCount;
        $syncCount = $dataCount;
        return response()->json([
            'received' => $dataCount,
            'new_data' => $newDataCount,
            'synced' => $syncCount
        ], 200);
    }
    
    public function syncGlucometerData(Request $request, User $user) {
        $type = HealthLogType::where('type', 'glucose')->firstOrFail();
        $glucometerDevice = $user->glucometerDevice()->first();
        $lastSyncedLog = $glucometerDevice->healthLogs()->orderBy('log_ts', 'DESC')->first();
        
        $dtStart = Carbon::createFromTime(0, 0, 0);
        $dtLunch = Carbon::createFromTime(11, 0, 0);
        $dtDinner = Carbon::createFromTime(17, 0, 0);
        $dtEnd = Carbon::createFromTime(23, 59, 59);
            
        $data = $request->json()->all();
        $healthLogs = [];
        $newHealthLogsCount = 0;
        foreach($data as $record) {
            $recordTsObj = Carbon::create($record['time']['year'], $record['time']['month']+1, $record['time']['dayOfMonth'], 
                    $record['time']['hourOfDay'], $record['time']['minute'], $record['time']['second']);
            if ($lastSyncedLog == null || $lastSyncedLog->log_ts->lt($recordTsObj)) {
                $newHealthLogsCount++;
                $timestampTime = $recordTsObj->copy()->format('H:i:s');
                $recordTimeObj = Carbon::createFromFormat("H:i:s", $timestampTime);

                $meal = -1;
                $labelKey = "other";
                if (isset($record["context"])) {
                    $meal = $record["context"]["meal"];
                    if ($recordTimeObj->between($dtStart, $dtLunch)) {
                        ($meal == 1) ? $labelKey = "before-breakfast" : $labelKey = "after-breakfast";
                    }
                    else if ($recordTimeObj->between($dtLunch, $dtDinner)) {
                        ($meal == 1) ? $labelKey = "before-lunch" : $labelKey = "after-lunch";
                    }
                    else if ($recordTimeObj->between($dtDinner, $dtEnd)) {
                        ($meal == 1) ? $labelKey = "before-dinner" : $labelKey = "after-dinner";
                    }
                }

                $healthLog = new HealthLog();
                $healthLog->value = round($record['glucoseConcentration']*1000, 1);
                $healthLog->log_ts = $recordTsObj;
                $healthLog->insert_type = 1;
                $healthLog->logType()->associate($type);
                $healthLog->patient()->associate($user);

                $log = new GlucoseLog();
                $label = HealthLogLabel::where('key', $labelKey)->firstOrFail();
                $log->label()->associate($label);
                $log->save();

                $log->log()->save($healthLog);
                $glucometerDevice->healthLogs()->attach($healthLog->id);
                $healthLogs[] = $healthLog;
            }
        }
        
        $glucometerDevice->last_sync_ts = Carbon::now();
        $glucometerDevice->save();
        
        $dataCount = count($data);
        $newDataCount = $newHealthLogsCount;
        $syncCount = count($healthLogs);
        
        return response()->json([
            'received' => $dataCount,
            'new_data' => $newDataCount,
            'synced' => $syncCount
        ], 200);
    }
    
    public function getGlucometerForUser(Request $request, User $user) {
        $glucometer = $user->glucometerDevice()->first();
        return response()->json($glucometer, 200);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $glucometerDevice = new GlucometerDevice();
        $glucometerDevice->device_address = $request->input('device_address');
        $glucometerDevice->name = $request->input('name');
        
        $user->glucometerDevice()->save($glucometerDevice);
        
        return response()->json($glucometerDevice, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
