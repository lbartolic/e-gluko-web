<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\HealthLog\HealthLogLabel;
use App\Models\HealthLog\Insulin\InsulinLogType;

use App\Repositories\Backend\HealthGoal\HealthGoalRepository;
use App\Repositories\Backend\HealthLog\HealthLogRepository;

class HealthLogController extends Controller
{
    public function __construct(HealthGoalRepository $healthGoalRepo, HealthLogRepository $healthLogRepo) {
        $this->now = Carbon::instance(config('application.time.now'));
        $this->healthGoalRepo = $healthGoalRepo;
        $this->healthLogRepo = $healthLogRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $user, $date = null, $sort = null)
    {
        // currently: fixed amount of logs: weekly
        if ($date == null) {
            $lastLog = $this->healthLogRepo->getLastPatientLog($patient);
            if ($lastLog) $date = $lastLog->log_ts;
            else $date = $this->now;
        }
        $date = Carbon::parse($date);
        $dateFrom = $date->copy()->startOfWeek();
        $dateTo = $date->copy()->endOfWeek();

        $healthGoals = $this->healthGoalRepo->getPatientGoalsBetweenDates($user, $dateFrom, $dateTo);
        $healthLogs = $this->healthLogRepo->getPatientLogsList($user, $healthGoals, $dateFrom, $dateTo, true, $sort);

        return json_encode($healthLogs);
    }

    public function getStats(Request $request, $user, $dateFrom, $dateTo = null)
    {
        // currently: fixed amount of stats: monthly (add possibility to choose a range in the future)
        $dateFrom = Carbon::parse($dateFrom);
        $dateTo = $dateFrom->copy()->addMonth();       

        $healthGoals = $this->healthGoalRepo->getPatientGoalsBetweenDates($user, $dateFrom, $dateTo);
        $stats = $this->healthLogRepo->getSeparatedLogStats($user, $dateFrom, $dateTo);

        $data = [
            'date_from' => $dateFrom->toDateString(),
            'date_to' => $dateTo->toDateString(),
            'stats' => $stats,
            'health_goals' => $healthGoals
        ];

        return json_encode($data);
    }

    public function getCreateData() {
        $labels = HealthLogLabel::get();
        $types = InsulinLogType::get();

        $data = [
            'labels' => $labels,
            'insulin_types' => $types
        ];
        return json_encode($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $user)
    {
        $userId = $user->id;
        $healthLogTypeKey = $request->input('log_type_key');
        $logValue = $request->input('value');
        $logTs = $request->input('log_ts');
        $labelId = $request->input('health_log_label_id');
        $insulinTypeId = $request->input('insulin_type_id');
        
        $healthLog = $this->healthLogRepo->storeHealthLog($userId, $healthLogTypeKey, $logValue, $logTs, $labelId, $insulinTypeId);

        return json_encode($healthLog);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
