<?php

namespace App\Repositories\Backend\Patient;

use App\Models\Patient\PatientInfo;
use App\Models\Access\User\User;
use Illuminate\Http\Request;
use Carbon\Carbon;

class PatientRepository {
	protected $now;

	public function __construct() {
		$this->now = config('application.time.now');
	}

    public function get($lastLog = false) {
        if ($lastLog == true) {
            return User::with(['healthLogs' => function($q) {
                $q->orderBy('log_ts', 'DESC')->first();
            }])->patients()->get();
        }
        else {
            return User::patients()->get();
        }
    }
    
    public function storeOrUpdate(Request $request, $id = null) {
        if ($id == null) {
            $user = new User();
            $user->api_token = md5(Carbon::now() . str_random(10));
            $patientInfo = new PatientInfo();
        }
        else {
            $user = User::where('id', $id)->first();
            $patientInfo = $user->patientInfo()->first();
        }
        
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        if (($id != null && $request->input('password') != '' && $request->input('password') != null) || $id == null) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->status = $request->input('status');
        
        $user->save();
        if ($id == null) $user->roles()->attach(2);
        
        $patientInfo->birth_date_ts = Carbon::createFromFormat('d.m.Y.', $request->input('patientInfo.birth_date_ts'));
        $patientInfo->sex = $request->input('patientInfo.sex');
        $patientInfo->diabetes_type = $request->input('patientInfo.diabetes_type');
        $patientInfo->diabetes_from_ts = Carbon::createFromFormat('d.m.Y.', $request->input('patientInfo.diabetes_from_ts'));
        $patientInfo->height = $request->input('patientInfo.height');
        
        $user->patientInfo()->save($patientInfo);
        
        return $user;
    }
}
