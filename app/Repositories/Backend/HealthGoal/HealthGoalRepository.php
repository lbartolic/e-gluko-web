<?php

namespace App\Repositories\Backend\HealthGoal;

use Carbon\Carbon;

use App\Models\Goal\HealthGoal;
use App\Models\Access\User\User;

class HealthGoalRepository {
    protected $now;

    public function __construct() {
        $this->now = Carbon::instance(config('application.time.now'));
    }

    public function getPatientGoalsBetweenDates($user, $dateFrom, $dateTo) {
    	$dateFromObj = Carbon::parse($dateFrom);
    	$dateToObj = Carbon::parse($dateTo);
        $previousGoal = $user->healthGoals()->orderBy('goal_from_ts', 'DESC')->whereDate('goal_from_ts', '<=', $dateFromObj)->take(1)->get();
        $betweenGoals = $user->healthGoals()->orderBy('goal_from_ts', 'DESC')->whereBetween('goal_from_ts', [$dateFromObj, $dateToObj])->get();
        return $previousGoal->merge($betweenGoals);
    }
}
