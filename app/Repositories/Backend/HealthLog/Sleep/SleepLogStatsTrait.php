<?php

namespace App\Repositories\Backend\HealthLog\Sleep;

use Illuminate\Support\Collection;

trait SleepLogStatsTrait {
    /*
     * Calculate insulin log data depending on associated health goals
     */
    protected function calculateSleepStats(Collection $logs, Collection $healthGoals, $daysDiff) {
        $totalLogs = $logs->count();
        $loggedDays = count($this->getLogsGroupedByDay($logs));
        $tempStatsData = [
            'total_duration' => ['value' => 0],
            'total_asleep_mins' => 0,
            'total_awake_mins' => 0,
            'total_awake_count' => 0,
            'total_restless_mins' => 0,
            'total_restless_count' => 0,
            'avg_quality' => 0,
            'daily_avg_duration' => 0, // for days with activities
        ];

        $logs->each(function($log, $logKey) use(&$logs, &$tempStatsData) {
            $logDate = $log->log_ts->format('Y-m-d');
            $logVal = $log->value;
            $tempStatsData['total_duration']['value'] += $logVal;
            $tempStatsData['avg_quality'] += $log->loggable->sleep_quality;
            $tempStatsData['total_asleep_mins'] += $log->loggable->asleep_mins;
            $tempStatsData['total_awake_mins'] += $log->loggable->awake_mins;
            $tempStatsData['total_restless_mins'] += $log->loggable->restless_mins;
            $tempStatsData['total_awake_count'] += $log->loggable->awake_count;
            $tempStatsData['total_restless_count'] += $log->loggable->restless_count;
        });
        $avgDuration = 0;
        $avgAsleepMins = 0;
        $avgRestlessMins = 0;
        $avgAwakeMins = 0;
        $avgRestlessCount = 0;
        $avgAwakeCount = 0;
        if ($totalLogs > 0) {
            $avgDuration = $this->formatDurationMsValue($tempStatsData['total_duration']['value']/$totalLogs);
            $avgAsleepMins = $this->formatDurationMinValue($tempStatsData['total_asleep_mins']/$totalLogs);
            $avgRestlessMins = $this->formatDurationMinValue($tempStatsData['total_restless_mins']/$totalLogs);
            $avgAwakeMins = $this->formatDurationMinValue($tempStatsData['total_awake_mins']/$totalLogs);
            $avgRestlessCount = round($tempStatsData['total_restless_count']/$totalLogs, 0);
            $avgAwakeCount = round($tempStatsData['total_awake_count']/$totalLogs, 0);
            $tempStatsData['avg_quality'] = round($tempStatsData['avg_quality']/$totalLogs, 0);
        }

        // divided by $loggedDays to skip days that have no steps (probably device was not worn - DISCUSSABLE!)
        if ($loggedDays > 0) {
            $tempStatsData['daily_avg_duration'] = $this->formatDurationMsValue($tempStatsData['total_duration']['value']/$loggedDays);
        }

        return [
            'logged_days' => $loggedDays,
            'total_logs' => $totalLogs,
            'total_duration' => $tempStatsData['total_duration'],
            'asleep' => [
                'total_mins' => $tempStatsData['total_asleep_mins'],
                'avg_mins' => $avgAsleepMins
            ],
            'awake' => [
                'total_mins' => $tempStatsData['total_awake_mins'],
                'total_count' => $tempStatsData['total_awake_count'],
                'avg_mins' => $avgAwakeMins,
                'avg_count' => $avgAwakeCount
            ],
            'restless' => [
                'total_mins' => $tempStatsData['total_restless_mins'],
                'total_count' => $tempStatsData['total_restless_count'],
                'avg_mins' => $avgRestlessMins,
                'avg_count' => $avgRestlessCount
            ],
            'daily_avg_duration' => $tempStatsData['daily_avg_duration'],
            'avg_duration' => $avgDuration,
            'avg_quality' => $tempStatsData['avg_quality']
        ];
    }
}
