<?php

namespace App\Repositories\Backend\HealthLog;

use Carbon\Carbon;
use Illuminate\Support\Collection;

use App\Models\Unit\UnitTrait;
use App\Repositories\Backend\HealthLog\BaseHealthLogStatsTrait;
use App\Models\Access\User\User;
use App\Models\HealthGoal\HealthGoal;
use App\Models\HealthLog\Glucose\GlucoseLog;
use App\Models\HealthLog\Insulin\InsulinLog;
use App\Models\HealthLog\Step\StepLog;
use App\Models\HealthLog\Calorie\CalorieLog;
use App\Models\HealthLog\PhysicalActivity\PhysicalActivityLog;
use App\Repositories\Backend\HealthGoal\HealthGoalRepository;
use App\Repositories\Backend\HealthLog\Glucose\GlucoseLogStatsTrait;
use App\Repositories\Backend\HealthLog\Glucose\GlucoseLogRepository;
use App\Repositories\Backend\HealthLog\BaseHealthLogRepository;

abstract class AbstractHealthLogRepository extends BaseHealthLogRepository {
	protected $now;
	protected $healthGoals;

	public function __construct() {
		$this->now = Carbon::instance(config('application.time.now'));
	}

    abstract protected function getLogsForList(User $user, Carbon $dateFromObj, Carbon $dateToObj);
    abstract protected function getCalculatedStats(Collection $logs, Collection $healthGoals, $daysDiff);

    public function getPatientLogsList(User $user, Collection $healthGoals, $dateFrom, $dateTo, $stdWeekDays = false) {
        // stdWeekDays - if required to generate log stats for week days through the week (standardized)
        $dateFromObj = Carbon::parse($dateFrom);
        $dateToObj = Carbon::parse($dateTo);
        $logs = $this->getLogsForList($user, $dateFromObj, $dateToObj);
        $logsList = $this->createLogsList($logs, $healthGoals, $dateFrom, $dateTo, $stdWeekDays);
        return $logsList;
    }

    public function createLogsList(Collection $logs, Collection $healthGoals, $dateFrom, $dateTo, $stdWeekDays = false) {
        $dateFromObj = Carbon::parse($dateFrom);
        $dateToObj = Carbon::parse($dateTo);
        $daysDiff = $dateFromObj->diffInDays($dateToObj);
        $stats = $this->getCalculatedStats($logs, $healthGoals, $daysDiff);
        $stdWeekDaysStats = null;
        if ($stdWeekDays == true) {
            $stdWeekDaysStats = $this->getWeekDaysCalculatedStats($logs, $healthGoals, $dateFromObj, $dateToObj);
        }

        $logsGrouped = $this->getLogsGroupedByDay($logs);

        $logsList = [
            'date_from' => $dateFromObj->toDateString(),
            'date_to' => $dateToObj->toDateString(),
            'days' => $daysDiff,
            'data' => null,
            'health_goals' => $healthGoals,
            'stats' => $stats,
            'std_week_days_stats' => $stdWeekDaysStats
        ];
        $index = 0;
        $logsGrouped->each(function($dailyLogs, $key) use(&$index, $healthGoals, &$logsList) {
            // healthGoals method for getCurrentForDate
            $currentHealthGoal = $healthGoals->where('goal_from_ts', '<=', $dailyLogs->first()->log_ts)->first();
            $logsList['data'][$index]['date'] = $key;
            $logsList['data'][$index]['logs'] = $dailyLogs;
            $logsList['data'][$index]['health_goal'] = $currentHealthGoal;
            $index++;
        });
        
        return $logsList;
    }

    protected function getWeekDaysCalculatedStats(Collection $logs, Collection $healthGoals, Carbon $dateFromObj, Carbon $dateToObj) {
        if ($logs->first()) {
            $log = $logs->first();
            if ($log->logType->type == 'glucose') {
                return ['glucose' => $this->calculateWeekDaysStats($logs, $healthGoals, 'glucose', $dateFromObj, $dateToObj)];
            }
            if ($log->logType->type == 'insulin') {
                $basalLogs = $logs->where('loggable.insulinType.key', 'basal');
                $bolusLogs = $logs->where('loggable.insulinType.key', 'bolus');
                return [
                    'bolus' => $this->calculateWeekDaysStats($bolusLogs, $healthGoals, 'insulin_bolus', $dateFromObj, $dateToObj),
                    'basal' => $this->calculateWeekDaysStats($basalLogs, $healthGoals, 'insulin_basal', $dateFromObj, $dateToObj)
                ];
            }
            if ($log->logType->type == 'step') {
                // temporary [where value != 0] because for each day there is 1 log (total)
                // and if device was not worn value is 0 (in most cases)
                $logs = $logs->where('value', '!=', 0);
                return ['step' => $this->calculateWeekDaysStats($logs, $healthGoals, 'step', $dateFromObj, $dateToObj)];
            }
            if ($log->logType->type == 'calorie') {
                // temporary [where value > 10] because for each day there is 1 log (total)
                // and offset (10) helps if device was not worn or there were some errors with data (Fitbit)
                $logs = $logs->where('value', '>', 10);
                return ['calorie' => $this->calculateWeekDaysStats($logs, $healthGoals, 'calorie', $dateFromObj, $dateToObj)];
            }
            if ($log->logType->type == 'physical_activity') {
                return ['activity' => $this->calculateWeekDaysStats($logs, $healthGoals, 'activity', $dateFromObj, $dateToObj)];
            }
            if ($log->logType->type == 'sleep') {
                return ['sleep' => $this->calculateWeekDaysStats($logs, $healthGoals, 'sleep', $dateFromObj, $dateToObj)];
            }
        }
        return null;
    }
}
