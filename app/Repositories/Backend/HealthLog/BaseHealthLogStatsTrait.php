<?php

namespace App\Repositories\Backend\HealthLog;

use Illuminate\Support\Collection;
use Carbon\Carbon;

trait BaseHealthLogStatsTrait {
    // generate log stats for week days through the week (standardized)
    // same for all health logs
    protected function calculateWeekDaysStats(Collection $logs, Collection $healthGoals, $typeKey, Carbon $dateFromObj, Carbon $dateToObj) {
        $avgGoalMinValue = null;
        $avgGoalMaxValue = null;
        $weeksDiff = $dateFromObj->diffInDays($dateToObj);

        switch($typeKey) {
            case 'glucose':
                $avgGoalMinValue = $this->formatBGValue($healthGoals->avg('min_bg_before_meal'));
                $avgGoalMaxValue = $this->formatBGValue($healthGoals->avg('max_bg_after_meal'));
                break;
            case 'insulin_bolus':
                $avgGoalMaxValue = $this->formatInsulinValue($healthGoals->avg('daily_bolus_total'));
                break;
            case 'insulin_basal':
                $avgGoalMaxValue = $this->formatInsulinValue($healthGoals->avg('daily_basal_total'));
                break;
            case 'insulin':
                $avgGoalMaxValue = $this->formatInsulinValue($healthGoals->avg('tdd'));
                break;
            case 'step':
                $avgGoalMaxValue = $this->formatStepValue($healthGoals->avg('min_daily_steps'));
                break;
            case 'calorie':
                break;
            case 'activity':
                $avgGoalMaxValue = $healthGoals->avg('min_daily_excercise')*60*1000;
                break;
            case 'sleep':
                break;
        }
        if ($avgGoalMinValue == null) $avgGoalMinValue = $avgGoalMaxValue;
        $averages = [
            'type' => $typeKey,
            'days' => null,
            'avg_goal_max' => $avgGoalMaxValue,
            'avg_goal_min' => $avgGoalMinValue
        ];

        $tmpLogs = clone $logs;
        $nowRef = Carbon::now();
        for($i = 0; $i < 7; $i++) {
            $averages['days'][$i] = [
                'ref_date' => $nowRef->copy()->startOfWeek()->addDays($i),
                'data' => [],
                'day_avg_value' => null,
                'day_max' => null,
                'day_min' => null
            ];
            for($j = 0; $j < 24; $j++) {
                $averages['days'][$i]['data'][$j] = [
                    'hour' => $nowRef->copy()->startOfDay()->addHours($j)->format('H'),
                    'sum' => 0,
                    'count' => 0,
                    'avg' => 0,
                    'max' => null,
                    'min' => null,
                    'log_ts' => null
                ];
            }
        }
        foreach($averages['days'] as $avgKey => &$avgValue) {
            $tempDayAvgData = ['sum' => 0, 'count' => 0, 'avg_sum' => 0];
            $dayMaxVal = null;
            $dayMinVal = null;
            $dayMaxSum = null;
            $dayMinSum = null;
            $matchedWeekDays = [];
            if ($typeKey != 'glucose' && $typeKey != 'step' && $typeKey != 'calorie' && $typeKey != 'sleep') {
                // comment out for loop if need to calculate only for days with activities (days without activities will be ignored)
                for($i = 0; $i < $weeksDiff ; $i++) {
                    $date = $dateFromObj->copy()->addDays($i);
                    if ($date->format('l') == $avgValue['ref_date']->format('l') && $date->lt($dateToObj)) {
                        $matchedWeekDays[$date->format('Y-m-d')] = ['values' => [], 'log_sum' => 0, 'log_count' => 0, 'date' => $date];
                    }
                }
            }
            $weekDaysMax = [];
            $refDate = $avgValue['ref_date'];
            $weekDay = $refDate->diffInDays($refDate->copy()->startOfWeek());
            foreach($tmpLogs as $logKey => $logValue) {
                $logVal = $logValue->value;
                $logTs = $logValue->log_ts;
                $logWeekDay = $logTs->diffInDays($logTs->copy()->startOfWeek());
                $logRefDate = $nowRef->copy()->startOfWeek()->addDays($logWeekDay)->format('Y-m-d');
                $logRefDateTime = Carbon::createFromFormat('Y-m-d H:i:s', $logRefDate . ' ' . $logTs->format('H:i:s'));
                if ($logRefDateTime->format("Y-m-d") == $refDate->format("Y-m-d")) {
                    $tempDayAvgData['sum'] += $logVal;
                    $tempDayAvgData['count']++;
                    if ($logWeekDay == $weekDay) {
                        // set all dates which represent the same week day (eg. Monday) and calculate some values for that dates
                        if (array_key_exists($logTs->format('Y-m-d'), $matchedWeekDays)) {
                            // total log value sum for that date (eg. total units of insulin)
                            // can be used for getting MAX and MIN values for each day in the week where SUM of log value is needed
                            $matchedWeekDays[$logTs->format('Y-m-d')]['values'][] = $logVal;
                            $matchedWeekDays[$logTs->format('Y-m-d')]['log_sum'] += $logVal;
                            $matchedWeekDays[$logTs->format('Y-m-d')]['log_count']++;
                        }
                        // uncomment if only for days where there are activities
                        else {
                            // inital value (first log for that date)
                            $matchedWeekDays[$logTs->format('Y-m-d')] = ['values' => [$logVal], 'log_sum' => $logVal, 'log_count' => 1, 'date' => $logTs];
                        }
                    }
                    foreach($avgValue['data'] as $hourKey => &$hourValue) {
                        if ($logRefDateTime->format("H") == $hourValue["hour"]) {
                            $tmpLogs->forget($logKey); // remove this log (not needed anymore) for performance (40% faster)
                            $hourValue["sum"] += $logVal;
                            $hourValue["count"]++;
                            $hourValue["avg"] = $hourValue["sum"]/$hourValue["count"];
                            if ($hourValue["max"] == null || $hourValue["max"] < $logVal) {
                                $hourValue["max"] = $logVal;
                            }
                            if ($hourValue["min"] == null || $hourValue["min"] > $logVal) {
                                $hourValue["min"] = $logVal;
                            }
                            $hourValue["log_ts"] = $logTs->format("H");
                            $tempDayAvgData["avg_sum"] += $hourValue["avg"];
                        }
                    }
                }
            }
            foreach($matchedWeekDays as $dayKey => $dayValue) {
                if ($dayMaxSum == null || $dayMaxSum['value'] < $dayValue['log_sum']) {
                    $dayMaxSum['value'] = $dayValue['log_sum'];
                    $dayMaxSum['date'] = $dayValue['date'];
                }
                if ($dayMinSum == null || $dayMinSum['value'] > $dayValue['log_sum']) {
                    $dayMinSum['value'] = $dayValue['log_sum'];
                    $dayMinSum['date'] = $dayValue['date'];

                }
                if (count($dayValue['values']) > 0) {
                    $tempMax = max($dayValue['values']);
                    $tempMin = min($dayValue['values']);
                    if ($dayMaxVal == null || $dayMaxVal['value'] < $tempMax) {
                        $dayMaxVal['date'] = $dayValue['date'];
                        $dayMaxVal['value'] = $tempMax;
                    }
                    if ($dayMinVal == null || $dayMinVal['value'] > $tempMin) {
                        $dayMinVal['date'] = $dayValue['date'];
                        $dayMinVal['value'] = $tempMin;
                    }
                }
            }

            if (($typeKey == 'glucose') && $tempDayAvgData["count"] > 0) {
                $dayAvgValue = $tempDayAvgData["sum"]/$tempDayAvgData["count"];
                if ($typeKey == 'glucose') {
                    // average daily reading (total day sum / count of logs that day)
                    $avgValue["day_avg_value"]["value"] = $this->formatBGValue($dayAvgValue);
                }
                /* 
                 * NOTE: dayMax and dayMin are different for different log types and for different goals
                 * (eg. for glucose logs max/min log value is important, but for insulin logs max daily sum is important)
                 */
                $avgValue["day_max"] = $dayMaxVal;
                $avgValue["day_min"] = $dayMinVal;
            }
            if (($typeKey == 'insulin_basal' || $typeKey == 'insulin_bolus' || $typeKey == 'activity' || $typeKey == 'step' || $typeKey == 'calorie' || $typeKey == 'sleep') && count($matchedWeekDays) > 0) {
                $dayAvgValue = $tempDayAvgData["avg_sum"]/count($matchedWeekDays);
                if ($typeKey == 'insulin_basal' || $typeKey == 'insulin_bolus') {
                    // average daily sum of units (sum of day averages / count of days)
                    $avgValue["day_avg_value"]["value"] = $this->formatInsulinValue($dayAvgValue);
                }
                if ($typeKey == 'activity' || $typeKey == 'sleep') {
                    $avgValue["day_avg_value"]["value"] = $this->formatDurationMsValue($dayAvgValue);
                }
                if ($typeKey == 'step') {
                    $avgValue["day_avg_value"]["value"] = $this->formatStepValue($dayAvgValue);
                }
                if ($typeKey == 'calorie') {
                    $avgValue["day_avg_value"]["value"] = $this->formatCalorieValue($dayAvgValue);
                }
                $avgValue["day_max"] = $dayMaxSum;
                $avgValue["day_min"] = $dayMinSum;
            }

            /* !! REUSE THIS LOGIC (AVERAGE VALUE LEVELS) TO REDUCE CODE !! */
            if ($avgGoalMaxValue != null && $avgGoalMinValue != null) {
                if ($avgValue["day_avg_value"] != null && isset($avgValue["day_avg_value"]["value"])) {
                    $avgValue["day_avg_value"]["desc"] = "normal";
                    if ($avgValue["day_avg_value"]["value"] > $avgGoalMaxValue) $avgValue["day_avg_value"]["desc"] = "high";
                    if ($avgValue["day_avg_value"]["value"] < $avgGoalMinValue) $avgValue["day_avg_value"]["desc"] = "low";
                }
                if ($avgValue["day_max"] != null && isset($avgValue["day_max"]["value"])) {
                    $avgValue["day_max"]["desc"] = "normal";
                    if ($avgValue["day_max"]["value"] > $avgGoalMaxValue) $avgValue["day_max"]["desc"] = "high";
                    if ($avgValue["day_max"]["value"] < $avgGoalMinValue) $avgValue["day_max"]["desc"] = "low";
                    $avgValue["day_max"]["deviation_percentage"] = round(($avgValue["day_max"]["value"]/$avgGoalMaxValue)*100, 1) - 100;
                }
                if ($avgValue["day_min"] != null && isset($avgValue["day_min"]["value"])) {
                    $avgValue["day_min"]["desc"] = "normal";
                    if ($avgValue["day_min"]["value"] > $avgGoalMaxValue) $avgValue["day_min"]["desc"] = "high";
                    if ($avgValue["day_min"]["value"] < $avgGoalMinValue) $avgValue["day_min"]["desc"] = "low";
                    $avgValue["day_min"]["deviation_percentage"] = round(($avgValue["day_min"]["value"]/$avgGoalMinValue)*100, 1) - 100;
                }
            }
        }
        return $averages;
    }
}
