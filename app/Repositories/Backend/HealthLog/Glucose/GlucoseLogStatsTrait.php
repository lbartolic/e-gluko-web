<?php

namespace App\Repositories\Backend\HealthLog\Glucose;

use Illuminate\Support\Collection;

trait GlucoseLogStatsTrait {
    /*
     * Calculate glucose log data depending on associated health goals
     */
    protected function calculateGlucoseStats(Collection $logs, Collection $healthGoals, $daysDiff) {
    	$totalCount = $logs->count();    	
    	$countsPerDay = $this->getLogsGroupedByDay($logs);
        $loggedDays = count($countsPerDay);
    	$averagePerDay = null;
    	if (count($countsPerDay) != 0) {
			$averagePerDay = (int)round(($totalCount)/(count($countsPerDay)));
		}

        $avgValue['value'] = $this->formatBGValue($logs->avg('value'));

    	$logValueLevels = []; // high/low/normal
        $totalHighValues = 0;
        $totalLowValues = 0;
        $totalNormalValues = 0;
        $valuesBeforeAfter = [
        	'min_before' => null,
        	'max_before' => null,
        	'avg_before' => null,
        	'before_low_percentage' => 0,
        	'before_normal_percentage' => 0,
        	'before_high_percentage' => 0,
        	'before_count' => 0,
        	'min_after' => null,
        	'max_after' => null,
        	'avg_after' => null,
        	'after_low_percentage' => 0,
        	'after_normal_percentage' => 0,
        	'after_high_percentage' => 0,
        	'after_count' => 0
        ];
        $totalMaxValue = null;
        $totalMinValue = null;
        $beforeSum = 0;
        $afterSum = 0;
        $avgMinBefore = 0;
        $avgMaxBefore = 0;
        $avgMaxAfter = 0;
        $avgMaxA1C = 0;

        /* 
         * sortByDesc to enable correct comparison between each log and each goal.
         * Log that shows up after the higher date of the goal is counted 
         * under that goal and removed from the list of logs to enable
         * correct next iterations (in other case it would be counted)
         * under all other goals because of higher date value.
         */
		$healthGoals = $healthGoals->sortByDesc('goal_from_ts');
		$logs = $logs->sortByDesc(function($log) {
            return $log->log_ts;
        });
    	$healthGoals->each(function($healthGoal, $key) use($logs, &$logValueLevels, &$totalHighValues, &$totalLowValues, &$totalNormalValues, &$valuesBeforeAfter, &$beforeSum, &$afterSum, &$totalMaxValue, &$totalMinValue, &$avgMinBefore, &$avgMaxBefore, &$avgMaxAfter, &$avgMaxA1C) {
        	$logs->each(function($log, $logKey) use(&$logs, $healthGoal, &$logValueLevels, &$totalHighValues, &$totalLowValues, &$totalNormalValues, &$valuesBeforeAfter, &$beforeSum, &$afterSum, &$totalMaxValue, &$totalMinValue, &$avgMinBefore, &$avgMaxBefore, &$avgMaxAfter, &$avgMaxA1C) {
        		// compare current goal with current log (both ordered DESC)
        		if ($healthGoal->goal_from_ts <= $log->log_ts) {
                    $minBefore = $healthGoal->min_bg_before_meal;
                    $maxBefore = $healthGoal->max_bg_before_meal;
                    $maxAfter = $healthGoal->max_bg_after_meal;
                    $maxA1C = $healthGoal->max_a1c;
        			$logs->forget($logKey); // remove this log as it was just matched with the goal
                    $avgMinBefore += $minBefore;
                    $avgMaxBefore += $maxBefore;
                    $avgMaxAfter += $maxAfter;
                    $avgMaxA1C += $maxA1C;
	        		$mealRel = $log->loggable->label->meal_rel;
                    $tmpDesc = "normal"; // default value if in goal range
                    $tmpDeviation = null; // default value if in goal range
	        		// 1 - before meal, 2 - after meal
	        		if ($mealRel == 1 || $mealRel == 2) {
	        			if ($mealRel == 1) {
	        				$baseLabel = "before";
	        				$maxValue = $maxBefore;
	        				$minValue = $minBefore;
	        				$beforeSum += $log->value;
	        			}
	        			if ($mealRel == 2) {
	        				$baseLabel = "after";
	        				$maxValue = $maxAfter;
	        				$minValue = $minBefore; // because there is no "min_after" parameter
	        				$afterSum += $log->value;
	        			}
	        			$valuesBeforeAfter["{$baseLabel}_count"]++;

		        		if ($log->value < $minValue) {
		        			$logValueLevels["{$baseLabel}"]["low"][] = $log;
	        				$valuesBeforeAfter["{$baseLabel}_low_percentage"]++;
		        			$totalLowValues++;
                            $tmpDesc = "low";
                            $tmpDeviation = round(($log->value/$minValue)*100, 1) - 100;
		        		}
		        		elseif ($log->value > $maxValue) {
		        			$logValueLevels["{$baseLabel}"]["high"][] = $log;
	        				$valuesBeforeAfter["{$baseLabel}_high_percentage"]++;
		        			$totalHighValues++;
                            $tmpDesc = "high";
                            $tmpDeviation = round(($log->value/$maxValue)*100, 1) - 100;
		        		}
		        		else {
		        			$logValueLevels["{$baseLabel}"]["normal"][] = $log;
	        				$valuesBeforeAfter["{$baseLabel}_normal_percentage"]++;
		        			$totalNormalValues++;
		        		}

                        if ($valuesBeforeAfter["min_{$baseLabel}"] == null || ($log->value < $valuesBeforeAfter["min_{$baseLabel}"]["log"]->value)) {
                            $valuesBeforeAfter["min_{$baseLabel}"]["log"] = $log;
                            $valuesBeforeAfter["min_{$baseLabel}"]["desc"] = $tmpDesc;
                            $valuesBeforeAfter["min_{$baseLabel}"]["deviation_percentage"] = $tmpDeviation;
                        }
                        if ($valuesBeforeAfter["max_{$baseLabel}"] == null || ($log->value > $valuesBeforeAfter["max_{$baseLabel}"]["log"]->value)) {
                            $valuesBeforeAfter["max_{$baseLabel}"]["log"] = $log;
                            $valuesBeforeAfter["max_{$baseLabel}"]["desc"] = $tmpDesc;
                            $valuesBeforeAfter["max_{$baseLabel}"]["deviation_percentage"] = $tmpDeviation;
                        }
	        		}
	        		else {
	        			// if glucose log is not labeled as before/after meal
	        			if ($log->value < $minBefore) {
                            $totalLowValues++;
                            $tmpDesc = "low";
                            $tmpDeviation = round(($log->value/$minBefore)*100, 1) - 100;
                        }
		        		elseif ($log->value > $maxAfter) {
                            $totalHighValues++;
                            $tmpDesc = "high";
                            $tmpDeviation = round(($log->value/$maxAfter)*100, 1) - 100;
                        }
		        		else {
                            $totalNormalValues++;
                        }
	        		}
                    if ($totalMaxValue == null || ($log->value > $totalMaxValue["log"]->value)) {
                        $totalMaxValue["log"] = $log;
                        $totalMaxValue["desc"] = $tmpDesc;
                        $totalMaxValue["deviation_percentage"] = $tmpDeviation;
                    }
                    if ($totalMinValue == null || ($log->value < $totalMinValue["log"]->value)) {
                        $totalMinValue["log"] = $log;
                        $totalMinValue["desc"] = $tmpDesc;
                        $totalMinValue["deviation_percentage"] = $tmpDeviation;
                    }
	        	}
        	});
        });
        if ($totalCount > 0) {
            $avgMinBefore = $this->formatBGValue($avgMinBefore/$totalCount);
            $avgMaxBefore = $this->formatBGValue($avgMaxBefore/$totalCount);
            $avgMaxAfter = $this->formatBGValue($avgMaxAfter/$totalCount);
            $avgMaxA1C = $this->formatA1CValue($avgMaxA1C/$totalCount);
        }

        /* !! REUSE THIS LOGIC (AVERAGE VALUE LEVELS) TO REDUCE CODE !! */
        if ($valuesBeforeAfter['before_count'] != 0) {
        	$valuesBeforeAfter['avg_before']['value'] = $this->formatBGValue($beforeSum/$valuesBeforeAfter['before_count']);
            $valuesBeforeAfter['avg_before']['desc'] = 'normal';
            if ($valuesBeforeAfter['avg_before']['value'] < $avgMinBefore) {
                $valuesBeforeAfter['avg_before']['desc'] = 'low';
            }
            if ($valuesBeforeAfter['avg_before']['value'] > $avgMaxBefore) {
                $valuesBeforeAfter['avg_before']['desc'] = 'high';
            }

        	$valuesBeforeAfter['before_low_percentage'] = round(($valuesBeforeAfter['before_low_percentage']/$valuesBeforeAfter['before_count'])*100, 1);
        	$valuesBeforeAfter['before_high_percentage'] = round(($valuesBeforeAfter['before_high_percentage']/$valuesBeforeAfter['before_count'])*100, 1);
        	$valuesBeforeAfter['before_normal_percentage'] = round(($valuesBeforeAfter['before_normal_percentage']/$valuesBeforeAfter['before_count'])*100, 1);
        }
        if ($valuesBeforeAfter['after_count'] != 0) {
        	$valuesBeforeAfter['avg_after']['value'] = $this->formatBGValue($afterSum/$valuesBeforeAfter['after_count']);
            $valuesBeforeAfter['avg_after']['desc'] = 'normal';
            if ($valuesBeforeAfter['avg_after']['value'] > $avgMaxAfter) {
                $valuesBeforeAfter['avg_after']['desc'] = 'high';
            }

        	$valuesBeforeAfter['after_low_percentage'] = round(($valuesBeforeAfter['after_low_percentage']/$valuesBeforeAfter['after_count'])*100, 1);
        	$valuesBeforeAfter['after_high_percentage'] = round(($valuesBeforeAfter['after_high_percentage']/$valuesBeforeAfter['after_count'])*100, 1);
        	$valuesBeforeAfter['after_normal_percentage'] = round(($valuesBeforeAfter['after_normal_percentage']/$valuesBeforeAfter['after_count'])*100, 1);
        }

        if ($totalCount != 0) {
        	$highLevelPerc = round(($totalHighValues/$totalCount)*100, 1);
	        $lowLevelPerc = round(($totalLowValues/$totalCount)*100, 1);
	        $normalLevelPerc = round(($totalNormalValues/$totalCount)*100, 1);
	    }
        
        // TOTAL average value - "normal" if between "min before meal" and "max after meal"
        $avgValue['desc'] = 'normal';
        $avgValue['deviation_percentage'] = null;
        if ($avgValue['value'] < $avgMinBefore) {
            $avgValue['desc'] = 'low';
            $avgValue['deviation_percentage'] = round(($avgValue['value']/$avgMinBefore)*100, 1) - 100;
        }
        if ($avgValue['value'] > $avgMaxAfter) {
            $avgValue['desc'] = 'high';
            $avgValue['deviation_percentage'] = round(($avgValue['value']/$avgMaxAfter)*100, 1) - 100;
        }

        // estimated A1C [%] for period (available if period is at least 1 month)
        // minimum of logged days set to more than 3 weeks
        // null if no enough data
        $estimatedA1C = null;
        if ($loggedDays > 21) {
            $estimatedA1C['value'] = $this->formatA1CValue((2.59 + $avgValue['value']) / 1.59);
            if ($estimatedA1C['value'] > $avgMaxA1C) $estimatedA1C['desc'] = 'high';
            elseif ($estimatedA1C['value'] < $avgMaxA1C) $estimatedA1C['desc'] = 'low';
            else $estimatedA1C['desc'] = 'normal';
            $estimatedA1C['deviation_percentage'] =  round(($estimatedA1C['value']/$avgMaxA1C)*100, 1) - 100;
        }

        return [
        	//'log_levels' => $logValueLevels,
        	'total_logs' => $totalCount,
            'logged_days' => $loggedDays,
        	'avg_logs_per_day' => $averagePerDay,
        	'high_percentage' => (isset($highLevelPerc)) ? $highLevelPerc : null,
        	'low_percentage' => (isset($lowLevelPerc)) ? $lowLevelPerc : null,
        	'normal_percentage' => (isset($normalLevelPerc)) ? $normalLevelPerc : null,
        	'max_value' => $totalMaxValue,
        	'min_value' => $totalMinValue,
        	'avg_value' => $avgValue,
            'estimated_a1c' => $estimatedA1C,
            'goal_avgs' => [
                'min_before' => $avgMinBefore,
                'max_before' => $avgMaxBefore,
                'max_after' => $avgMaxAfter,
                'max_a1c' => $avgMaxA1C
            ],
        	'before_after' => $valuesBeforeAfter
        ];
    }
}
