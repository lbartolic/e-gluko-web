<?php

namespace App\Repositories\Backend\HealthLog\Calorie;

use Illuminate\Support\Collection;

trait CalorieLogStatsTrait {
    /*
     * Calculate insulin log data depending on associated health goals
     */
    protected function calculateCalorieStats(Collection $logs, Collection $healthGoals, $daysDiff) {
        // temporary [where value > 10] because for each day there is 1 log (total)
        // and offset (10) helps if device was not worn or there were some errors with data (Fitbit)
        $logs = $logs->where('value', '>', 10);
        $totalLogs = $logs->count();
    	$loggedDays = count($this->getLogsGroupedByDay($logs));
        $tempStatsData = [
        	'total_calories' => 0,
        	'daily_avg_calories' => ['value' => 0],
        	'day_max_calories' => null
        ];
    	$logs->each(function($log, $logKey) use(&$logs, &$tempStatsData) {
            $logDate = $log->log_ts->format('Y-m-d');
            $logVal = $log->value;

            $tempStatsData['daily_avg_calories']['value'] += $logVal;
            $tempStatsData['total_calories'] += $logVal;
            if ($tempStatsData['day_max_calories'] == null || ($logVal > $tempStatsData['day_max_calories']['value'])) {
                $tempStatsData['day_max_calories']['value'] = $logVal;
                $tempStatsData['day_max_calories']['date'] = $log->log_ts;
            }
    	});
        // divided by $loggedDays to skip days that have no steps (probably device was not worn - DISCUSSABLE!)
        if ($loggedDays > 0) {
            $tempStatsData['daily_avg_calories']['value'] = round($tempStatsData['daily_avg_calories']['value']/$loggedDays, 0);
        }

        return [
            'logged_days' => $loggedDays,
            'total_logs' => $totalLogs,
            'total_calories' => $tempStatsData['total_calories'],
            'daily_avg_calories' => $tempStatsData['daily_avg_calories'],
            'day_max_calories' => $tempStatsData['day_max_calories']
        ];
    }
}
