<?php

namespace App\Repositories\Backend\HealthLog\PhysicalActivity;

use Illuminate\Support\Collection;

trait PhysicalActivityLogStatsTrait {
    /*
     * Calculate insulin log data depending on associated health goals
     */
    protected function calculateActivityStats(Collection $logs, Collection $healthGoals, $daysDiff) {
        $totalLogs = $logs->count();
    	$loggedDays = count($this->getLogsGroupedByDay($logs));
        $tempStatsData = [
        	'avg_goal_min_daily_excercise' => 0,
            'total_duration' => ['value' => 0, 'deviation_percentage' => 100, 'desc' => 'low'],
            'total_steps' => 0,
        	'total_calories' => 0,
            'total_heart_rate' => 0,
            'daily_avg_duration' => 0, // for days with activities (active days)
            'days_above_goal' => [],
            'goal_period_min_sum' => 0
        ];
        $aboveGoalCheck = [];

        /* 
         * sortByDesc to enable correct comparison between each log and each goal.
         * Log that shows up after the higher date of the goal is counted 
         * under that goal and removed from the list of logs to enable
         * correct next iterations (in other case it would be counted)
         * under all other goals because of higher date value.
         */
		$healthGoals = $healthGoals->sortByDesc('goal_from_ts');
		$logs = $logs->sortByDesc(function($log) {
            return $log->log_ts;
        });
    	$healthGoals->each(function($healthGoal, $key) use($logs, &$tempStatsData, &$aboveGoalCheck) {
            unset($aboveGoalCheck);
        	$logs->each(function($log, $logKey) use(&$logs, $healthGoal, &$tempStatsData, &$aboveGoalCheck) {
        		// compare current goal with current log (both ordered DESC)
                if ($healthGoal->goal_from_ts <= $log->log_ts) {
                    $goalDayMinExcercise = $healthGoal->min_daily_excercise*60*1000; // to ms
                    $tempStatsData['goal_period_min_sum'] += $goalDayMinExcercise;
                    $tempStatsData['avg_goal_min_daily_excercise'] += $goalDayMinExcercise;
        			$logs->forget($logKey); // remove this log as it was just matched with the goal
                    $logDate = $log->log_ts->format('Y-m-d');
                    $logVal = $log->value;

                    $tempStatsData['total_duration']['value'] += $logVal;
                    $tempStatsData['total_steps'] += $log->loggable->steps;
                    $tempStatsData['total_calories'] += $log->loggable->calories;
                    $tempStatsData['total_heart_rate'] += $log->loggable->average_heart_rate;

                    if (!isset($aboveGoalCheck[$logDate])) {
                        $aboveGoalCheck[$logDate] = [
                            'date' => $log->log_ts,
                            'value' => $logVal
                        ];
                    }
                    else $aboveGoalCheck[$logDate]['value'] += $logVal;
                    if ($aboveGoalCheck[$logDate]['value'] > $goalDayMinExcercise) {
                        $totalDuration = $aboveGoalCheck[$logDate]['value'];
                        $aboveGoalCheck[$logDate]['deviation_percentage'] = round(($totalDuration/$goalDayMinExcercise)*100, 1) - 100;
                        $tempStatsData['days_above_goal'][] = $aboveGoalCheck[$logDate];
                    }
	        	}
        	});
        });
        $avgDuration = 0;
        $avgSteps = 0;
        $avgCalories = 0;
        $avgHeartRate = 0;
        if ($totalLogs > 0) {
            $tempStatsData['avg_goal_min_daily_excercise'] = round($tempStatsData['avg_goal_min_daily_excercise']/$totalLogs, 0);
            $avgDuration = $this->formatDurationMsValue($tempStatsData['total_duration']['value']/$totalLogs);
            $avgSteps = $this->formatStepValue($tempStatsData['total_steps']/$totalLogs);
            $avgCalories = $this->formatCalorieValue($tempStatsData['total_calories']/$totalLogs);
            $avgHeartRate = round($tempStatsData['total_heart_rate']/$totalLogs, 0);
            $tempStatsData['goal_period_min_sum'] = ($tempStatsData['goal_period_min_sum']/$totalLogs)*$daysDiff;

            // REUSE...
            $tempStatsData['total_duration']['deviation_percentage'] = round(($tempStatsData['total_duration']['value']/$tempStatsData['goal_period_min_sum'])*100, 1) - 100;
            $tempStatsData['total_duration']['desc'] = "normal";
            if ($tempStatsData['total_duration']['value'] > $tempStatsData['goal_period_min_sum']) {
                $tempStatsData['total_duration']['desc'] = "high";
            }
            if ($tempStatsData['total_duration']['value'] < $tempStatsData['goal_period_min_sum']) {
                $tempStatsData['total_duration']['desc'] = "low";
            }
        }

        if ($loggedDays > 0) {
            $tempStatsData['daily_avg_duration'] = $this->formatDurationMsValue($tempStatsData['total_duration']['value']/$loggedDays);
        }

        return [
            'logged_days' => $loggedDays,
            'total_logs' => $totalLogs,
            'total_duration' => $tempStatsData['total_duration'],
            'total_steps' => $tempStatsData['total_steps'],
            'total_calories' => $tempStatsData['total_calories'],
            'daily_avg_duration' => $tempStatsData['daily_avg_duration'],
            'avg_duration' => $avgDuration,
            'avg_steps' => $avgSteps,
            'avg_calories' => $avgCalories,
            'avg_heart_rate' => $avgHeartRate,
            'days_above_goal' => $tempStatsData['days_above_goal'],
            'goal_avgs' => [
                'min_daily_excercise' => $tempStatsData['avg_goal_min_daily_excercise'],
                'period_min_sum' => $tempStatsData['goal_period_min_sum']
            ]
        ];
    }
}
