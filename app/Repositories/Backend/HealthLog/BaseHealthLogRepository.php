<?php

namespace App\Repositories\Backend\HealthLog;

use Carbon\Carbon;
use Illuminate\Support\Collection;

use App\Models\Unit\UnitTrait;
use App\Repositories\Backend\HealthLog\BaseHealthLogStatsTrait;
use App\Repositories\Backend\HealthLog\Calorie\CalorieLogStatsTrait;
use App\Repositories\Backend\HealthLog\Glucose\GlucoseLogStatsTrait;
use App\Repositories\Backend\HealthLog\Insulin\InsulinLogStatsTrait;
use App\Repositories\Backend\HealthLog\PhysicalActivity\PhysicalActivityLogStatsTrait;
use App\Repositories\Backend\HealthLog\Step\StepLogStatsTrait;
use App\Repositories\Backend\HealthLog\Sleep\SleepLogStatsTrait;

class BaseHealthLogRepository {
	use UnitTrait;
	use BaseHealthLogStatsTrait;
   use GlucoseLogStatsTrait;
   use CalorieLogStatsTrait;
   use InsulinLogStatsTrait;
   use PhysicalActivityLogStatsTrait;
   use StepLogStatsTrait;
   use SleepLogStatsTrait;

	protected $now;
	protected $healthGoals;

    protected function getLogsGroupedByDay(Collection $logs) {
        $logsPerDay = $logs->groupBy(function($log) {
            return $log->log_ts->format('Y-m-d');
        });
        return $logsPerDay;
    }
}
