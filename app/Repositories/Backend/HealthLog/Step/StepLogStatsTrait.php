<?php

namespace App\Repositories\Backend\HealthLog\Step;

use Illuminate\Support\Collection;

trait StepLogStatsTrait {
    /*
     * Calculate insulin log data depending on associated health goals
     */
    protected function calculateStepStats(Collection $logs, Collection $healthGoals, $daysDiff) {
        // temporary [where value != 0] because for each day there is 1 log (total)
        // and if device was not worn value is 0 (in most cases)
        $logs = $logs->where('value', '!=', 0);
        $totalLogs = $logs->count();
    	$loggedDays = count($this->getLogsGroupedByDay($logs));
        $tempStatsData = [
        	'avg_goal_min_daily_steps' => 0,
        	'total_steps' => 0,
        	'daily_avg_steps' => ['value' => 0, 'deviation_percentage' => null, 'desc' => null],
        	'day_max_steps' => null,
            'days_above_goal' => []
        ];

        /* 
         * sortByDesc to enable correct comparison between each log and each goal.
         * Log that shows up after the higher date of the goal is counted 
         * under that goal and removed from the list of logs to enable
         * correct next iterations (in other case it would be counted)
         * under all other goals because of higher date value.
         */
		$healthGoals = $healthGoals->sortByDesc('goal_from_ts');
		$logs = $logs->sortByDesc(function($log) {
            return $log->log_ts;
        });
    	$healthGoals->each(function($healthGoal, $key) use($logs, &$tempStatsData) {
        	$logs->each(function($log, $logKey) use(&$logs, $healthGoal, &$tempStatsData) {
        		// compare current goal with current log (both ordered DESC)
                if ($healthGoal->goal_from_ts <= $log->log_ts) {
                    $goalDayMinSteps = $healthGoal->min_daily_steps;
                    $tempStatsData['avg_goal_min_daily_steps'] += $healthGoal->min_daily_steps;
        			$logs->forget($logKey); // remove this log as it was just matched with the goal
                    $logDate = $log->log_ts->format('Y-m-d');
                    $logVal = $log->value;

                    $tempStatsData['daily_avg_steps']['value'] += $logVal;
                    $tempStatsData['total_steps'] += $logVal;
                    $tmpDesc = "normal";
                    if ($logVal != $goalDayMinSteps) {
                        if ($logVal >= $goalDayMinSteps) {
                            array_push($tempStatsData['days_above_goal'], [
                                'date' => $logDate,
                                'value' => $logVal,
                                'deviation_percentage' => round(($logVal/$goalDayMinSteps)*100, 1) - 100
                            ]);
                        }
                        if ($logVal > $goalDayMinSteps) $tmpDesc = "high";
                        if ($logVal < $goalDayMinSteps) $tmpDesc = "low";
                    }
                    if ($tempStatsData['day_max_steps'] == null || ($logVal > $tempStatsData['day_max_steps']['value'])) {
                        $tempStatsData['day_max_steps']['value'] = $logVal;
                        $tempStatsData['day_max_steps']['desc'] = $tmpDesc;
                        $tempStatsData['day_max_steps']['date'] = $log->log_ts;
                        $tempStatsData['day_max_steps']["deviation_percentage"] = round(($logVal/$goalDayMinSteps)*100, 1) - 100;
                    }
	        	}
        	});
        });
        if ($totalLogs > 0) {
            $tempStatsData['avg_goal_min_daily_steps'] = round($tempStatsData['avg_goal_min_daily_steps']/$totalLogs, 0);
            
            // divided by $loggedDays to skip days that have no steps (probably device was not worn - DISCUSSABLE!)
            $tempStatsData['daily_avg_steps']['value'] = $this->formatStepValue($tempStatsData['daily_avg_steps']['value']/$loggedDays);
            $tempStatsData['daily_avg_steps']['deviation_percentage'] = round(($tempStatsData['daily_avg_steps']['value']/$tempStatsData['avg_goal_min_daily_steps'])*100, 1) - 100;
            $tempStatsData['daily_avg_steps']['desc'] = "normal";
            if ($tempStatsData['daily_avg_steps']['value'] > $tempStatsData['avg_goal_min_daily_steps']) {
                $tempStatsData['daily_avg_steps']['desc'] = "high";
            }
            if ($tempStatsData['daily_avg_steps']['value'] < $tempStatsData['avg_goal_min_daily_steps']) {
                $tempStatsData['daily_avg_steps']['desc'] = "low";
            }
        }

        return [
            'logged_days' => $loggedDays,
            'total_logs' => $totalLogs,
            'total_steps' => $tempStatsData['total_steps'],
            'daily_avg_steps' => $tempStatsData['daily_avg_steps'],
            'day_max_steps' => $tempStatsData['day_max_steps'],
            'days_above_goal' => $tempStatsData['days_above_goal'],
            'goal_avgs' => [
                'min_daily_steps' => $tempStatsData['avg_goal_min_daily_steps']
            ]
        ];
    }
}
