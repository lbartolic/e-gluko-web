<?php

namespace App\Repositories\Backend\HealthLog\Step;

use Carbon\Carbon;
use Illuminate\Support\Collection;

use App\Models\Unit\UnitTrait;
use App\Models\Access\User\User;
use App\Models\HealthGoal\HealthGoal;
use App\Models\HealthLog\Step\StepLog;
use App\Repositories\Backend\HealthGoal\HealthGoalRepository;
use App\Repositories\Backend\HealthLog\Step\StepLogStatsTrait;
use App\Repositories\Backend\HealthLog\AbstractHealthLogRepository;

class StepLogRepository extends AbstractHealthLogRepository {
	public function __construct() {
		parent::__construct();
	}

	public function getLogsBetweenDates(User $user, Carbon $dateFromObj, Carbon $dateToObj) {
		return $user->healthLogs()->stepLogs()
			->whereBetween('log_ts', [$dateFromObj, $dateToObj])
        	->orderBy('log_ts')
 	       	->get();
	}

	protected function getLogsForList(User $user, Carbon $dateFromObj, Carbon $dateToObj) {
		return $this->getLogsBetweenDates($user, $dateFromObj, $dateToObj);
	}

	protected function getCalculatedStats(Collection $logs, Collection $healthGoals, $daysDiff) {
		return $this->calculateStepStats($logs, $healthGoals, $daysDiff);
	}

    public function getPatientStepSummary(User $user, $dateFrom, $dateTo) {
    	$dateFromObj = Carbon::parse($dateFrom);
    	$dateToObj = Carbon::parse($dateTo);
    	$daysDiff = $dateFromObj->diffInDays($dateToObj);

        $logs = $this->getLogsBetweenDates($user, $dateFromObj, $dateToObj);

        $healthGoals = $user->healthGoals;
        $summaryStats = $this->calculateStepStats($logs, $healthGoals, $daysDiff);

		$summary = array_merge([
			'health_goals' => $healthGoals
		], $summaryStats);

		return $summary;
    }
}
