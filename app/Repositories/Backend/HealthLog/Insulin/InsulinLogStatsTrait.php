<?php

namespace App\Repositories\Backend\HealthLog\Insulin;

use Illuminate\Support\Collection;

trait InsulinLogStatsTrait {
    /*
     * Calculate insulin log data depending on associated health goals
     */
    protected function calculateInsulinStats(Collection $logs, Collection $healthGoals, $daysDiff) {
        $totalLogs = $logs->count();
    	$loggedDays = count($this->getLogsGroupedByDay($logs));

        $basalLogsCount = 0;
        $bolusLogsCount = 0;
        $beforeMealSum = 0;
        $afterMealSum = 0;
        $beforeBedSum = 0;
        $bolusBeforeMeal = ['sum' => 0, 'count' => 0, 'avg' => 0];
        $basalBeforeMeal = ['sum' => 0, 'count' => 0, 'avg' => 0];
        $bolusAfterMeal = ['sum' => 0, 'count' => 0, 'avg' => 0];
        $basalAfterMeal = ['sum' => 0, 'count' => 0, 'avg' => 0];
        $bolusBeforeBed = ['sum' => 0, 'count' => 0, 'avg' => 0];
        $basalBeforeBed = ['sum' => 0, 'count' => 0, 'avg' => 0];
        $stats = [
        	'avg_units_bolus' => 0,
        	'avg_units_basal' => 0,
        	'total_units_bolus' => 0,
        	'total_units_basal' => 0,
        	'daily_avg_units_bolus' => 0,
        	'daily_avg_units_basal' => 0,
        	'avg_units_before_meal' => 0,
        	'avg_units_after_meal' => 0,
        	'avg_units_before_bed' => 0
        ];
        $highDosageDays = [];
        $highDosageCheck = [];
        $avgBolusTotal = 0;
        $avgBasalTotal = 0;
        $avgTdd = 0;

        /* 
         * sortByDesc to enable correct comparison between each log and each goal.
         * Log that shows up after the higher date of the goal is counted 
         * under that goal and removed from the list of logs to enable
         * correct next iterations (in other case it would be counted)
         * under all other goals because of higher date value.
         */
		$healthGoals = $healthGoals->sortByDesc('goal_from_ts');
		$logs = $logs->sortByDesc(function($log) {
            return $log->log_ts;
        });
    	$healthGoals->each(function($healthGoal, $key) use($logs, &$stats, &$highDosageDays, &$highDosageCheck, &$basalLogsCount, &$bolusLogsCount, &$beforeMealSum, &$afterMealSum, &$beforeBedSum, &$dailyTddCheck, &$bolusBeforeMeal, &$basalBeforeMeal, &$bolusAfterMeal, &$basalAfterMeal, &$bolusBeforeBed, &$basalBeforeBed, &$avgBolusTotal, &$avgBasalTotal, &$avgTdd) {
            unset($highDosageCheck);
        	$logs->each(function($log, $logKey) use(&$logs, $healthGoal, &$stats, &$highDosageDays, &$highDosageCheck, &$basalLogsCount, &$bolusLogsCount, &$beforeMealSum, &$afterMealSum, &$beforeBedSum, &$dailyTddCheck, &$bolusBeforeMeal, &$basalBeforeMeal, &$bolusAfterMeal, &$basalAfterMeal, &$bolusBeforeBed, &$basalBeforeBed, &$avgBolusTotal, &$avgBasalTotal, &$avgTdd) {
        		// compare current goal with current log (both ordered DESC)
                if ($healthGoal->goal_from_ts <= $log->log_ts) {
        			$logs->forget($logKey); // remove this log as it was just matched with the goal
                    $logDate = $log->log_ts->format('Y-m-d');
                    $logVal = $log->value;
                    $insulinType = $log->loggable->insulinType->key;
                    if ($insulinType == 'basal') {
                        $basalLogsCount++;
                        $stats['total_units_basal'] += $logVal;
                    }
                    if ($insulinType == 'bolus') {
                        $bolusLogsCount++;
                        $stats['total_units_bolus'] += $logVal;
                    }
	        		$timeRel = $log->loggable->label->meal_rel;
                    $labelKey = $log->loggable->label->key;
	        		// 1 - before meal, 2 - after meal, 3 - before bed
	        		if ($timeRel == 1 || $timeRel == 2 || ($timeRel == NULL && $labelKey == 'before-bed')) {
	        			if ($timeRel == 1) {
	        				$baseLabel = "before_meal";
                            $beforeMealSum += $logVal;
                            ($insulinType == 'basal') ? $timeRelInsRef = &$basalBeforeMeal : $timeRelInsRef = &$bolusBeforeMeal;
	        			}
	        			if ($timeRel == 2) {
	        				$baseLabel = "after_meal";
                            $afterMealSum += $logVal;
                            ($insulinType == 'basal') ? $timeRelInsRef = &$basalAfterMeal : $timeRelInsRef = &$bolusAfterMeal;
	        			}
                        if ($labelKey == 'before-bed') {
                            $baseLabel = "before_bed";
                            $beforeBedSum += $logVal;
                            ($insulinType == 'basal') ? $timeRelInsRef = &$basalBeforeBed : $timeRelInsRef = &$bolusBeforeBed;
                        }
                        $stats["avg_units_{$baseLabel}"]++;
                        $timeRelInsRef['sum'] += $logVal;
                        $timeRelInsRef['count']++;
                        $timeRelInsRef['avg'] = $this->formatInsulinValue($timeRelInsRef['sum']/$timeRelInsRef['count']);
                    }
                    $goalTdd = $healthGoal->tdd;
                    if (!isset($highDosageCheck[$logDate])) {
                        $highDosageCheck[$logDate] = [
                            'date' => $logDate,
                            'bolus_units' => ($insulinType == 'bolus') ? $logVal : 0,
                            'basal_units' => ($insulinType == 'bolus') ? 0 : $logVal,
                            'total_units' => $logVal
                        ];
                    }
                    else {
                        $highDosageCheck[$logDate]['total_units'] += $logVal;
                        if ($insulinType == 'bolus') {
                            $highDosageCheck[$logDate]['bolus_units'] += $logVal;
                        }
                        else {
                            $highDosageCheck[$logDate]['basal_units'] += $logVal;
                        }
                    }
                    if ($highDosageCheck[$logDate]['total_units'] > $goalTdd) {
                        $totalUnits = $highDosageCheck[$logDate]['total_units'];
                        $highDosageCheck[$logDate]['deviation'] = $totalUnits - $goalTdd;
                        $highDosageDays[] = $highDosageCheck[$logDate];
                    }

                    $avgBolusTotal += $healthGoal->daily_bolus_total;
                    $avgBasalTotal += $healthGoal->daily_basal_total;
                    $avgTdd += $healthGoal->tdd;
	        	}
        	});
        });
        if ($totalLogs > 0) {
            $avgBolusTotal = $this->formatInsulinValue($avgBolusTotal/$totalLogs);
            $avgBasalTotal = $this->formatInsulinValue($avgBasalTotal/$totalLogs);
            $avgTdd = $this->formatInsulinValue($avgTdd/$totalLogs);
        }
        $dailyAvgUnitsBasal = null;
        $dailyAvgUnitsBolus = null;
        $dailyAvgUnitsTotal = null;
        if ($loggedDays != 0) {
            /* !! REUSE THIS LOGIC (AVERAGE VALUE LEVELS) TO REDUCE CODE !! */
            $dailyAvgUnitsBasal['value'] = $this->formatInsulinValue($stats['total_units_basal']/$loggedDays);
            $dailyAvgUnitsBasal['deviation_percentage'] = round(($dailyAvgUnitsBasal['value']/$avgBasalTotal)*100, 1) - 100;
            $dailyAvgUnitsBasal['desc'] = 'normal';
            if ($dailyAvgUnitsBasal['value'] > $avgBasalTotal) $dailyAvgUnitsBasal['desc'] = 'high';
            if ($dailyAvgUnitsBasal['value'] < $avgBasalTotal) $dailyAvgUnitsBasal['desc'] = 'low';

            $dailyAvgUnitsBolus['value'] = $this->formatInsulinValue($stats['total_units_bolus']/$loggedDays);
            $dailyAvgUnitsBolus['deviation_percentage'] = round(($dailyAvgUnitsBolus['value']/$avgBolusTotal)*100, 1) - 100;
            $dailyAvgUnitsBolus['desc'] = 'normal';
            if ($dailyAvgUnitsBolus['value'] > $avgBolusTotal) $dailyAvgUnitsBolus['desc'] = 'high';
            if ($dailyAvgUnitsBolus['value'] < $avgBolusTotal) $dailyAvgUnitsBolus['desc'] = 'low';

            $dailyAvgUnitsTotal['value'] = $dailyAvgUnitsBasal['value'] + $dailyAvgUnitsBolus['value'];
            $dailyAvgUnitsTotal['deviation_percentage'] = round(($dailyAvgUnitsTotal['value']/$avgTdd)*100, 1) - 100;
            $dailyAvgUnitsTotal['desc'] = 'normal';
            if ($dailyAvgUnitsTotal['value'] > $avgTdd) $dailyAvgUnitsTotal['desc'] = 'high';
            if ($dailyAvgUnitsTotal['value'] < $avgTdd) $dailyAvgUnitsTotal['desc'] = 'low';
        }

        return [
            'logged_days' => $loggedDays,
            'total_logs' => $totalLogs,
            'total_logs_basal' => $basalLogsCount,
            'total_logs_bolus' => $bolusLogsCount,
            'avg_units_bolus' => ($bolusLogsCount != 0) ? $this->formatInsulinValue($stats['total_units_bolus']/$bolusLogsCount) : 0,
            'avg_units_basal' => ($basalLogsCount != 0) ? $this->formatInsulinValue($stats['total_units_basal']/$basalLogsCount) : 0,
            'total_units_bolus' => $stats['total_units_bolus'],
            'total_units_basal' => $stats['total_units_basal'],
            'daily_avg_units_bolus' => $dailyAvgUnitsBolus,
            'daily_avg_units_basal' => $dailyAvgUnitsBasal,
            'daily_avg_units_total' => $dailyAvgUnitsTotal,
            'avg_units_before_meal' => ($beforeMealSum != 0) ? $this->formatInsulinValue($beforeMealSum/$stats["avg_units_before_meal"]) : 0,
            'avg_units_after_meal' => ($afterMealSum != 0) ? $this->formatInsulinValue($afterMealSum/$stats["avg_units_after_meal"]) : 0,
            'avg_units_before_bed' => ($beforeBedSum != 0) ? $this->formatInsulinValue($beforeBedSum/$stats["avg_units_before_bed"]) : 0,
            'bolus_before_meal' => $bolusBeforeMeal,
            'basal_before_meal' => $basalBeforeMeal,
            'bolus_after_meal' => $bolusAfterMeal,
            'basal_after_meal' => $basalAfterMeal,
            'bolus_before_bed' => $bolusBeforeBed,
            'basal_before_bed' => $basalBeforeBed,
            'high_dosage_days' => $highDosageDays,
            'goal_avgs' => [
                'bolus_total' => $avgBolusTotal,
                'basal_total' => $avgBasalTotal,
                'tdd' => $avgTdd
            ],
        ];
    }
}
