<?php

namespace App\Repositories\Backend\HealthLog\Insulin;

use Carbon\Carbon;
use Illuminate\Support\Collection;

use App\Models\Unit\UnitTrait;
use App\Models\Access\User\User;
use App\Models\HealthGoal\HealthGoal;
use App\Models\HealthLog\Insulin\InsulinLog;
use App\Repositories\Backend\HealthLog\Insulin\InsulinLogStatsTrait;
use App\Repositories\Backend\HealthLog\AbstractHealthLogRepository;

class InsulinLogRepository extends AbstractHealthLogRepository {
	public function __construct() {
		parent::__construct();
	}

	public function getLogsBetweenDates(User $user, Carbon $dateFromObj, Carbon $dateToObj) {
		return $user->healthLogs()->insulinLogs()
			->whereBetween('log_ts', [$dateFromObj, $dateToObj])
        	->orderBy('log_ts')
 	       	->get();
	}

	protected function getLogsForList(User $user, Carbon $dateFromObj, Carbon $dateToObj) {
		return $this->getLogsBetweenDates($user, $dateFromObj, $dateToObj);
	}

	protected function getCalculatedStats(Collection $logs, Collection $healthGoals, $daysDiff) {
		return $this->calculateInsulinStats($logs, $healthGoals, $daysDiff);
	}

    public function getPatientInsulinSummary(User $user, $dateFrom, $dateTo) {
    	$dateFromObj = Carbon::parse($dateFrom);
    	$dateToObj = Carbon::parse($dateTo);
    	$daysDiff = $dateFromObj->diffInDays($dateToObj);

        $logs = $this->getLogsBetweenDates($user, $dateFromObj, $dateToObj);

        $healthGoals = $user->healthGoals;
        $summaryStats = $this->calculateInsulinStats($logs, $healthGoals, $daysDiff);

		$summary = array_merge([
			'health_goals' => $healthGoals
		], $summaryStats);

		return $summary;
    }
}
