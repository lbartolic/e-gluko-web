<?php

namespace App\Repositories\Backend\HealthLog;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use App\Models\Access\User\User;
use App\Models\HealthLog\HealthLogType;
use App\Repositories\Backend\HealthLog\BaseHealthLogRepository;
use App\Repositories\Backend\HealthLog\Glucose\GlucoseLogRepository;
use App\Repositories\Backend\HealthLog\Insulin\InsulinLogRepository;
use App\Repositories\Backend\HealthLog\Step\StepLogRepository;
use App\Repositories\Backend\HealthLog\Calorie\CalorieLogRepository;
use App\Repositories\Backend\HealthLog\PhysicalActivity\PhysicalActivityLogRepository;
use App\Repositories\Backend\HealthLog\Sleep\SleepLogRepository;
use App\Models\HealthLog\HealthLog;
use App\Models\HealthLog\Glucose\GlucoseLog;
use App\Models\HealthLog\Insulin\InsulinLog;
use App\Models\HealthLog\Insulin\InsulinLogType;
use App\Models\HealthLog\HealthLogLabel;
use Excel;

class HealthLogRepository extends BaseHealthLogRepository {

    public function __construct(GlucoseLogRepository $glucoseLogRepo, InsulinLogRepository $insulinLogRepo, StepLogRepository $stepLogRepo, CalorieLogRepository $calorieLogRepo, PhysicalActivityLogRepository $physicalActivityLogRepo, SleepLogRepository $sleepLogRepo) {
        $this->glucoseLogRepo = $glucoseLogRepo;
        $this->insulinLogRepo = $insulinLogRepo;
        $this->stepLogRepo = $stepLogRepo;
        $this->calorieLogRepo = $calorieLogRepo;
        $this->physicalActivityLogRepo = $physicalActivityLogRepo;
        $this->sleepLogRepo = $sleepLogRepo;
    }
    
    public function getLastPatientLog(User $user) {
        $log = $user->healthLogs()->orderBy('log_ts', 'DESC')->first();
        return $log;
    }

    public function getLastLogDate(User $user) {
        $lastLog = $this->getLastPatientLog($user);
        if ($lastLog) $date = $lastLog->log_ts;
        else $date = Carbon::instance(config('application.time.now'));
        return $date;
    }

    public function getPatientLogs(User $user, $dateFrom, $dateTo, $grouped = false) {
        $dateFromObj = Carbon::parse($dateFrom);
        $dateToObj = Carbon::parse($dateTo);
        if ($grouped == false) {
            $logs = $user->healthLogs()
                ->whereBetween('log_ts', [$dateFromObj, $dateToObj])
                ->orderBy('log_ts', 'ASC')
                ->get();
        }
        else {
            $logs = HealthLogType::with(['logs' => function($q) use($user, $dateFromObj, $dateToObj) {
                $q->where('user_id', $user->id)
                    ->whereBetween('log_ts', [$dateFromObj, $dateToObj])
                    ->orderBy('log_ts', 'ASC');
            }])->get()->keyBy('type')->map(function($item) { return $item->logs; });
        }
        return $logs;
    }

    public function getPatientLogsList(User $user, Collection $healthGoals, $dateFrom, $dateTo, $dailyStats = false, $sort = null) {
        $logs = $this->getPatientLogs($user, $dateFrom, $dateTo);
        $logsList = $this->createLogsList($logs, $healthGoals, $dateFrom, $dateTo, $dailyStats, $sort);
            
        return $logsList;
    }

    public function createLogsList(Collection $logs, Collection $healthGoals, $dateFrom, $dateTo, $dailyStats = false, $sort = null) {
        // MOZDA PREBACITI METODU U BASEHEALTHLOGREPOSITORY I ISKORISTITI ISTU I ZA ABSTRACTHEALTHLOGREPOSITORY I OVAJ REPOSITORY
        $dateFromObj = Carbon::parse($dateFrom);
        $dateToObj = Carbon::parse($dateTo);
        $daysDiff = ceil($dateFromObj->diffInHours($dateToObj)/24);

        $groupedLogs = $logs->groupBy(function($log) {
                return $log->log_ts->format('Y-m-d');
            });
        if ($sort != 'asc') $groupedLogs = $groupedLogs->reverse();

        $logsList = [
            'date_from' => $dateFromObj->toDateString(),
            'date_to' => $dateTo->toDateString(),
            'days' => $daysDiff,
            'data' => [],
            'health_goals' => $healthGoals,
            'stats' => $this->getTotalBasicStats($logs, $healthGoals, $daysDiff)
        ];
        $groupedLogs->each(function($dailyLogs, $key) use($healthGoals, &$logsList, $dailyStats) {
            $healthGoal = $healthGoals->where('goal_from_ts', '<=', $dailyLogs->first()->log_ts)->first();
            array_push($logsList['data'], [
                'date' => $key,
                'logs' => $dailyLogs,
                'health_goal' => $healthGoal,
                'stats' => ($dailyStats == true) ? $this->getTotalBasicStats($dailyLogs, $healthGoals, 1) : null
            ]);
        });
        /*$logsList['data'] = array_values(array_sort($logsList['data'], function($value) {
            return $value['stats']['glucose_avg'];
        }));*/
        return $logsList;
    }

    public function getSeparatedLogLists(User $user, Collection $healthGoals, Carbon $dateFrom, Carbon $dateTo, $stdWeekDays = false) {
        $healthLogs = $this->getPatientLogs($user, $dateFrom, $dateTo, true);
        return [
            'glucose' => $this->glucoseLogRepo->createLogsList($healthLogs['glucose'], $healthGoals, $dateFrom, $dateTo, $stdWeekDays),
            'insulin' => $this->insulinLogRepo->createLogsList($healthLogs['insulin'], $healthGoals, $dateFrom, $dateTo, $stdWeekDays),
            'step' => $this->stepLogRepo->createLogsList($healthLogs['step'], $healthGoals, $dateFrom, $dateTo, $stdWeekDays),
            'calorie' => $this->calorieLogRepo->createLogsList($healthLogs['calorie'], $healthGoals, $dateFrom, $dateTo, $stdWeekDays),
            'physical_activity' => $this->physicalActivityLogRepo->createLogsList($healthLogs['physical_activity'], $healthGoals, $dateFrom, $dateTo, $stdWeekDays),
            'sleep' => $this->sleepLogRepo->createLogsList($healthLogs['sleep'], $healthGoals, $dateFrom, $dateTo, $stdWeekDays)
        ];
    }

    public function getSeparatedLogStats(User $user, $dateFrom, $dateTo) {
        return [
            'glucose' => $this->glucoseLogRepo->getPatientGlucoseSummary($user, $dateFrom, $dateTo),
            'insulin' => $this->insulinLogRepo->getPatientInsulinSummary($user, $dateFrom, $dateTo),
            'physical_activity' => $this->physicalActivityLogRepo->getPatientActivitySummary($user, $dateFrom, $dateTo),
            'step' => $this->stepLogRepo->getPatientStepSummary($user, $dateFrom, $dateTo),
            'calorie' => $this->calorieLogRepo->getPatientCalorieSummary($user, $dateFrom, $dateTo),
            'sleep' => $this->sleepLogRepo->getPatientSleepSummary($user, $dateFrom, $dateTo)
        ];
    }

    public function getTotalBasicStats(Collection $logs, Collection $healthGoals, $daysDiff) {
        $totalStats = [
            'glucose_avg' => null,
            'glucose_max' => null,
            'glucose_min' => null,
            'insulin_basal_daily_avg' => null,
            'insulin_bolus_daily_avg' => null,
            'activity_total' => null,
            'activity_daily_avg' => null,
            'activity_heart_rate_avg' => null,
            'calorie_daily_avg' => null,
            'step_daily_avg' => null,
            'sleep_total' => null,
            'sleep_avg' => null,
            'sleep_avg_quality' => null
        ];
        $logsByType = $logs->groupBy('logType.type');
        $logsByType->each(function($logs, $typeKey) use(&$totalStats, $healthGoals, $daysDiff) {
            if ($typeKey == 'glucose') {
                $stats = $this->calculateGlucoseStats($logs, $healthGoals, $daysDiff);
                $totalStats['glucose_avg'] = $stats['avg_value'];
                $totalStats['glucose_max'] = $stats['max_value'];
                $totalStats['glucose_min'] = $stats['min_value'];
            }
            if ($typeKey == 'insulin') {
                $stats = $this->calculateInsulinStats($logs, $healthGoals, $daysDiff);
                $totalStats['insulin_basal_daily_avg'] = $stats['daily_avg_units_basal'];
                $totalStats['insulin_bolus_daily_avg'] = $stats['daily_avg_units_bolus'];
            }
            if ($typeKey == 'physical_activity') {
                $stats = $this->calculateActivityStats($logs, $healthGoals, $daysDiff);
                $totalStats['activity_total'] = $stats['total_duration'];
                $totalStats['activity_daily_avg'] = $stats['daily_avg_duration'];
                $totalStats['activity_heart_rate_avg'] = $stats['avg_heart_rate'];
            }
            if ($typeKey == 'calorie') {
                $stats = $this->calculateCalorieStats($logs, $healthGoals, $daysDiff);
                $totalStats['calorie_daily_avg'] = $stats['daily_avg_calories'];
            }
            if ($typeKey == 'step') {
                $stats = $this->calculateStepStats($logs, $healthGoals, $daysDiff);
                $totalStats['step_daily_avg'] = $stats['daily_avg_steps'];
            }
            if ($typeKey == 'sleep') {
                $stats = $this->calculateSleepStats($logs, $healthGoals, $daysDiff);
                $totalStats['sleep_total'] = $stats['total_duration'];
                $totalStats['sleep_avg'] = $stats['avg_duration'];
                $totalStats['sleep_avg_quality'] = $stats['avg_quality'];
            }
        });
        return $totalStats;
    }

    public function generateXlsExport($userId, $dateFrom, $dateTo, $fileName) {
        $user = User::findOrFail($userId);
        $separatedLogs = $this->getPatientLogs($user, $dateFrom, $dateTo, true);
        $logsExport = [];
        $separatedLogs->each(function($logType, $typeKey) use(&$logsExport) {
            $logType->each(function($log, $key) use(&$logsExport, $logType, $typeKey) {
                $logCollection = collect($log)->only(['value', 'log_ts', 'insert_type'])->flatten();
                $merged = $logCollection;
                $extraCollections = null;
                switch ($typeKey) {
                    case 'glucose':
                        $extraCollections = collect($log->loggable->label)->only(['id', 'display_value'])->flatten();
                        break;
                    case 'insulin':
                        $labelCollection = collect($log->loggable->label)->only(['id', 'display_value'])->flatten();
                        $typeCollection = collect($log->loggable->insulinType)->only(['key'])->flatten();
                        $extraCollections = $labelCollection->merge($typeCollection);
                        break;
                    case 'calorie':
                        break;
                    case 'step':
                        break;
                    case 'physical_activity':
                        $extraCollections = collect($log->loggable)->except(['id'])->flatten();
                        break;
                    case 'sleep':
                        $extraCollections = collect($log->loggable)->except(['id'])->flatten();
                        break;
                }
                if ($extraCollections != null) $merged = $merged->merge($extraCollections);
                $logsExport[$typeKey][] = $merged->toArray();
            });
        });
        $sharedHeadings = config('application.excel.headings.health_log.all');
        return Excel::create("{$fileName}", function($excel) use($logsExport, $sharedHeadings) {
            foreach($logsExport as $key => $value) {
                $excel->sheet($key, function($sheet) use($value, $key, $sharedHeadings) {
                    $headingArr = array_merge($sharedHeadings, config('application.excel.headings.health_log.models.' . $key));
                    $sheet->fromArray($value, null, 'A1', true, false);
                    $sheet->prependRow($headingArr);
                });
            }
        })->export('xls');
    }

    public function getOtherDateForPeriod($date, $period, $type) {
        $period = strtolower($period);
        $dateObj = Carbon::parse($date);
        $dateOther = null;
        switch ($period) {
            case '1d':
                ($type == 0) ? $dateOther = $dateObj->subDay() : $dateOther = $dateObj->addDay();
                break;
            case '1w':
                ($type == 0) ? $dateOther = $dateObj->subWeek() : $dateOther = $dateObj->addWeek();
                break;
            case '1m':
                ($type == 0) ? $dateOther = $dateObj->subMonth() : $dateOther = $dateObj->addMonth();
                break;
            case '3m':
                ($type == 0) ? $dateOther = $dateObj->subMonths(3) : $dateOther = $dateObj->addMonths(3);
                break;
            case '6m':
                ($type == 0) ? $dateOther = $dateObj->subMonths(6) : $dateOther = $dateObj->addMonths(6);
                break;
        }
        return $dateOther;
    }

    public function storeHealthLog($userId, $healthLogTypeKey, $logValue, $logTs, $labelId, $insulinTypeId) {
        $user = User::findOrFail($userId);
        $type = HealthLogType::where('type', $healthLogTypeKey)->firstOrFail();
        $healthLog = new HealthLog;
        $healthLog->value = $logValue;
        $healthLog->log_ts = $logTs;
        $healthLog->logType()->associate($type);
        $healthLog->patient()->associate($user);
        /*if ($type->type == 'glucose') {
            $log = new GlucoseLog;
            $label = HealthLogLabel::findOrFail($labelId);
            $log->label()->associate($label);
            $log->save();
        }*/
        if ($type->type == 'insulin') {
            $log = new InsulinLog;
            $label = HealthLogLabel::findOrFail($labelId);
            $insulinType = InsulinLogType::findOrFail($insulinTypeId);
            $log->label()->associate($label);
            $log->insulinType()->associate($insulinType);
            $log->save();
        }

        $healthLog = $log->log()->save($healthLog);
        return $healthLog;
    }
}
