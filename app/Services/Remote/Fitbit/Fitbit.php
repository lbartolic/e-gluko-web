<?php
namespace App\Services\Remote\Fitbit;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use App\Services\Remote\Fitbit\BaseApiAbstract;
use App\Models\Access\User\User;
use App\Models\Fitbit\FitbitUser;
use App\Models\Fitbit\FitbitDevice;

class Fitbit extends BaseApiAbstract {
	public function test() {
		$this->initialFitbitUserSetup(User::find(2), '688022488f6d278cb9b5052e8780e6337b8e134a');
		die();
		//$this->postRetrieveUserAccessData();
		//$this->postRefreshUserTokens();
		//die();
		/*$this->postRefreshUserTokens();*/

		// get user data with access token
		$userId = $this->user_id;
		//try {
			$r = $this->client->get("1/user/{$userId}/profile.json", [
				'headers' => [
					'Authorization' => 'Bearer ' . $this->access_token
				]
			]);
			$responseData = json_decode($r->getBody());
			dd($responseData);
		/*} catch(RequestException $e) {
			$response = $e->getResponse();
			if (isset($response) && $response->getStatusCode() == 401) {
				$this->access_token = $this->postRefreshUserTokens()->access_token;
				$this->test();
			}
		}*/
	}

	public function initialFitbitUserSetup(User $user, $authCode) {
		parent::initialFitbitUserSetup($user, $authCode);
	}

	public function getDeviceInfo(User $user) {
		$fitbitUser = $user->fitbitUser;
		if ($fitbitUser != null) {
			$fitbitUser = $this->getUserAccessTokenForApi($fitbitUser);
			$responseData = null;
			try {
				$r = $this->client->get("1/user/{$fitbitUser->fitbit_uid}/devices.json", [
					'headers' => ['Authorization' => 'Bearer ' . $fitbitUser->access_token]
				]);
				$responseData = json_decode($r->getBody())[0];
			} catch(RequestException $e) {
				// TODO: HANDLE EXCEPTION
				return null;
			}
			return $responseData;
		}
		return null;
	}

	public function updateDbDeviceInfo(User $user, $deviceInfo) {
		$fitbitUser = $user->fitbitUser;
		$device = $fitbitUser->device;
		$device->last_sync_time = \Carbon\Carbon::parse($deviceInfo->lastSyncTime);
		$device->save();
		dd($device);
	}

}