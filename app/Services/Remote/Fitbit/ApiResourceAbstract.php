<?php
namespace App\Services\Remote\Fitbit;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use Carbon\Carbon;
use App\Services\Remote\Fitbit\BaseApiAbstract;
use App\Models\Access\User\User;
use App\Models\Fitbit\FitbitUser;
use App\Models\Fitbit\FitbitDevice;

abstract class ApiResourceAbstract extends BaseApiAbstract {
	abstract protected function getResourceModel();
	abstract protected function getResourceData(FitbitUser $fitbitUser, Carbon $dateFrom);
	abstract public function updateResourceTable(User $user, $logs, Carbon $dateFrom);

	public function getLastInDb(FitbitUser $fitbitUser) {
		$user = $fitbitUser->user;
		return $this->getResourceModel()::with(['log', 'log.patient'])->whereHas('log.patient', function($q) use($user) {
				return $q->where('id', $user->id);
			})->get()->sortByDesc('log.log_ts')->first();
	}

	public function getDateFromForApiCall(FitbitUser $fitbitUser) {
		$last = $this->getLastInDb($fitbitUser);
		if ($last == null) {
			return $fitbitUser->member_since;
		}
		return $last->log->log_ts;
	}

	public function getMaxDateToForApiCall(FitbitUser $fitbitUser) {
		return $fitbitUser->device->last_sync_time;
	}

	public function getData(User $user, Carbon $dateFrom) {
		$fitbitUser = $user->fitbitUser;
		if ($fitbitUser != null) {
			$fitbitUser = $this->getUserAccessTokenForApi($fitbitUser);
			return $this->getResourceData($fitbitUser, $dateFrom);
		}
		return null;
	}

	protected function getApiLogsDatesRange($logs, $key) {
		$dates = array_values(array_pluck($logs, $key));
		$dtFrom = Carbon::parse(array_first($dates));
		$dtTo = Carbon::parse(array_last($dates))->endOfDay();
		return [$dtFrom, $dtTo];
	}

}