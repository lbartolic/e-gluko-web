<?php
namespace App\Services\Remote\Fitbit;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use App\Models\Access\User\User;
use App\Models\Fitbit\FitbitUser;
use App\Models\Fitbit\FitbitDevice;

abstract class BaseApiAbstract {
	const BASE_URL = "https://api.fitbit.com/";
	const CLIENT_ID = "228JZX";
	const CLIENT_SECRET = "c0bc15b7cb8341d125c17a1e948088d6";
	const TOKEN_EXPIRES = 3600;
	const GRAND_TYPE_AUTH_CODE = "authorization_code";
	const GRAND_TYPE_REFR_TOKEN = "refresh_token";
	const REDIRECT_URI = "http://localhost:8000/admin/fitbit/authorization";
	const DATE_FORMAT = "Y-m-d";
	
	protected $client;

	public function __construct() {
		$this->client = new Client([
			'base_uri' => self::BASE_URL
		]);
	}

	private function encodeAppCredentials() {
		return base64_encode(self::CLIENT_ID . ':' . self::CLIENT_SECRET);
	}

	/**
	 * Get inital access data needed for user setup
	 */
	private function postRetrieveUserAccessData($authCode) {
		$credentials = $this->encodeAppCredentials();
		$r = $this->client->post('oauth2/token', [
			'headers' => [
				'Authorization' => "Basic {$credentials}",
				'Content-Type' => 'application/x-www-form-urlencoded'
			],
			'form_params' => [
				'client_id' => self::CLIENT_ID,
				'grant_type' => self::GRAND_TYPE_AUTH_CODE,
				'code' => $authCode,
				'redirect_uri' => self::REDIRECT_URI,
				'expires_in' => self::TOKEN_EXPIRES
			]
		]);
		return json_decode($r->getBody());
	}

	protected function initialFitbitUserSetup(User $user, $authCode) {
		$tokenRequestTimestamp = \Carbon\Carbon::now();
		if ($user->fitbitUser == null) {
			$fitbitUser = new FitbitUser;
			$fitbitDevice = new FitbitDevice;
		}
		else {
			$fitbitUser = $user->fitbitUser;
			$fitbitDevice = $fitbitUser->device;
		}
		$accessData = $this->postRetrieveUserAccessData($authCode);

		$fitbitUserData = $this->client->get("1/user/{$accessData->user_id}/profile.json", [
			'headers' => ['Authorization' => 'Bearer ' . $accessData->access_token]
		]);
		$fitbitUserData = json_decode($fitbitUserData->getBody());

		$deviceData = $this->client->get("1/user/{$accessData->user_id}/devices.json", [
			'headers' => ['Authorization' => 'Bearer ' . $accessData->access_token]
		]);
		$deviceData = json_decode($deviceData->getBody())[0];

		$fitbitUser->auth_code = $authCode;
		$fitbitUser->fitbit_uid = $accessData->user_id;
		$fitbitUser->access_token = $accessData->access_token;
		$fitbitUser->refresh_token = $accessData->refresh_token;
		$fitbitUser->token_expires_in = $accessData->expires_in;
		$fitbitUser->token_timestamp = $tokenRequestTimestamp;
		$fitbitUser->member_since = $fitbitUserData->user->memberSince;
		$fitbitDevice->device_version = $deviceData->deviceVersion;
		$fitbitDevice->device_id = $deviceData->id;
		$fitbitDevice->last_sync_time = substr(str_replace("T", " ", $deviceData->lastSyncTime), 0, 19);

		$fitbitUser = $user->fitbitUser()->save($fitbitUser);
		$fitbitUser->device()->save($fitbitDevice);
		return $fitbitUser;
	}

	protected function postRefreshUserTokens(FitbitUser $fitbitUser) {
		$tokenRequestTimestamp = \Carbon\Carbon::now();
		$credentials = $this->encodeAppCredentials();
		$r = $this->client->post('oauth2/token', [
			'headers' => [
				'Authorization' => "Basic {$credentials}",
				'Content-Type' => 'application/x-www-form-urlencoded'
			],
			'form_params' => [
				'grant_type' => self::GRAND_TYPE_REFR_TOKEN,
				'refresh_token' => $fitbitUser->refresh_token,
				'expires_in' => self::TOKEN_EXPIRES
			]
		]);
		$accessData = json_decode($r->getBody());
		$fitbitUser->access_token = $accessData->access_token;
		$fitbitUser->refresh_token = $accessData->refresh_token;
		$fitbitUser->token_expires_in = $accessData->expires_in;
		$fitbitUser->token_timestamp = $tokenRequestTimestamp;
		$fitbitUser->save();

		return $fitbitUser;
	}

	protected function getUserAccessTokenForApi(FitbitUser $fitbitUser) {
		$tokenTimestamp = $fitbitUser->token_timestamp;
		$tokenExpiresIn = $fitbitUser->token_expires_in;
		$tokenExpiration = $tokenTimestamp->copy()->addSeconds($tokenExpiresIn);
		$now = \Carbon\Carbon::now();
		if ($tokenExpiration <= $now) {
			$fitbitUser = $this->postRefreshUserTokens($fitbitUser);
		}
		return $fitbitUser;
	}
}