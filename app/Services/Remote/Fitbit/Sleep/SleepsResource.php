<?php
namespace App\Services\Remote\Fitbit\Sleep;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use Carbon\Carbon;
use App\Services\Remote\Fitbit\ApiResourceAbstract;
use App\Models\Access\User\User;
use App\Models\Fitbit\FitbitUser;
use App\Models\Fitbit\FitbitDevice;
use App\Models\HealthLog\Sleep\SleepLog;
use App\Models\HealthLog\HealthLog;
use App\Models\HealthLog\HealthLogType;

class SleepsResource extends ApiResourceAbstract {
	const API_DAYS_OFFSET = 0;

	protected function getResourceModel() {
		return SleepLog::class;
	}

	protected function getResourceData(FitbitUser $fitbitUser, Carbon $dateFrom) {
		$maxDateTo = $this->getMaxDateToForApiCall($fitbitUser);
		$dateTo = $dateFrom->copy()->addDays(self::API_DAYS_OFFSET);
		if ($dateTo > $maxDateTo) {
			$dateTo = $maxDateTo;
		}
		$dateTo = $dateTo->format(self::DATE_FORMAT);
		$dateFrom = $dateFrom->format(self::DATE_FORMAT);
		$responseData = null;
		try {
			$r = $this->client->get("1.2/user/{$fitbitUser->fitbit_uid}/sleep/list.json?afterDate={$dateFrom}&sort=asc&limit=10&offset=0", [
				'headers' => ['Authorization' => 'Bearer ' . $fitbitUser->access_token]
			]);
			$responseData = json_decode($r->getBody());
		} catch(RequestException $e) {
			$response = $e->getResponse();
			//dd($response, $e->getMessage(), json_decode($response->getBody())->errors[0]->message, json_decode($response->getBody())->errors[0]->errorType);
			if ($response->getStatusCode() == 401) {
				
			}
			// TODO: HANDLE EXCEPTION
			return null;
		}
		return $responseData->{'sleep'};
	}

	public function updateResourceTable(User $user, $logs, Carbon $dateFrom) {
		$datesRange = $this->getApiLogsDatesRange($logs, 'startTime');
		SleepLog::whereHas('log', function($q) use($datesRange) { $q->whereBetween('log_ts', $datesRange); })->delete();
		HealthLog::sleepLogs()->whereBetween('log_ts', $datesRange)->delete();

		$logType = HealthLogType::where('type', 'sleep')->first();
		foreach ($logs as $key => $value) {
			$healthLog = new HealthLog;
			$sleepLog = new SleepLog;

	    	$healthLog->user_id = $user->id;
	    	$healthLog->log_ts = Carbon::parse($value->startTime);
	    	$healthLog->created_at = Carbon::now();
		    $healthLog->value = $value->duration; //[ms]
		    $healthLog->logType()->associate($logType);
		    $healthLog->insert_type = 1;

		    $sleepLog->asleep_mins = $value->levels->summary->asleep->minutes;
		    $sleepLog->awake_mins = $value->levels->summary->awake->minutes;
		    $sleepLog->awake_count = $value->levels->summary->awake->count;
		    $sleepLog->restless_mins = $value->levels->summary->restless->minutes;
		    $sleepLog->restless_count = $value->levels->summary->restless->count;
		    $sleepLog->sleep_quality = round(($value->levels->summary->asleep->minutes/(($value->duration/1000)/60))*100, 0);
		    $sleepLog->save();
		    $healthLog = $sleepLog->log()->save($healthLog);
	    }
	}
}