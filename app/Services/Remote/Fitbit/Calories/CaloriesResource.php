<?php
namespace App\Services\Remote\Fitbit\Calories;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use Carbon\Carbon;
use App\Services\Remote\Fitbit\ApiResourceAbstract;
use App\Models\Access\User\User;
use App\Models\Fitbit\FitbitUser;
use App\Models\Fitbit\FitbitDevice;
use App\Models\HealthLog\Calorie\CalorieLog;
use App\Models\HealthLog\HealthLog;
use App\Models\HealthLog\HealthLogType;

class CaloriesResource extends ApiResourceAbstract {
	const API_DAYS_OFFSET = 6;

	protected function getResourceModel() {
		return CalorieLog::class;
	}

	protected function getResourceData(FitbitUser $fitbitUser, Carbon $dateFrom) {
		$maxDateTo = $this->getMaxDateToForApiCall($fitbitUser);
		$dateTo = $dateFrom->copy()->addDays(self::API_DAYS_OFFSET);
		if ($dateTo > $maxDateTo) {
			$dateTo = $maxDateTo;
		}
		$dateTo = $dateTo->format(self::DATE_FORMAT);
		$dateFrom = $dateFrom->format(self::DATE_FORMAT);
		$responseDataTotal = null;
		$responseDataBmr = null;
		try {
			$r = $this->client->get("1/user/{$fitbitUser->fitbit_uid}/activities/calories/date/{$dateFrom}/{$dateTo}.json", [
				'headers' => ['Authorization' => 'Bearer ' . $fitbitUser->access_token]
			]);
			$responseDataTotal = json_decode($r->getBody())->{'activities-calories'};
		} catch(RequestException $e) { return null; } // TODO: HANDLE EXCEPTION
		try {
			$r = $this->client->get("1/user/{$fitbitUser->fitbit_uid}/activities/caloriesBMR/date/{$dateFrom}/{$dateTo}.json", [
				'headers' => ['Authorization' => 'Bearer ' . $fitbitUser->access_token]
			]);
			$responseDataBmr = json_decode($r->getBody())->{'activities-caloriesBMR'};
		} catch(RequestException $e) { return null; } // TODO: HANDLE EXCEPTION
		return [
			'total' => $responseDataTotal,
			'bmr' => $responseDataBmr
		];
	}

	public function updateResourceTable(User $user, $logs, Carbon $dateFrom) {
		$logsTotal = $logs['total'];
		$logsBmr = $logs['bmr'];
		$datesRange = $this->getApiLogsDatesRange($logsTotal, 'dateTime');
		$logs = [];
		$index = 0;
		foreach($logsTotal as $totalKey => $totalValue) {
			foreach($logsBmr as $bmrKey => $bmrValue) {
				if ($totalValue->dateTime == $bmrValue->dateTime) {
					$logs[$index]['date_time'] = $totalValue->dateTime;
					$logs[$index]['activity_calories'] = $totalValue->value - $bmrValue->value;
					$index++;
				}
			}
		}
		CalorieLog::whereHas('log', function($q) use($datesRange) { $q->whereBetween('log_ts', $datesRange); })->delete();
		HealthLog::calorieLogs()->whereBetween('log_ts', $datesRange)->delete();
		$logType = HealthLogType::where('type', 'calorie')->first();
		foreach ($logs as $key => $value) {
			$healthLog = new HealthLog;
			$calorieLog = new CalorieLog;

	    	$healthLog->user_id = $user->id;
	    	$healthLog->log_ts = Carbon::parse($value['date_time'])->endOfDay();
	    	$healthLog->created_at = Carbon::now();
		    $healthLog->value = $value['activity_calories'];
		    $healthLog->logType()->associate($logType);
		    $healthLog->insert_type = 1;

		    $calorieLog->save();
		    $healthLog = $calorieLog->log()->save($healthLog);
	    }
	}
}