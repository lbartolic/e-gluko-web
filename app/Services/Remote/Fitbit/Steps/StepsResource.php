<?php
namespace App\Services\Remote\Fitbit\Steps;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use Carbon\Carbon;
use App\Services\Remote\Fitbit\ApiResourceAbstract;
use App\Models\Access\User\User;
use App\Models\Fitbit\FitbitUser;
use App\Models\Fitbit\FitbitDevice;
use App\Models\HealthLog\Step\StepLog;
use App\Models\HealthLog\HealthLog;
use App\Models\HealthLog\HealthLogType;

class StepsResource extends ApiResourceAbstract {
	const API_DAYS_OFFSET = 6;

	protected function getResourceModel() {
		return StepLog::class;
	}

	protected function getResourceData(FitbitUser $fitbitUser, Carbon $dateFrom) {
		$maxDateTo = $this->getMaxDateToForApiCall($fitbitUser);
		$dateTo = $dateFrom->copy()->addDays(self::API_DAYS_OFFSET);
		if ($dateTo > $maxDateTo) {
			$dateTo = $maxDateTo;
		}
		$dateTo = $dateTo->format(self::DATE_FORMAT);
		$dateFrom = $dateFrom->format(self::DATE_FORMAT);
		$responseData = null;
		try {
			$r = $this->client->get("1/user/{$fitbitUser->fitbit_uid}/activities/steps/date/{$dateFrom}/{$dateTo}.json", [
				'headers' => ['Authorization' => 'Bearer ' . $fitbitUser->access_token]
			]);
			$responseData = json_decode($r->getBody());
		} catch(RequestException $e) {
			$response = $e->getResponse();
			dd($response, $e->getMessage(), json_decode($response->getBody())->errors[0]->message, json_decode($response->getBody())->errors[0]->errorType);
			if ($response->getStatusCode() == 401) {
				
			}
			// TODO: HANDLE EXCEPTION
			return null;
		}
		return $responseData->{'activities-steps'};
	}

	public function updateResourceTable(User $user, $logs, Carbon $dateFrom) {
		$datesRange = $this->getApiLogsDatesRange($logs, 'dateTime');
		StepLog::whereHas('log', function($q) use($datesRange) { $q->whereBetween('log_ts', $datesRange); })->delete();
		HealthLog::stepLogs()->whereBetween('log_ts', $datesRange)->delete();

		$logType = HealthLogType::where('type', 'step')->first();
		foreach ($logs as $key => $value) {
			$healthLog = new HealthLog;
			$stepLog = new StepLog;

	    	$healthLog->user_id = $user->id;
	    	$healthLog->log_ts = Carbon::parse($value->dateTime)->endOfDay();
	    	$healthLog->created_at = Carbon::now();
		    $healthLog->value = $value->value;
		    $healthLog->logType()->associate($logType);
		    $healthLog->insert_type = 1;

		    $stepLog->save();
		    $healthLog = $stepLog->log()->save($healthLog);
	    }
	}
}