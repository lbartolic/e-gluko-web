<?php
namespace App\Services\Remote\Fitbit\Activities;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use Carbon\Carbon;
use App\Services\Remote\Fitbit\ApiResourceAbstract;
use App\Models\Access\User\User;
use App\Models\Fitbit\FitbitUser;
use App\Models\Fitbit\FitbitDevice;
use App\Models\HealthLog\PhysicalActivity\PhysicalActivityLog;
use App\Models\HealthLog\HealthLog;
use App\Models\HealthLog\HealthLogType;

class ActivitiesResource extends ApiResourceAbstract {
	const API_DAYS_OFFSET = 0;

	protected function getResourceModel() {
		return PhysicalActivityLog::class;
	}

	protected function getResourceData(FitbitUser $fitbitUser, Carbon $dateFrom) {
		$maxDateTo = $this->getMaxDateToForApiCall($fitbitUser);
		$dateTo = $dateFrom->copy()->addDays(self::API_DAYS_OFFSET);
		if ($dateTo > $maxDateTo) {
			$dateTo = $maxDateTo;
		}
		$dateTo = $dateTo->format(self::DATE_FORMAT);
		$dateFrom = $dateFrom->format(self::DATE_FORMAT);
		$responseData = null;
		try {
			$r = $this->client->get("1/user/{$fitbitUser->fitbit_uid}/activities/list.json?afterDate={$dateFrom}&sort=asc&limit=20&offset=0", [
				'headers' => ['Authorization' => 'Bearer ' . $fitbitUser->access_token]
			]);
			$responseData = json_decode($r->getBody());
		} catch(RequestException $e) {
			$response = $e->getResponse();
			//dd($response, $e->getMessage(), json_decode($response->getBody())->errors[0]->message, json_decode($response->getBody())->errors[0]->errorType);
			if ($response->getStatusCode() == 401) {
				
			}
			// TODO: HANDLE EXCEPTION
			return null;
		}
		return $responseData->{'activities'};
	}

	public function updateResourceTable(User $user, $logs, Carbon $dateFrom) {
		$datesRange = $this->getApiLogsDatesRange($logs, 'startTime');
		PhysicalActivityLog::whereHas('log', function($q) use($datesRange) { $q->whereBetween('log_ts', $datesRange); })->delete();
		HealthLog::physicalActivityLogs()->whereBetween('log_ts', $datesRange)->delete();

		$logType = HealthLogType::where('type', 'physical_activity')->first();
		foreach ($logs as $key => $value) {
			$healthLog = new HealthLog;
			$activityLog = new PhysicalActivityLog;

	    	$healthLog->user_id = $user->id;
	    	$healthLog->log_ts = Carbon::parse($value->startTime);
	    	$healthLog->created_at = Carbon::now();
		    $healthLog->value = $value->duration; //[ms]
		    $healthLog->logType()->associate($logType);
		    $healthLog->insert_type = 1;

		    $activityLog->calories = $value->calories;
		    if (isset($value->steps)) $activityLog->steps = $value->steps;
		    else $activityLog->steps = 0;
		    if (isset($value->averageHeartRate)) $activityLog->average_heart_rate = $value->averageHeartRate;
		    $activityLog->save();
		    $healthLog = $activityLog->log()->save($healthLog);
	    }
	}
}