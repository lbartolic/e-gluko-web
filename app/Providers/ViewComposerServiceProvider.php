<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Access\User\User;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composePatientMenu();
        $this->composeSideBar();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function composePatientMenu() {
        view()->composer('backend.layouts.patients.includes.menu-items', function($view) {
            $userId = \Route::current()->parameter('patient');
            $user = User::findOrFail($userId);
            return $view->with('user', $user);
        });
    }
    
    private function composeSideBar() {
        view()->composer('backend.includes.sidebar', function($view) {
            $patientId = \Route::current()->parameter('patient');
            if ($patientId != null) {
                $patient = User::findOrFail($patientId);
                return $view->with('patient', $patient);
            }
            else return $view->with('patient', null);
        });
    }
}
