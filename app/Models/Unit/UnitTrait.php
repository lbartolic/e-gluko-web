<?php

namespace App\Models\Unit;

trait UnitTrait {
	public function formatBGValue($value) {
		return round($value, 1);
	}

	public function formatA1CValue($value) {
		return round($value, 1);
	}

	public function formatCholValue($value) {
		return round($value, 1);
	}

	public function formatWeightValue($value) {
		return round($value, 0);
	}

	public function formatInsulinValue($value) {
		return round($value, 0);
	}

	public function formatStepValue($value) {
		return round($value, 0);
	}

	public function formatCalorieValue($value) {
		return round($value, 0);
	}

	public function formatDurationMsValue($value) {
		return round($value, 0);
	}

	public function formatDurationMinValue($value) {
		return round($value, 1);
	}
}