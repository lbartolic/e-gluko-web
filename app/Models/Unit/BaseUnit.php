<?php

namespace App\Models\Unit;

use Illuminate\Database\Eloquent\Model;

class BaseUnit extends Model
{
    protected $table = 'base_units';
}
