<?php

namespace App\Models\Goal;

trait HealthGoalAttribute
{
    /**
     *
     * HealthGoal's Accessors
     *
     */
    
    public function getGoalFromFormattedAttribute() {
    	return $this->goal_from_ts->format(config('application.time.date_formats.default_date'));
    }

    public function getBloodPressureFormatAttribute() {
        return "{$this->max_sistolic_bp}/{$this->max_diastolic_bp}";
    }

    public function getMaxA1cAttribute($value) {
        return $this->formatA1CValue($value);
    }

    public function getMaxTotalCholesterolAttribute($value) {
        return $this->formatCholValue($value);
    }

    public function getMinHdlCholesterolAttribute($value) {
        return $this->formatCholValue($value);
    }

    public function getMaxLdlCholesterolAttribute($value) {
        return $this->formatCholValue($value);
    }

    public function getMinBgBeforeMealAttribute($value) {
        return $this->formatBGValue($value);
    }

    public function getMaxBgBeforeMealAttribute($value) {
        return $this->formatBGValue($value);
    }

    public function getMaxBgAfterMealAttribute($value) {
        return $this->formatBGValue($value);
    }

    public function getWeightAttribute($value) {
        return $this->formatWeightValue($value);
    }
}
