<?php

namespace App\Models\Goal;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\User\User;
use App\Models\Unit\UnitTrait;

class HealthGoal extends Model
{
    use UnitTrait;
    use HealthGoalAttribute;

    protected $table = 'health_goals';
    protected $fillable = [
        'max_a1c',
        'min_bg_before_meal',
        'max_bg_before_meal',
        'max_bg_after_meal',
        'weight',
        'min_daily_steps',
        'min_daily_excercise',
        'goal_from_ts',
        'daily_basal_total',
        'daily_bolus_total',
        'tdd',
        'user_id'
    ];
    protected $dates = [
    	'goal_from_ts'
    ];

    public function patient() {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeBetweenDates($query, $dateFromObj, $dateToObj) {
        return $query->orderBy('goal_from_ts', 'DESC')->where(function($q) use($dateFromObj) {
        	$q->orderBy('goal_from_ts', 'DESC')->where('goal_from_ts', '<', $dateFromObj)->take(1);
        })->orWhere('goal_from_ts', '>', $dateFromObj);
    }
}
