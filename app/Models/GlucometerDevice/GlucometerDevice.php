<?php

namespace App\Models\GlucometerDevice;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\User\User;
use App\Models\HealthLog\HealthLog;

class GlucometerDevice extends Model
{
    protected $table = "glucometer_devices";
	protected $dates = [
		'last_sync_ts'
	];

	public function user() {
		return $this->belongsTo(User::class, 'user_id');
	}
    
    public function healthLogs() {
        return $this->belongsToMany(HealthLog::class, 'glucometer_devices_logs');
    }
}
