<?php

namespace App\Models\Patient;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\User\User;
use Carbon\Carbon;

class PatientInfo extends Model
{
    protected $table = 'patient_infos';
    protected $dates = [
    	'birth_date_ts', 'diabetes_from_ts'
    ];
    protected $appends = ['sex_char'];

    public function patient() {
    	return $this->belongsTo(User::class, 'patient_id');
    }

    // Accessors

    public function getBirthDateFormattedAttribute() {
    	return $this->birth_date_ts->format(config('application.time.date_formats.default_date'));
    }

    public function getDiabetesFromFormattedAttribute() {
    	return $this->diabetes_from_ts->format(config('application.time.date_formats.default_date'));
    }

    public function getSexCharAttribute() {
    	return ($this->sex == 0) ? 'M' : 'Ž';
    }

    public function getYearsAttribute() {
    	return $this->birth_date_ts->diffInYears(Carbon::now());
    }

    public function getYearsDiabetesAttribute() {
    	return $this->diabetes_from_ts->diffInYears(Carbon::now());
    }
}
