<?php

namespace App\Models\Access\User;

use Illuminate\Notifications\Notifiable;
use App\Models\Access\User\Traits\UserAccess;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\User\Traits\Scope\UserScope;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\User\Traits\UserSendPasswordReset;
use App\Models\Access\User\Traits\Attribute\UserAttribute;
use App\Models\Access\User\Traits\Relationship\UserRelationship;
use App\Models\Fitbit\FitbitUser;
use App\Models\Goal\HealthGoal;
use App\Models\MedicalTest\MedicalTest;
use App\Models\Patient\PatientInfo;
use App\Models\HealthLog\HealthLog;
use App\Models\GlucometerDevice\GlucometerDevice;


/**
 * Class User.
 */
class User extends Authenticatable
{
    use UserScope,
        UserAccess,
        Notifiable,
        SoftDeletes,
        UserAttribute,
        UserRelationship,
        UserSendPasswordReset;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'status', 'confirmation_code', 'confirmed'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
    protected $appends = ['full_name', 'name'];

    protected $with = ['roles'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('access.users_table');
    }

    /* Relations */

    public function fitbitUser() {
        return $this->hasOne(FitbitUser::class, 'user_id');
    }
    
    public function glucometerDevice() {
        return $this->hasOne(GlucometerDevice::class, 'user_id');
    }

    public function healthGoals() {
        return $this->hasMany(HealthGoal::class, 'user_id');
    }

    public function healthLogs() {
        return $this->hasMany(HealthLog::class, 'user_id');
    }

    public function patientMedicalTests() {
        return $this->hasMany(MedicalTest::class, 'patient_id');
    }

    public function doctorMedicalTests() {
        return $this->hasMany(MedicalTest::class, 'doctor_id');
    }

    public function patientInfo() {
        return $this->hasOne(PatientInfo::class, 'patient_id');
    }

    public function insulinPerscribedDosages() {
        return $this->hasMany(InsulinPrescribedDosage::class, 'patient_id');
    }

    /* Scopes */
    
    public function scopePatients($query) {
        return $query->whereHas('roles', function($q) {
            return $q->where('roles.id', 2);
        });
    }

    public function scopeFitbitUsersWithSetAccess($query) {
        return $query->whereHas('fitbitUser', function($q) { 
            $q->hasSetAccess(); 
        });
    }
}
