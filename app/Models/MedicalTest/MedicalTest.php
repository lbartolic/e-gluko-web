<?php

namespace App\Models\MedicalTest;

use Illuminate\Database\Eloquent\Model;
use App\Models\HealthLog\Weight\WeightLog;
use App\Models\Access\User\User;

class MedicalTest extends Model
{
    protected $table = 'medical_tests';

    public function weightLog() {
    	return $this->belongsTo(WeightLog::class, 'weight_log_id');
    }

    public function patient() {
    	return $this->belongsTo(User::class, 'patient_id');
    }

    public function doctor() {
    	return $this->belongsTo(User::class, 'doctor_id');
    }
}
