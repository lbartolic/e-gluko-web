<?php

namespace App\Models\Fitbit;

use Illuminate\Database\Eloquent\Model;
use App\Models\Fitbit\FitbitUser;

class FitbitDevice extends Model
{
	protected $table = "fitbit_devices";
	protected $dates = [
		'last_sync_time'
	];

	public function user() {
		return $this->belongsTo(FitbitUser::class, 'fitbit_user_id');
	}
}
