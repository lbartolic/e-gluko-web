<?php

namespace App\Models\Fitbit;

use Illuminate\Database\Eloquent\Model;
use App\Models\Fitbit\FitbitDevice;
use App\Models\Access\User\User;

class FitbitUser extends Model
{
    protected $table = "fitbit_users";
    protected $dates = [
		'member_since', 'token_timestamp'
	];

	public function device() {
		return $this->hasOne(FitbitDevice::class, 'fitbit_user_id');
	}

	public function user() {
		return $this->belongsTo(User::class, 'user_id');
	}

	public function scopeHasSetAccess($q) {
		return $q->whereNotNull('member_since')
			->whereNotNull('access_token')
			->whereNotNull('refresh_token');
	}
}
