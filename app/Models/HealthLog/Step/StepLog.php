<?php

namespace App\Models\HealthLog\Step;

use App\Models\HealthLog\HealthLog;
use Illuminate\Database\Eloquent\Model;

class StepLog extends Model
{
    protected $table = 'step_logs';
    public $timestamps = false;

    public function log() {
        return $this->morphOne(HealthLog::class, 'loggable');
    }
}
