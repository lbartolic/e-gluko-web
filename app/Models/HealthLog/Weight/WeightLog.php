<?php

namespace App\Models\HealthLog\Weight;

use Illuminate\Database\Eloquent\Model;
use App\Models\HealthLog\Weight\WeightLogType;
use App\Models\HealthLog\Weight\WeightLogCalc;
use App\Models\Access\User\User;
use App\Models\HealthLog\HealthLog;

class WeightLog extends Model
{
    protected $table = "weight_logs";
    protected $with = ['calculations'];

    public function log() {
        return $this->morphOne(HealthLog::class, 'loggable');
    }

    public function weightLogType() {
    	return $this->belongsTo(WeightLogType::class, 'weight_log_type_id');
    }

    public function calculations() {
    	return $this->hasOne(WeightLogCalc::class, 'weight_log_id');
    }

    public function patient() {
    	return $this->belongsTo(User::class, 'user_id');
    }
}
