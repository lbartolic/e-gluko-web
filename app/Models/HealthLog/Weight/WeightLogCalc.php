<?php

namespace App\Models\HealthLog\Weight;

use Illuminate\Database\Eloquent\Model;
use App\Models\HealthLog\Weight\WeightLog;

class WeightLogCalc extends Model
{
    protected $table = 'weight_log_calcs';

    public function log() {
    	return $this->belongsTo(WeightLog::class, 'weight_log_id');
    }

    public function getBmiAttribute($value) {
    	return round($value, 1);
    }

    public function getWhtrAttribute($value) {
    	return round($value, 1);
    }
}
