<?php

namespace App\Models\HealthLog\Weight;

use Illuminate\Database\Eloquent\Model;
use App\Models\HealthLog\Weight\WeightLog;

class WeightLogType extends Model
{
    protected $table = 'weight_log_types';

    public function log() {
    	return $this->hasMany(WeightLog::class, 'weight_log_type_id');
    }
}
