<?php

namespace App\Models\HealthLog\Insulin;

use Illuminate\Database\Eloquent\Model;
use App\Models\HealthLog\Insulin\InsulinLog;

class InsulinLogType extends Model
{
    protected $table = 'insulin_log_types';

    public function log() {
    	return $this->hasMany(InsulinLog::class, 'insulin_type_id');
    }
}
