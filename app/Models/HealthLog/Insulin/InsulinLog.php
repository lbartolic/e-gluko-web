<?php

namespace App\Models\HealthLog\Insulin;

use App\Models\HealthLog\HealthLog;
use Illuminate\Database\Eloquent\Model;
use App\Models\Access\User\User;
use App\Models\HealthLog\Insulin\InsulinLogType;
use App\Models\HealthLog\HealthLogLabel;

class InsulinLog extends Model
{
    protected $table = 'insulin_logs';
    protected $with = ['insulinType', 'label'];

    public function log() {
        return $this->morphOne(HealthLog::class, 'loggable');
    }

    public function insulinType() {
    	return $this->belongsTo(InsulinLogType::class, 'insulin_type_id');
    }

    public function label() {
    	return $this->belongsTo(HealthLogLabel::class, 'health_log_label_id');
    }
}
