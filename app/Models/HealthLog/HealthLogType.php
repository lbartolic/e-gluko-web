<?php

namespace App\Models\HealthLog;

use Illuminate\Database\Eloquent\Model;
use App\Models\HealthLog\HealthLog;

class HealthLogType extends Model
{
	protected $table = "health_log_types";
	
    public function logs() {
    	return $this->hasMany(HealthLog::class, 'log_type');
    }
}
