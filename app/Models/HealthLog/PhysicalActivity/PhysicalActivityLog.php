<?php

namespace App\Models\HealthLog\PhysicalActivity;

use App\Models\HealthLog\HealthLog;
use Illuminate\Database\Eloquent\Model;

class PhysicalActivityLog extends Model
{
    protected $table = 'physical_activity_logs';
    public $timestamps = false;

    public function log() {
        return $this->morphOne(HealthLog::class, 'loggable');
    }

    public function formattedDurationMinutes() {
    	return round(($this->log->value/1000)/60, 0);
    }

    public function formattedDurationHi() {
    	return gmdate('H:i', $this->log->value/1000);
    }
}
