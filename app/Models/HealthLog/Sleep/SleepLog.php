<?php

namespace App\Models\HealthLog\Sleep;

use App\Models\HealthLog\HealthLog;
use Illuminate\Database\Eloquent\Model;

class SleepLog extends Model
{
    protected $table = 'sleep_logs';
    public $timestamps = false;

    public function log() {
        return $this->morphOne(HealthLog::class, 'loggable');
    }
}
