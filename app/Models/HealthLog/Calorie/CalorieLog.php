<?php

namespace App\Models\HealthLog\Calorie;

use App\Models\HealthLog\HealthLog;
use Illuminate\Database\Eloquent\Model;

class CalorieLog extends Model
{
    protected $table = 'calorie_logs';
    public $timestamps = false;

    public function log() {
    	return $this->morphOne(HealthLog::class, 'loggable');
    }
}
