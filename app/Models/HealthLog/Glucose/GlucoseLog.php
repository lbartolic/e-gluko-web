<?php

namespace App\Models\HealthLog\Glucose;

use App\Models\HealthLog\HealthLog;
use Illuminate\Database\Eloquent\Model;
use App\Models\HealthLog\HealthLogLabel;
use App\Models\Access\User\User;

class GlucoseLog extends Model
{
    protected $table = 'glucose_logs';
    protected $with = ['label'];

    public function log() {
        return $this->morphOne(HealthLog::class, 'loggable');
    }

    public function label() {
    	return $this->belongsTo(HealthLogLabel::class, 'health_log_label_id');
    }

    public function scopeBetweenDates($query, $dateFromObj, $dateToObj) {
        return $query->whereHas('log', function($query) use($dateFromObj, $dateToObj) {
            $query->whereBetween('log_ts', [$dateFromObj, $dateToObj]);
        });
    }

    public function getDescGoalDiff($logParent, $healthGoal) {
        // get descriptive goal difference (low/normal/high) with deviation percentage
        $minBefore = $healthGoal->min_bg_before_meal;
        $maxBefore = $healthGoal->max_bg_before_meal;
        $maxAfter = $healthGoal->max_bg_after_meal;
        $mealRel = $this->label->meal_rel;
        $value = $logParent->value;
        $maxValue = $maxAfter;
        $minValue = $minBefore;
        if ($mealRel == 1 || $mealRel == 2) {
            if ($mealRel == 1) {
                $maxValue = $maxBefore;
                $minValue = $minBefore;
            }
            if ($mealRel == 2) {
                $maxValue = $maxAfter;
                $minValue = $minBefore;
            }
        }
        else {
        }

        $desc = 'normal';
        $deviationPerc = null;
        $log = $logParent;
        if ($value < $minValue) {
            $desc = 'low';
            $deviationPerc = round(($value/$minBefore)*100, 1) - 100;
        }
        if ($value > $maxValue) {
            $desc = 'high';
            $deviationPerc = round(($value/$maxAfter)*100, 1) - 100;
        }
        return [
            'log' => $log,
            'desc' => $desc,
            'deviation_percentage' => $deviationPerc
        ];
    }
}
