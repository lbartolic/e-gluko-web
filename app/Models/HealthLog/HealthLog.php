<?php

namespace App\Models\HealthLog;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\User\User;
use App\Models\HealthLog\HealthLogType;
use App\Models\GlucometerDevice\GlucometerDevice;

class HealthLog extends Model
{
    protected $table = 'health_logs';
    protected $dates = [
		'log_ts'
	];
    protected $with = [
        'logType', 'loggable'
    ];

    public function loggable() {
        return $this->morphTo();
    }

    public function patient() {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function logType() {
        return $this->belongsTo(HealthLogType::class, 'log_type');
    }
    
    public function glucometerDevice() {
        return $this->belongsTo(GlucometerDevice::class, 'glucometer_devices_logs');
    }

    public function scopeGlucoseLogs($q) {
        return $q->whereHas('logType', function($q) { $q->where('type', 'glucose'); });
    }

    public function scopeInsulinLogs($q) {
        return $q->whereHas('logType', function($q) { $q->where('type', 'insulin'); });
    }

    public function scopeStepLogs($q) {
        return $q->whereHas('logType', function($q) { $q->where('type', 'step'); });
    }

    public function scopeCalorieLogs($q) {
        return $q->whereHas('logType', function($q) { $q->where('type', 'calorie'); });
    }

    public function scopePhysicalActivityLogs($q) {
        return $q->whereHas('logType', function($q) { $q->where('type', 'physical_activity'); });
    }

    public function scopeSleepLogs($q) {
        return $q->whereHas('logType', function($q) { $q->where('type', 'sleep'); });
    }

    public function getInsertTypeStringAttribute() {
        if ($this->insert_type == 1) return 'uređaj';
        return 'korisnik';
    }

    public function getHmsFromSecAttribute() {
        // value in ms
        $seconds = $this->value/1000;
        $H = floor($seconds/3600);
        $m = ($seconds/60)%60;
        $s = $seconds%60;
        return sprintf("%02d:%02d:%02d", $H, $m, $s);
    }

    public function getValueAttribute($value) {
        if ($this->log_type == 1) return round($value, 1);
        return round($value, 0);
    }
}
