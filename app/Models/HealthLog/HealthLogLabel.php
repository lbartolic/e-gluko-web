<?php

namespace App\Models\HealthLog;

use Illuminate\Database\Eloquent\Model;
use App\Models\HealthLog\Glucose\GlucoseLog;

class HealthLogLabel extends Model
{
    protected $table = 'health_log_labels';

    public function glucoseLogs() {
    	return $this->hasMany(GlucoseLog::class, 'health_log_label_id');
    }
}
