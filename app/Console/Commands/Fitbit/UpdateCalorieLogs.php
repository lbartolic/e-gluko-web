<?php

namespace App\Console\Commands\Fitbit;

use Illuminate\Console\Command;
use App\Services\Remote\Fitbit\Calories\CaloriesResource;
use App\Models\HealthLog\Calorie\CalorieLog;
use App\Models\HealthLog\Glucose\GlucoseLog;
use App\Models\Access\User\User;

class UpdateCalorieLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateCalorieLogs:update-calories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update calories';
    protected $caloriesResource;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->caloriesResource = new CaloriesResource;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::fitbitUsersWithSetAccess()->get();
        foreach($users as $user) {
            $dateFrom = $this->caloriesResource->getDateFromForApiCall($user->fitbitUser);
            $logs = $this->caloriesResource->getData($user, $dateFrom);
            $this->caloriesResource->updateResourceTable($user, $logs, $dateFrom);
        }
    }
}
