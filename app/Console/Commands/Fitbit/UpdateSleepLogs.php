<?php

namespace App\Console\Commands\Fitbit;

use Illuminate\Console\Command;
use App\Services\Remote\Fitbit\Sleep\SleepsResource;
use App\Models\HealthLog\Step\StepLog;
use App\Models\HealthLog\Glucose\GlucoseLog;
use App\Models\Access\User\User;

class UpdateSleepLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateSleepLogs:update-sleeps';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update sleeps';
    protected $sleepsResource;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->sleepsResource = new SleepsResource;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::fitbitUsersWithSetAccess()->get();
        foreach($users as $user) {
            $dateFrom = $this->sleepsResource->getDateFromForApiCall($user->fitbitUser);
            $logs = $this->sleepsResource->getData($user, $dateFrom);
            $this->sleepsResource->updateResourceTable($user, $logs, $dateFrom);
        }
    }
}
