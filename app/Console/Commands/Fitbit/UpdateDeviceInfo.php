<?php

namespace App\Console\Commands\Fitbit;

use Illuminate\Console\Command;
use App\Services\Remote\Fitbit\Fitbit;
use App\Models\HealthLog\Step\StepLog;
use App\Models\HealthLog\Glucose\GlucoseLog;
use App\Models\Access\User\User;

class UpdateDeviceInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateDeviceInfo:update-device-info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update device info';
    protected $fitbit;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->fitbit = new Fitbit;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::fitbitUsersWithSetAccess()->get();
        foreach($users as $user) {
            $deviceInfo = $this->fitbit->getDeviceInfo($user);
            $this->fitbit->updateDbDeviceInfo($user, $deviceInfo);
        }
    }
}
