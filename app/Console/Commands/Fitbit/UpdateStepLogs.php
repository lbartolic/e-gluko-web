<?php

namespace App\Console\Commands\Fitbit;

use Illuminate\Console\Command;
use App\Services\Remote\Fitbit\Steps\StepsResource;
use App\Models\HealthLog\Step\StepLog;
use App\Models\HealthLog\Glucose\GlucoseLog;
use App\Models\Access\User\User;

class UpdateStepLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateStepLogs:update-steps';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update steps';
    protected $stepsResource;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->stepsResource = new StepsResource;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::fitbitUsersWithSetAccess()->get();
        foreach($users as $user) {
            $dateFrom = $this->stepsResource->getDateFromForApiCall($user->fitbitUser);
            $logs = $this->stepsResource->getData($user, $dateFrom);
            $this->stepsResource->updateResourceTable($user, $logs, $dateFrom);
        }
    }
}
