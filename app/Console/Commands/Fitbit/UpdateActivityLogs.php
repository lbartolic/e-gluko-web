<?php

namespace App\Console\Commands\Fitbit;

use Illuminate\Console\Command;
use App\Services\Remote\Fitbit\Activities\ActivitiesResource;
use App\Models\HealthLog\Step\StepLog;
use App\Models\HealthLog\Glucose\GlucoseLog;
use App\Models\Access\User\User;

class UpdateActivityLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateActivityLogs:update-activities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update activities';
    protected $activitiesResource;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->activitiesResource = new ActivitiesResource;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::fitbitUsersWithSetAccess()->get();
        foreach($users as $user) {
            $dateFrom = $this->activitiesResource->getDateFromForApiCall($user->fitbitUser);
            $logs = $this->activitiesResource->getData($user, $dateFrom);
            $this->activitiesResource->updateResourceTable($user, $logs, $dateFrom);
        }
    }
}
