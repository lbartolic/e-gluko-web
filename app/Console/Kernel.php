<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\Fitbit\UpdateStepLogs;
use App\Console\Commands\Fitbit\UpdateDeviceInfo;
use App\Console\Commands\Fitbit\UpdateCalorieLogs;
use App\Console\Commands\Fitbit\UpdateActivityLogs;
use App\Console\Commands\Fitbit\UpdateSleepLogs;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        UpdateStepLogs::class,
        UpdateDeviceInfo::class,
        UpdateCalorieLogs::class,
        UpdateActivityLogs::class,
        UpdateSleepLogs::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('UpdateDeviceInfo:update-device-info')->everyFiveMinutes();
        $schedule->command('UpdateStepLogs:update-steps')->everyFiveMinutes();
        $schedule->command('UpdateCalorieLogs:update-calories')->everyFiveMinutes();
        $schedule->command('UpdateActivityLogs:update-activities')->everyFiveMinutes();
        $schedule->command('UpdateSleepLogs:update-sleeps')->everyFiveMinutes();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
