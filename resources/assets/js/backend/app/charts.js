/* global moment, eGlukoData, Highcharts */

/**

	TODO:
	- grafove slicnih vrsta generalizirati (minimizirati kod, napraviti funkciju koja pokriva vise grafova - reuse)
	- pojedine izracune u funkcijama koje se ponavljaju za grafove generalizirati, reducirati kod

 */

Highcharts.setOptions({
	chart: {
		style: {
			fontFamily: 'Source Sans Pro,Helvetica Neue,Helvetica,Arial,sans-serif'
		}
	},
	lang: {
		weekdays: ['Nedjelja', 'Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak', 'Subota'],
        decimalPoint: '.',
        thousandsSep: ''
	}
});


    
function getFormatterForPeriodChart(timeDiffMonths, ticksFormat, item) {
    if (timeDiffMonths > 1) {
        return '<b>' + moment(item.value).utc().format(ticksFormat) + " - " 
                    + moment(item.value).utc().add(6, "days").format(ticksFormat) + '</b>';
    }
    else {
        return '<b>' + moment(item.value).utc().format(ticksFormat) + '</b>'; 
    }
}

function getTopFormatterForPeriodChart(timeDiffMonths, ticksFormat, item) {
    if (timeDiffMonths > 1) {
        return moment(item.value).format("MMMM");
    }
    else {
        return Highcharts.dateFormat("%A", item.value) + ' ' + moment(item.value).format("DD.MM.");
    }
}

function getPeriodChartX(monthsDiff, weeksDiff, dateFrom, dateTo) {
    var chartProps = {
        step: 1,
        top_step: 1,
        tick_format: "DD.MM.",
        start_on_tick: null,
        tick_interval: null,
        minor_tick_interval: null,
        top_tick_interval: null,
        grid_line_width: null,
        minor_grid_line_width: null,
        top_grid_line_color: null,
        top_grid_line_width: null,
        x_min: null,
        x_max: null
    }
    if (monthsDiff > 1) {
        chartProps.start_on_tick = true;
        chartProps.step = 7;
        chartProps.top_step = 1;
        chartProps.tick_interval = 3600*1000*24;
        chartProps.minor_tick_interval = 3600*1000*24*7*4;
        chartProps.top_tick_interval = 3600*1000*24*7*4;
        chartProps.grid_line_width = 0;
        chartProps.minor_grid_line_width = 0;
        chartProps.top_grid_line_color = '#ddd';
        chartProps.top_grid_line_width = 1;
        chartProps.x_min = moment(dateFrom).add(12, "hours");
		chartProps.x_max = moment(dateFrom).add(moment(dateTo).diff(moment(dateFrom), 'weeks'), "weeks");
    }
    else {
        chartProps.top_grid_line_width = 0;
        chartProps.top_grid_line_color = null;
        chartProps.step = 1;
        chartProps.top_step = 2;
        chartProps.tick_interval = 3600*1000*8;
        chartProps.top_tick_interval = 3600*1000*12;
        chartProps.minor_tick_interval = 3600*1000*24;
        chartProps.grid_line_width = 1;
        chartProps.x_min = dateFrom;
		chartProps.x_max = dateTo;
        chartProps.minor_grid_line_width = 1;
        chartProps.start_on_tick = false;
        if (weeksDiff > 1) {
            chartProps.tick_interval = null;
            chartProps.top_tick_interval = 3600*1000*42;
            chartProps.tick_format = "DD.MM. HH:mm";
        }
        else {
            chartProps.tick_format = "HH:mm";
        }
    }
    
    var xSettings = [
        {
			gridLineWidth: chartProps.grid_line_width,
			gridLineDashStyle: 'ShortDot',
			gridLineColor: '#a4a4a4',
			minorGridLineWidth: chartProps.minor_grid_line_width,
  			minorTickInterval: chartProps.minor_tick_interval,
			minorGridLineColor: '#959595',
			title: {
				enabled: false
			},
            startOnTick: chartProps.start_on_tick,
            endOnTick: true,
			min: chartProps.x_min,
			max: chartProps.x_max,
			tickInterval: chartProps.tick_interval,
			type: 'datetime',
			labels: {
                step: chartProps.step,
                formatter: function() {
                    return getFormatterForPeriodChart(monthsDiff, chartProps.tick_format, this);
                },
				y: 13     
            },
            tickLength: 0,
            events: {
                setExtremes: syncExtremes
            }
		}, {
			linkedTo: 0,
			tickInterval: chartProps.top_tick_interval,
            gridLineWidth: chartProps.top_grid_line_width,
			gridLineColor: chartProps.top_grid_line_color,
			opposite: true,
			showLastLabel: true,
			min: dateFrom,
			type: 'datetime',
			labels: {
				step: chartProps.top_step,
				formatter: function() {
                    return getTopFormatterForPeriodChart(monthsDiff, chartProps.tick_format, this);
				},
				y: -8
			},
            tickLength: 5
		}
    ];
    
    return xSettings;
}

/**
 * Synchronize zooming through the setExtremes event handler.
 */
function syncExtremes(e) {
    var thisChart = this.chart;

    if (e.trigger !== 'syncExtremes') { // Prevent feedback loop
        Highcharts.each(Highcharts.charts, function (chart) {
            if (chart !== thisChart) {
                if (chart.xAxis[0].setExtremes) { // It is null while updating
                    chart.xAxis[0].setExtremes(e.min, e.max, undefined, false, { trigger: 'syncExtremes' });
                }
            }
        });
    }
}

function getChartMaxVal(minMax, val) {
	if (val < minMax) return Math.ceil(minMax);
	return Math.ceil(val);
}

function getGlucoseMaxVal(max) {
	var minMax = 18;
	getChartMaxVal(minMax, max);
}

function getInsulinMaxVal(max) {
	var minMax = 15;
	getChartMaxVal(minMax, max);
}

function drawGlucoseSummaryChart(data, elementId) {
    var a1cDev = (data.estimated_a1c.deviation_percentage)/100;
    var a1cData2 = Math.abs(a1cDev*50);
    var a1cData2Color = "rgba(0, 166, 90, 1)";
    var a1cData1Color = "rgba(0, 166, 90, 0.3)";
    var a1cData1 = 50 - a1cData2
    if (a1cDev > 0) {
        a1cData2Color = "rgba(166, 0, 0, 1)";
        a1cData1Color = "rgba(166, 0, 0, 0.3)";
        a1cData1 = 50;
    }
    var a1cData3 = 100 - (a1cData1 + a1cData2);
	Highcharts.chart(elementId, {
		chart: {
			backgroundColor: null,
			height: '50%',
			spacingTop: 0,
			spacingBottom: 0,
			marginBottom: 10
		},
		credits: false,
		title: {
			text: '',
			align: 'left',
			verticalAlign: 'middle'
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions: {
			pie: {
				startAngle: 0,
				endAngle: 360,
				showInLegend: true,
				point: {
					events: {
						legendItemClick: function () {
							return false;
						}
					}
				}
			}
		},
		legend: {
			itemStyle: {
				fontSize: '13px',
				fontWeight: '400'
			},
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle',
			backgroundColor: '#FFFFFF',
			floating: false,
			useHTML: true,
			labelFormatter: function() {
				return '<div style="margin-bottom: 6px;"><span class="text-uppercase">' + this.name + '</span><div style="display: block; font-size: 20px;"><strong>' + this.y + "%</strong></div></div>";
			},
			margin: 0,
			x: -35
		},
		series: [{
			type: 'pie',
			name: 'Razina glukoze',
			dataLabels: { enabled: false },
			size: '100%',
			innerSize: '60%',
			colors: [eGlukoData.bg_high, eGlukoData.bg_low, eGlukoData.bg_normal],
			data: [
			['Visoka', data.high_perc],
			['Niska', data.low_perc],
			['Normalna', data.normal_perc]
			]
		},{
			type: 'pie',
			colors: [a1cData1Color, a1cData2Color, '#fff'],
			name: 'Doza inzulina',
			showInLegend: false,
            borderWidth: 0,
            /*borderWidth: 1,
            borderColor: "#000",*/
            innerSize: '35%',
			dataLabels: {
                formatter: function() {
                    if (this.series.data.indexOf( this.point ) == 0) return 'A1C';
                },
				distance: -25,
				color: 'white'
			},
			name: 'Odstupanje od A1C',
            startAngle: -90,
            endAngle: 270,
			size: '55%',
			data: [
                ['Ciljna vrijednost', a1cData1],
                ['Odstupanje', a1cData2],
                ['Bolus', a1cData3],
			],
            tooltip: {
                headerFormat: '',
	        	pointFormatter: function() {
                    if (this.index == 0) {
                        return "A1C ciljna vrijednost<br><b>" + data.goal_avgs.max_a1c + "</b>"
                    }
                    if (this.index == 1) {
                        return "Odstupanje A1C<br><b>" + Math.round(this.y*10)/10 + "%</b>" + " (" + data.estimated_a1c.value + ")"
                    }
                    else return '';
	            }
			}
		}]
	});
}

function drawBGMealRelChart(data, elementId) {
	Highcharts.chart(elementId, {
		chart: {
			type: 'bar',
			spacingBottom: 0,
			spacingTop: 0,
			height: '10%',
			margin: 0
		},
		credits: false,
		title: { text: null },
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		xAxis: {
			categories: ['Razina glukoze'],
			lineWidth: 0,
			labels: false
		},
		yAxis: {
			min: 0,
			endOnTick: false,
			maxPadding: 0,
			gridLineWidth: 0,
			minorGridLineWidth: 0,
			labels: false,
			title: { text: null }
		},
		legend: false,
		plotOptions: {
			series: {
				stacking: 'normal',
				dataLabels: {
					enabled: true,
					format: '{point.percentage:.1f}%'
				}
			}
		},
		colors: ['#f56954', '#f39c12', '#00a65a'],
		series: [{
			name: 'Visoka',
			data: [data.high_perc]
		}, {
			name: 'Niska',
			data: [data.low_perc]
		}, {
			name: 'Normalna',
			data: [data.normal_perc]
		}]
	});
}

function drawSleepDayViewChart(dataObj, elementId) {
	$.each(dataObj.maxs, function(key, value) { value.y = (value.y/1000)/60; });
	$.each(dataObj.hourly_sums, function(key, value) { value.y = (value.y/1000)/60; });
	$.each(dataObj.day_avg, function(key, value) { value.y = (value.y/1000)/60; });
	var maxVal = _.max(dataObj.hourly_sums, function(item) { return item.y; });
	Highcharts.chart(elementId, {
		chart: {
			zoomType: 'x',
			height: '190px',
			zoomtype: 'y',
			spacing: [0, 5, 0, 0]
		},
		title: {
			text: null
		},	
		legend: {
			align: 'right',
			verticalAlign: 'bottom',
			margin: 0
		},
		credits: false,
		xAxis: [{
			gridLineWidth: 1,
			gridLineDashStyle: 'ShortDot',
			gridLineColor: '#a4a4a4',
			minorGridLineWidth: 1,
  			minorTickInterval: 3600*1000*24,
			minorGridLineColor: '#959595',
			title: {
				enabled: false
			},
			endOnTick: true,
			min: moment().startOf('isoweek'),
			max: moment().endOf('isoweek'),
			tickInterval: 3600*1000*8,
			showLastLabel: true,
			type: 'datetime',
			labels: {
                formatter: function() {
					return moment(this.value).utc().format("HH");
                },
				y: 13     
            },
            tickLength: 0
		}, {
			linkedTo: 0,
			tickInterval: 3600*1000*12,
			opposite: true,
			showLastLabel: false,
			type: 'datetime',
			align: 'right',
			labels: {
				step: 2,
				formatter: function() {
					return '<b>' + Highcharts.dateFormat("%A", this.value) + '</b>';
				},
				y: -8
			},
            tickLength: 5,
            events: {
                setExtremes: syncExtremes
            }
		}],
		yAxis: [{
			allowDecimals: false,
			gridLineColor: '#ccc',
			title: {
				text: "San",
				align: "low",
				margin: 3,
				style: {
					"color": "#888",
					"text-transform": "uppercase",
					"font-size": "14px",
					"font-weight": 700
				}
			},
			min: 0,
			max: maxVal.y,
            labels: {
				//align: 'left',
				x: -5,
				//y: -3
			},
            tickLength: 0,
            tickAmount: 5
		}, {
			// REUSE 2ND AXIS
			min: 0,
			allowDecimals: false,
			gridLineWidth: 1,
			gridLineDashStyle: 'longdash',
			title: {
				text: null
			},
			labels: {
				x: -5,
				align: 'right',
				formatter: function() {
		            if (this.isFirst) { return ''; }
		            return this.value;
		        }
			},
			opposite: true
		}],
		tooltip: {
	        crosshairs: true,
	        //shared: true
	    },
	    plotOptions: {
	    	spline: {
	    		dataLabels: {
	    			formatter: function() {
	    				return Math.round(this.y);
	    			}
	    		}
	    	}
	    },
		series: [{
			type: 'column',
	        name: 'San',
	        xAxis: 0,
	        pointWidth: 8,
	        data: dataObj.hourly_sums,
	        zIndex: 2,
	        color: 'rgba(52, 152, 219, 0.8)',
	        tooltip: {
	        	headerFormat: '<b>{series.name}</b><br>',
	        	pointFormatter: function() {
	                return this.time_hour + 'h <b>' + moment().startOf('day').seconds(this.y*60).format('HH:mm:ss') + '</b>' + ' (' + this.count + 'x)';
	            }
			}
	    }, {
            name: 'MAX',
			type: 'line',
            showInLegend: false, 
            data: dataObj.maxs,
            zIndex: 1,
            lineWidth: 1,
            linkedTo: ':previous',
	        color: 'rgb(52, 152, 219)',
            marker: {
            	enabled: true,
				fillColor: 'white',
        		lineWidth: 1,
	        	lineColor: 'rgb(52, 152, 219)',
        		symbol: 'circle',
        		radius: 2
            },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
	        	pointFormatter: function() {
	                return this.time_hour + 'h <b>' + moment().startOf('day').seconds(this.y*60).format('HH:mm:ss') + '</b>';
	            }
			}
        }, {
			type: 'spline',
	        name: 'Ukupni dnevni prosjek',
	        groupPadding: 0.005,
	        xAxis: 1,
	        yAxis: 1,
	        data: dataObj.day_avg,
	        zIndex: 2,
	        color: new Highcharts.Color("rgb(142, 68, 173)").setOpacity(0.4).get(),
	        lineWidth: 1,
	        marker: {
	        	enabled: true,
	            fillColor: "rgb(155, 89, 182)",
	        	radius: 2,
	            lineWidth: 0,
	            symbol: 'circle'
	        },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
	        	pointFormatter: function() {
	                return '<b>' + moment().startOf('day').seconds(this.y*60).format('HH:mm:ss') + '</b>';
	            }
			}
	    }, /*{
            name: 'MAX',
			type: 'line',
            data: dataObj.maxs,
            zIndex: 1,
            dashStyle: 'longdash',
            lineWidth: 1,
	        color: 'rgb(230, 126, 34)',
            marker: {
            	enabled: true,
				fillColor: 'white',
        		lineWidth: 1,
	        	lineColor: 'rgb(230, 126, 34)',
        		symbol: 'circle',
        		radius: 2
            },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '{point.date}, <b>{point.y}</b>koraci'
			}
        }*/]
	});
}

function drawPhysicalActivityDayViewChart(dataObj, elementId) {
	$.each(dataObj.maxs, function(key, value) {
		value.y = (value.y/1000)/60;
	});
	$.each(dataObj.hourly_sums, function(key, value) {
		value.y = (value.y/1000)/60;
	});
	$.each(dataObj.day_avg, function(key, value) {
		value.y = (value.y/1000)/60;
	});
	var maxVal = _.max(dataObj.hourly_sums, function(item) { return item.y; });
	Highcharts.chart(elementId, {
		chart: {
			zoomType: 'x',
			height: '190px',
			zoomtype: 'y',
			spacing: [0, 5, 0, 0]
		},
		title: {
			text: null
		},	
		legend: {
			align: 'right',
			verticalAlign: 'bottom',
			margin: 0
		},
		credits: false,
		xAxis: [{
			gridLineWidth: 1,
			gridLineDashStyle: 'ShortDot',
			gridLineColor: '#a4a4a4',
			minorGridLineWidth: 1,
  			minorTickInterval: 3600*1000*24,
			minorGridLineColor: '#959595',
			title: {
				enabled: false
			},
			endOnTick: true,
			min: moment().startOf('isoweek'),
			max: moment().endOf('isoweek'),
			tickInterval: 3600*1000*8,
			showLastLabel: true,
			type: 'datetime',
			labels: {
                formatter: function() {
					return moment(this.value).utc().format("HH");
                },
                y: 13
            },
            tickLength: 0
		}, {
			linkedTo: 0,
			tickInterval: 3600*1000*12,
			opposite: true,
			showLastLabel: false,
			type: 'datetime',
			align: 'right',
			labels: {
				step: 2,
				formatter: function() {
					return '<b>' + Highcharts.dateFormat("%A", this.value) + '</b>';
				},
				y: -8
			},
            tickLength: 5,
            events: {
                setExtremes: syncExtremes
            }
		}],
		yAxis: [{
			allowDecimals: false,
			gridLineColor: '#ccc',
			title: {
				text: "Fizičke aktivnosti",
				align: "low",
				margin: 3,
				style: {
					"color": "#888",
					"text-transform": "uppercase",
					"font-size": "14px",
					"font-weight": 700
				}
			},
			min: 0,
			max: maxVal.y,
            labels: {
				//align: 'left',
				x: -5,
				//y: -3
			},
            tickLength: 0,
            tickAmount: 5
		}, {
			// REUSE 2ND AXIS
			min: 0,
			allowDecimals: false,
			gridLineWidth: 1,
			gridLineDashStyle: 'longdash',
			title: {
				text: null
			},
			labels: {
				x: -5,
				align: 'right',
				formatter: function() {
		            if (this.isFirst) { return ''; }
		            return this.value;
		        }
			},
			opposite: true
		}],
		tooltip: {
	        crosshairs: true,
	        //shared: true
	    },
	    plotOptions: {
	    	spline: {
	    		dataLabels: {
	    			formatter: function() {
	    				return Math.round(this.y);
	    			}
	    		}
	    	}
	    },
		series: [{
			type: 'column',
	        name: 'Aktivnost',
	        xAxis: 0,
	        pointWidth: 8,
	        data: dataObj.hourly_sums,
	        zIndex: 0,
	        color: 'rgba(46, 204, 113, 0.8)',
	        tooltip: {
	        	headerFormat: '<b>{series.name}</b><br>',
	        	pointFormatter: function() {
	                return this.time_hour + 'h <b>' + moment().startOf('day').seconds(this.y*60).format('HH:mm:ss') + '</b>' + ' (' + this.count + 'x)';
	            }
			}
	    }, {
            name: 'MAX',
			type: 'line',
            showInLegend: false, 
            data: dataObj.maxs,
            zIndex: 1,
            lineWidth: 1,
            linkedTo: ':previous',
	        color: 'rgb(46, 204, 113)',
            marker: {
            	enabled: true,
				fillColor: 'white',
        		lineWidth: 1,
	        	lineColor: 'rgb(46, 204, 113)',
        		symbol: 'circle',
        		radius: 2
            },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
	        	pointFormatter: function() {
	                return this.time_hour + 'h <b>' + moment().startOf('day').seconds(this.y*60).format('HH:mm:ss') + '</b>';
	            }
			}
        }, {
			type: 'spline',
	        name: 'Ukupni dnevni prosjek',
	        groupPadding: 0.005,
	        xAxis: 1,
	        yAxis: 1,
	        data: dataObj.day_avg,
	        zIndex: 2,
	        color: new Highcharts.Color("rgb(142, 68, 173)").setOpacity(0.4).get(),
	        lineWidth: 1,
	        marker: {
	        	enabled: true,
	            fillColor: "rgb(155, 89, 182)",
	        	radius: 2,
	            lineWidth: 0,
	            symbol: 'circle'
	        },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
	        	pointFormatter: function() {
	                return '<b>' + moment().startOf('day').seconds(this.y*60).format('HH:mm:ss') + '</b>';
	            }
			}
	    }, /*{
            name: 'MAX',
			type: 'line',
            data: dataObj.maxs,
            zIndex: 1,
            dashStyle: 'longdash',
            lineWidth: 1,
	        color: 'rgb(230, 126, 34)',
            marker: {
            	enabled: true,
				fillColor: 'white',
        		lineWidth: 1,
	        	lineColor: 'rgb(230, 126, 34)',
        		symbol: 'circle',
        		radius: 2
            },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '{point.date}, <b>{point.y}</b>koraci'
			}
        }*/]
	});
}

function drawSleepPeriodChart(dataSleeps, dataAsleep, dataAwake, dataAwakeCount, dataRestless, dataRestlessCount, maxValue, dateFrom, dateTo, elementId) {
    var timeDiff = dateTo.clone().diff(dateFrom.clone(), 'weeks');
    var timeDiffMonths = moment(dateTo).diff(moment(dateFrom), 'months');
    
    var chartSeries = [];
    
    if (timeDiffMonths > 1) {
        var weeklyData = getChartDataGroupedByWeek(dataSleeps, dateFrom, dateTo, "sleep");
        var weeklyFinal = weeklyData.weekly_data;
        var weeklyAwake = weeklyData.extra.awake;
        var weeklyAsleep = weeklyData.extra.asleep;
        var weeklyRestless = weeklyData.extra.restless;
        var weeklyAwakeCount = weeklyData.extra.awake_count;
        maxValue = _.max(weeklyFinal, function(item) { return item.y; });
        if (maxValue) maxValue = maxValue.y;
        
        chartSeries = [{
            type: 'column',
	        name: 'Spavanje',
	        xAxis: 0,
	        pointWidth: 30,
	        data: weeklyAsleep,
	        zIndex: 0,
	        color: 'rgba(41, 128, 185, 0.65)',
	        tooltip: {
	        	pointFormatter: function() {
	                return '<b>' + moment(this.x).format("DD.MM.") + " - " + moment(this.x).add(6, "days").format("DD.MM.") + "</b><br>"
                        + this.series.name + ': <b>' + moment().startOf('day').seconds(this.y*3600).format('HH:mm:ss') + '</b><br>';
	            }
			}
        }, {
			name: 'Budan',
			data: weeklyAwake,
			type: 'column',
			xAxis: 0,
	        pointWidth: 30,
	        zIndex: 0,
	        color: 'rgba(231, 76, 60, 0.65)',
	        tooltip: {
	        	pointFormatter: function() {
	                return this.series.name + ': <b>' + moment().startOf('day').seconds(this.y*3600).format('HH:mm:ss') + '</b><br>';
	            }
			}
		}, {
			name: 'Nemiran',
			data: weeklyRestless,
			type: 'column',
			xAxis: 0,
	        pointWidth: 30,
	        zIndex: 0,
	        color: 'rgba(243, 156, 18, 0.65)',
	        tooltip: {
	        	pointFormatter: function() {
	                return this.series.name + ': <b>' + moment().startOf('day').seconds(this.y*3600).format('HH:mm:ss') + '</b><br>';
	            }
			}
		}, {
			name: 'Broj buđenja',
			data: weeklyAwakeCount,
			type: 'spline',
			zIndex: 5,
	        yAxis: 1,
			color: 'rgb(231, 76, 60)',
			lineWidth: 2,
			marker: {
	        	enabled: true,
	            fillColor: 'rgb(231, 76, 60)',
            	radius: 4,
	            lineWidth: 0,
	            symbol: 'circle'
	        },
	        tooltip: {
	        	pointFormatter: function() {
	                return this.series.name + ': <b>' + Math.round(this.y) + '</b><br>';
	            }
			}
		}]
    }
    else {
        chartSeries = [{
			type: 'column',
	        name: 'Spavanje',
	        xAxis: 1,
	        pointWidth: 20,
	        data: dataAsleep,
	        zIndex: 2,
	        color: 'rgba(41, 128, 185, 0.65)',
	        tooltip: {
	        	headerFormat: '<b>{point.point.orig_date_time}</b><br>'
			}
	    }, {
			type: 'column',
	        name: 'Budan',
	        xAxis: 1,
	        pointWidth: 20,
	        data: dataAwake,
	        zIndex: 2,
	        color: 'rgba(231, 76, 60, 0.65)'
	    }, {
			type: 'column',
	        name: 'Nemiran',
	        xAxis: 1,
	        pointWidth: 20,
	        data: dataRestless,
	        zIndex: 2,
	        color: 'rgba(243, 156, 18, 0.65)'
	    }, {
			type: 'spline',
	        name: 'Broj buđenja',
	        xAxis: 0,
	        yAxis: 1,
	        data: dataAwakeCount,
	        zIndex: 2,
	        color: 'rgb(231, 76, 60)',
	        lineWidth: 1,
	        marker: {
	        	enabled: true,
	            fillColor: "rgb(231, 76, 60)",
	        	radius: 3,
	            lineWidth: 0,
	            symbol: 'circle'
	        }
	    }, {
			type: 'spline',
	        name: 'Broj nemirnosti',
	        xAxis: 0,
	        yAxis: 1,
	        data: dataRestlessCount,
	        zIndex: 2,
	        color: 'rgba(243, 156, 18, 1)',
	        lineWidth: 1,
	        marker: {
	        	enabled: true,
	            fillColor: "rgba(243, 156, 18, 1)",
	        	radius: 3,
	            lineWidth: 0,
	            symbol: 'circle'
	        }
	    }];
    }
    
	Highcharts.chart(elementId, {
		chart: {
			zoomType: 'x',
			height: '290px',
			zoomtype: 'y',
			spacing: [0, 5, 0, 0]
		},
		title: {
			text: null
		},
		legend: {
			align: 'right',
			verticalAlign: 'bottom',
			margin: 0
		},
		credits: false,
		xAxis: getPeriodChartX(timeDiffMonths, timeDiff, dateFrom, dateTo),
		yAxis: [{
			allowDecimals: false,
			gridLineColor: '#ccc',
			title: {
				text: "San",
				align: "low",
				margin: 3,
				style: {
					"color": "#888",
					"text-transform": "uppercase",
					"font-size": "14px",
					"font-weight": 700
				}
			},
			stackLabels: {
				enabled: true,
				formatter: function() {
					return moment().startOf('day').seconds(this.total*60*60).format('HH:mm:ss');
				},
				style: {
					fontWeight: 'bold',
					color: 'white'
				},
				rotation: -90,
				align: 'center',
				y: 30,
				x: 2
			},
			min: 0,
			max: maxValue,
            labels: {
				//align: 'left',
				x: -5,
				//y: -3
			},
            tickLength: 0,
            //tickInterval: 4
            plotLines: [{
				zIndex: 1
			}]
		}, {
			plotLines: [{
				zIndex: 0
			}],
			allowDecimals: false,
			gridLineWidth: 1,
			gridLineDashStyle: 'longdash',
			min: 0,
			//max: 100,
			alignTicks: false,
			title: {
				//text: "Kalorije"
				text: null
			},
			labels: {
				x: -5,
				align: 'right',
				formatter: function() {
		            if (this.isFirst) { return ''; }
		            return this.value;
		        }
			},
			opposite: true
		}],
		tooltip: {
	        crosshairs: true,
	        shared: true,
            headerFormat: ''
		},
		plotOptions: {
			column: {
                stacking: "normal",
				tooltip: {
					split: true,
					pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y_format}</b><br/>'
				}
			}
		},
		series: chartSeries
	});
}

function drawPhysicalActivityPeriodChart(dataActivities, dataHeartRates, maxValue, dateFrom, dateTo, elementId) {
    var timeDiff = dateTo.clone().diff(dateFrom.clone(), 'weeks');
    var timeDiffMonths = moment(dateTo).diff(moment(dateFrom), 'months');
    
    var chartSeries = [];
    
    if (timeDiffMonths > 1) {
        var weeklyData = getChartDataGroupedByWeek(dataActivities, dateFrom, dateTo, "physical_activity");
        var weeklyFinal = weeklyData.weekly_data;
        var weeklyHeartRate = weeklyData.extra.heart_rate;
        maxValue = _.max(weeklyFinal, function(item) { return item.y; });
        if (maxValue) maxValue = maxValue.y;
        
        chartSeries = [{
            type: 'column',
	        name: 'Aktivnost',
	        xAxis: 0,
	        pointWidth: 30,
	        data: weeklyFinal,
	        zIndex: 0,
	        color: 'rgba(46, 204, 113, 0.6)',
	        tooltip: {
	        	pointFormatter: function() {
	                return '<b>' + moment(this.x).format("DD.MM.") + " - " + moment(this.x).add(6, "days").format("DD.MM.") + "</b><br>"
                        + this.series.name + ': <b>' + moment().startOf('day').seconds(this.y*60).format('HH:mm:ss') + '</b><br>';
	            }
			}
        }, {
			name: 'Otkucaji srca',
			data: weeklyHeartRate,
			type: 'spline',
			zIndex: 5,
	        yAxis: 1,
			color: "rgb(231, 76, 60)",
			lineWidth: 2,
			marker: {
	        	enabled: true,
	            fillColor: "rgb(231, 76, 60)",
            	radius: 4,
	            lineWidth: 0,
	            symbol: 'circle'
	        },
            tooltip: {
	        	pointFormatter: function() {
	                return this.series.name + ': <b>' + Math.round(this.y) + '</b>bpm<br>';
	            }
			}
		}];
    }
    else {
        chartSeries = [{
			type: 'column',
	        name: 'Aktivnost',
	        xAxis: 1,
	        pointWidth: 8,
	        data: dataActivities,
	        zIndex: 2,
	        color: 'rgba(46, 204, 113, 0.8)',
	        tooltip: {
	        	pointFormatter: function() {
	                return '<b>' + this.orig_date_time + "</b><br>"
                        + this.series.name + ': <b>' + moment().startOf('day').seconds(this.value_seconds).format('HH:mm:ss') + '</b><br>';
	            }
			}
	    }, {
			type: 'spline',
	        name: 'Otkucaji srca',
	        xAxis: 0,
	        yAxis: 1,
	        data: dataHeartRates,
	        zIndex: 2,
	        color: 'rgb(231, 76, 60)',
	        lineWidth: 1,
	        marker: {
	        	enabled: true,
	            fillColor: "rgb(231, 76, 60)",
	        	radius: 3,
	            lineWidth: 0,
	            symbol: 'circle'
	        },
	        tooltip: {
				pointFormatter: function() {
	                return this.series.name + ': <b>' + this.y + '</b>bpm<br>';
	            }
			}
	    }];
    }
    
	Highcharts.chart(elementId, {
		chart: {
			zoomType: 'x',
			height: '190px',
			zoomtype: 'y',
			spacing: [0, 5, 0, 0]
		},
		title: {
			text: null
		},
		legend: {
			align: 'right',
			verticalAlign: 'bottom',
			margin: 0
		},
		credits: false,
		xAxis: getPeriodChartX(timeDiffMonths, timeDiff, dateFrom, dateTo),
		yAxis: [{
			allowDecimals: false,
			gridLineColor: '#ccc',
			title: {
				text: "Fizičke aktivnosti",
				align: "low",
				margin: 3,
				style: {
					"color": "#888",
					"text-transform": "uppercase",
					"font-size": "14px",
					"font-weight": 700
				}
			},
			min: 0,
			max: maxValue,
            labels: {
				//align: 'left',
				x: -5,
				//y: -3
			},
            tickLength: 0,
            //tickInterval: 4
            tickAmount: 5
		}, {
			allowDecimals: false,
			gridLineWidth: 1,
			gridLineDashStyle: 'longdash',
            min: 0,
			title: {
				//text: "Kalorije"
				text: null
			},
			labels: {
				x: -5,
				align: 'right',
				formatter: function() {
		            if (this.isFirst) { return ''; }
		            return this.value;
		        }
			},
			opposite: true
		}],
		tooltip: {
            headerFormat: '',
            shared: true,
	        crosshairs: true
		},
		series: chartSeries
	});
}

function drawStepCaloriePeriodChart(dataSteps, dataCalories, maxValue, dateFrom, dateTo, elementId) {
	var timeDiff = dateTo.clone().diff(dateFrom.clone(), 'weeks');
    var timeDiffMonths = moment(dateTo).diff(moment(dateFrom), 'months');
    
    var chartSeries = [];
    
    if (timeDiffMonths > 1) {
        var weeklyDataSteps = getChartDataGroupedByWeek(dataSteps, dateFrom, dateTo, "step").weekly_data;
        var weeklyDataCalories = getChartDataGroupedByWeek(dataCalories, dateFrom, dateTo, "calorie").weekly_data;
        
        chartSeries = [{
			type: 'column',
	        name: 'Kretanje',
	        pointWidth: 15,
	        xAxis: 0,
	        data: weeklyDataSteps,
	        zIndex: 2,
	        color: 'rgb(230, 126, 34)',
	        tooltip: {
	        	pointFormatter: function() {
	                return '<b>' + moment(this.x).format("DD.MM.") + " - " + moment(this.x).add(6, "days").format("DD.MM.") + "</b><br>"
                        + this.series.name + ': <b>' + Math.round(this.y) + '</b><br>';
	            }
			}
	    }, {
			type: 'column',
	        name: 'Kalorije',
	        pointWidth: 15,
	        xAxis: 0,
	        yAxis: 1,
	        data: weeklyDataCalories,
	        zIndex: 2,
	        color: 'rgb(26, 188, 156)',
	        tooltip: {
	        	pointFormatter: function() {
	                return '<b>' + moment(this.x).format("DD.MM.") + " - " + moment(this.x).add(6, "days").format("DD.MM.") + "</b><br>"
                        + this.series.name + ': <b>' + Math.round(this.y) + '</b><br>';
	            }
			}
	    }];
    }
    else {
        chartSeries = [{
			type: 'areaspline',
	        name: 'Kretanje',
	        xAxis: 0,
	        data: dataSteps,
	        zIndex: 2,
	        color: 'rgb(230, 126, 34)',
	        lineWidth: 1,
	        fillOpacity: 0.2,
	        marker: {
	        	enabled: true,
	            fillColor: "rgb(230, 126, 34)",
	        	radius: 3,
	            lineWidth: 0,
	            symbol: 'circle'
	        }
	    }, {
			type: 'areaspline',
	        name: 'Kalorije',
	        xAxis: 0,
	        data: dataCalories,
	        zIndex: 2,
	        color: 'rgb(26, 188, 156)',
	        lineWidth: 1,
	        fillOpacity: 0.2,
	        marker: {
	        	enabled: true,
	            fillColor: "rgb(26, 188, 156)",
	        	radius: 3,
	            lineWidth: 0,
	            symbol: 'circle'
	        }
	    }];
    }
    
	Highcharts.chart(elementId, {
		chart: {
			zoomType: 'x',
			height: '190px',
			zoomtype: 'y',
			spacing: [0, 5, 0, 0]
		},
		title: {
			text: null
		},
		legend: {
			align: 'right',
			verticalAlign: 'bottom',
			margin: 0
		},
		credits: false,
		xAxis: getPeriodChartX(timeDiffMonths, timeDiff, dateFrom, dateTo),
		yAxis: [{
			allowDecimals: false,
			gridLineColor: '#ccc',
			title: {
				text: "Kretanje i kalorije",
				align: "low",
				margin: 3,
				style: {
					"color": "#888",
					"text-transform": "uppercase",
					"font-size": "14px",
					"font-weight": 700
				}
			},
			min: 0,
			max: maxValue,
            labels: {
				//align: 'left',
				x: -5,
				//y: -3
			},
            tickLength: 0,
            //tickInterval: 4
            tickAmount: 5
		}, {
			allowDecimals: false,
			gridLineWidth: 1,
			gridLineDashStyle: 'longdash',
			title: {
				//text: "Kalorije"
				text: null
			},
			labels: {
				x: -5,
				align: 'right',
				formatter: function() {
		            if (this.isFirst) { return ''; }
		            return this.value;
		        }
			},
			opposite: true
		}],
		tooltip: {
	        crosshairs: true,
            headerFormat: ''
		},
		plotOptions: {
			line: {
				tooltip: {
					headerFormat: '<b>{series.name}</b><br>',
					pointFormat: '{point.orig_date_time}, {point.y}U <br><b>{point.label}</b>'
				},
			}
		},
		series: chartSeries
	});
}

function drawStepCalorieDayViewChart(dataObj, elementId) {
	var maxValStep = _.max(dataObj.step_maxs, function(item) { return item.y; });
	var maxValCalorie = _.max(dataObj.calorie_maxs, function(item) { return item.y; });
	var maxVal = maxValStep;
	if (maxVal.y < maxValCalorie.y) maxVal = maxValCalorie;
	Highcharts.chart(elementId, {
		chart: {
			zoomType: 'x',
			height: '190px',
			zoomtype: 'y',
			spacing: [0, 5, 0, 0]
		},
		title: {
			text: null
		},	
		legend: {
			align: 'right',
			verticalAlign: 'bottom',
			margin: 0
		},
		credits: false,
		xAxis: [{
			gridLineWidth: 1,
			gridLineDashStyle: 'ShortDot',
			gridLineColor: '#a4a4a4',
			minorGridLineWidth: 1,
  			minorTickInterval: 3600*1000*24,
			minorGridLineColor: '#959595',
			title: {
				enabled: false
			},
			endOnTick: true,
			min: moment().startOf('isoweek'),
			max: moment().endOf('isoweek'),
			tickInterval: 3600*1000*8,
			showLastLabel: true,
			type: 'datetime',
			labels: {
                formatter: function() {
					return '';
                }  
            },
            tickLength: 0
		}, {
			linkedTo: 0,
			tickInterval: 3600*1000*12,
			opposite: true,
			showLastLabel: false,
			type: 'datetime',
			align: 'right',
			labels: {
				step: 2,
				formatter: function() {
					return '<b>' + Highcharts.dateFormat("%A", this.value) + '</b>';
				},
				y: -8
			},
            tickLength: 5,
            events: {
                setExtremes: syncExtremes
            }
		}],
		yAxis: [{
			allowDecimals: false,
			gridLineColor: '#ccc',
			title: {
				text: "Kretanje i kalorije",
				align: "low",
				margin: 3,
				style: {
					"color": "#888",
					"text-transform": "uppercase",
					"font-size": "14px",
					"font-weight": 700
				}
			},
			min: 0,
			//max: maxVal.y,
            labels: {
				//align: 'left',
				x: -5,
				//y: -3
			},
            tickLength: 0,
            tickAmount: 5
		}, {
			// REUSE 2ND AXIS
			min: 0,
			allowDecimals: false,
			gridLineWidth: 1,
			gridLineDashStyle: 'longdash',
			title: {
				text: null
			},
			labels: {
				x: -5,
				align: 'right',
				formatter: function() {
		            if (this.isFirst) { return ''; }
		            return this.value;
		        }
			},
			opposite: true
		}],
		tooltip: {
	        crosshairs: true,
	        //shared: true
	    },
	    plotOptions: {
	    	spline: {
	    		dataLabels: {
	    			formatter: function() {
	    				return Math.round(this.y);
	    			}
	    		}
	    	}
	    },
		series: [{
			type: 'line',
	        name: 'Ukupni dnevni prosjek (kretanje)',
	        data: dataObj.day_avg_step,
	        zIndex: 2,
	        yAxis: 1,
	        color: 'rgb(230, 126, 34)',
	        marker: {
	        	enabled: true,
	            fillColor: 'white',
            	radius: 3,
	            lineWidth: 2,
	            lineColor: 'rgb(230, 126, 34)',
	            symbol: 'circle'
	        },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '<b>{point.y}</b>koraci'
			}
	    }, {
            name: 'MAX (kretanje)',
			type: 'line',
            data: dataObj.step_maxs,
            zIndex: 1,
            dashStyle: 'longdash',
	        yAxis: 1,
            lineWidth: 1,
	        color: 'rgb(230, 126, 34)',
            marker: {
            	enabled: true,
				fillColor: 'white',
        		lineWidth: 1,
	        	lineColor: 'rgb(230, 126, 34)',
        		symbol: 'circle',
        		radius: 2
            },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '{point.date}, <b>{point.y}</b>koraci'
			}
        }, {
			type: 'line',
	        name: 'Ukupni dnevni prosjek (kalorije)',
	        data: dataObj.day_avg_calorie,
	        yAxis: 0,
	        zIndex: 2,
	        color: 'rgb(26, 188, 156)',
	        marker: {
	        	enabled: true,
	            fillColor: 'white',
            	radius: 3,
	            lineWidth: 2,
	            lineColor: 'rgb(26, 188, 156)',
	            symbol: 'circle'
	        },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '<b>{point.y}</b>kcal'
			}
	    }, {
            name: 'MAX (kalorije)',
			type: 'line',
	        yAxis: 1,
            data: dataObj.calorie_maxs,
	        yAxis: 0,
            zIndex: 1,
            dashStyle: 'longdash',
            lineWidth: 1,
	        color: 'rgb(26, 188, 156)',
            marker: {
            	enabled: true,
				fillColor: 'white',
        		lineWidth: 1,
	        	lineColor: 'rgb(26, 188, 156)',
        		symbol: 'circle',
        		radius: 2
            },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '{point.date}, <b>{point.y}</b>kcal'
			}
        }]
	});
}

function drawInsulinPeriodChart(dataBolus, dataBasal, maxValue, intraDayTotal, dateFrom, dateTo, elementId, dataTotal) {
	var timeDiff = dateTo.clone().diff(dateFrom.clone(), 'weeks');
    var timeDiffMonths = moment(dateTo).diff(moment(dateFrom), 'months');
    
    var chartSeries = [];
    var tooltipShared = false;
    
    if (timeDiffMonths > 1) {  
        tooltipShared = true;
        var weeklyData = getChartDataGroupedByWeek(dataTotal, dateFrom, dateTo, "insulin");
        var weeklyFinal = weeklyData.weekly_data;
        var weeklyBolus = weeklyData.extra.bolus;
        var weeklyBolusBefore = weeklyData.extra.bolus_before;
        var weeklyBasal = weeklyData.extra.basal;
        
        chartSeries = [{
            type: 'column',
	        name: 'Uk. jed. kroz dan',
	        xAxis: 0,
	        pointWidth: 30,
	        data: weeklyFinal,
	        zIndex: 0,
	        color: 'rgba(142, 68, 173, 0.4)',
	        tooltip: {
	        	pointFormatter: function() {
	                return '<b>' + moment(this.x).format("DD.MM.") + " - " + moment(this.x).add(6, "days").format("DD.MM.") + "</b><br>"
                        + this.series.name + ': <b>' + Math.round(this.y) + '</b><br>';
	            }
			}
        }, {
			name: 'Bolus prije obroka',
			data: weeklyBolusBefore,
			dashStyle: 'ShortDot',
			type: 'spline',
			zIndex: 5,
			color: eGlukoData.bg_bolus,
			lineWidth: 2,
			marker: {
	        	enabled: true,
	            fillColor: eGlukoData.bg_bolus,
            	radius: 4,
	            lineWidth: 0,
	            lineColor: eGlukoData.bg_bolus,
	            symbol: 'circle'
	        },
	        tooltip: {
	        	pointFormatter: function() {
	                return this.series.name + ': <b>' + Math.round(this.y) + '</b><br>';
	            }
			}
		}, {
			name: 'Bolus',
			data: weeklyBolus,
			type: 'spline',
			zIndex: 0,
			color: eGlukoData.bg_bolus,
			lineWidth: 2,
			marker: {
	        	enabled: true,
	            fillColor: 'white',
            	radius: 4,
	            lineWidth: 2,
	            lineColor: eGlukoData.bg_bolus,
	            symbol: 'circle'
	        },
	        tooltip: {
	        	pointFormatter: function() {
	                return this.series.name + ': <b>' + Math.round(this.y) + '</b><br>';
	            }
			}
		}, {
			name: 'Bazalni',
			data: weeklyBasal,
			type: 'spline',
			zIndex: 0,
			color: eGlukoData.bg_basal,
			lineWidth: 2,
			marker: {
				enabled: true,
	            fillColor: 'white',
            	radius: 4,
	            lineWidth: 2,
	            lineColor: eGlukoData.bg_basal,
	            symbol: 'circle'
	        },
	        tooltip: {
	        	pointFormatter: function() {
	                return this.series.name + ': <b>' + Math.round(this.y) + '</b><br>';
	            }
			}
		}];
    }
    else { 
        var logsBeforeMeal = []; //bolus
        var logsAfterMeal = []; //bolus
        $.each(dataBolus, function(key, value) {
            if (value.label_rel == 1) logsBeforeMeal.push(value);
            if (value.label_rel == 2) logsAfterMeal.push(value);
        });
        chartSeries = [{
			type: 'line',
	        name: 'Bolus',
	        yAxis: 0,
	        data: dataBolus,
	        zIndex: 4,
	        color: eGlukoData.bg_bolus,
	        marker: {
	        	enabled: true,
	            fillColor: 'white',
            	radius: 3,
	            lineWidth: 2,
	            lineColor: eGlukoData.bg_bolus,
	            symbol: 'circle'
	        }
	    }, {
			name: 'Bolus prije obroka',
	        yAxis: 0,
			data: logsBeforeMeal,
			dashStyle: 'ShortDot',
			type: 'line',
			zIndex: 5,
			color: eGlukoData.bg_bolus,
			lineWidth: 2,
			marker: {
	        	enabled: true,
	            fillColor: eGlukoData.bg_bolus,
            	radius: 3,
	            lineWidth: 0,
	            lineColor: eGlukoData.bg_bolus,
	            symbol: 'circle'
	        }
		}, {
			type: 'line',
	        name: 'Bazalni',
	        yAxis: 0,
	        data: dataBasal,
	        zIndex: 3,
	        color: eGlukoData.bg_basal,
	        marker: {
	        	enabled: true,
	            fillColor: 'white',
            	radius: 3,
	            lineWidth: 2,
	            lineColor: eGlukoData.bg_basal,
	            symbol: 'circle'
	        }
	    }, {
			type: 'areaspline',
			stack: 0,
	        name: 'Uk. jed. kroz dan',
	        yAxis: 1,
	        data: intraDayTotal,
	        zIndex: 2,
	        color: "rgb(142, 68, 173)",
	        fillOpacity: 0.15,
	        lineWidth: 0,
	        marker: {
	        	enabled: true,
	            fillColor: "rgb(155, 89, 182)",
	        	radius: 2,
	            lineWidth: 0,
	            symbol: 'circle'
	        },
	        tooltip: {
	        	pointFormatter: function() {
	                return '<b>' + this.series.name + "</b><br>"
                        + moment(this.x).format("DD.MM.") + ', <b>' + Math.round(this.y) + 'U</b><br>';
	            }
			}
	    }];
    }
    
	Highcharts.chart(elementId, {
		chart: {
			zoomType: 'x',
			height: '280px',
			zoomtype: 'y',
			spacing: [0, 5, 0, 0]
		},
		title: {
			text: null
		},
		legend: {
			align: 'right',
			verticalAlign: 'bottom',
			margin: 0
		},
		credits: false,
		xAxis: getPeriodChartX(timeDiffMonths, timeDiff, dateFrom, dateTo),
		yAxis: [{
			allowDecimals: false,
			gridLineColor: '#ccc',
			title: {
				text: "Inzulin doze",
				align: "low",
				margin: 3,
				style: {
					"color": "#888",
					"text-transform": "uppercase",
					"font-size": "14px",
					"font-weight": 700
				}
			},
			min: 0,
			max: getInsulinMaxVal(maxValue),
            labels: {
				//align: 'left',
				x: -5,
				//y: -3
			},
            tickLength: 0,
            //tickInterval: 4
            tickAmount: 5
		}, {
			allowDecimals: false,
			gridLineWidth: 1,
			gridLineDashStyle: 'longdash',
			title: {
				//text: "Količina kroz dan"
				text: null
			},
			labels: {
				x: -5,
				align: 'right',
				formatter: function() {
		            if (this.isFirst) { return ''; }
		            return this.value;
		        }
			},
			opposite: true
		}],
		tooltip: {
	        crosshairs: true,
            shared: tooltipShared,
            headerFormat: ''
		},
		plotOptions: {
			line: {
				tooltip: {
					headerFormat: '<b>{series.name}</b><br>',
					pointFormat: '{point.orig_date_time}, <b>{point.y}U</b> <br><b>{point.label}</b>'
				}
			},
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            var date = moment(this.x).add(7, "days").format(eGlukoData.db_date_format);
                            window.open(APP_URL + '/admin/patients/2/health-logs/trends/period/' + date + '/1w', '_blank');
                        }
                    }
                }
            }
		},
		series: chartSeries
	});
}

function drawGlucosePeriodChart(data, maxValue, dateFrom, dateTo, minGoalBeforeValue, maxGoalAfterValue, maxGoalBeforeValue, elementId) {
    var timeDiff = dateTo.clone().diff(dateFrom.clone(), 'weeks');
    var timeDiffMonths = moment(dateTo).diff(moment(dateFrom), "months");
    
    var chartSeries = [];
    var tooltipShared = false;
    
    if (timeDiffMonths > 1) {  
        tooltipShared = true;
        var weeklyData = getChartDataGroupedByWeek(data, dateFrom, dateTo, "glucose");
        var weeklyFinal = weeklyData.weekly_data;
        var weeklyFinalBefore = weeklyData.extra.before;
        var weeklyFinalAfter = weeklyData.extra.after;
        $.each(weeklyFinal, function(wKey, wValue) {
            wValue.color = getGlucoseGoalDiffColor(wValue.y, wValue.health_goal, null);
        });
        $.each(weeklyFinalBefore, function(wKey, wValue) {
            wValue.color = getGlucoseGoalDiffColor(wValue.y, wValue.health_goal, 1);
        });
        $.each(weeklyFinalAfter, function(wKey, wValue) {
            wValue.color = getGlucoseGoalDiffColor(wValue.y, wValue.health_goal, 2);
        });
        chartSeries = [{
            type: 'column',
            borderWidth: 0,
	        name: 'GUK',
	        xAxis: 0,
	        pointWidth: 30,
	        data: weeklyFinal,
	        zIndex: 0,
	        color: 'rgba(46, 204, 113, 0.4)',
	        tooltip: {
	        	pointFormatter: function() {
	                return '<b>' + moment(this.x).format("DD.MM.") + " - " + moment(this.x).add(6, "days").format("DD.MM.") + "</b><br>"
                        + this.series.name + ': <b>' + Math.round(this.y*10)/10 + '</b><br>';
	            }
			}
        }, {
			name: 'GUK prije obroka',
			data: weeklyFinalBefore,
			type: 'spline',
			zIndex: 0,
			color: 'rgb(155, 89, 182)',
			lineWidth: 2,
			marker: {
				enabled: true,
	        	radius: 4,
	            lineWidth: 1,
	            lineColor: '#444',
				symbol: 'circle'
	        },
	        tooltip: {
	        	pointFormatter: function() {
	                return this.series.name + ': <b>' + Math.round(this.y*10)/10 + '</b><br>';
	            }
			}
		}, {
			name: 'GUK poslije obroka',
			data: weeklyFinalAfter,
			type: 'spline',
			zIndex: 0,
			color: 'rgb(52, 152, 219)',
			lineWidth: 2,
			marker: {
				enabled: true,
	        	radius: 4,
	            lineWidth: 1,
	            lineColor: '#444',
				symbol: 'circle'
	        },
	        tooltip: {
	        	pointFormatter: function() {
	                return this.series.name + ': <b>' + Math.round(this.y*10)/10 + '</b><br>';
	            }
			}
		}];
    }
    else { 
        var logsBeforeMeal = [];
        var logsAfterMeal = [];
        $.each(data, function(key, value) {
            if (value.label_rel == 1) logsBeforeMeal.push(value);
            if (value.label_rel == 2) logsAfterMeal.push(value);
        });
        chartSeries = [{
			name: 'GUK',
			data: data,
			type: 'spline',
			zIndex: 1,
			color: '#444',
			lineWidth: 1,
			marker: {
				enabled: true,
	        	radius: 3,
	            lineWidth: 1,
	            lineColor: '#444',
				symbol: 'circle'
	        }
		}, {
			name: 'GUK prije obroka',
			data: logsBeforeMeal,
			dashStyle: 'ShortDot',
			type: 'line',
			zIndex: 0,
			color: 'rgb(155, 89, 182)',
			lineWidth: 2,
			marker: {
				enabled: true,
	        	radius: 3,
	            lineWidth: 1,
	            lineColor: '#444',
				symbol: 'circle'
	        },
	        tooltip: {
	        	pointFormatter: function() {
	                return '<b>' + this.series.name + "</b><br>"
                        + moment(this.x).format("DD.MM. HH:mm") + ', <b>' + this.y + 'U</b><br><b>' + this.label + '</b>';
	            }
			}
		}, {
			name: 'GUK poslije obroka',
			data: logsAfterMeal,
			dashStyle: 'ShortDot',
			type: 'line',
			zIndex: 0,
			color: 'rgb(52, 152, 219)',
			lineWidth: 2,
			marker: {
				enabled: true,
	        	radius: 3,
	            lineWidth: 1,
	            lineColor: '#444',
				symbol: 'circle'
	        },
	        tooltip: {
	        	pointFormatter: function() {
	                return '<b>' + this.series.name + "</b><br>"
                        + moment(this.x).format("DD.MM. HH:mm") + ', <b>' + this.y + 'U</b><br><b>' + this.label + '</b>';
	            }
			}
		}];
    }
	
	Highcharts.chart(elementId, {
		chart: {
			zoomType: 'x',
			height: '280px',
			zoomtype: 'y',
			spacing: [0, 5, 0, 0]
		},
		title: {
			text: null
		},
		legend: {
			align: 'right',
			verticalAlign: 'bottom',
			margin: 0
		},
		credits: false,
		xAxis: getPeriodChartX(timeDiffMonths, timeDiff, dateFrom, dateTo),
		yAxis: {
			allowDecimals: false,
			gridLineColor: '#ccc',
			title: {
				text: "Glukoza u krvi",
				align: "low",
				margin: 3,
				style: {
					"color": "#888",
					"text-transform": "uppercase",
					"font-size": "14px",
					"font-weight": 700
				}
			},
			min: 0,
			max: getGlucoseMaxVal(maxValue),
			plotBands: [{
                color: 'rgba(0, 166, 90, 0.18)',
                from: minGoalBeforeValue,
                to: maxGoalBeforeValue,
                label: {
	                text: 'prije obroka',
	                align: 'right',
	                x: -5,
	                style: { 'font-size': '9px' }
	            }
            }, {
                color: 'rgba(0, 166, 90, 0.08)',
                from: maxGoalBeforeValue,
                to: maxGoalAfterValue,
                label: {
	                text: 'poslije obroka',
	                align: 'right',
	                x: -5,
	                style: { 'font-size': '9px' }
	            }
            }],
            labels: {
				//align: 'left',
				x: -5,
				//y: -3
			},
            tickLength: 0,
            //tickInterval: 4
            tickAmount: 5
		},
		tooltip: {
	        crosshairs: true,
            shared: tooltipShared,
            headerFormat: ''
		},
		plotOptions: {
			spline: {
				tooltip: {
					headerFormat: '<b>{series.name}</b><br>',
					pointFormat: '{point.orig_date_time}, {point.y}mmol/l <br><b>{point.label}</b>'
				}
			}
		},
		series: chartSeries 
        /*{
	        name: 'Srednja vrijednost',
	        data: dataAvg,
	        type: 'spline',
	        color: '#000',
	        zIndex: 0,
	        lineWidth: 1,
	        dashStyle: "ShortDash",
	        states: {
	        	hover: {
	        		enabled: false
	        	}
	        }
	    }*/
	});
}

function drawGlucoseDayViewChart(data, dataAvg, maxValue, minGoalBeforeValue, maxGoalAfterValue, maxGoalBeforeValue, dayAvgs, elementId) {
	Highcharts.chart(elementId, {
		chart: {
			zoomType: 'x',
			height: '280px',
			zoomtype: 'y',
			spacing: [0, 5, 0, 0]
		},
		title: {
			text: null
		},
		legend: {
			align: 'right',
			verticalAlign: 'bottom',
			margin: 0
		},
		credits: false,
		xAxis: [{
			gridLineWidth: 1,
			gridLineDashStyle: 'ShortDot',
			gridLineColor: '#a4a4a4',
			minorGridLineWidth: 1,
  			minorTickInterval: 3600*1000*24,
			minorGridLineColor: '#959595',
			title: {
				enabled: false
			},
			endOnTick: true,
			min: moment().startOf('isoweek'),
			max: moment().endOf('isoweek'),
			tickInterval: 3600*1000*8,
			showLastLabel: true,
			type: 'datetime',
			labels: {
                formatter: function() {
					return moment(this.value).utc().format("HH");
                },
				y: 13     
            },
            tickLength: 0
		}, {
			linkedTo: 0,
			tickInterval: 3600*1000*12,
			opposite: true,
			showLastLabel: false,
			type: 'datetime',
			align: 'right',
			labels: {
				step: 2,
				formatter: function() {
					return '<b>' + Highcharts.dateFormat("%A", this.value) + '</b>';
				},
				y: -8
			},
            tickLength: 5,
            events: {
                setExtremes: syncExtremes
            }
		}],
		yAxis: {
			allowDecimals: false,
			gridLineColor: '#ccc',
			title: {
				text: "Glukoza u krvi",
				align: "low",
				margin: 3,
				style: {
					"color": "#888",
					"text-transform": "uppercase",
					"font-size": "14px",
					"font-weight": 700
				}
			},
			min: 0,
			max: getGlucoseMaxVal(maxValue),
			plotBands: [{
                color: 'rgba(0, 166, 90, 0.18)',
                from: minGoalBeforeValue,
                to: maxGoalBeforeValue,
                label: {
	                text: 'prije obroka',
	                align: 'right',
	                x: -5,
	                style: { 'font-size': '9px' }
	            }
            }, {
                color: 'rgba(0, 166, 90, 0.08)',
                from: maxGoalBeforeValue,
                to: maxGoalAfterValue,
                label: {
	                text: 'poslije obroka',
	                align: 'right',
	                x: -5,
	                style: { 'font-size': '9px' }
	            }
            }],
            labels: {
				//align: 'left',
				x: -5,
				//y: -3
			},
            tickLength: 0,
            //tickInterval: 4
            tickAmount: 5
		},
		tooltip: {
	        crosshairs: true
		},
		plotOptions: {
			scatter: {
				tooltip: {
					headerFormat: '<b>{series.name}</b><br>',
					pointFormat: '{point.orig_date_time}, {point.y}mmol/l <br><b>{point.label}</b>'
				}
			},
			spline: {
				tooltip: {
					headerFormat: '<b>{series.name}</b><br>',
					pointFormat: '{point.time_hour}h, {point.y}mmol/l ({point.count}x)'
				},
				marker: {
					enabled: true,
					fillColor: 'white',
            		lineWidth: 1,
            		lineColor: '#000',
            		radius: 2
				}
			}
		},
		series: [{
			name: 'GUK',
			data: data,
			type: 'scatter',
			zIndex: 1,
			marker: {
	        	radius: 3
	        }
		}, {
	        name: 'Srednja vrijednost',
	        data: dataAvg,
	        type: 'spline',
	        color: '#000',
	        zIndex: 0,
	        lineWidth: 1,
	        dashStyle: "ShortDash",
	        states: {
	        	hover: {
	        		enabled: false
	        	}
	        }
	    }, {
			type: 'spline',
	        name: 'Dnevni prosjek',
	        groupPadding: 0.005,
	        xAxis: 1,
	        data: dayAvgs,
	        zIndex: 2,
	        color: new Highcharts.Color("rgb(142, 68, 173)").setOpacity(0.4).get(),
	        lineWidth: 1,
	        marker: {
	        	enabled: true,
	            fillColor: "rgb(155, 89, 182)",
	        	radius: 2,
	            lineWidth: 0,
	            symbol: 'circle'
	        }
	    }]
	});
}

function drawInsulinDayViewChart(dataObj, elementId) {
	var maxValBolus = _.max(dataObj.bolus_maxs, function(item) { return item.y; });
	var maxValBasal = _.max(dataObj.basal_maxs, function(item) { return item.y; });
	totalInsulinDayAvgs = [];
	$.each(dataObj.day_avg_bolus, function(key, value) {
		$.each(dataObj.day_avg_basal, function(basalKey, basalValue) {
			if (basalValue.x.isSame(value.x)) totalInsulinDayAvgs.push({
				x: value.x,
				y: value.y + basalValue.y
			});
		});
	});
	var maxVal = maxValBolus;
	if (maxVal.y < maxValBasal.y) maxVal = maxValBasal;
	Highcharts.chart(elementId, {
		chart: {
			zoomType: 'x',
			height: '280px',
			zoomtype: 'y',
			spacing: [0, 5, 0, 0]
		},
		title: {
			text: null
		},	
		legend: {
			align: 'right',
			verticalAlign: 'bottom',
			margin: 0
		},
		credits: false,
		xAxis: [{
			gridLineWidth: 1,
			gridLineDashStyle: 'ShortDot',
			gridLineColor: '#a4a4a4',
			minorGridLineWidth: 1,
  			minorTickInterval: 3600*1000*24,
			minorGridLineColor: '#959595',
			title: {
				enabled: false
			},
			endOnTick: true,
			min: moment().startOf('isoweek'),
			max: moment().endOf('isoweek'),
			tickInterval: 3600*1000*8,
			showLastLabel: true,
			type: 'datetime',
			labels: {
                formatter: function() {
					return moment(this.value).utc().format("HH");
                },
				y: 13     
            },
            tickLength: 0
		}, {
			linkedTo: 0,
			tickInterval: 3600*1000*12,
			opposite: true,
			showLastLabel: false,
			type: 'datetime',
			align: 'right',
			labels: {
				step: 2,
				formatter: function() {
					return '<b>' + Highcharts.dateFormat("%A", this.value) + '</b>';
				},
				y: -8
			},
            tickLength: 5,
            events: {
                setExtremes: syncExtremes
            }
		}],
		yAxis: [{
			allowDecimals: false,
			gridLineColor: '#ccc',
			title: {
				text: "Inzulin doze",
				align: "low",
				margin: 3,
				style: {
					"color": "#888",
					"text-transform": "uppercase",
					"font-size": "14px",
					"font-weight": 700
				}
			},
			min: 0,
			max: getInsulinMaxVal(maxVal.y),
            labels: {
				//align: 'left',
				x: -5,
				//y: -3
			},
            tickLength: 0,
            tickAmount: 5
		}, {
			// REUSE 2ND AXIS
			allowDecimals: false,
			gridLineWidth: 1,
			gridLineDashStyle: 'longdash',
			title: {
				text: null
			},
			labels: {
				x: -5,
				align: 'right',
				formatter: function() {
		            if (this.isFirst) { return ''; }
		            return this.value;
		        }
			},
			opposite: true
		}],
		tooltip: {
	        crosshairs: true,
	        //shared: true
	    },
	    plotOptions: {
	    	spline: {
	    		dataLabels: {
	    			formatter: function() {
	    				return Math.round(this.y);
	    			}
	    		}
	    	}
	    },
		series: [{
			type: 'line',
	        name: 'Srednja vrijednost (bolus)',
	        data: dataObj.bolus_hourly_avgs,
	        zIndex: 2,
	        color: eGlukoData.bg_bolus,
	        marker: {
	        	enabled: true,
	            fillColor: 'white',
            	radius: 3,
	            lineWidth: 2,
	            lineColor: eGlukoData.bg_bolus,
	            symbol: 'circle'
	        },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '{point.time_hour}h, {point.y}U ({point.count}x)'
			}
	    }, {
            name: 'Raspon (bolus)',
            data: dataObj.bolus_ranges,
            type: 'arearange',
            lineWidth: 0,
	        color: eGlukoData.bg_bolus,
            fillOpacity: 0.25,
            zIndex: 0
        }, {
            name: 'MAX',
			type: 'line',
            showInLegend: false, 
            data: dataObj.bolus_maxs,
            zIndex: 1,
            lineWidth: 1,
            linkedTo: ':previous',
	        color: new Highcharts.Color(eGlukoData.bg_bolus).setOpacity(0.4).get(),
            marker: {
            	enabled: true,
				fillColor: 'white',
        		lineWidth: 1,
	        	lineColor: eGlukoData.bg_bolus,
        		symbol: 'circle',
        		radius: 2
            },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '{point.time_hour}h, {point.y}U'
			}
        }, {
            name: 'MIN',
			type: 'line',
            showInLegend: false, 
            data: dataObj.bolus_mins,
            zIndex: 1,
            lineWidth: 1,
            linkedTo: ':previous',
	        color: new Highcharts.Color(eGlukoData.bg_bolus).setOpacity(0.4).get(),
            marker: {
            	enabled: true,
				fillColor: 'white',
        		lineWidth: 1,
	        	lineColor: eGlukoData.bg_bolus,
        		symbol: 'circle',
        		radius: 2
            },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '{point.time_hour}h, {point.y}U'
			}
        }, /*{
			type: 'areaspline',
			stack: 0,
	        name: 'Uk. kol. kroz dan',
	        yAxis: 1,
	        data: dataObj.intraday_total,
	        zIndex: 2,
	        color: "rgb(142, 68, 173)",
	        fillOpacity: 0.13,
	        lineWidth: 0,
	        marker: {
	        	enabled: true,
	            fillColor: "rgb(155, 89, 182)",
	        	radius: 2,
	            lineWidth: 0,
	            symbol: 'circle'
	        }
	    }, */{
			type: 'spline',
	        name: 'Ukupni dnevni prosjek',
	        groupPadding: 0.005,
	        xAxis: 1,
	        yAxis: 1,
	        data: totalInsulinDayAvgs,
	        zIndex: 2,
	        color: new Highcharts.Color("rgb(142, 68, 173)").setOpacity(0.4).get(),
	        lineWidth: 1,
	        marker: {
	        	enabled: true,
	            fillColor: "rgb(155, 89, 182)",
	        	radius: 2,
	            lineWidth: 0,
	            symbol: 'circle'
	        },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '{point.y}U'
			}
	    }, {
			type: 'line',
	        name: 'Srednja vrijednost (bazalni)',
	        data: dataObj.basal_hourly_avgs,
	        zIndex: 2,
	        color: eGlukoData.bg_basal,
	        marker: {
	        	enabled: true,
	            fillColor: 'white',
            	radius: 3,
	            lineWidth: 2,
	            lineColor: eGlukoData.bg_basal,
	            symbol: 'circle'
	        },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '{point.time_hour}h, {point.y}U ({point.count}x)'
			}
	    }, {
            name: 'Raspon (bazalni)',
            data: dataObj.basal_ranges,
            type: 'arearange',
            lineWidth: 0,
	        color: eGlukoData.bg_basal,
            fillOpacity: 0.25,
            zIndex: 0
        }, {
            name: 'MAX',
			type: 'line',
            showInLegend: false, 
            data: dataObj.basal_maxs,
            zIndex: 1,
            lineWidth: 1,
            linkedTo: ':previous',
	        color: new Highcharts.Color(eGlukoData.bg_basal).setOpacity(0.4).get(),
            marker: {
            	enabled: true,
				fillColor: 'white',
        		lineWidth: 1,
	        	lineColor: eGlukoData.bg_basal,
        		symbol: 'circle',
        		radius: 2
            },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '{point.time_hour}h, {point.y}U'
			}
        }, {
            name: 'MIN',
			type: 'line',
            showInLegend: false, 
            data: dataObj.basal_mins,
            zIndex: 1,
            lineWidth: 1,
            linkedTo: ':previous',
	        color: new Highcharts.Color(eGlukoData.bg_basal).setOpacity(0.4).get(),
            marker: {
            	enabled: true,
				fillColor: 'white',
        		lineWidth: 1,
	        	lineColor: eGlukoData.bg_basal,
        		symbol: 'circle',
        		radius: 2
            },
	        tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '{point.time_hour}h, {point.y}U'
			}
        }]
	});
}