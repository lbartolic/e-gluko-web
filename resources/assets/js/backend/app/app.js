/* global moment */

/**

    TODO:
    - funkcije poput setupPeriodInsulinChart, setupGlucoseInsulinChart,... generalizirati i minimizirati kod,
    napraviti funkciju koja pokriva vise srodnih slucajeva za pripremu podataka za grafove

 */

Handlebars.registerHelper('getBgIndicator', function(desc) { 
    if (desc == 'high') {
        return 'fa fa-exclamation-triangle _fa-top-left text-red';
    }
    if (desc == 'low') {
        return 'fa fa-exclamation-triangle _fa-top-left text-orange';
    }
    if (desc == 'normal') {
        return 'fa fa-check-square-o _fa-top-left text-green';
    }
});

moment.locale('hr');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// !! povuci iz config sa servera -- [dupliciranje]
var eGlukoData = {
    now: moment('2017-08-20', 'YYYY-MM-DD'),
    utc_offset: moment().utcOffset(),
    def_m_y_format: 'MM/YYYY',
    def_date_format: 'DD.MM.YYYY.',
    def_date_hms_format: 'DD.MM.YYYY. HH:mm:ss',
    def_date_hm_format: 'DD.MM.YYYY. HH:mm',
    db_date_format: 'YYYY-MM-DD',
    db_timestamp_format: 'YYYY-MM-DD HH:mm:ss',
    bg_high: 'rgb(245, 105, 84)',
    bg_low: 'rgb(243, 156, 18)',
    bg_normal: 'rgb(0, 166, 90)',
    bg_bolus: 'rgb(52, 152, 219)',
    bg_basal: 'rgb(61, 86, 112)',
    msg_success_title: 'OK'
};

function setupLinkFormSubmit() {
	$(document).ready(function() {
	    $('._aFormSubmit').click(function(e) {
	    	e.preventDefault();
	    	$(this).closest('form').submit();
	    });
    });
}
setupLinkFormSubmit();


function showNotification(notifTitle, notifMsg, notifType, notifDelayInd) {
    var title = true,
        msg = '',
        type = 'success',
        delayIndicator = true;
            
    if (notifTitle !== undefined && notifTitle !== null) title = notifTitle;
    if (notifMsg !== undefined && notifMsg !== null) msg = notifMsg;
    if (notifType !== undefined && notifType !== null) type = notifType;
    if (notifDelayInd !== undefined && notifDelayInd !== null) delayIndicator = notifDelayInd;
    
    Lobibox.notify(type, {
        title: title,
        size: 'normal',
        icon: false,
        delayIndicator: delayIndicator,
        delay: 8000,
        rounded: false,
        position: 'center bottom',
        msg: msg,
        sound: false,
        showClass: 'fadeInUp',
        hideClass: 'fadeOutDown'
    });
}

function isNullOrUndefined(variable) {
    if (variable !== null && variable !== undefined) return false;
    return true;
}

/**
 * Initialize JS DOM actions on each page load.
 */
$('document').ready(function() {
	$('#gluc-summ-date-from').datetimepicker({
        format: eGlukoData.def_date_format,
        allowInputToggle: true,
        defaultDate: eGlukoData.now.clone().subtract(1, 'months')
    }).off('dp.change').on('dp.change', function(event) {
        setupGlucoseSummaryUI(true);
    });
    $('#gluc-summ-date-to').datetimepicker({
        format: eGlukoData.def_date_format,
        allowInputToggle: true,
        defaultDate: eGlukoData.now.clone()
    }).off('dp.change').on('dp.change', function(event) {
        setupGlucoseSummaryUI(true);
    });
    $('#trends-date-from').datetimepicker({
        format: eGlukoData.def_date_format,
        allowInputToggle: true
    });
    $('#trends-date-to').datetimepicker({
        format: eGlukoData.def_date_format,
        allowInputToggle: true
    });
});

function showModalCreateHealthLog(type, patientId) {
    $.get(APP_URL + '/admin/patients/' + patientId + '/health-logs/create/' + type, function(r) {}, "json")
        .done(function(data) {
            $('#modal-new-log').modal('show');
            var createData = {'data': data};
            var template;
            if (type == 'glucose') {
                template = $('#glucose-log-create-hb').html();
            }
            if (type == 'weight') {
                template = $('#weight-log-create-hb').html();
            }
            $('#modal-new-log-create-holder').html(Handlebars.compile(template)(createData));
            $('#log-create-datetime').datetimepicker({
                format: eGlukoData.def_date_hm_format,
                allowInputToggle: true
            });
            $('#modal-new-log-btn-save').off().click(function(e) {
                e.preventDefault();
                storeHealthLog(type, patientId, function(data) {
                    console.log('done');
                });
            });
        })
        .fail(function (data) {
            console.log(data);
        });
}

function storeHealthLog(type, patientId, callback) {
    appendBoxLoadingState('#modal-new-log')
    var formData = $('#modal-new-log-create-holder form').serializeArray();
    var successMessage;
    if (type == 'glucose') {
        successMessage = 'Novi zapis razine glukoze u krvi uspješno dodan.'
    }
    if (type == 'weight') {
        successMessage = 'Novi zapis tjelesne mase uspješno dodan.'
    }
    formData.forEach(function(item) {
        if (item.name == "log_ts") item.value = moment(item.value, eGlukoData.def_date_hm_format).format(eGlukoData.db_timestamp_format);
    });
    
    $.post(APP_URL + '/admin/patients/' + patientId + '/health-logs/' + type, formData, function(r) {}, "json")
        .done(function(data) {
            console.info(data);
            callback(data);
        })
        .fail(function (data) {
            console.log(data);
        });
}

function setupGlucoseSummaryUI(showLoading) {
    if (showLoading == true) appendBoxLoadingState('#bg-summary');

    var patientId = $('#bg-summary').attr('data-patient');
    var dateFromElDate = $('#gluc-summ-date-from').data("DateTimePicker").date();
    var dateToElDate = $('#gluc-summ-date-to').data("DateTimePicker").date();
    var dateToVal = eGlukoData.now.clone();
    var dateFromVal = dateToVal.clone().subtract(1, 'months');

    if (!isNullOrUndefined(dateFromElDate)) dateFromVal = dateFromElDate;
    if (!isNullOrUndefined(dateToElDate)) dateToVal = dateToElDate;

    getGlucoseSummary(patientId, dateFromVal.format(eGlukoData.db_date_format), dateToVal.format(eGlukoData.db_date_format), function(data) {
        console.log(data);
        var template = $('#bg-summary-hb').html();
        console.log(data);
        $.each(data, function(key, value) {
            console.log(value);
        });
        $('#bg-summary-hb-holder').html(Handlebars.compile(template)(data));

        var chartDataTotal = {
            high_perc: data.high_percentage,
            low_perc: data.low_percentage,
            normal_perc: data.normal_percentage,
            estimated_a1c: data.estimated_a1c,
            goal_avgs: data.goal_avgs
        };
        var chartDataBefore = {
            high_perc: data.before_after.before_high_percentage,
            low_perc: data.before_after.before_low_percentage,
            normal_perc: data.before_after.before_normal_percentage
        };
        var chartDataAfter = {
            high_perc: data.before_after.after_high_percentage,
            low_perc: data.before_after.after_low_percentage,
            normal_perc: data.before_after.after_normal_percentage
        };
        drawGlucoseSummaryChart(chartDataTotal, 'bg-summary-total-chart');
        drawBGMealRelChart(chartDataBefore, 'bg-summary-before-chart');
        drawBGMealRelChart(chartDataAfter, 'bg-summary-after-chart');
        removeBoxLoadingState('#bg-summary');
    });
}

function getGlucoseSummary(patientId, dateFrom, dateTo, callback) {
    $.get(APP_URL + '/admin/patients/' + patientId + '/health-logs/glucose-logs/summary/'+ dateFrom + '/' + dateTo, {}, function(r) {}, "json")
        .done(function(data) {
            console.log(data);
            callback(data);
        })
        .fail(function (data) {
        });
}

function setupTrendCharts(data) {
    console.log(data);
    console.time('setupTrendCharts');
    var dateFrom = data.date_from;
    var dateTo = data.date_to;
    var period = data.period;
    $('#trends-date-from').data("DateTimePicker").date(moment(dateFrom, 'YYYY-MM-DD'));
    if (data.type == 'day-view') {
        setupDayViewGlucoseChart(data.glucose_logs_list);
        setupDayViewInsulinChart(data.insulin_logs_list);
        setupDayViewStepCalorieChart(data.step_logs_list, data.calorie_logs_list);
        setupDayViewPhysicalActivityChart(data.activity_logs_list);
        setupDayViewSleepChart(data.sleep_logs_list);
    }
    if (data.type == 'period') {
        setupPeriodGlucoseChart(data.glucose_logs_list, period);
        setupPeriodInsulinChart(data.insulin_logs_list);
        setupPeriodStepCalorieChart(data.step_logs_list, data.calorie_logs_list);
        setupPeriodPhysicalActivityChart(data.activity_logs_list);
        setupPeriodSleepChart(data.sleep_logs_list);
    }
    //setupPeriodCharts();
    console.timeEnd('setupTrendCharts');
}

function createDayViewRefDateTime(logTs) {
    var logTsObj = moment(logTs, eGlukoData.db_timestamp_format);
    var day = logTsObj.clone().isoWeekday();
    var refDate = moment().isoWeekday(day).format('YYYY-MM-DD');
    return moment(refDate + ' ' + logTsObj.clone().format('HH:mm'), eGlukoData.db_timestamp_format).add(eGlukoData.utc_offset, 'minutes');
}

function createPeriodDateTime(logTs) {
    return moment(logTs, eGlukoData.db_timestamp_format).add(eGlukoData.utc_offset, 'minutes');
}

function setupPeriodSleepChart(data) {
    var sleepLogs = data.data;
    if (sleepLogs != null) {
        var chartDataSleeps = [];
        var chartDataAsleep = [];
        var chartDataAwake = [];
        var chartDataQuality = [];
        var chartDataAwakeCount = [];
        var chartDataRestless = [];
        var chartDataRestlessCount = [];
        var dateFrom = moment(data.date_from, eGlukoData.db_date_format);
        var dateTo = moment(data.date_to, eGlukoData.db_date_format);
        var chartMaxValue = 0;
        $.each(sleepLogs, function(key, value) {
            $.each(value.logs, function(logKey, logValue) {
                var sleepChartValue = ((logValue.value/60)/1000)/60;
                if (sleepChartValue > chartMaxValue) chartMaxValue = sleepChartValue;
                var dateTime = createPeriodDateTime(logValue.log_ts);

                chartDataSleeps.push({
                    x: dateTime, 
                    y: sleepChartValue,
                    asleep: logValue.loggable.asleep_mins/60,
                    awake: logValue.loggable.awake_mins/60,
                    restless: logValue.loggable.restless_mins/60,
                    awake_count: logValue.loggable.awake_count,
                    y_format: moment().startOf('day').seconds(sleepChartValue*60).format('HH:mm:ss'),
                    log_ts: logValue.log_ts,
                    orig_date_time: moment(logValue.log_ts, 'YYYY-MM-DD HH:mm:ss').format("DD.MM. HH:mm")
                });
                chartDataAsleep.push({
                    x: dateTime, 
                    y: logValue.loggable.asleep_mins/60,
                    y_format: moment().startOf('day').seconds(logValue.loggable.asleep_mins*60).format('HH:mm:ss'),
                    log_ts: logValue.log_ts,
                    orig_date_time: moment(logValue.log_ts, 'YYYY-MM-DD HH:mm:ss').format("DD.MM. HH:mm")
                });
                chartDataAwake.push({
                    x: dateTime, 
                    y: logValue.loggable.awake_mins/60,
                    y_format: moment().startOf('day').seconds(logValue.loggable.awake_mins*60).format('HH:mm:ss'),
                    log_ts: logValue.log_ts,
                    orig_date_time: moment(logValue.log_ts, 'YYYY-MM-DD HH:mm:ss').format("DD.MM. HH:mm")
                });
                chartDataAwakeCount.push({
                    x: dateTime, 
                    y: logValue.loggable.awake_count,
                    y_format: logValue.loggable.awake_count,
                    log_ts: logValue.log_ts,
                    orig_date_time: moment(logValue.log_ts, 'YYYY-MM-DD HH:mm:ss').format("DD.MM. HH:mm")
                });
                chartDataRestless.push({
                    x: dateTime, 
                    y: logValue.loggable.restless_mins/60,
                    y_format: moment().startOf('day').seconds(logValue.loggable.restless_mins*60).format('HH:mm:ss'),
                    log_ts: logValue.log_ts,
                    orig_date_time: moment(logValue.log_ts, 'YYYY-MM-DD HH:mm:ss').format("DD.MM. HH:mm")
                });
                chartDataRestlessCount.push({
                    x: dateTime, 
                    y: logValue.loggable.restless_count,
                    y_format: logValue.loggable.restless_count,
                    log_ts: logValue.log_ts,
                    orig_date_time: moment(logValue.log_ts, 'YYYY-MM-DD HH:mm:ss').format("DD.MM. HH:mm")
                });
            });
        });
        drawSleepPeriodChart(chartDataSleeps, chartDataAsleep, chartDataAwake, chartDataAwakeCount, chartDataRestless, chartDataRestlessCount, chartMaxValue, dateFrom, dateTo, 'sleep-logs-chart');
    }
}

function setupPeriodPhysicalActivityChart(data) {
    var activityLogs = data.data;
    if (activityLogs != null) {
        var chartDataActivities = [];
        var chartDataHearthRates = [];
        var dateFrom = moment(data.date_from, eGlukoData.db_date_format);
        var dateTo = moment(data.date_to, eGlukoData.db_date_format);
        var chartMaxValue = 0;
        $.each(activityLogs, function(key, value) {
            $.each(value.logs, function(logKey, logValue) {
                var activityChartValue = (logValue.value/60)/1000;
                if (activityChartValue > chartMaxValue) chartMaxValue = activityChartValue;
                var dateTime = createPeriodDateTime(logValue.log_ts);

                chartDataActivities.push({
                    x: dateTime, 
                    y: activityChartValue,
                    heart_rate: logValue.loggable.average_heart_rate,
                    value_seconds: activityChartValue*60,
                    log_ts: logValue.log_ts,
                    orig_date_time: moment(logValue.log_ts, 'YYYY-MM-DD HH:mm:ss').format("DD.MM. HH:mm")
                });
                chartDataHearthRates.push({
                    x: dateTime, 
                    y: logValue.loggable.average_heart_rate,
                    log_ts: logValue.log_ts,
                    orig_date_time: moment(logValue.log_ts, 'YYYY-MM-DD HH:mm:ss').format("DD.MM. HH:mm")
                });
            });
        });
        drawPhysicalActivityPeriodChart(chartDataActivities, chartDataHearthRates, chartMaxValue, dateFrom, dateTo, 'activity-logs-chart');
    }
}

function setupPeriodStepCalorieChart(dataSteps, dataCalories) {
    var stepLogs = dataSteps.data;
    if (stepLogs != null) {
        var calorieLogs = dataCalories.data;
        var logs = stepLogs.concat(calorieLogs);
        var dateFrom = moment(dataSteps.date_from, eGlukoData.db_date_format);
        var dateTo = moment(dataSteps.date_to, eGlukoData.db_date_format);
        var chartDataSteps = [];
        var chartDataCalories = [];
        var chartMaxValue = 0;
        $.each(logs, function(key, value) {
            $.each(value.logs, function(logKey, logValue) {
                if (logValue.value > chartMaxValue) chartMaxValue = logValue.value;
                var dateTime = createPeriodDateTime(logValue.log_ts).subtract(12, 'hours');
                var chartDataArray;
                if (logValue.log_type.type == "step") chartDataArray = chartDataSteps;
                else chartDataArray = chartDataCalories;
                chartDataArray.push({
                    x: dateTime, 
                    y: logValue.value,
                    log_ts: logValue.log_ts,
                    orig_date_time: moment(logValue.log_ts, 'YYYY-MM-DD HH:mm:ss').format("DD.MM. HH:mm")
                });
            });
        });
        drawStepCaloriePeriodChart(chartDataSteps, chartDataCalories, chartMaxValue, dateFrom, dateTo, 'step-calorie-logs-chart');
    }
}

function setupPeriodInsulinChart(data) {
    var insulinLogs = data.data;
    if (insulinLogs != null) {
        var insulinChartDataBolus = [];
        var insulinChartDataBasal = [];
        var chartDataIntraDayTotal = []; // sum of insulin dosage during each day for area chart
        var intraDayTotalIndex = 0;
        var logsBasalArr = [];
        var logsBolusArr = [];
        var insulinChartData = [];
        var dateFrom = moment(data.date_from, eGlukoData.db_date_format);
        var dateTo = moment(data.date_to, eGlukoData.db_date_format);
        var chartMaxValue = 0;
        $.each(insulinLogs, function(key, value) {
            var intraDaySumTotal = 0;
            $.each(value.logs, function(logKey, logValue) {
                if (logValue.value > chartMaxValue) chartMaxValue = logValue.value;
                var dateTime = createPeriodDateTime(logValue.log_ts);
                var chartDataArray = insulinChartDataBolus;
                var logsDataArray = logsBolusArr;
                if (logValue.loggable.insulin_type.key == "basal") {
                    chartDataArray = insulinChartDataBasal; // reference
                    logsDataArray = logsBasalArr; // reference
                }

                intraDaySumTotal += logValue.value;
                if (chartDataIntraDayTotal[intraDayTotalIndex-1] !== undefined && chartDataIntraDayTotal[intraDayTotalIndex-1].x.isSame(dateTime)) {
                    chartDataIntraDayTotal[intraDayTotalIndex-1].y = intraDaySumTotal;
                }
                else {
                    chartDataIntraDayTotal.push({ x: dateTime, y: intraDaySumTotal });
                    intraDayTotalIndex++;
                }

                logsDataArray.push(logValue);
                var chartData = {
                    x: dateTime, 
                    y: logValue.value,
                    log_ts: logValue.log_ts,
                    insulin_type: logValue.loggable.insulin_type,
                    orig_date_time: moment(logValue.log_ts, 'YYYY-MM-DD HH:mm:ss').format("DD.MM. HH:mm"),
                    label: logValue.loggable.label.display_value,
                    label_rel: logValue.loggable.label.meal_rel
                };
                insulinChartData.push(chartData);
                chartDataArray.push(chartData);
            });
        });
        drawInsulinPeriodChart(insulinChartDataBolus, insulinChartDataBasal, chartMaxValue, chartDataIntraDayTotal, dateFrom, dateTo, 'insulin-logs-chart', insulinChartData);
    }
}

function setupPeriodGlucoseChart(data, period) {
    var glucoseLogs = data.data;
    if (glucoseLogs != null) {
        var chartType = 0;
        var chartMaxValue = data.stats.max_value.log.value;
        var dateFrom = moment(data.date_from, eGlukoData.db_date_format);
        var dateTo = moment(data.date_to, eGlukoData.db_date_format);
        var glucoseChartData = [];
        var logsArr = [];
        var avgMinBeforeBg = data.stats.goal_avgs.min_before;
        var avgMaxAfterBg = data.stats.goal_avgs.max_after;
        var avgMaxBeforeBg = data.stats.goal_avgs.max_before;
        $.each(glucoseLogs, function(key, value) {
            var healthGoal = value.health_goal;
            $.each(value.logs, function(logKey, logValue) {
                logsArr.push(logValue);
                var color = getGlucoseGoalDiffColor(logValue.value, healthGoal, logValue.loggable.label.meal_rel);
                glucoseChartData.push({
                    x: createPeriodDateTime(logValue.log_ts), 
                    y: logValue.value, 
                    log_ts: logValue.log_ts,
                    color: color,
                    health_goal: healthGoal,
                    orig_date_time: moment(logValue.log_ts, 'YYYY-MM-DD HH:mm:ss').format("DD.MM. HH:mm"),
                    label: logValue.loggable.label.display_value,
                    label_rel: logValue.loggable.label.meal_rel
                });
            });
        });

        drawGlucosePeriodChart(glucoseChartData, chartMaxValue, dateFrom, dateTo, avgMinBeforeBg, avgMaxAfterBg, avgMaxBeforeBg, 'glucose-logs-chart');
    }
}

function setupDayViewSleepChart(data) {
    var sleepData = data.data;
    if (sleepData != null) {
        var sleepAvgs = calcChartDailyViewAverages(null, null, data.std_week_days_stats.sleep.days);
        var chartDataObj = {
            hourly_avgs: sleepAvgs.averages,
            hourly_sums: sleepAvgs.sums,
            maxs: sleepAvgs.maxs,
            day_avg: sleepAvgs.day_avg_value
        };
        drawSleepDayViewChart(chartDataObj, 'sleep-logs-chart');
    }
}

function setupDayViewPhysicalActivityChart(data) {
    var activityLogs = data.data;
    if (activityLogs != null) {
        var activityAvgs = calcChartDailyViewAverages(null, null, data.std_week_days_stats.activity.days);
        var chartDataObj = {
            hourly_avgs: activityAvgs.averages,
            hourly_sums: activityAvgs.sums,
            maxs: activityAvgs.maxs,
            day_avg: activityAvgs.day_avg_value
        };
        drawPhysicalActivityDayViewChart(chartDataObj, 'activity-logs-chart')
    }
}

function setupDayViewStepCalorieChart(dataSteps, dataCalories) {
    if (dataSteps.data != null && dataCalories.data != null) {
        var stepAvgs = calcChartDailyViewAverages(null, null, dataSteps.std_week_days_stats.step.days);
        var calorieAvgs = calcChartDailyViewAverages(null, null, dataCalories.std_week_days_stats.calorie.days);
        var chartDataObj = {
            'step_mins': stepAvgs.day_avg_mins,
            'calorie_mins': calorieAvgs.day_avg_mins,
            'step_maxs': stepAvgs.day_avg_maxs,
            'calorie_maxs': calorieAvgs.day_avg_maxs,
            'step_ranges': stepAvgs.day_avg_ranges,
            'calorie_ranges': calorieAvgs.day_avg_ranges,
            'day_avg_step': stepAvgs.day_avg_value,
            'day_avg_calorie': calorieAvgs.day_avg_value,
        };
        drawStepCalorieDayViewChart(chartDataObj, 'step-calorie-logs-chart');
    }
}

function setupDayViewInsulinChart(data) {
    var insulinLogs = data.data;
    if (insulinLogs != null) {
        var bolusAvgs = calcChartDailyViewAverages(null, null, data.std_week_days_stats.bolus.days);
        var basalAvgs = calcChartDailyViewAverages(null, null, data.std_week_days_stats.basal.days);
        var chartDataObj = {
            bolus_hourly_avgs: bolusAvgs.averages,
            basal_hourly_avgs: basalAvgs.averages,
            bolus_mins: bolusAvgs.mins,
            bolus_maxs: bolusAvgs.maxs,
            bolus_ranges: bolusAvgs.ranges,
            basal_mins: basalAvgs.mins,
            basal_maxs: basalAvgs.maxs,
            basal_ranges: basalAvgs.ranges,
            day_avg_bolus: bolusAvgs.day_avg_value,
            day_avg_basal: basalAvgs.day_avg_value
        };
        drawInsulinDayViewChart(chartDataObj, 'insulin-logs-chart')
    }
}

function setupDayViewGlucoseChart(data) {
    var glucoseLogs = data.data;
    if (glucoseLogs != null) {
        var chartMaxValue = data.stats.max_value.log.value;
        var glucoseChartData = [];
        var avgMinBeforeBg = data.stats.goal_avgs.min_before;
        var avgMaxAfterBg = data.stats.goal_avgs.max_after;
        var avgMaxBeforeBg = data.stats.goal_avgs.max_before;
        $.each(glucoseLogs, function(key, value) {
            var healthGoal = value.health_goal;
            $.each(value.logs, function(logKey, logValue) {
                var chartDateTime = createDayViewRefDateTime(logValue.log_ts);

                var color = getGlucoseGoalDiffColor(logValue.value, healthGoal, logValue.loggable.label.meal_rel);

                glucoseChartData.push({
                    x: chartDateTime, 
                    y: logValue.value,
                    log_ts: logValue.log_ts,
                    color: color,
                    orig_date_time: moment(logValue.log_ts, 'YYYY-MM-DD HH:mm:ss').format("DD.MM. HH:mm"),
                    label: logValue.loggable.label.display_value
                });
            });
        });

        var glucoseAveragesData = calcChartDailyViewAverages(null, 10, data.std_week_days_stats.glucose.days);
        drawGlucoseDayViewChart(glucoseChartData, glucoseAveragesData.averages, chartMaxValue, avgMinBeforeBg, avgMaxAfterBg, avgMaxBeforeBg, glucoseAveragesData.day_avg_value, 'glucose-logs-chart');
    }
}

function getBoxLoadingStateHtml() {
    return '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
}
function appendBoxLoadingState(selector) {
    $(selector).append(getBoxLoadingStateHtml());
}
function removeBoxLoadingState(selector) {
    $(selector + ' .overlay').remove();
}

function getWithCallback(callback) {
	$.get(APP_URL + '/admin/ajax/check-in/get-checked-in', function(r) {}, "json")
        .done(function(data) {
            callback(data);
        })
        .fail(function (data) {
        });
}

function modalHandlebars(resId) {
    $.get(APP_URL + '/admin/ajax/check-in/' + resId + '/edit', function(r) {}, "json")
        .done(function(data) {
            $('#modal-edit-checkin').modal().show();
            var hbTemplate = document.getElementById('modal-edit-checkin-hbt').innerHTML;
			var context = {'data': data};
			var compiled = Handlebars.compile(hbTemplate)(context);
			$('#modal-edit-checkin-holder').html(compiled);
			$('._stop-current-checkin').off().click(function() {
				var id = $(this).attr('data-check-in');
				stopCurrentCheckIn(id, function() {
					$('#modal-edit-checkin').modal('hide');
				});
			});
			$('._delete-current-checkin').off().click(function() {
				var id = $(this).attr('data-check-in');
				deleteCheckIn(id, function() {
					$('#modal-edit-checkin').modal('hide');
				});
			})
        })
        .fail(function (data) {
        });
}

function swalConfirmDelete(type, id, callback) {
	showDefaultConfirmSwal(null, 'Odabrana stavka bit će obrisana.', null, null, function() {    
        $.post(APP_URL + '/admin/ajax/absence/' + type + '/' + id + '/destroy', function(r) {}, "json")
	        .done(function(data) {
	            callback(data);
	        })
	        .fail(function (data) {
	        });
    });
}