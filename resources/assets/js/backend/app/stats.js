/*
 * param data - array of log objects
 */
/* global moment, eGlukoData */

function calcChartDailyViewAverages(data, roundAccur, avgs) {
	if (roundAccur === undefined || roundAccur == null) roundAccur = 1;
	if (data === undefined || data == null) var averages = avgs;
	else var averages = calcDayHourAverages(data, roundAccur);
    dayAveragesData = [];
    dayAveragesMaxsData = [];
    dayAveragesMinsData = [];
    dayAveragesRangesData = [];
    averagesData = [];
    sumsData = [];
    minsData = [];
    maxsData = [];
    rangesData = [];
    $.each(averages, function(key, value) {
    	if (value.ref_date instanceof moment) {
	        var refDate = value.ref_date.format("YYYY-MM-DD");
	    }
	    else {
	    	var refDate = moment(value.ref_date.date, eGlukoData.db_timestamp_format).format("YYYY-MM-DD");
	    }
        $.each(value.data, function(hourKey, hourValue) {
            if (hourValue.count != 0) {
            	var logRefDateTime = moment(refDate + " " + hourValue.hour, "YYYY-MM-DD HH").add(eGlukoData.utc_offset, 'minutes');
            	var avgRound = Math.round(hourValue.avg*roundAccur)/roundAccur;
                console.log(hourValue.hour + " " + hourValue.avg + " " + hourValue.sum);
                averagesData.push({
                    x: logRefDateTime, 
                    y: avgRound,
                    count: hourValue.count,
                    time_hour: hourValue.log_ts
                });
                sumsData.push({
                    x: logRefDateTime, 
                    y: hourValue.sum,
                    count: hourValue.count,
                    time_hour: hourValue.log_ts
                });
                rangesData.push({
                	x: logRefDateTime,
                    high: hourValue.max,
                    low: hourValue.min,
                    time_hour: hourValue.log_ts
                });
                minsData.push({
                	x: logRefDateTime,
                    y: hourValue.min,
                    time_hour: hourValue.log_ts
                });
                maxsData.push({
                	x: logRefDateTime, 
                    y: hourValue.max,
                    time_hour: hourValue.log_ts
                });
            }
        });
        var dayAveragesDateTime = moment(value.ref_date.date, eGlukoData.db_timestamp_format).startOf('day').add(12, 'hours').format(eGlukoData.db_timestamp_format);
        if (value.day_avg_value != null) {
            dayAveragesData.push({
            	x: createDayViewRefDateTime(dayAveragesDateTime), 
                y: value.day_avg_value.value
            });
        }
        if (value.day_max != null) {
            dayAveragesMaxsData.push({
                x: createDayViewRefDateTime(dayAveragesDateTime), 
                y: value.day_max.value,
                date: moment(value.day_max.date.date, eGlukoData.db_timestamp_format).format(eGlukoData.def_date_format)
            });
        }
        if (value.day_min != null) {
            dayAveragesMinsData.push({
                x: createDayViewRefDateTime(dayAveragesDateTime), 
                y: value.day_min.value,
                date: moment(value.day_min.date.date, eGlukoData.db_timestamp_format).format(eGlukoData.def_date_format)
            });
        }
        if (value.day_max != null && value.day_min != null) {
            dayAveragesRangesData.push({
                x: createDayViewRefDateTime(dayAveragesDateTime), 
                high: value.day_max.value,
                low: value.day_min.value
            });
        }
    });

    return {
        day_avg_value: dayAveragesData,
        day_avg_maxs: dayAveragesMaxsData,
        day_avg_mins: dayAveragesMinsData,
    	day_avg_ranges: dayAveragesRangesData,
    	daily_avg: averages,
    	averages: averagesData,
    	sums: sumsData,
    	mins: minsData,
    	maxs: maxsData,
    	ranges: rangesData
    };
}

/*
 * param data - array of log objects
 */
function calcDayHourAverages(data, roundAccur) {
	if (roundAccur === undefined || roundAccur == null) roundAccur = 1;
	var averages = [];
    for(var i = 0; i < 7; i++) {
        averages.push({ref_date: moment().isoWeekday("Monday").add(i, "days"), data: [], 'day_avg_value': 0, 'day_avg_sum': 0});
        for(var j = 0 ; j < 24 ; j++) {
            averages[i].data.push({hour: moment("00", "HH").add(j, 'hours').format("HH"), sum: 0, count: 0, avg: 0, max: null, min: null, log_ts: null});
        }
    }
    $.each(averages, function(avgKey, avgValue) {
    	var tempDayAvgData = { sum: 0, count: 0, avg_sum: 0 };
		var matchedWeekDays = [];
        var refDate = avgValue.ref_date;
    	var weekDay = refDate.clone().isoWeekday();
    	$.each(data, function(key, logValue) {
            var logRefDateTime = createDayViewRefDateTime(logValue.log.log_ts);
            var logTsMoment = moment(logValue.log.log_ts, eGlukoData.db_timestamp_format);
            if (logRefDateTime.clone().format("YYYY-MM-DD") == refDate.clone().format("YYYY-MM-DD")) {
            	tempDayAvgData.sum += logValue.log.value;
            	tempDayAvgData.count++;
            	if (logTsMoment.clone().isoWeekday() == weekDay && !(matchedWeekDays.indexOf(logTsMoment.clone().format("YYYY-MM-DD")) > -1)) {
            		matchedWeekDays.push(logTsMoment.clone().format("YYYY-MM-DD"));
            	}
                $.each(avgValue.data, function(hourkey, hourValue) {
                    if ((logRefDateTime.clone().format("HH") == hourValue.hour)) {
                        hourValue.sum += logValue.log.value;
                        hourValue.count++;
                        hourValue.avg = hourValue.sum/hourValue.count;
                        if (hourValue.max == null || hourValue.max < logValue.log.value) {
                            hourValue.max = logValue.log.value;
                        }
                        if (hourValue.min == null || hourValue.min > logValue.log.value) {
                            hourValue.min = logValue.log.value;
                        }
                        hourValue.log_ts = moment(logValue.log.log_ts, eGlukoData.db_timestamp_format).format("HH");
            			tempDayAvgData.avg_sum += hourValue.avg;
                    }
                });
            }
        });
        avgValue.day_avg_value = Math.round((tempDayAvgData.sum/tempDayAvgData.count)*roundAccur)/roundAccur;
        avgValue.day_avg_sum = Math.round((tempDayAvgData.avg_sum/matchedWeekDays.length)*roundAccur)/roundAccur;
    });
    return averages;
}

function getGlucoseGoalDiffColor(logValue, healthGoal, mealRel) {
    var color = '#fff';
    var maxVal, minVal;
    if (healthGoal != null) {
        if (mealRel == 1 || mealRel == 2) {
            minVal = healthGoal.min_bg_before_meal;
            if (mealRel == 1) {
                maxVal = healthGoal.max_bg_before_meal;
            }
            else {
                maxVal = healthGoal.max_bg_after_meal;
            }
            if (logValue > maxVal) {
                color = eGlukoData.bg_high;
            }
            else if (logValue < minVal) {
                color = eGlukoData.bg_low;
            }
            else {
                color = eGlukoData.bg_normal;
            }
        }
        else {
            if (logValue > healthGoal.max_bg_after_meal) color = eGlukoData.bg_high;
            else if (logValue < healthGoal.min_bg_before_meal) color = eGlukoData.bg_low;
            else color = eGlukoData.bg_normal;
        }
    }
    return color;
}

function getChartDataGroupedByWeek(chartData, dateFrom, dateTo, type) {
    console.time('getChartDataGroupedByWeek');
    // could be reused for different time grouping if needed (not only week)
    var diffDays = dateTo.clone().diff(dateFrom.clone(), 'days');
    var diffWeeks = moment(dateTo).diff(moment(dateFrom), 'weeks');
    var weeklyData = [];
    var glucoseExtra = {before: [], after: []};
    var insulinExtra = {bolus: [], bolus_before: [], basal: []};
    var physicalActivityExtra = {heart_rate: []};
    var sleepExtra = {asleep: [], awake: [], restless: [], awake_count: []};
    for(var i = 0 ; i < diffWeeks ; i++) {
        var dataStruct = {
            start_date: moment(dateFrom).add(i*7, "days").format(eGlukoData.db_timestamp_format), 
            end_date: moment(dateFrom).add((i*7)+7, "days").format(eGlukoData.db_timestamp_format), 
            x: moment(dateFrom).add((i*7), "days"), 
            y: null, 
            health_goal: null
        };
        
        weeklyData.push(Object.assign({}, dataStruct));
        if (type == "glucose") $.each(glucoseExtra, function(key, value) { value.push(Object.assign({}, dataStruct)); });
        if (type == "insulin") $.each(insulinExtra, function(key, value) { value.push(Object.assign({}, dataStruct)); });
        if (type == "physical_activity") $.each(physicalActivityExtra, function(key, value) { value.push(Object.assign({}, dataStruct)); });
        if (type == "sleep") $.each(sleepExtra, function(key, value) { value.push(Object.assign({}, dataStruct)); });
    }
    for(var i = 0 ; i < weeklyData.length ; i++) {
        $.each(chartData, function(key, value) {
            var momentLogTs = moment(value.log_ts, eGlukoData.db_timestamp_format);
            var momentEDate = moment(weeklyData[i].end_date, eGlukoData.db_timestamp_format);
            var momentSDate = moment(weeklyData[i].start_date, eGlukoData.db_timestamp_format);
            if (momentLogTs.isBefore(momentEDate) && momentLogTs.isAfter(momentSDate)) {
                if (weeklyData[i].y === null) {
                    weeklyData[i].health_goal = value.health_goal;
                    weeklyData[i].y = value.y;
                }
                else weeklyData[i].y = (weeklyData[i].y + value.y)/2;
                
                if (type == "glucose") {
                    if (value.label_rel == 1) {
                        if (glucoseExtra.before[i].y === null) {
                            glucoseExtra.before[i].health_goal = value.health_goal;
                            glucoseExtra.before[i].y = value.y;
                        }
                        else glucoseExtra.before[i].y = (glucoseExtra.before[i].y + value.y)/2;
                    }
                    if (value.label_rel == 2) {
                        if (glucoseExtra.after[i].y === null) {
                            glucoseExtra.after[i].health_goal = value.health_goal;
                            glucoseExtra.after[i].y = value.y;
                        }
                        else glucoseExtra.after[i].y = (glucoseExtra.after[i].y + value.y)/2;
                    }
                }
                if (type == "insulin") {
                    if (value.insulin_type.key == "bolus") {
                        if (insulinExtra.bolus[i].y === null) insulinExtra.bolus[i].y = value.y;
                        else insulinExtra.bolus[i].y = (insulinExtra.bolus[i].y + value.y)/2;
                        if (value.label_rel == 1) {
                            if (insulinExtra.bolus_before[i].y === null) insulinExtra.bolus_before[i].y = value.y;
                            else insulinExtra.bolus_before[i].y = (insulinExtra.bolus_before[i].y + value.y)/2;
                        }
                    }
                    if (value.insulin_type.key == "basal") {
                        if (insulinExtra.basal[i].y === null) insulinExtra.basal[i].y = value.y;
                        else insulinExtra.basal[i].y = (insulinExtra.basal[i].y + value.y)/2;
                    }
                }
                if (type == "physical_activity") {
                    if (value.heart_rate != null) {
                        if (physicalActivityExtra.heart_rate[i].y === null) physicalActivityExtra.heart_rate[i].y = value.heart_rate;
                        else physicalActivityExtra.heart_rate[i].y = (physicalActivityExtra.heart_rate[i].y + value.heart_rate)/2;
                    }
                }
                if (type == "sleep") {
                    if (sleepExtra.asleep[i].y === null) sleepExtra.asleep[i].y = value.asleep;
                    else sleepExtra.asleep[i].y = (sleepExtra.asleep[i].y + value.asleep)/2;
                    if (sleepExtra.restless[i].y === null) sleepExtra.restless[i].y = value.restless;
                    else sleepExtra.restless[i].y = (sleepExtra.restless[i].y + value.restless)/2;
                    if (sleepExtra.awake[i].y === null) sleepExtra.awake[i].y = value.awake;
                    else sleepExtra.awake[i].y = (sleepExtra.awake[i].y + value.awake)/2;
                    if (sleepExtra.awake_count[i].y === null) sleepExtra.awake_count[i].y = value.awake_count;
                    else sleepExtra.awake_count[i].y = (sleepExtra.awake_count[i].y + value.awake_count)/2;
                }
            }
        });
    }
    console.timeEnd('getChartDataGroupedByWeek');
    
    var extra = null;
    if (type == "glucose") extra = glucoseExtra;
    else if (type == "insulin") extra = insulinExtra;
    else if (type == "physical_activity") extra = physicalActivityExtra;
    else if (type == "sleep") extra = sleepExtra;
    
    return {
        weekly_data: weeklyData,
        extra: extra
    };
}