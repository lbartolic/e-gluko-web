<table class="stats-table">
	<tr>
		<th>AVG razina glukoze <small>[{{ $_base_units['bg'] }}]</small></th>
		<th>Raspon razine glukoze <small>[{{ $_base_units['bg'] }}]</small></th>
		<th>AVG dnevni bazalni/bolus <small>[{{ $_base_units['insulin'] }}]</small></th>
		<th>AVG dnevne kalorije/koraci <small>[{{ $_base_units['calories'] }}/koraci]<small></th>
		<th>Uk. aktivnosti</th>
		<th>AVG puls aktivnosti <small>[{{ $_base_units['heart_rate'] }}]</small></th>
		<th>Uk. san</th>
		<th>AVG kvaliteta sna</th>
	</tr>
	<tr>
		<td>
			@include('pdf.health-logs.includes.desc-indicator', [
				'level' => $stats['glucose_avg']['desc'],
				'high' => 1
			])
			{{ $stats['glucose_avg']['value'] or '-' }}
		</td>
		<td>
			@include('pdf.health-logs.includes.desc-indicator', [
				'level' => $stats['glucose_min']['desc'],
				'high' => 1
			])
			{{ $stats['glucose_min']['log']->value or '-' }}
			 - 
			@include('pdf.health-logs.includes.desc-indicator', [
				'level' => $stats['glucose_max']['desc'],
				'high' => 1
			])
			{{ $stats['glucose_max']['log']->value or '-' }}
		</td>
		<td>
			@include('pdf.health-logs.includes.desc-indicator', [
				'level' => $stats['insulin_basal_daily_avg']['desc']
			])
			{{ $stats['insulin_basal_daily_avg']['value'] }}
			 / 
			@include('pdf.health-logs.includes.desc-indicator', [
				'level' => $stats['insulin_bolus_daily_avg']['desc']
			])
			{{ $stats['insulin_bolus_daily_avg']['value'] }}
		</td>
		<td>
			{{ $stats['calorie_daily_avg']['value'] }}
			 / 
			@include('pdf.health-logs.includes.desc-indicator', [
				'level' => $stats['step_daily_avg']['desc'],
				'high' => 0
			])
			{{ $logs['stats']['step_daily_avg']['value'] }}
		<td>
			@include('pdf.health-logs.includes.desc-indicator', [
				'level' => $stats['activity_total']['desc'],
				'high' => 0
			])
			{{ hmsFromSec($stats['activity_total']['value']/1000) }}
		</td>
		<td>{{ $stats['activity_heart_rate_avg'] or '-' }}</td>
		<td>{{ hmsFromSec($stats['sleep_total']['value']/1000) }}</td>
		<td>{{ $stats['sleep_avg_quality'] or '-' }}%</td>
	</tr>
</table>