@if($level == 'low')
	@if(isset($high))
		<b class="desc-indicator-symbol desc-red">&#x2193;</b>
	@else
		<b class="desc-indicator-symbol">&#x2193;</b>
	@endif
@elseif($level == 'high')
	@if(isset($high))
		@if($high == 1)
			<b class="desc-indicator-symbol desc-red">&#x2191;</b>
		@else
			<b class="desc-indicator-symbol desc-green">&#x2191;</b>
		@endif
	@else
		<b class="desc-indicator-symbol">&#x2191;</b>
	@endif
@elseif($level == 'normal')
	@if(isset($high))
		<b class="desc-indicator-symbol desc-green">&#x2609;</b>
	@else
		<b class="desc-indicator-symbol">&#x2609;</b>
	@endif
@endif