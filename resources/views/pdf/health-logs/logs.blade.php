<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style>
			body {
				font-family: DejaVu Sans, sans-serif;
			}
			table {
				font-size: 10px;
				border-collapse: collapse;
			}
			table th, table td {
				padding: 4px;
				border: 1px solid #333;
				background: #fff;
			}
			.desc-indicator-symbol.desc-green {
				color: #00a65a;
			}
			.desc-indicator-symbol.desc-red {
				color: #dd4b39;
			}
			table .desc-indicator-symbol {
				font-size: 13px;
			}
			.table-cell-inactive {
				background: #e6e6e6;
				border: 1px solid #333;
			}
			h1 {
				font-size: 18px;
				margin-top: 15px;
				margin-bottom: 5px;
			}
			h2 {
				font-size: 15px;
				padding-top: 20px !important;
				margin-bottom: 5px;
			}
			.stats-table {
				margin-top: 0;
				margin-bottom: 20px;
			}
		</style>
	</head>
	<body>
		<h1>Izvještaj za period:<b> {{ \Carbon\Carbon::parse($logs['date_from'])->format(config('application.time.date_formats.default_date')) }} - {{ \Carbon\Carbon::parse($logs['date_to'])->format(config('application.time.date_formats.default_date')) }}</b></h1>
		@include('pdf.health-logs.includes.total-stats-table', [
			'stats' => $logs['stats']
		])
		@foreach($logs['data'] as $dataKey => $dataValue)
			<h2>
				{{ \Carbon\Carbon::parse($dataValue['date'])->formatLocalized(config('application.time.locale_date_formats.default_day_date')) }}
			</h2>
			@include('pdf.health-logs.includes.total-stats-table', [
				'stats' => $dataValue['stats']
			])
            <div>
                <table>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Unos</th>
                        <th>Iznos</th>
                        <th>Oznaka</th>
                        <th>Vrsta</th>
                        <th>Kalorije</th>
                        <th>Koraci</th>
                        <th>Otkucaji srca</th>
                        <th>Spavanje</th>
                        <th>Buđenje/nemirnost</th>
                    </tr>
                    @foreach($dataValue['logs'] as $logKey => $logValue)
                        @if($logValue->logType->type == 'glucose')
                            <tr>
                                <td>{{ $logValue->insert_type_string }}</td>
                                <td>{{ $logValue->log_ts->format('H:i') }}</td>
                                <td><b>{{ $logValue->logType->type_name }}</b></td>
                                <td>
                                    @include('pdf.health-logs.includes.glucose-log-value-desc', [
                                        'data' => $logValue->loggable->getDescGoalDiff($logValue, $dataValue['health_goal'])
                                    ])
                                </td>
                                <td>{{ $logValue->loggable->label->display_value }}</td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                            </tr>
                        @endif
                        @if($logValue->logType->type == 'insulin')
                            <tr>
                                <td>{{ $logValue->insert_type_string }}</td>
                                <td>{{ $logValue->log_ts->format('H:i') }}</td>
                                <td><b>{{ $logValue->logType->type_name }}</b></td>
                                <td>{{ $logValue->value }} {{ $_base_units['insulin'] }}</td>
                                <td>{{ $logValue->loggable->label->display_value }}</td>
                                <td>{{ $logValue->loggable->insulinType->display_value }}</td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                            </tr>
                        @endif
                        @if($logValue->logType->type == 'physical_activity')
                            <tr>
                                <td>{{ $logValue->insert_type_string }}</td>
                                <td>{{ $logValue->log_ts->format('H:i') }}</td>
                                <td><b>{{ $logValue->logType->type_name }}</b></td>
                                <td>{{ gmdate('H:i:s', $logValue->value/1000) }}</td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td>{{ $logValue->loggable->calories }} {{ $_base_units['calories'] }}</td>
                                <td>{{ $logValue->loggable->steps }}</td>
                                <td>{{ $logValue->loggable->average_heart_rate }} {{ $_base_units['heart_rate'] }}</td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                            </tr>
                        @endif
                        @if($logValue->logType->type == 'step')
                            <tr>
                                <td>{{ $logValue->insert_type_string }}</td>
                                <td>{{ $logValue->log_ts->format('H:i') }}</td>
                                <td><b>{{ $logValue->logType->type_name }}</b></td>
                                <td>{{ $logValue->value }}</td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                            </tr>
                        @endif
                        @if($logValue->logType->type == 'calorie')
                            <tr>
                                <td>{{ $logValue->insert_type_string }}</td>
                                <td>{{ $logValue->log_ts->format('H:i') }}</td>
                                <td><b>{{ $logValue->logType->type_name }}</b></td>
                                <td>{{ $logValue->value }} {{ $_base_units['calories'] }}</td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                            </tr>
                        @endif
                        @if($logValue->logType->type == 'sleep')
                            <tr>
                                <td>{{ $logValue->insert_type_string }}</td>
                                <td>{{ $logValue->log_ts->format('H:i') }}</td>
                                <td><b>{{ $logValue->logType->type_name }}</b></td>
                                <td>{{ gmdate('H:i:s', $logValue->value/1000) }}</td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td class="table-cell-inactive"></td>
                                <td>{{ gmdate('H:i:s', $logValue->loggable->asleep_mins*60) }}</td>
                                <td>
                                    {{ gmdate('H:i:s', $logValue->loggable->awake_mins*60) }} ({{$logValue->loggable->awake_count}}x)
                                    <br>
                                    {{ gmdate('H:i:s', $logValue->loggable->restless_mins*60) }} ({{$logValue->loggable->restless_count}}x)
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>
		@endforeach
	</body>
</html>