<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style>
			body {
				font-family: DejaVu Sans, sans-serif;
			}
			table {
				font-size: 10px;
				border-collapse: collapse;
			}
			table th, table td {
				padding: 4px;
				border: 1px solid #333;
				background: #fff;
			}
			.desc-indicator-symbol.desc-green {
				color: #00a65a;
			}
			.desc-indicator-symbol.desc-red {
				color: #dd4b39;
			}
			table .desc-indicator-symbol {
				font-size: 13px;
			}
			.table-cell-inactive {
				background: #e6e6e6;
				border: 1px solid #333;
			}
			h1 {
				font-size: 18px;
				margin-top: 15px;
				margin-bottom: 5px;
			}
			h2 {
				font-size: 15px;
				padding-top: 20px !important;
				margin-bottom: 5px;
			}
			.stats-table {
				margin-top: 0;
				margin-bottom: 20px;
			}

			p {
				font-size: 12px;
				margin: 5px 0 12px 0;
			}
			.page-break-before {
				page-break-before: always;
			}
		</style>
	</head>
	<body>
		<h1>Statistika za period:<b> {{ $dateFrom->format(config('application.time.date_formats.default_date')) }} - {{ $dateTo->format(config('application.time.date_formats.default_date')) }}</b></h1>
		
		<div>
			<h2>Razina glukoze</h2>
			@if($stats['glucose']['logged_days'] > 0)
				@if($stats['glucose']['estimated_a1c'] == null)
					<p><i class="icon fa _fa fa-info"></i> Estimacija <b>A1C vrijednosti</b> za odabrani period nije dostupna zbog nedovoljne količine podataka.</p>
				@else
					<p>	
						@include('pdf.health-logs.includes.desc-indicator', [
							'level' => $stats['glucose']['estimated_a1c']['desc'],
							'high' => 1
						])
						Estimirana <b>A1C vrijednost</b> za odabrani period: <b>{{ $stats['glucose']['estimated_a1c']['value'] }}{{$_base_units['a1c']}}</b> (est. cilj. vr.: <b>{{ $stats['glucose']['goal_avgs']['max_a1c'] }}{{$_base_units['a1c']}}</b>).
					</p>
				@endif
				<table style="width: 100%;">
					<tbody>
						<tr>
							<th></th>
							<th>Mjerenja</th>
							<th>Prosjek [<small>{{ $_base_units['bg'] }}</small>]</th>
							<th>Visoka [<small>%</small>]</th>
							<th>Niska [<small>%</small>]</th>
							<th>Normalna [<small>%</small>]</th>
							<th>MIN [<small>{{ $_base_units['bg'] }}</small>]</th>
							<th>MAX [<small>{{ $_base_units['bg'] }}</small>]</th>
						</tr>
						<tr>
							<td>
								<b>Prije obroka</b>
								<div>
									<small>
										Cilj. vr.: <b>{{ $stats['glucose']['goal_avgs']['min_before'] }} - {{ $stats['glucose']['goal_avgs']['max_before'] }}</b> {{ $_base_units['bg'] }}
									</small>
								</div>
							</td>
							<td>{{ $stats['glucose']['before_after']['before_count'] }}</td>
							<td>
								@if($stats['glucose']['before_after']['avg_before'] != null)
									@include('pdf.health-logs.includes.desc-indicator', [
										'level' => $stats['glucose']['before_after']['avg_before']["desc"],
										'high' => 1
									])
									{{ $stats['glucose']['before_after']['avg_before']['value'] }}
								@else
									-
								@endif
							</td>
							<td>{{ $stats['glucose']['before_after']['before_high_percentage'] }}</td>
							<td>{{ $stats['glucose']['before_after']['before_low_percentage'] }}</td>
							<td>{{ $stats['glucose']['before_after']['before_normal_percentage'] }}</td>
							<td>
								@if($stats['glucose']['before_after']['min_before'] != null)
									@include('pdf.health-logs.includes.desc-indicator', [
										'level' => $stats['glucose']['before_after']['min_before']["desc"],
										'high' => 1
									])
									{{ $stats['glucose']['before_after']['min_before']["log"]->value }} 
									<small>{{ $stats['glucose']['before_after']['min_before']["deviation_percentage"] }}</small>
									<div>
										<small>
											{{ $stats['glucose']['before_after']['min_before']["log"]->loggable->label->display_value }}, 
											{{ $stats['glucose']['before_after']['min_before']["log"]->log_ts->format(config('application.time.date_formats.default_date')) }}
										</small>
									</div>
								@else
									-
								@endif
							</td>
							<td>
								@if($stats['glucose']['before_after']['max_before'] != null)
									@include('pdf.health-logs.includes.desc-indicator', [
										'level' => $stats['glucose']['before_after']['max_before']["desc"],
										'high' => 1
									])
									{{ $stats['glucose']['before_after']['max_before']["log"]->value }} 
									<small>{{ $stats['glucose']['before_after']['max_before']["deviation_percentage"] }}</small>
									<div>
										<small>
											{{ $stats['glucose']['before_after']['max_before']["log"]->loggable->label->display_value }}, 
											{{ $stats['glucose']['before_after']['max_before']["log"]->log_ts->format(config('application.time.date_formats.default_date')) }}
										</small>
									</div>
								@else
									-
								@endif
							</td>
						</tr>
						<tr>
							<td>
								<b>Poslije obroka</b>
								<div>
									<small>
										Cilj. vr.: <b>{{ $stats['glucose']['goal_avgs']['min_before'] }} - {{ $stats['glucose']['goal_avgs']['max_after'] }}</b> {{ $_base_units['bg'] }}
									</small>
								</div>
							</td>
							<td>{{ $stats['glucose']['before_after']['after_count'] }}</td>
							<td>
								@if($stats['glucose']['before_after']['avg_after'] != null)
									@include('pdf.health-logs.includes.desc-indicator', [
										'level' => $stats['glucose']['before_after']['avg_after']["desc"],
										'high' => 1
									])
									{{ $stats['glucose']['before_after']['avg_after']['value'] }}
								@else
									-
								@endif
							</td>
							<td>{{ $stats['glucose']['before_after']['after_high_percentage'] }}</td>
							<td>{{ $stats['glucose']['before_after']['after_low_percentage'] }}</td>
							<td>{{ $stats['glucose']['before_after']['after_normal_percentage'] }}</td>
							<td>
								@if($stats['glucose']['before_after']['min_after'] != null)
									@include('pdf.health-logs.includes.desc-indicator', [
										'level' => $stats['glucose']['before_after']['min_after']["desc"],
										'high' => 1
									])
									{{ $stats['glucose']['before_after']['min_after']["log"]->value }} 
									<small>{{ $stats['glucose']['before_after']['min_after']["deviation_percentage"] }}</small>
									<div>
										<small>
											{{ $stats['glucose']['before_after']['min_after']["log"]->loggable->label->display_value }}, 
											{{ $stats['glucose']['before_after']['min_after']["log"]->log_ts->format(config('application.time.date_formats.default_date')) }}
										</small>
									</div>
								@else
									-
								@endif
							</td>
							<td>
								@if($stats['glucose']['before_after']['max_after'] != null)
									@include('pdf.health-logs.includes.desc-indicator', [
										'level' => $stats['glucose']['before_after']['max_after']["desc"],
										'high' => 1
									])
									{{ $stats['glucose']['before_after']['max_after']["log"]->value }} 
									<small>{{ $stats['glucose']['before_after']['max_after']["deviation_percentage"] }}</small>
									<div>
										<small>
											{{ $stats['glucose']['before_after']['max_after']["log"]->loggable->label->display_value }}, 
											{{ $stats['glucose']['before_after']['max_after']["log"]->log_ts->format(config('application.time.date_formats.default_date')) }}
										</small>
									</div>
								@else
									-
								@endif
							</td>
						</tr>
						<tr>
							<td>
								<b>Ukupno</b>
								<div>
									<small>
										Cilj. vr.: <b>{{ $stats['glucose']['goal_avgs']['min_before'] }} - {{ $stats['glucose']['goal_avgs']['max_after'] }}</b> {{ $_base_units['bg'] }}
									</small>
								</div>
							</td>
							<td>{{ $stats['glucose']['total_logs'] }}</td>
							<td>
								@include('pdf.health-logs.includes.desc-indicator', [
									'level' => $stats['glucose']['avg_value']["desc"],
									'high' => 1
								])
								{{ $stats['glucose']['avg_value']['value'] }}
							</td>
							<td>{{ $stats['glucose']['high_percentage'] }}</td>
							<td>{{ $stats['glucose']['low_percentage'] }}</td>
							<td>{{ $stats['glucose']['normal_percentage'] }}</td>
							<td>
								@include('pdf.health-logs.includes.desc-indicator', [
									'level' => $stats['glucose']['min_value']["desc"],
									'high' => 1
								])
								{{ $stats['glucose']['min_value']["log"]->value }} 
								<small>{{ $stats['glucose']['min_value']["deviation_percentage"] }}</small>
								<div>
									<small>
										{{ $stats['glucose']['min_value']["log"]->loggable->label->display_value }}, 
										{{ $stats['glucose']['min_value']["log"]->log_ts->format(config('application.time.date_formats.default_date')) }}
									</small>
								</div>
							</td>
							<td>
								@include('pdf.health-logs.includes.desc-indicator', [
									'level' => $stats['glucose']['max_value']["desc"],
									'high' => 1
								])
								{{ $stats['glucose']['max_value']["log"]->value }} 
								<small>{{ $stats['glucose']['max_value']["deviation_percentage"] }}</small>
								<div>
									<small>
										{{ $stats['glucose']['max_value']["log"]->loggable->label->display_value }}, 
										{{ $stats['glucose']['max_value']["log"]->log_ts->format(config('application.time.date_formats.default_date')) }}
									</small>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			@endif
		</div>
		<div>
			<h2>Doze inzulina</h2>
			@if($stats['insulin']['logged_days'] > 0)
				<p>
					@include('pdf.health-logs.includes.desc-indicator', [
						'level' => $stats['insulin']['daily_avg_units_total']['desc']
					]) 
					Ukupna prosječna <b>dnevna doza inzulina</b> za odabrani period: <b>{{ $stats['insulin']['daily_avg_units_total']['value'] }} {{ $_base_units['insulin'] }}</b> (est. TDD cilj. vr.: <b>{{ $stats['insulin']['goal_avgs']['tdd'] }} {{ $_base_units['insulin'] }}</b>).
				</p>
				<table style="width: 100%;">
					<tbody>
						<tr class="_colspan-row">
							<th></th>
							<th colspan="3">Ukupno</th>
							<th colspan="4">Prosjek [<small>{{ $_base_units['insulin'] }}</small>]</th>
						</tr>
						<tr>
							<th></th>
							<th>Doziranja</th>
							<th>Jedinica [<small>{{ $_base_units['insulin'] }}</small>]</th>
							<th>Jedinica/dan</th>
							<th>Prije obroka</th>
							<th>Poslije obroka</th>
							<th>Prije spavanja</th>
							<th>Ukupno</th>
						</tr>
						<tr>
							<td>
								<b>Bazalni</b>
								<div>
									<small>
										Cilj. vr. (dan): <b>{{ $stats['insulin']['goal_avgs']['basal_total'] }}</b> {{ $_base_units['insulin'] }}
									</small>
								</div>
							</td>
							<td>{{ $stats['insulin']['total_logs_basal'] }}</td>
							<td>{{ $stats['insulin']['total_units_basal'] }}</td>
							<td>
								@include('pdf.health-logs.includes.desc-indicator', [
									'level' => $stats['insulin']['daily_avg_units_basal']["desc"]
								]) 
								{{ $stats['insulin']['daily_avg_units_basal']['value'] }} 
								<small>{{ $stats['insulin']['daily_avg_units_basal']["deviation_percentage"] }}%</small>
							</td>
							<td>{{ $stats['insulin']['basal_before_meal']['avg'] }}</td>
							<td>{{ $stats['insulin']['basal_after_meal']['avg'] }}</td>
							<td>{{ $stats['insulin']['basal_before_bed']['avg'] }}</td>
							<td>{{ $stats['insulin']['avg_units_basal'] }}</td>
						</tr>
						<tr>
							<td>
								<b>Bolus</b>
								<div>
									<small>
										Cilj. vr. (dan): <b>{{ $stats['insulin']['goal_avgs']['bolus_total'] }}</b> {{ $_base_units['insulin'] }}
									</small>
								</div>
							</td>
							<td>{{ $stats['insulin']['total_logs_bolus'] }}</td>
							<td>{{ $stats['insulin']['total_units_bolus'] }}</td>
							<td>
								@include('pdf.health-logs.includes.desc-indicator', [
									'level' => $stats['insulin']['daily_avg_units_bolus']["desc"]
								]) 
								{{ $stats['insulin']['daily_avg_units_bolus']['value'] }}
								<small>{{ $stats['insulin']['daily_avg_units_bolus']["deviation_percentage"] }}%</small>
							</td>
							<td>{{ $stats['insulin']['bolus_before_meal']['avg'] }}</td>
							<td>{{ $stats['insulin']['bolus_after_meal']['avg'] }}</td>
							<td>{{ $stats['insulin']['bolus_before_bed']['avg'] }}</td>
							<td>{{ $stats['insulin']['avg_units_bolus'] }}</td>
						</tr>
					</tbody>
				</table>
			@endif
		</div>
		<div>
			<h2>Fizička aktivnost</h2>
			@if($stats['physical_activity']['logged_days'] > 0)
				<table style="width: 100%;">
				<tbody>
					<tr class="_colspan-row">
						<th></th>
						<th colspan="5">Ukupno</th>
						<th colspan="4">Prosjek po aktivnosti</th>
					</tr>
					<tr>
						<th></th>
						<th>Mj. dana</th>
						<th>Trajanje</th>
						<th>Kalorije</th>
						<th>Koraci</th>
						<th>Trajanje/dan</th>
						<th>Trajanje</th>
						<th>Kalorije</th>
						<th>Koraci</th>
						<th>Otkucaji srca</th>
					</tr>
					<tr>
						<td>
							<b>Aktivnost</b>
							<div>
								<small>
									Cilj. vr. (dan): <b>{{ hmsFromSec($stats['physical_activity']['goal_avgs']['min_daily_excercise']/1000) }}</b>
								</small>
							</div>
							<div>
								<small>
									Cilj. vr. (period): <b>{{ hmsFromSec($stats['physical_activity']['goal_avgs']['period_min_sum']/1000) }}</b>
								</small>
							</div>
						</td>
						<td>{{ $stats['physical_activity']['logged_days'] }}</td>
						<td>
							@include('pdf.health-logs.includes.desc-indicator', [
								'level' => $stats['physical_activity']['total_duration']["desc"],
								'high' => 0
							])
							{{ hmsFromSec($stats['physical_activity']['total_duration']['value']/1000) }}
							<small>{{ $stats['physical_activity']['total_duration']["deviation_percentage"] }}%</small>
						</td>
						<td>{{ $stats['physical_activity']['total_calories'] }}</td>
						<td>{{ $stats['physical_activity']['total_steps'] }}</td>
						<td>
							{{ hmsFromSec($stats['physical_activity']['daily_avg_duration']/1000) }}
						</td>
						<td>{{ hmsFromSec($stats['physical_activity']['avg_duration']/1000) }}</td>
						<td>{{ $stats['physical_activity']['avg_calories'] }}</td>
						<td>{{ $stats['physical_activity']['avg_steps'] }}</td>
						<td>{{ $stats['physical_activity']['avg_heart_rate'] }}</td>
					</tr>
				</tbody>
			</table>
			@endif
		</div>
		<div class="page-break-before">
			<div style="display: inline-block; width: 48%; vertical-align: top;">
				<h2>Kretanje</h2>
				@if($stats['step']['logged_days'] > 0)
					<table style="width: 100%;">
						<tbody>
							<tr>
								<th></th>
								<th>Ukupno</th>
								<th>Prosjek/dan</th>
								<th>MAX dan</th>
							</tr>
							<tr>
								<td>
									Kretanje
									<div>
										<small>
											Cilj. vr. (dan): <b>{{ $stats['step']['goal_avgs']['min_daily_steps'] }}</b> koraci
										</small>
									</div>
								</td>
								<td>{{ $stats['step']['total_steps'] }}</td>
								<td>
									@include('pdf.health-logs.includes.desc-indicator', [
										'level' => $stats['step']['daily_avg_steps']["desc"],
										'high' => 0
									])
									{{ $stats['step']['daily_avg_steps']['value'] }} 
									<small>{{ $stats['step']['daily_avg_steps']["deviation_percentage"] }}%</small>
								</td>
								<td>
									@include('pdf.health-logs.includes.desc-indicator', [
										'level' => $stats['step']['day_max_steps']["desc"],
										'high' => 0
									])
									{{ $stats['step']['day_max_steps']['value'] }} 
									<small>{{ $stats['step']['day_max_steps']["deviation_percentage"] }}%</small>
									<div>
										<small>
											{{ $stats['step']['day_max_steps']['date']->format(config('application.time.date_formats.default_date')) }}
										</small>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				@endif
			</div>
			<div style="display: inline-block; width: 50%; vertical-align: top; padding-left: 1%;">
				<h2>Kalorije</h2>
				@if($stats['calorie']['logged_days'] > 0)
					<table style="width: 100%;">
						<tbody>
							<tr>
								<th>Ukupno [<small>{{ $_base_units['calories'] }}</small>]</th>
								<th>Prosjek/dan [<small>{{ $_base_units['calories'] }}</small>]</th>
								<th>MAX [<small>{{ $_base_units['calories'] }}</small>]</th>
							</tr>
							<tr>
								<td>{{ $stats['calorie']['total_calories'] }}</td>
								<td>{{ $stats['calorie']['daily_avg_calories']['value'] }}</td>
								<td>
									{{ $stats['calorie']['day_max_calories']['value'] }} 
									<div>
										<small>
											{{ $stats['calorie']['day_max_calories']['date']->format(config('application.time.date_formats.default_date')) }}
										</small>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				@endif
			</div>
		</div>
		<div>
			<h2>San</h2>
			@if($stats['sleep']['logged_days'] > 0)
				<table style="width: 100%;">
					<tbody>
						<tr class="_colspan-row">
							<th colspan="6">Ukupno</th>
							<th colspan="4">Prosjek po snu</th>
						</tr>
						<tr>
							<th>Mj. dana</th>
							<th>Trajanje sna</th>
							<th>Spavanje</th>
							<th>Buđenje</th>
							<th>Nemirnost</th>
							<th>Trajanje/dan</th>
							<th>Trajanje</th>
							<th>Spavanje</th>
							<th>Buđenje</th>
							<th>Nemirnost</th>
						</tr>
						<tr>
							<td>{{ $stats['sleep']['logged_days'] }}</td>
							<td>
								{{ hmsFromSec($stats['sleep']['total_duration']['value']/1000) }}
							</td>
							<td>{{ hmsFromSec($stats['sleep']['asleep']['total_mins']*60) }}</td>
							<td>{{ hmsFromSec($stats['sleep']['awake']['total_mins']*60) }} ({{$stats['sleep']['awake']['total_count']}}x)</td>
							<td>{{ hmsFromSec($stats['sleep']['restless']['total_mins']*60) }} ({{$stats['sleep']['restless']['total_count']}}x)</td>
							<td>
								{{ hmsFromSec($stats['sleep']['daily_avg_duration']/1000) }}
							</td>
							<td>
								{{ hmsFromSec($stats['sleep']['avg_duration']/1000) }}
							</td>
							<td>{{ hmsFromSec($stats['sleep']['asleep']['avg_mins']*60) }}</td>
							<td>{{ hmsFromSec($stats['sleep']['awake']['avg_mins']*60) }} ({{$stats['sleep']['awake']['avg_count']}}x)</td>
							<td>{{ hmsFromSec($stats['sleep']['restless']['avg_mins']*60) }} ({{$stats['sleep']['restless']['avg_count']}}x)</td>
						</tr>
					</tbody>
				</table>
			@endif
		</div>
	</body>
</html>