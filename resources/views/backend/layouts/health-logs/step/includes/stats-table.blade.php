<table class="table table-bordered _table-v-middle _margin-b-0 _table-tiny-pad">
	<tbody>
		<tr>
			<th></th>
			<th>Ukupno</th>
			<th>Prosjek/dan</th>
			<th>MAX dan</th>
		</tr>
		<tr>
			<th>
				Kretanje
				<div class="_lh1 _fw-clear">
					<small class="text-muted">
						Cilj. vr. (dan): <b>{{ $stats['goal_avgs']['min_daily_steps'] }}</b> koraci
					</small>
				</div>
			</th>
			<td>{{ $stats['total_steps'] }}</td>
			<td>
				@include('backend.layouts.health-logs.includes.step-indicator', ['level' => $stats['daily_avg_steps']["desc"]])
				{{ $stats['daily_avg_steps']['value'] }} 
				<small class="text-purple _margin-l-6">{{ $stats['daily_avg_steps']["deviation_percentage"] }}%</small>
			</td>
			<td>
				@include('backend.layouts.health-logs.includes.step-indicator', ['level' => $stats['day_max_steps']["desc"]])
				{{ $stats['day_max_steps']['value'] }} 
				<small class="text-purple _margin-l-6">{{ $stats['day_max_steps']["deviation_percentage"] }}%</small>
				<div class="_lh1">
					<small class="text-muted">
						{{ $stats['day_max_steps']['date']->format(config('application.time.date_formats.default_date')) }}
					</small>
				</div>
			</td>
		</tr>
	</tbody>
</table>