<table class="table table-bordered _table-v-middle _margin-b-0 _table-tiny-pad">
	<tbody>
		<tr class="_colspan-row">
			<th colspan="6">Ukupno</th>
			<th colspan="4">Prosjek po snu</th>
		</tr>
		<tr>
			<th>Mj. dana</th>
			<th>Trajanje sna</th>
			<th>Spavanje</th>
			<th>Buđenje</th>
			<th>Nemirnost</th>
			<th>Trajanje/dan</th>
			<th>Trajanje</th>
			<th>Spavanje</th>
			<th>Buđenje</th>
			<th>Nemirnost</th>
		</tr>
		<tr>
			<td>{{ $stats['logged_days'] }}</td>
			<td>
				{{ hmsFromSec($stats['total_duration']['value']/1000) }}
			</td>
			<td>{{ hmsFromSec($stats['asleep']['total_mins']*60) }}</td>
			<td>{{ hmsFromSec($stats['awake']['total_mins']*60) }} ({{$stats['awake']['total_count']}}x)</td>
			<td>{{ hmsFromSec($stats['restless']['total_mins']*60) }} ({{$stats['restless']['total_count']}}x)</td>
			<td>
				{{ hmsFromSec($stats['daily_avg_duration']/1000) }}
			</td>
			<td>
				{{ hmsFromSec($stats['avg_duration']/1000) }}
			</td>
			<td>{{ hmsFromSec($stats['asleep']['avg_mins']*60) }}</td>
			<td>{{ hmsFromSec($stats['awake']['avg_mins']*60) }} ({{$stats['awake']['avg_count']}}x)</td>
			<td>{{ hmsFromSec($stats['restless']['avg_mins']*60) }} ({{$stats['restless']['avg_count']}}x)</td>
		</tr>
	</tbody>
</table>