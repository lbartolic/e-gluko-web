<script id="weight-log-create-hb" type="text/x-handlebars-template">
	<h3 class="_section-title _margin-t-0">Tjelesna masa</h3>
	<form>
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label>Masa [{{data.units.weight}}]</label>
                    <input class="form-control input-lg" type="number" step="0.1" placeholder="vrijednost" name="value">
            	</div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label>Datum i vrijeme</label>
                    <div id="log-create-datetime" class='input-group input-group-lg date'>
						<input type='text' class="form-control" placeholder="datum i vrijeme" name="log_ts" onkeydown="return false;"/>
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label>Opseg struka [{{data.units.waist}}]</label>
                    <input class="form-control input-lg" type="number" step="0.1" placeholder="vrijednost" name="waist">
            	</div>
            </div>
        </div>
    </form>
</script>