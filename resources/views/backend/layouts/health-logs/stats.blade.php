@extends ('backend.layouts.app')

@section ('title', 'Pacijenti')

@section('after-styles')
@endsection

@section('page-header')
<h1>
	Statistika
	<small>Luka Bartolić</small>
</h1>
@endsection

@section('content')
<div class="_flex">
	<div class="_margin-center-horiz">
		<div class="box box-solid">
			<div class="box-body">
				<div class="form-group _date-range-pickers">
					<big>Odabir perioda:</big>
					<a href="{{ route('admin.health-logs.stats.show', [$user->id, $prev_from, $prev_to]) }}">
						<button class="btn btn-sm btn-default _pickers-arrow-btn">
							<i class="fa fa-caret-left"></i>
						</button>
					</a>
					<div class='input-group input-group-sm date' id='stats-date-from-picker'>
						<input type='text' class="form-control" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					<div class="_pickers-divider pull-left">do</div>
					<div class="input-group input-group-sm date" id="stats-date-to-picker">
						<input type="text" class="form-control">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					<a href="{{ route('admin.health-logs.stats.show', [$user->id, $next_from, $next_to]) }}">
						<button class="btn btn-sm btn-default _pickers-arrow-btn">
							<i class="fa fa-caret-right"></i>
						</button>
					</a>
					<button class="btn btn-primary btn-sm" id="stats-dates-btn">Prikaži</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
		<div class="box-group" id="logs-collapse-holder">
			<div class="panel box box-default _margin-b-0">
				<div class="box-header with-border">
					<h4 class="box-title">
						<a data-toggle="collapse" data-parent="#logs-collapse-holder" href="#logs-goals-collapse" aria-expanded="true" class="collapsed">
							Ciljne vrijednosti
						</a>
					</h4>
				</div>
				<div id="logs-goals-collapse" class="panel-collapse collapse" aria-expanded="true">
					@include('backend.layouts.health-goals.includes.tabbed-content-goals', [
						'healthGoals' => $healthGoals
					])
				</div>
			</div>
			<div class="panel box box-default">
				<div class="box-header with-border">
					<h4 class="box-title">
						<a data-toggle="collapse" data-parent="#logs-collapse-holder" href="#logs-export-collapse" aria-expanded="false" class="collapsed">
							Opcije izvoza podataka
						</a>
					</h4>
				</div>
				<div id="logs-export-collapse" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
					<div class="nav-tabs-custom _margin-b-0">
						<div class="tab-content">
							<div class="_flex">
                                <div class="_margin-center-horiz clearfix">
                                    <div class="pull-left">
                                        <div class="form-group _date-range-pickers">
                                            <div class="pull-left _margin-l-6 _data-export-types-holder">
                                                <button type="button" class="btn btn-sm bg-purple active _margin-r-6 text-uppercase">PDF</button>
                                            </div>
                                            <div class="input-group input-group-sm date _margin-l-6 _margin-r-6" id="data-export-date-from">
                                                <input type="text" class="form-control">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                            <div class="_pickers-divider pull-left">do</div>
                                            <div class="input-group input-group-sm date _margin-l-6 _margin-r-6" id="data-export-date-to">
                                                <input type="text" class="form-control">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                            <button class="btn btn-primary btn-sm _margin-l-6 _margin-r-6" id="data-export-btn" data-user="2">Preuzmi</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<h2 class="_sub-title _margin-t-0">Prikaz podataka za period:<b> {{ $dateFrom->format(config('application.time.date_formats.default_date')) }} - {{ $dateTo->format(config('application.time.date_formats.default_date')) }}</b></h2>
		
		<div class="row">
			<div class="col-sm-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Razina glukoze</h3>
					</div>
					<div class="box-body">
						@if($stats['glucose']['logged_days'] > 0)
							@include('backend.layouts.health-logs.includes.a1c-estimated-info', [
								'a1cValue' => $stats['glucose']['estimated_a1c']['value'],
								'a1cGoal' => $stats['glucose']['goal_avgs']['max_a1c'],
								'a1cLevel' => $stats['glucose']['estimated_a1c']['desc']
							])
							<div class="table table-responsive _margin-b-0">
								@include('backend.layouts.health-logs.glucose.includes.stats-table', [
									'stats' => $stats['glucose']
								])
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Doze inzulina</h3>
					</div>
					<div class="box-body">
						@if($stats['insulin']['logged_days'] > 0)
							@include('backend.layouts.health-logs.includes.tdd-estimated-info', [
								'totalDose' => $stats['insulin']['daily_avg_units_total']['value'],
								'doseLevel' => $stats['insulin']['daily_avg_units_total']['desc'],
								'tddGoal' => $stats['insulin']['goal_avgs']['tdd']
							])
							<div class="table table-responsive _margin-b-0">
								@include('backend.layouts.health-logs.insulin.includes.stats-table', [
									'stats' => $stats['insulin']
								])
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Fizička aktivnost</h3>
					</div>
					<div class="box-body">
						@if($stats['physical_activity']['logged_days'] > 0)
							<div class="table table-responsive _margin-b-0">
								@include('backend.layouts.health-logs.physical-activity.includes.stats-table', [
									'stats' => $stats['physical_activity']
								])
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Kretanje</h3>
					</div>
					<div class="box-body">
						@if($stats['step']['logged_days'] > 0)
							<div class="table table-responsive _margin-b-0">
								@include('backend.layouts.health-logs.step.includes.stats-table', [
									'stats' => $stats['step']
								])
							</div>
						@endif
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Kalorije</h3>
					</div>
					<div class="box-body">
						@if($stats['calorie']['logged_days'] > 0)
							<div class="table table-responsive _margin-b-0">
								@include('backend.layouts.health-logs.calorie.includes.stats-table', [
									'stats' => $stats['calorie']
								])
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">San</h3>
					</div>
					<div class="box-body">
						@if($stats['sleep']['logged_days'] > 0)
							<div class="table table-responsive _margin-b-0">
								@include('backend.layouts.health-logs.sleep.includes.stats-table', [
									'stats' => $stats['sleep']
								])
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="btn-group btn-group-justified">
				<div class="btn-group">
					<a href="{{ route('admin.health-logs.stats.show', [$user->id, $prev_from, $prev_to]) }}" class="btn btn-default btn-lg"><i class="fa fa-angle-left _margin-r-6"></i> Prošli period</a>
				</div>
				<div class="btn-group">
					<a href="{{ route('admin.health-logs.stats.show', [$user->id, $next_from, $next_to]) }}" class="btn btn-default btn-lg">Sljedeći period <i class="fa fa-angle-right _margin-l-6"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('after-scripts')
<script>
	$(document).ready(function() {
		var dateFrom = {!! json_encode($dateFrom->format('Y-m-d')) !!};
		var dateTo = {!! json_encode($dateTo->format('Y-m-d')) !!};
		$('#stats-date-from-picker').datetimepicker({
	        format: eGlukoData.def_date_format,
	        allowInputToggle: true,
	        locale: 'hr'
	    });
		$('#stats-date-to-picker').datetimepicker({
	        format: eGlukoData.def_date_format,
	        allowInputToggle: true,
	        locale: 'hr'
	    });
		$('#stats-date-from-picker').data("DateTimePicker").date(moment(dateFrom, eGlukoData.db_date_format));
		$('#stats-date-to-picker').data("DateTimePicker").date(moment(dateTo, eGlukoData.db_date_format));
		$('#stats-dates-btn').click(function() {
			var dateFrom = moment($('#stats-date-from-picker').data("date"), eGlukoData.def_date_format);
			var dateTo = moment($('#stats-date-to-picker').data("date"), eGlukoData.def_date_format);
			console.log(dateFrom, dateTo);
			window.location = APP_URL + "/admin/patients/2/health-logs/stats/" + dateFrom.format(eGlukoData.db_date_format) + "/" + dateTo.format(eGlukoData.db_date_format);
		});
        
		$('#data-export-date-from, #data-export-date-to').datetimepicker({
	        format: eGlukoData.def_date_format,
	        allowInputToggle: true,
	        locale: 'hr'
	    });
		$('#data-export-date-from').data("DateTimePicker").date(moment(dateFrom, eGlukoData.db_date_format));
		$('#data-export-date-to').data("DateTimePicker").date(moment(dateTo, eGlukoData.db_date_format));
        $('#data-export-btn').click(function() {
			var dateFrom = moment($('#data-export-date-from').data("date"), eGlukoData.def_date_format).format(eGlukoData.db_date_format);
			var dateTo = moment($('#data-export-date-to').data("date"), eGlukoData.def_date_format).format(eGlukoData.db_date_format);
            var userId = $(this).attr("data-user");
			window.location = APP_URL + "/admin/patients/" + userId + "/health-logs/stats/" + dateFrom + "/" + dateTo + "/pdf";
		});
	});
</script>
@endsection
