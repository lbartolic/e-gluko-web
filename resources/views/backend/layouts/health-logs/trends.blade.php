@extends ('backend.layouts.app')

@section ('title', 'Pacijenti')

@section('after-styles')
@endsection

@section('page-header')
<h1>
	Trendovi
	<small>Luka Bartolić</small>
</h1>
@endsection


@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="nav-tabs-custom _margin-b-0">
					<ul class="nav nav-tabs">
						<li class="{{ ($type == 'day-view') ? 'active' : '' }} _trends-type-tab" style="width: 50%; margin: 0;" data-trends-type="day-view">
							<a class="text-center" href="{{ route('admin.health-logs.trends.show', [$user->id, 'day-view', $dateFrom->toDateString(), $period]) }}">
								Dnevni pregled kroz tjedan
							</a>
						</li>
						<li class="{{ ($type == 'period') ? 'active' : '' }} _trends-type-tab" style="width: 50%; margin: 0;" data-trends-type="period">
							<a class="text-center" href="{{ route('admin.health-logs.trends.show', [$user->id, 'period', $dateFrom->toDateString(), $period]) }}">
								Vremenski pregled
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="_charts-options-holder _flex">
			<div class="_margin-center-horiz clearfix">
				<div class="pull-left">
					<div class="form-group _date-range-pickers">
						<a href="#">
							<a class="btn btn-sm btn-default _pickers-arrow-btn" href="{{ route('admin.health-logs.trends.show', [$user->id, $type, $prevDateFrom, $period]) }}">
								<i class="fa fa-caret-left"></i>
							</a>
						</a>
                        <div class="pull-left _period-btns-holder _margin-l-6">
                            <button type="button" class="btn btn-sm {{ ($period == '1w') ? 'bg-purple active' : 'bg-default' }} _margin-r-6 text-uppercase" data-period="1w">1t</button>
                            <button type="button" class="btn btn-sm {{ ($period == '1m') ? 'bg-purple active' : 'bg-default' }} _margin-r-6 text-uppercase" data-period="1m">1m</button>
                            <button type="button" class="btn btn-sm {{ ($period == '3m') ? 'bg-purple active' : 'bg-default' }} _margin-r-6 text-uppercase" data-period="3m">3m</button>
                        </div>
						<div class="_pickers-divider pull-left _margin-l-6 _margin-r-6">od</div>
						<div class='input-group input-group-sm date _margin-l-6 _margin-r-6' id='trends-date-from'>
							<input type='text' class="form-control" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
                        <button class="btn btn-primary btn-sm _margin-l-6 _margin-r-6" id="trends-period-btn" data-user="{{ $user->id }}">Prikaži</button>
						<a href="#">
							<a class="btn btn-sm btn-default _pickers-arrow-btn" href="{{ route('admin.health-logs.trends.show', [$user->id, $type, $nextDateFrom, $period]) }}">
								<i class="fa fa-caret-right"></i>
							</a>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
                <div class="text-center _margin-b-10">
                    <span class="text-muted text-uppercase text-center"><i class="fa _fa fa-info-circle"></i> Odabran period:<b> {{ $dateFrom->format(config('application.time.date_formats.default_date')) }} - {{ $dateTo->format(config('application.time.date_formats.default_date')) }}</b></span>
                </div>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Analiza</h3>
					</div>
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab_1" data-toggle="tab">Glukoza u krvi</a></li>
							<li><a href="#tab_2" data-toggle="tab">Inzulin doze</a></li>
							<li><a href="#tab_3" data-toggle="tab">Kretanje i kalorije</a></li>
							<li><a href="#tab_4" data-toggle="tab">Fizička aktivnost</a></li>
							<li><a href="#tab_5" data-toggle="tab">San</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1">
								<div class="row">
									<div class="col-xs-12">
										@if($glucoseLogsList['stats']['logged_days'] > 0)
											@include('backend.layouts.health-logs.includes.a1c-estimated-info', [
												'a1cValue' => $glucoseLogsList['stats']['estimated_a1c']['value'],
												'a1cGoal' => $glucoseLogsList['stats']['goal_avgs']['max_a1c'],
												'a1cLevel' => $glucoseLogsList['stats']['estimated_a1c']['desc']
											])
											@if($type == 'period')
												@include('backend.layouts.health-logs.glucose.includes.stats-table', [
													'stats' => $glucoseLogsList['stats']
												])
											@endif
										@endif
										@if($type == 'day-view')
											@include('backend.layouts.health-logs.includes.week-days-stats', [
												'stats' => $glucoseLogsList['std_week_days_stats']['glucose'],
												'units' => $_base_units['bg'],
												'indicatorPath' => 'backend.layouts.health-logs.includes.bg-indicator',
												'indicatorLevelKey' => 'bgLevel'
											])
										@endif
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab_2">
								<div class="row">
									<div class="col-xs-12">
										@if($insulinLogsList['stats']['logged_days'] > 0)
											@include('backend.layouts.health-logs.includes.tdd-estimated-info', [
												'totalDose' => $insulinLogsList['stats']['daily_avg_units_total']['value'],
												'doseLevel' => $insulinLogsList['stats']['daily_avg_units_total']['desc'],
												'tddGoal' => $insulinLogsList['stats']['goal_avgs']['tdd']
											])
											@if($type == 'period')
												@include('backend.layouts.health-logs.insulin.includes.stats-table', [
													'stats' => $insulinLogsList['stats']
												])
											@endif
										@endif
										@if($type == 'day-view')
											@include('backend.layouts.health-logs.includes.week-days-stats', [
												'stats' => [
													[
														'stats' => $insulinLogsList['std_week_days_stats']['basal'],
														'title' => 'Bazalne doze',
														'units' => $_base_units['insulin'],
														'indicatorPath' => 'backend.layouts.health-logs.includes.insulin-indicator',
														'indicatorLevelKey' => 'doseLevel'
													], [
														'stats' => $insulinLogsList['std_week_days_stats']['bolus'],
														'title' => 'Bolus doze',
														'units' => $_base_units['insulin'],
														'indicatorPath' => 'backend.layouts.health-logs.includes.insulin-indicator',
														'indicatorLevelKey' => 'doseLevel'
													]
												]
											])
										@endif
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab_3">
								<div class="row">
									<div class="col-xs-12">
										@if($stepLogsList['stats']['logged_days'] > 0)
											@if($type == 'period')
												@include('backend.layouts.health-logs.includes.step-calorie-stats-table', [
													'statsStep' => $stepLogsList['stats'],
													'statsCalorie' => $calorieLogsList['stats']
												])
											@endif
										@endif
										@if($type == 'day-view')
											@include('backend.layouts.health-logs.includes.week-days-stats', [
												'stats' => [
													[
														'stats' => $stepLogsList['std_week_days_stats']['step'],
														'title' => 'Kretanje',
														'units' => 'koraci',
														'indicatorPath' => 'backend.layouts.health-logs.includes.step-indicator',
														'indicatorLevelKey' => 'level'
													], [
														'stats' => $calorieLogsList['std_week_days_stats']['calorie'],
														'title' => 'Kalorije',
														'units' => $_base_units['calories'],
														'indicatorPath' => null,
														'indicatorLevelKey' => null
													]
												]
											])
										@endif
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab_4">
								<div class="row">
									<div class="col-xs-12">
										@if($physicalActivityLogsList['stats']['logged_days'] > 0)
											@if($type == 'period')
												@include('backend.layouts.health-logs.physical-activity.includes.stats-table', [
													'stats' => $physicalActivityLogsList['stats']
												])
											@endif
										@endif
										@if($type == 'day-view')
											@include('backend.layouts.health-logs.includes.week-days-stats', [
												'stats' => $physicalActivityLogsList['std_week_days_stats']['activity'],
												'units' => 'min',
												'indicatorPath' => 'backend.layouts.health-logs.includes.step-indicator',
												'indicatorLevelKey' => 'level'
											])
										@endif
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab_5">
								<div class="row">
									<div class="col-xs-12">
										@if($sleepLogsList['stats']['logged_days'] > 0)
											@if($type == 'period')
												@include('backend.layouts.health-logs.sleep.includes.stats-table', [
													'stats' => $sleepLogsList['stats']
												])
											@endif
										@endif
										@if($type == 'day-view')
											@include('backend.layouts.health-logs.includes.week-days-stats', [
												'stats' => $sleepLogsList['std_week_days_stats']['sleep'],
												'units' => 'min',
												'indicatorPath' => null,
												'indicatorLevelKey' => null
											])
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid" id="trends-box">
					<div class="box-body">
						<div id="glucose-logs-chart" width="100%"></div>
						<div id="insulin-logs-chart" width="100%"></div>
						<div id="activity-logs-chart" width="100%"></div>
						<div id="step-calorie-logs-chart" width="100%"></div>
						<div id="sleep-logs-chart" width="100%"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('after-scripts')
<script>
	$(document).ready(function() {
		var data = {
			'type': {!! json_encode($type) !!},
			'date_from': {!! json_encode($dateFrom->toDateString()) !!},
			'date_to': {!! json_encode($dateTo->toDateString()) !!},
			'period': {!! json_encode($period) !!},
			'glucose_logs_list': {!! json_encode($glucoseLogsList) !!},
			'insulin_logs_list': {!! json_encode($insulinLogsList) !!},
			'step_logs_list': {!! json_encode($stepLogsList) !!},
			'calorie_logs_list': {!! json_encode($calorieLogsList) !!},
			'activity_logs_list': {!! json_encode($physicalActivityLogsList) !!},
			'sleep_logs_list': {!! json_encode($sleepLogsList) !!}
		};
		setupTrendCharts(data);
        
        $('._period-btns-holder button').click(function() {
            $('._period-btns-holder button').removeClass('bg-purple active').addClass('bg-default');
            $(this).addClass('bg-purple active');
        });
        $('#trends-period-btn').click(function() {
			var dateFrom = moment($('#trends-date-from').data("date"), eGlukoData.def_date_format);
            var period = $('._period-btns-holder button.active').attr('data-period');
            var userId = $(this).attr("data-user");
            var trendsType = $('._trends-type-tab.active').attr('data-trends-type');
			console.log(dateFrom.format(eGlukoData.db_date_format), period, userId, trendsType);
			window.location = APP_URL + "/admin/patients/" + userId + "/health-logs/trends/" + trendsType + "/" + dateFrom.format(eGlukoData.db_date_format) + "/" + period;
		});
	});
</script>
@endsection
