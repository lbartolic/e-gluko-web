<table class="table table-bordered _table-v-middle _margin-b-0 _table-tiny-pad">
	<tbody>
		<tr>
			<th></th>
			<th>Ukupno</th>
			<th>Prosjek/dan</th>
			<th>MAX dan</th>
		</tr>
		<tr>
			<th style="box-shadow: inset 6px 0 0 rgb(230, 126, 34)">
				<span class="_margin-l-6">Kretanje</span>
				<div class="_margin-l-6 _lh1 _fw-clear">
					<small class="text-muted">
						Cilj. vr. (dan): <b>{{ $statsStep['goal_avgs']['min_daily_steps'] }}</b> koraci
					</small>
				</div>
			</th>
			<td>{{ $statsStep['total_steps'] }}</td>
			<td>
				@include('backend.layouts.health-logs.includes.step-indicator', ['level' => $statsStep['daily_avg_steps']["desc"]])
				{{ $statsStep['daily_avg_steps']['value'] }} 
				<small class="text-purple _margin-l-6">{{ $statsStep['daily_avg_steps']["deviation_percentage"] }}%</small>
			</td>
			<td>
				@include('backend.layouts.health-logs.includes.step-indicator', ['level' => $statsStep['day_max_steps']["desc"]])
				{{ $statsStep['day_max_steps']['value'] }} 
				<small class="text-purple _margin-l-6">{{ $statsStep['day_max_steps']["deviation_percentage"] }}%</small>
				<div class="_lh1">
					<small class="text-muted">
						{{ $statsStep['day_max_steps']['date']->format(config('application.time.date_formats.default_date')) }}
					</small>
				</div>
			</td>
		</tr>
		<tr>
			<th style="box-shadow: inset 6px 0 0 rgb(26, 188, 156)">
				<span class="_margin-l-6">Kalorije</span>
			</th>
			<td>{{ $statsCalorie['total_calories'] }}</td>
			<td>{{ $statsCalorie['daily_avg_calories']['value'] }}</td>
			<td>
				{{ $statsCalorie['day_max_calories']['value'] }} 
				<div class="_lh1">
					<small class="text-muted">
						{{ $statsCalorie['day_max_calories']['date']->format(config('application.time.date_formats.default_date')) }}
					</small>
				</div>
			</td>
		</tr>
	</tbody>
</table>