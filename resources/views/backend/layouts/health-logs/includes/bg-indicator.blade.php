@if($bgLevel == 'low')
	<i class="fa fa-exclamation-triangle {{ (isset($colored) && $colored == false) ? '' : 'text-orange' }} _fa"></i>
@elseif($bgLevel == 'high')
	<i class="fa fa-exclamation-triangle {{ (isset($colored) && $colored == false) ? '' : 'text-red' }} _fa"></i>
@elseif($bgLevel == 'normal')
	<i class="fa fa-check-square-o {{ (isset($colored) && $colored == false) ? '' : 'text-green' }} _fa"></i>
@endif