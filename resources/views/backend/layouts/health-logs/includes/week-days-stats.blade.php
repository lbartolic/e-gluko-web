@if(isset($stats[0]['stats']))
	<div class="nav-tabs-custom _margin-b-0 _tabs-slim _no-shadow">
		<ul class="nav nav-tabs">
			@foreach($stats as $sKey => $sValue)
				<li class="{{ ($loop->first) ? 'active' : '' }}">
					<a href="#day-view-stats-tab-{{ str_slug($sValue['title']) }}" data-toggle="tab" aria-expanded="{{ ($loop->first) ? 'true' : '' }}">
						{{ $sValue['title'] }}
					</a>
				</li>
			@endforeach
		</ul>
		<div class="tab-content _full">
			@foreach($stats as $sKey => $sValue)
				<div class="tab-pane {{ ($loop->first) ? 'active' : '' }}" id="day-view-stats-tab-{{ str_slug($sValue['title']) }}">
					@include('backend.layouts.health-logs.includes.week-days-stats-table', [
						'stats' => $sValue["stats"],
						'units' => $sValue["units"],
						'indicatorPath' => $sValue["indicatorPath"],
						'indicatorLevelKey' => $sValue["indicatorLevelKey"]
					])
				</div>
			@endforeach
		</div>
	</div>
@else
	@if($stats != null && isset($stats['type']))
		@include('backend.layouts.health-logs.includes.week-days-stats-table', [
			'stats' => $stats
		])
	@endif
@endif