@if($level == 'low')
	<i class="fa fa-arrow-down _fa text-orange"></i>
@elseif($level == 'high')
	<i class="fa fa-arrow-up _fa text-green"></i>
@elseif($level == 'normal')
	<i class="fa fa-dot-circle-o _fa text-green"></i>
@endif