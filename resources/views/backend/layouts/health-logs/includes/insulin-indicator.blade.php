@if($doseLevel == 'low')
	<i class="fa fa-arrow-down _fa"></i>
@elseif($doseLevel == 'high')
	<i class="fa fa-arrow-up _fa"></i>
@elseif($doseLevel == 'normal')
	<i class="fa fa-dot-circle-o _fa"></i>
@endif