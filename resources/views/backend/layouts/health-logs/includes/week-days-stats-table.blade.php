<div class="table table-responsive _margin-b-0">
	<table class="table table-bordered _table-v-middle _margin-b-0 _table-tiny-pad">
		<tbody>
			<tr>
				<th></th>
				<th>Ponedjeljak</th>
				<th>Utorak</th>
				<th>Srijeda</th>
				<th>Četvrtak</th>
				<th>Petak</th>
				<th>Subota</th>
				<th>Nedjelja</th>
			</tr>
			<tr>
				<th>
					Prosjek
					@if(isset($stats['avg_goal_min']) && isset($stats['avg_goal_max']))
						<div class="_lh1 _fw-clear">
							<small class="text-muted">
								Cilj. vr.: <b>
								@if($stats['avg_goal_min'] == $stats['avg_goal_max'])
									{{-- find better way to format time for values that need formatting --}}
									@if ($units == 'min')
										{{ hmsFromSec($stats['avg_goal_min']/1000) }}
									@else
										{{ $stats['avg_goal_min'] }}
									@endif
								@else
									{{ $stats['avg_goal_min'] }} - {{ $stats['avg_goal_max'] }}
								@endif
								</b> {{ $units }}
							</small>
						</div>
					@endif
				</th>
				@foreach($stats['days'] as $key => $value)
				<td>
					@if($value['day_avg_value'] != null)
						@if($indicatorPath != null)
							@include($indicatorPath, [$indicatorLevelKey => $value['day_avg_value']["desc"]])
						@endif
						{{-- find better way to format time for values that need formatting --}}
						@if ($units == 'min')
							{{ hmsFromSec($value['day_avg_value']['value']/1000) }}
						@else
							{{ $value['day_avg_value']['value'] }}
						@endif
					@else
						-
					@endif
				</td>
				@endforeach
			</tr>
			<tr>
				<th>
					MIN
					@if(isset($stats['avg_goal_min']) && isset($stats['avg_goal_max']))
						<div class="_lh1 _fw-clear">
							<small class="text-muted">
								Cilj. vr.: <b>
								@if($stats['avg_goal_min'] == $stats['avg_goal_max'])
									{{-- find better way to format time for values that need formatting --}}
									@if ($units == 'min')
										{{ hmsFromSec($stats['avg_goal_min']/1000) }}
									@else
										{{ $stats['avg_goal_min'] }}
									@endif
								@else
									{{ $stats['avg_goal_min'] }} - {{ $stats['avg_goal_max'] }}
								@endif
								</b> {{ $units }}
							</small>
						</div>
					@endif
				</th>
				@foreach($stats['days'] as $key => $value)
				<td>
					@if($value['day_min'] != null)
						@if($indicatorPath != null)
							@include($indicatorPath, [$indicatorLevelKey => $value['day_min']["desc"]])
						@endif
						{{-- find better way to format time for values that need formatting --}}
						@if ($units == 'min')
							{{ hmsFromSec($value['day_min']['value']/1000) }}
						@else
							{{ $value['day_min']['value'] }}
						@endif
						@if(isset($value['day_min']['deviation_percentage']))
							<small class="text-purple _margin-l-6">{{ $value['day_min']["deviation_percentage"] }}%</small>
						@endif
						<div class="_lh1">
							<small class="text-muted">
								@if(isset($value['day_min']['date']))
									{{ $value['day_min']['date']->format(config('application.time.date_formats.default_date')) }}
								@endif
							</small>
						</div>
					@else
						-
					@endif
				</td>
				@endforeach
			</tr>
			<tr>
				<th>
					MAX
					@if(isset($stats['avg_goal_min']) && isset($stats['avg_goal_max']))
						<div class="_lh1 _fw-clear">
							<small class="text-muted">
								Cilj. vr.: <b>
								@if($stats['avg_goal_min'] == $stats['avg_goal_max'])
									{{-- find better way to format time for values that need formatting --}}
									@if ($units == 'min')
										{{ hmsFromSec($stats['avg_goal_min']/1000) }}
									@else
										{{ $stats['avg_goal_min'] }}
									@endif
								@else
									{{ $stats['avg_goal_min'] }} - {{ $stats['avg_goal_max'] }}
								@endif
								</b> {{ $units }}
							</small>
						</div>
					@endif
				</th>
				@foreach($stats['days'] as $key => $value)
				<td>
					@if($value['day_max'] != null)
						@if($indicatorPath != null)
							@include($indicatorPath, [$indicatorLevelKey => $value['day_max']["desc"]])
						@endif
						{{-- find better way to format time for values that need formatting --}}
						@if ($units == 'min')
							{{ hmsFromSec($value['day_max']['value']/1000) }}
						@else
							{{ $value['day_max']['value'] }}
						@endif
						@if(isset($value['day_max']['deviation_percentage']))
							<small class="text-purple _margin-l-6">{{ $value['day_max']["deviation_percentage"] }}%</small>
						@endif
						<div class="_lh1">
							<small class="text-muted">
								@if(isset($value['day_min']['date']))
									{{ $value['day_max']['date']->format(config('application.time.date_formats.default_date')) }}
								@endif
							</small>
						</div>
					@else
						-
					@endif
				</td>
				@endforeach
			</tr>
		</tbody>
	</table>
</div>