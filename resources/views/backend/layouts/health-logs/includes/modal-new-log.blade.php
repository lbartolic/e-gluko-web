<div class="modal fade" id="modal-new-log" tabindex="-1" role="dialog" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Novi unos</h4>
			</div>
			<div class="modal-body">
				@include('backend.layouts.health-logs.glucose.handlebars.create')
				@include('backend.layouts.health-logs.weight.handlebars.create')
				<div id="modal-new-log-create-holder"></div>
			</div>
			<div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zatvori</button>
                <button type="button" class="btn btn-primary" id="modal-new-log-btn-save">Spremi</button>
            </div>
		</div>
	</div>
</div>