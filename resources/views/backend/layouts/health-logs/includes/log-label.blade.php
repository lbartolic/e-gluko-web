@if($label->meal_rel == 1)
	<span class="label bg-teal">{{ $label->display_value }}</span>
@elseif($label->meal_rel == 2) 
	<span class="label bg-maroon">{{ $label->display_value }}</span>
@else
	<span class="label label-default">{{ $label->display_value }}</span>
@endif