<div class="row">
	<div class="col-sm-3">
		<div class="description-block">
			<h5 class="description-header">
				@if($stats['glucose_avg'] != null)
					@include('backend.layouts.health-logs.includes.bg-indicator', ['bgLevel' => $stats['glucose_avg']["desc"]])
					{{ $stats['glucose_avg']['value'] or '-' }} <small>{{ $_base_units['bg'] }}</small>
				@else
					-
				@endif
			</h5>
			<span class="description-text"><small>AVG razina glukoze</small></span>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="description-block">
			<h5 class="description-header">
				@if($stats['glucose_min'] != null)
					@include('backend.layouts.health-logs.includes.bg-indicator', ['bgLevel' => $stats['glucose_min']["desc"]])
					{{ $stats['glucose_min']['log']->value or '-' }}
				@else
					-
				@endif
				 - 
				@if($stats['glucose_max'] != null)
					@include('backend.layouts.health-logs.includes.bg-indicator', ['bgLevel' => $stats['glucose_max']["desc"]])
					{{ $stats['glucose_max']['log']->value or '-' }} <small>{{ $_base_units['bg'] }}</small>
				@else
					-
				@endif
			</h5>
			<span class="description-text"><small>Raspon razine glukoze</small></span>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="description-block">
			<h5 class="description-header">
				@if($stats['insulin_basal_daily_avg'] != null)
					@include('backend.layouts.health-logs.includes.insulin-indicator', ['doseLevel' => $stats['insulin_basal_daily_avg']["desc"]])
					{{ $stats['insulin_basal_daily_avg']['value'] }}
				@else
					-
				@endif
				/
				@if($stats['insulin_bolus_daily_avg'] != null)
					@include('backend.layouts.health-logs.includes.insulin-indicator', ['doseLevel' => $stats['insulin_bolus_daily_avg']["desc"]])
					{{ $stats['insulin_bolus_daily_avg']['value'] }} <small>{{ $_base_units['insulin'] }}</small>
				@else
					-
				@endif
			</h5>
			<span class="description-text"><small>AVG dnevni bazalni/bolus</small></span>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="description-block">
			<h5 class="description-header">
				{{ $stats['calorie_daily_avg']['value'] }} <small>{{ $_base_units['calories'] }}</small>/
				@if($stats['step_daily_avg'] != null)
					@include('backend.layouts.health-logs.includes.step-indicator', ['level' => $stats['step_daily_avg']["desc"]])
					{{ $stats['step_daily_avg']['value'] }}
				@else
					-
				@endif
			</h5>
			<span class="description-text"><small>AVG dnevne kalorije/koraci</small></span>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<div class="description-block">
			<h5 class="description-header">
				@if($stats['activity_total'] != null)
					@include('backend.layouts.health-logs.includes.step-indicator', ['level' => $stats['activity_total']["desc"]])
					{{ hmsFromSec($stats['activity_total']['value']/1000) }}
				@else
					-
				@endif
			</h5>
			<span class="description-text"><small>Uk. aktivnost</small></span>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="description-block">
			<h5 class="description-header">
				{{ $stats['activity_heart_rate_avg'] or '-' }} <small>{{ $_base_units['heart_rate'] }}</small>
			</h5>
			<span class="description-text"><small>AVG puls aktivnosti</small></span>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="description-block">
			<h5 class="description-header">
				@if($stats['sleep_total'] != null)
					{{ hmsFromSec($stats['sleep_total']['value']/1000) }}
				@else
					-
				@endif
			</h5>
			<span class="description-text"><small>Uk. san</small></span>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="description-block">
			<h5 class="description-header">
				@if($stats['sleep_avg_quality'] != null)
					{{ $stats['sleep_avg_quality'] }}%
				@else
					-
				@endif
			</h5>
			<span class="description-text"><small>AVG kvaliteta sna</small></span>
		</div>
	</div>
</div>