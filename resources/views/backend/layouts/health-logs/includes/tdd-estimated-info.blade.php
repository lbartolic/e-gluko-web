<div class="callout callout-info _callout-sm _inline-block">
	<p>	
		@include('backend.layouts.health-logs.includes.insulin-indicator', ['doseLevel' => $doseLevel]) 
		Ukupna prosječna <b>dnevna doza inzulina</b> za odabrani period: <b>{{ $totalDose }} {{ $_base_units['insulin'] }}</b> (est. TDD cilj. vr.: <b>{{ $tddGoal }} {{ $_base_units['insulin'] }}</b>).
	</p>
</div>