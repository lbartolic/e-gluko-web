<div class="dropdown _flex">
	<button type="button" class="_self-center btn btn-{{$style['type']}} btn-{{$style['size']}} dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		Dodaj novi zapis<span class="fa fa-caret-down _margin-l-6"></span>
	</button>
	<ul class="dropdown-menu pull-{{$style['position']}}">
		<li><a class="_show-health-log-create" href="#" data-type="glucose" data-patient="{{$patient->id}}">
			Glukoza u krvi
		</a></li>
		<li><a class="_show-health-log-create" href="#" data-type="insulin" data-patient="{{$patient->id}}">
			Doza inzulina
		</a></li>
		<li><a class="_show-health-log-create" href="#" data-type="weight" data-patient="{{$patient->id}}">
			Tjelesna masa
		</a></li>
	</ul>
</div>

@push('after-scripts')
<script>
    $('._show-health-log-create').click(function(e) {
        e.preventDefault();
        var type = $(this).attr('data-type');
        var patientId = $(this).attr('data-patient');
        showModalCreateHealthLog(type, patientId);
    })
</script>
@endpush