@foreach($logs as $logKey => $logValue)
    @if($logValue->logType->type == 'glucose')
        <li>
            <i class="fa fa-tint bg-red"></i>
            <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o _margin-r-6"></i>{{ $logValue->log_ts->format('H:i') }}</span>

                <h3 class="timeline-header clearfix">
                    <div class="_item _log-type">
                        {{ $logValue->logType->type_name }}
                    </div>
                    <div class="_item">
                        Iznos: 
                        @include('backend.layouts.health-logs.glucose.includes.log-value-desc', [
                            'data' => $logValue->loggable->getDescGoalDiff($logValue, $dataValue['health_goal'])
                        ])
                    </div>
                    <div class="_item">
                        Oznaka: 
                        @include('backend.layouts.health-logs.includes.log-label', [
                            'label' => $logValue->loggable->label
                        ])
                    </div>
                    <div class="_item">
                        Unos: 
                        @include('backend.layouts.health-logs.includes.insert-type', [
                            'insertType' => $logValue->insert_type
                        ])
                    </div>
                </h3>
            </div>
        </li>
    @endif
    @if($logValue->logType->type == 'insulin')
        <li>
            <i class="fa fa-eyedropper bg-aqua"></i>
            <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o _margin-r-6"></i>{{ $logValue->log_ts->format('H:i') }}</span>

                <h3 class="timeline-header clearfix">
                    <div class="_item _log-type">
                        {{ $logValue->logType->type_name }}
                    </div>
                    <div class="_item">
                        Iznos: <span class="_log-value">{{ $logValue->value }}</span> {{ $_base_units['insulin'] }}
                    </div>
                    <div class="_item">
                        Oznaka: 
                        @include('backend.layouts.health-logs.includes.log-label', [
                            'label' => $logValue->loggable->label
                        ])
                    </div>
                    <div class="_item">
                        Vrsta: 
                        @include('backend.layouts.health-logs.insulin.includes.type', [
                            'type' => $logValue->loggable->insulinType
                        ])
                    </div>
                    <div class="_item">
                        Unos: 
                        @include('backend.layouts.health-logs.includes.insert-type', [
                            'insertType' => $logValue->insert_type
                        ])
                    </div>
                </h3>
            </div>
        </li>
    @endif
    @if($logValue->logType->type == 'step')
        <li>
            <i class="fa fa-street-view bg-blue" style="background: #1abc9c !important;"></i>
            <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o _margin-r-6"></i>{{ $logValue->log_ts->format('H:i') }}</span>

                <h3 class="timeline-header clearfix">
                    <div class="_item _log-type">
                        {{ $logValue->logType->type_name }}
                    </div>
                    <div class="_item">
                        Iznos: <span class="_log-value">{{ $logValue->value }}</span>
                    </div>
                    <div class="_item">
                        Unos: 
                        @include('backend.layouts.health-logs.includes.insert-type', [
                            'insertType' => $logValue->insert_type
                        ])
                    </div>
                </h3>
            </div>
        </li>
    @endif
    @if($logValue->logType->type == 'calorie')
        <li>
            <i class="fa fa-bolt bg-blue" style="background: #e67e22 !important;"></i>
            <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o _margin-r-6"></i>{{ $logValue->log_ts->format('H:i') }}</span>

                <h3 class="timeline-header clearfix">
                    <div class="_item _log-type">
                        {{ $logValue->logType->type_name }}
                    </div>
                    <div class="_item">
                        Iznos: <span class="_log-value">{{ $logValue->value }}</span> {{ $_base_units['calories'] }}
                    </div>
                    <div class="_item">
                        Unos: 
                        @include('backend.layouts.health-logs.includes.insert-type', [
                            'insertType' => $logValue->insert_type
                        ])
                    </div>
                </h3>
            </div>
        </li>
    @endif
    @if($logValue->logType->type == 'weight')
        <li>
            <i class="fa fa-envelope bg-blue" style="background: #f1c40f !important;"></i>
            <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o _margin-r-6"></i>{{ $logValue->log_ts->format('H:i') }}</span>

                <h3 class="timeline-header clearfix">
                    <div class="_item _log-type">
                        {{ $logValue->logType->type_name }}
                    </div>
                    <div class="_item">
                        Iznos: <span class="_log-value">{{ $logValue->value }}</span> {{ $_base_units['weight'] }}
                    </div>
                    <div class="_item">
                        Struk: <span class="_log-value">{{ $logValue->loggable->waist }}</span> {{ $_base_units['waist'] }}
                    </div>
                    <div class="_item">
                        BMI: <span class="_log-value">{{ $logValue->calculations->bmi }}</span>
                    </div>
                    <div class="_item">
                        BMR: <span class="_log-value">{{ $logValue->calculations->bmr }}</span>
                    </div>
                    <div class="_item">
                        Unos: 
                        @include('backend.layouts.health-logs.includes.insert-type', [
                            'insertType' => $logValue->insert_type
                        ])
                    </div>
                </h3>
            </div>
        </li>
    @endif
    @if($logValue->logType->type == 'physical_activity')
        <li>
            <i class="fa fa-heartbeat bg-blue" style="background: #2ecc71 !important;"></i>
            <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o _margin-r-6"></i>{{ $logValue->log_ts->format('H:i') }}</span>

                <h3 class="timeline-header clearfix">
                    <div class="_item _log-type">
                        {{ $logValue->logType->type_name }}
                    </div>
                    <div class="_item">
                        Iznos: <span class="_log-value">{{ gmdate('H:i:s', $logValue->value/1000) }}</span>
                    </div>
                    <div class="_item">
                        Kalorije: <span class="_log-value">{{ $logValue->loggable->calories }}</span> {{ $_base_units['calories'] }}
                    </div>
                    <div class="_item">
                        Koraci: <span class="_log-value">{{ $logValue->loggable->steps }}</span>
                    </div>
                    <div class="_item">
                        Otkucaji srca: <span class="_log-value">{{ $logValue->loggable->average_heart_rate }}</span> {{ $_base_units['heart_rate'] }}
                    </div>
                    <div class="_item">
                        Unos: 
                        @include('backend.layouts.health-logs.includes.insert-type', [
                            'insertType' => $logValue->insert_type
                        ])
                    </div>
                </h3>
            </div>
        </li>
    @endif
    @if($logValue->logType->type == 'sleep')
        <li>
            <i class="fa fa-bed bg-blue" style="background: rgb(41, 128, 185) !important;"></i>
            <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o _margin-r-6"></i>{{ $logValue->log_ts->format('H:i') }}</span>

                <h3 class="timeline-header clearfix">
                    <div class="_item _log-type">
                        {{ $logValue->logType->type_name }}
                    </div>
                    <div class="_item">
                        Iznos: <span class="_log-value">{{ gmdate('H:i:s', $logValue->value/1000) }}</span>
                    </div>
                    <div class="_item">
                        Spavanje: <span class="_log-value">{{ gmdate('H:i:s', $logValue->loggable->asleep_mins*60) }}</span>
                    </div>
                    <div class="_item">
                        Buđenje: <span class="_log-value">{{ gmdate('H:i:s', $logValue->loggable->awake_mins*60) }} <small>({{$logValue->loggable->awake_count}}x)</small></span>
                    </div>
                    <div class="_item">
                        Nemirnost: <span class="_log-value">{{ gmdate('H:i:s', $logValue->loggable->restless_mins*60) }} <small>({{$logValue->loggable->restless_count}}x)</small></span>
                    </div>
                    <div class="_item">
                        Unos: 
                        @include('backend.layouts.health-logs.includes.insert-type', [
                            'insertType' => $logValue->insert_type
                        ])
                    </div>
                </h3>
            </div>
        </li>
    @endif
@endforeach
