@if($a1cValue == null)
	<div class="callout callout-info _callout-sm _inline-block">
		<p>
			<i class="icon fa _fa fa-info"></i> Estimacija <b>A1C vrijednosti</b> za odabrani period nije dostupna zbog nedovoljne količine podataka. 
		</p>
	</div>
@else
	<div class="callout callout-{{ ($a1cValue > $a1cGoal) ? 'danger' : 'success' }} _callout-sm _inline-block">
		<p>	
			@include('backend.layouts.health-logs.includes.bg-indicator', [
				'bgLevel' => $a1cLevel,
				'colored' => false
			]) 
			Estimirana <b>A1C vrijednost</b> za odabrani period: <b>{{ $a1cValue }}{{$_base_units['a1c']}}</b> (est. cilj. vr.: <b>{{ $a1cGoal }}{{$_base_units['a1c']}}</b>).
		</p>
	</div>
@endif