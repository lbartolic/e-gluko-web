<table class="table table-bordered _table-v-middle _margin-b-0 _table-tiny-pad">
	<tbody>
		<tr>
			<th>Ukupno [<small>{{ $_base_units['calories'] }}</small>]</th>
			<th>Prosjek/dan [<small>{{ $_base_units['calories'] }}</small>]</th>
			<th>MAX [<small>{{ $_base_units['calories'] }}</small>]</th>
		</tr>
		<tr>
			<td>{{ $stats['total_calories'] }}</td>
			<td>{{ $stats['daily_avg_calories']['value'] }}</td>
			<td>
				{{ $stats['day_max_calories']['value'] }} 
				<div class="_lh1">
					<small class="text-muted">
						{{ $stats['day_max_calories']['date']->format(config('application.time.date_formats.default_date')) }}
					</small>
				</div>
			</td>
		</tr>
	</tbody>
</table>