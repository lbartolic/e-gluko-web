<table class="table table-bordered _table-v-middle _margin-b-0 _table-tiny-pad">
	<tbody>
		<tr class="_colspan-row">
			<th></th>
			<th colspan="5">Ukupno</th>
			<th colspan="4">Prosjek po aktivnosti</th>
		</tr>
		<tr>
			<th></th>
			<th>Mj. dana</th>
			<th>Trajanje</th>
			<th>Kalorije</th>
			<th>Koraci</th>
			<th>Trajanje/dan</th>
			<th>Trajanje</th>
			<th>Kalorije</th>
			<th>Koraci</th>
			<th>Otkucaji srca</th>
		</tr>
		<tr>
			<th>
				Aktivnost
				<div class="_lh1 _fw-clear">
					<small class="text-muted">
						Cilj. vr. (dan): <b>{{ hmsFromSec($stats['goal_avgs']['min_daily_excercise']/1000) }}</b>
					</small>
				</div>
				<div class="_lh1 _fw-clear">
					<small class="text-muted">
						Cilj. vr. (period): <b>{{ hmsFromSec($stats['goal_avgs']['period_min_sum']/1000) }}</b>
					</small>
				</div>
			</th>
			<td>{{ $stats['logged_days'] }}</td>
			<td>
				@include('backend.layouts.health-logs.includes.step-indicator', ['level' => $stats['total_duration']["desc"]])
				{{ hmsFromSec($stats['total_duration']['value']/1000) }}
				<small class="text-purple _margin-l-6">{{ $stats['total_duration']["deviation_percentage"] }}%</small>
			</td>
			<td>{{ $stats['total_calories'] }}</td>
			<td>{{ $stats['total_steps'] }}</td>
			<td>
				{{ hmsFromSec($stats['daily_avg_duration']/1000) }}
			</td>
			<td>{{ hmsFromSec($stats['avg_duration']/1000) }}</td>
			<td>{{ $stats['avg_calories'] }}</td>
			<td>{{ $stats['avg_steps'] }}</td>
			<td>{{ $stats['avg_heart_rate'] }}</td>
		</tr>
	</tbody>
</table>