<table class="table table-bordered _table-v-middle _margin-b-0 _table-tiny-pad">
	<tbody>
		<tr>
			<th></th>
			<th>Mjerenja</th>
			<th>Prosjek [<small>{{ $_base_units['bg'] }}</small>]</th>
			<th>Visoka [<small>%</small>]</th>
			<th>Niska [<small>%</small>]</th>
			<th>Normalna [<small>%</small>]</th>
			<th>MIN [<small>{{ $_base_units['bg'] }}</small>]</th>
			<th>MAX [<small>{{ $_base_units['bg'] }}</small>]</th>
		</tr>
		<tr>
			<th>
				Prije obroka
				<div class="_lh1 _fw-clear">
					<small class="text-muted">
					<i class="fa _fa fa-thumbs-o-up"></i> <b>{{ $stats['goal_avgs']['min_before'] }} - {{ $stats['goal_avgs']['max_before'] }}</b> {{ $_base_units['bg'] }}
					</small>
				</div>
			</th>
			<td>{{ $stats['before_after']['before_count'] }}</td>
			<td>
				@if($stats['before_after']['avg_before'] != null)
					@include('backend.layouts.health-logs.includes.bg-indicator', ['bgLevel' => $stats['before_after']['avg_before']["desc"]])
					{{ $stats['before_after']['avg_before']['value'] }}
				@else
					-
				@endif
			</td>
			<td>{{ $stats['before_after']['before_high_percentage'] }}</td>
			<td>{{ $stats['before_after']['before_low_percentage'] }}</td>
			<td>{{ $stats['before_after']['before_normal_percentage'] }}</td>
			<td>
				@if($stats['before_after']['min_before'] != null)
					@include('backend.layouts.health-logs.includes.bg-indicator', ['bgLevel' => $stats['before_after']['min_before']["desc"]])
					{{ $stats['before_after']['min_before']["log"]->value }} 
					<small class="text-purple _margin-l-6">{{ $stats['before_after']['min_before']["deviation_percentage"] }}%</small>
					<div class="_lh1">
						<small class="text-muted">
							{{ $stats['before_after']['min_before']["log"]->loggable->label->display_value }}, 
							{{ $stats['before_after']['min_before']["log"]->log_ts->format(config('application.time.date_formats.default_date')) }}
						</small>
					</div>
				@else
					-
				@endif
			</td>
			<td>
				@if($stats['before_after']['max_before'] != null)
					@include('backend.layouts.health-logs.includes.bg-indicator', ['bgLevel' => $stats['before_after']['max_before']["desc"]])
					{{ $stats['before_after']['max_before']["log"]->value }} 
					<small class="text-purple _margin-l-6">{{ $stats['before_after']['max_before']["deviation_percentage"] }}%</small>
					<div class="_lh1">
						<small class="text-muted">
							{{ $stats['before_after']['max_before']["log"]->loggable->label->display_value }}, 
							{{ $stats['before_after']['max_before']["log"]->log_ts->format(config('application.time.date_formats.default_date')) }}
						</small>
					</div>
				@else
					-
				@endif
			</td>
		</tr>
		<tr>
			<th>
				Poslije obroka
				<div class="_lh1 _fw-clear">
					<small class="text-muted">
						<i class="fa _fa fa-thumbs-o-up"></i> <b>{{ $stats['goal_avgs']['min_before'] }} - {{ $stats['goal_avgs']['max_after'] }}</b> {{ $_base_units['bg'] }}
					</small>
				</div>
			</th>
			<td>{{ $stats['before_after']['after_count'] }}</td>
			<td>
				@if($stats['before_after']['avg_after'] != null)
					@include('backend.layouts.health-logs.includes.bg-indicator', ['bgLevel' => $stats['before_after']['avg_after']["desc"]])
					{{ $stats['before_after']['avg_after']['value'] }}
				@else
					-
				@endif
			</td>
			<td>{{ $stats['before_after']['after_high_percentage'] }}</td>
			<td>{{ $stats['before_after']['after_low_percentage'] }}</td>
			<td>{{ $stats['before_after']['after_normal_percentage'] }}</td>
			<td>
				@if($stats['before_after']['min_after'] != null)
					@include('backend.layouts.health-logs.includes.bg-indicator', ['bgLevel' => $stats['before_after']['min_after']["desc"]])
					{{ $stats['before_after']['min_after']["log"]->value }} 
					<small class="text-purple _margin-l-6">{{ $stats['before_after']['min_after']["deviation_percentage"] }}%</small>
					<div class="_lh1">
						<small class="text-muted">
							{{ $stats['before_after']['min_after']["log"]->loggable->label->display_value }}, 
							{{ $stats['before_after']['min_after']["log"]->log_ts->format(config('application.time.date_formats.default_date')) }}
						</small>
					</div>
				@else
					-
				@endif
			</td>
			<td>
				@if($stats['before_after']['max_after'] != null)
					@include('backend.layouts.health-logs.includes.bg-indicator', ['bgLevel' => $stats['before_after']['max_after']["desc"]])
					{{ $stats['before_after']['max_after']["log"]->value }} 
					<small class="text-purple _margin-l-6">{{ $stats['before_after']['max_after']["deviation_percentage"] }}%</small>
					<div class="_lh1">
						<small class="text-muted">
							{{ $stats['before_after']['max_after']["log"]->loggable->label->display_value }}, 
							{{ $stats['before_after']['max_after']["log"]->log_ts->format(config('application.time.date_formats.default_date')) }}
						</small>
					</div>
				@else
					-
				@endif
			</td>
		</tr>
		<tr>
			<th>
				Ukupno
				<div class="_lh1 _fw-clear">
					<small class="text-muted">
						<i class="fa _fa fa-thumbs-o-up"></i> <b>{{ $stats['goal_avgs']['min_before'] }} - {{ $stats['goal_avgs']['max_after'] }}</b> {{ $_base_units['bg'] }}
					</small>
				</div>
			</th>
			<td>{{ $stats['total_logs'] }}</td>
			<td>
				@include('backend.layouts.health-logs.includes.bg-indicator', ['bgLevel' => $stats['avg_value']["desc"]])
				{{ $stats['avg_value']['value'] }}
			</td>
			<td>{{ $stats['high_percentage'] }}</td>
			<td>{{ $stats['low_percentage'] }}</td>
			<td>{{ $stats['normal_percentage'] }}</td>
			<td>
				@include('backend.layouts.health-logs.includes.bg-indicator', ['bgLevel' => $stats['min_value']["desc"]])
				{{ $stats['min_value']["log"]->value }} 
				<small class="text-purple _margin-l-6">{{ $stats['min_value']["deviation_percentage"] }}%</small>
				<div class="_lh1">
					<small class="text-muted">
						{{ $stats['min_value']["log"]->loggable->label->display_value }}, 
						{{ $stats['min_value']["log"]->log_ts->format(config('application.time.date_formats.default_date')) }}</small>
					</div>
				</td>
				<td>
					@include('backend.layouts.health-logs.includes.bg-indicator', ['bgLevel' => $stats['max_value']["desc"]])
					{{ $stats['max_value']["log"]->value }} 
					<small class="text-purple _margin-l-6">{{ $stats['max_value']["deviation_percentage"] }}%</small>
					<div class="_lh1">
						<small class="text-muted">
							{{ $stats['max_value']["log"]->loggable->label->display_value }}, 
							{{ $stats['max_value']["log"]->log_ts->format(config('application.time.date_formats.default_date')) }}
						</small>
					</div>
				</td>
			</tr>
		</tbody>
	</table>