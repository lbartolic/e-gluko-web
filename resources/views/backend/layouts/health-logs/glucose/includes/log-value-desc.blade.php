@include('backend.layouts.health-logs.includes.bg-indicator', [
	'bgLevel' => $data['desc']
])
@if($data['desc'] == 'low')
	<span class="_log-value text-orange">{{ $data['log']->value }}</span> {{ $_base_units['bg'] }}
@elseif($data['desc'] == 'high')
	<span class="_log-value text-red">{{ $data['log']->value }}</span> {{ $_base_units['bg'] }}
@else
	<span class="_log-value text-green">{{ $data['log']->value }}</span> {{ $_base_units['bg'] }}
@endif