<script id="glucose-log-create-hb" type="text/x-handlebars-template">
	<h3 class="_section-title _margin-t-0">Glukoza u krvi</h3>
	<form>
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label>Razina glukoze [{{data.units.bg}}]</label>
                    <input class="form-control input-lg" type="number" step="0.1" placeholder="vrijednost" name="value">
            	</div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label>Datum i vrijeme</label>
                    <div id="log-create-datetime" class='input-group input-group-lg date'>
						<input type='text' class="form-control" placeholder="datum i vrijeme" name="log_ts" onkeydown="return false;"/>
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label>Vremenski odnos</label>
                    <select class="form-control input-lg" name="label_id">
                    	{{#each data.data.labels}}
                    		<option value="{{id}}">{{display_value}}</option>
                    	{{/each}}
                    </select>
            	</div>
            </div>
        </div>
    </form>
</script>