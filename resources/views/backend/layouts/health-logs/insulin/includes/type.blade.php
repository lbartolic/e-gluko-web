@if($type->key == 'basal')
	<span class="label label-primary" style="background: {{ config('application.colors.insulin_type.bg_basal') }} !important">{{ $type->display_value }}</span>
@elseif($type->key == 'bolus') 
	<span class="label label-primary" style="background: {{ config('application.colors.insulin_type.bg_bolus') }} !important">{{ $type->display_value }}</span>
@endif