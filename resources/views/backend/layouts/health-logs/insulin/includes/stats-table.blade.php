<table class="table table-bordered _table-v-middle _margin-b-0 _table-tiny-pad">
	<tbody>
		<tr class="_colspan-row">
			<th></th>
			<th colspan="3">Ukupno</th>
			<th colspan="4">Prosjek [<small>{{ $_base_units['insulin'] }}</small>]</th>
		</tr>
		<tr>
			<th></th>
			<th>Doziranja</th>
			<th>Jedinica [<small>{{ $_base_units['insulin'] }}</small>]</th>
			<th>Jedinica/dan</th>
			<th>Prije obroka</th>
			<th>Poslije obroka</th>
			<th>Prije spavanja</th>
			<th>Ukupno</th>
		</tr>
		<tr>
			<th style="box-shadow: inset 6px 0 0 {{config('application.colors.insulin_type.bg_basal')}};">
				<span class="_margin-l-6">Bazalni</span>
				<div class="_margin-l-6 _lh1 _fw-clear">
					<small class="text-muted">
						Cilj. vr. (dan): <b>{{ $stats['goal_avgs']['basal_total'] }}</b> {{ $_base_units['insulin'] }}
					</small>
				</div>
			</th>
			<td>{{ $stats['total_logs_basal'] }}</td>
			<td>{{ $stats['total_units_basal'] }}</td>
			<td>
				@include('backend.layouts.health-logs.includes.insulin-indicator', ['doseLevel' => $stats['daily_avg_units_basal']["desc"]])
				{{ $stats['daily_avg_units_basal']['value'] }} 
				<small class="text-purple _margin-l-6">{{ $stats['daily_avg_units_basal']["deviation_percentage"] }}%</small>
			</td>
			<td>{{ $stats['basal_before_meal']['avg'] }}</td>
			<td>{{ $stats['basal_after_meal']['avg'] }}</td>
			<td>{{ $stats['basal_before_bed']['avg'] }}</td>
			<td>{{ $stats['avg_units_basal'] }}</td>
		</tr>
		<tr>
			<th style="box-shadow: inset 6px 0 0 {{config('application.colors.insulin_type.bg_bolus')}};">
				<span class="_margin-l-6">Bolus</span>
				<div class="_margin-l-6 _lh1 _fw-clear">
					<small class="text-muted">
						Cilj. vr. (dan): <b>{{ $stats['goal_avgs']['bolus_total'] }}</b> {{ $_base_units['insulin'] }}
					</small>
				</div>
			</th>
			<td>{{ $stats['total_logs_bolus'] }}</td>
			<td>{{ $stats['total_units_bolus'] }}</td>
			<td>
				@include('backend.layouts.health-logs.includes.insulin-indicator', ['doseLevel' => $stats['daily_avg_units_bolus']["desc"]])
				{{ $stats['daily_avg_units_bolus']['value'] }}
				<small class="text-purple _margin-l-6">{{ $stats['daily_avg_units_bolus']["deviation_percentage"] }}%</small>
			</td>
			<td>{{ $stats['bolus_before_meal']['avg'] }}</td>
			<td>{{ $stats['bolus_after_meal']['avg'] }}</td>
			<td>{{ $stats['bolus_before_bed']['avg'] }}</td>
			<td>{{ $stats['avg_units_bolus'] }}</td>
		</tr>
	</tbody>
</table>