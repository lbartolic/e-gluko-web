@extends ('backend.layouts.app')

@section ('title', 'Pacijenti')

@section('after-styles')
@endsection

@section('page-header')
<h1>
	Zapisi i izvještaji
	<small>Luka Bartolić</small>
</h1>
@endsection

@section('content')
<div class="_flex">
	<div class="_margin-center-horiz">
		<div class="box box-solid">
			<div class="box-body">
				<div class="form-group _date-range-pickers">
					<big>Odabir tjedna:</big>
					<a href="{{ route('admin.health-logs.logs.show', [$user->id, $prev]) }}">
						<button class="btn btn-sm btn-default _pickers-arrow-btn">
							<i class="fa fa-caret-left"></i>
						</button>
					</a>
					<div class='input-group input-group-sm date' id='logs-date-picker'>
						<input type='text' class="form-control" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					<a href="{{ route('admin.health-logs.logs.show', [$user->id, $next]) }}">
						<button class="btn btn-sm btn-default _pickers-arrow-btn">
							<i class="fa fa-caret-right"></i>
						</button>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
		<div class="box-group" id="logs-collapse-holder">
			<div class="panel box box-default _margin-b-0">
				<div class="box-header with-border">
					<h4 class="box-title">
						<a data-toggle="collapse" data-parent="#logs-collapse-holder" href="#logs-goals-collapse" aria-expanded="true" class="collapsed">
							Ciljne vrijednosti
						</a>
					</h4>
				</div>
				<div id="logs-goals-collapse" class="panel-collapse collapse" aria-expanded="true">
					@include('backend.layouts.health-goals.includes.tabbed-content-goals', [
						'healthGoals' => $logs['health_goals']
					])
				</div>
			</div>
			<div class="panel box box-default">
				<div class="box-header with-border">
					<h4 class="box-title">
						<a data-toggle="collapse" data-parent="#logs-collapse-holder" href="#logs-export-collapse" aria-expanded="false" class="collapsed">
							Opcije izvoza podataka
						</a>
					</h4>
				</div>
				<div id="logs-export-collapse" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
					<div class="nav-tabs-custom _margin-b-0">
						<div class="tab-content">
							<div class="_flex">
                                <div class="_margin-center-horiz clearfix">
                                    <div class="pull-left">
                                        <div class="form-group _date-range-pickers">
                                            <div class="pull-left _margin-l-6 _data-export-types-holder">
                                                <button type="button" class="btn btn-sm bg-purple active _margin-r-6 text-uppercase" data-export-type="pdf">PDF</button>
                                                <button type="button" class="btn btn-sm bg-default _margin-r-6 text-uppercase" data-export-type="xls">XLS</button>
                                            </div>
                                            <div class="input-group input-group-sm date _margin-l-6 _margin-r-6" id="data-export-date">
                                                <input type="text" class="form-control">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                            <button class="btn btn-primary btn-sm _margin-l-6 _margin-r-6" id="data-export-btn" data-user="2">Preuzmi</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<h2 class="_sub-title _margin-t-0 _margin-b-6">Prikaz podataka za period:<b> {{ \Carbon\Carbon::parse($logs['date_from'])->format(config('application.time.date_formats.default_date')) }} - {{ \Carbon\Carbon::parse($logs['date_to'])->format(config('application.time.date_formats.default_date')) }}</b></h2>
		<div class="box-body _stats-box _margin-b-15">
			@include('backend.layouts.health-logs.includes.total-stats', [
				'stats' => $logs['stats']
			])
		</div>
		
		<ul class="timeline _logs-timeline">
			@foreach($logs['data'] as $dataKey => $dataValue)
				<li class="time-label">
					<span class="bg-navy">
						{{ \Carbon\Carbon::parse($dataValue['date'])->formatLocalized(config('application.time.locale_date_formats.default_day_date')) }}
					</span>
				</li>
				<li>
					<div class="timeline-item">
						<div class="box-body _stats-box">
							@include('backend.layouts.health-logs.includes.total-stats', [
								'stats' => $dataValue['stats'],
								'except' => ['activity_daily_avg']
							])
						</div>
					</div>
				</li>
				@include('backend.layouts.health-logs.includes.log-list', [
                    'logs' => $dataValue['logs']
                ])
			@endforeach
		</ul>
		<div class="form-group">
			<div class="btn-group btn-group-justified">
				<div class="btn-group">
					<a href="{{ route('admin.health-logs.logs.show', [$user->id, $prev]) }}" class="btn btn-default btn-lg"><i class="fa fa-angle-left _margin-r-6"></i> Prošli tjedan</a>
				</div>
				<div class="btn-group">
					<a href="{{ route('admin.health-logs.logs.show', [$user->id, $next]) }}" class="btn btn-default btn-lg">Sljedeći tjedan <i class="fa fa-angle-right _margin-l-6"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('after-scripts')
<script>
	$(document).ready(function() {
		var dateFrom = {!! json_encode($logs['date_from']) !!};
		console.log(dateFrom);
		$('#logs-date-picker').datetimepicker({
	        format: eGlukoData.def_date_format,
	        allowInputToggle: true,
	        locale: 'hr'
	    });
		$('#logs-date-picker').data("DateTimePicker").date(moment(dateFrom, eGlukoData.db_date_format));
		$("#logs-date-picker").on("dp.change", function(event) {
			var date = moment($('#logs-date-picker').data('date'), eGlukoData.def_date_format);
			window.location = APP_URL + "/admin/patients/2/health-logs/logs/" + date.format(eGlukoData.db_date_format);
		});
        
		$('#data-export-date').datetimepicker({
	        format: eGlukoData.def_m_y_format,
	        allowInputToggle: true,
	        locale: 'hr',
            viewMode: "months"
	    });
		$('#data-export-date').data("DateTimePicker").date(moment(dateFrom, eGlukoData.db_date_format));
        $('._data-export-types-holder button').click(function() {
            $('._data-export-types-holder button').removeClass('bg-purple active').addClass('bg-default');
            $(this).addClass('bg-purple active');
        });
        $('#data-export-btn').click(function() {
			var date = moment($('#data-export-date').data("date"), eGlukoData.def_m_y_format);
            var type = $('._data-export-types-holder .active').attr('data-export-type');
            var userId = $(this).attr("data-user");
            console.log(date.format(eGlukoData.db_date_format), type, userId);
			window.location = APP_URL + "/admin/patients/" + userId + "/health-logs/logs/" + date.format(eGlukoData.db_date_format) + "/" + type;
		});
	});
</script>
@endsection
