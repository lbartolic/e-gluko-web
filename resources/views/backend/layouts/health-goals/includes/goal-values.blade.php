<div class="box-body _stats-box">
	<div class="row _margin-b-2">
		<div class="col-sm-4 border-right">
			<div class="description-block">
				<h5 class="description-header">
					{{$goal->max_a1c}} <small>{{$_base_units['a1c']}}</small>
				</h5>
				<span class="description-text"><small>MAX A1C</small></span>
			</div>
		</div>
		<div class="col-sm-4 border-right">
			<div class="description-block">
				<h5 class="description-header">
					{{$goal->min_bg_before_meal}} - {{$goal->max_bg_before_meal}} <small>{{$_base_units['bg']}}</small>
				</h5>
				<span class="description-text"><small>GUK PRIJE OBROKA</small></span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="description-block">
				<h5 class="description-header">
					< {{$goal->max_bg_after_meal}} <small>{{$_base_units['bg']}}</small>
				</h5>
				<span class="description-text"><small>GUK POSLIJE OBROKA</small></span>
			</div>
		</div>
	</div>
	<div class="row _margin-b-2">
		<div class="col-sm-4 border-right">
			<div class="description-block">
				<h5 class="description-header">
					{{$goal->daily_basal_total}} <small>{{$_base_units['insulin']}}</small>
				</h5>
				<span class="description-text"><small>UK. BAZALNI/DAN</small></span>
			</div>
		</div>
		<div class="col-sm-4 border-right">
			<div class="description-block">
				<h5 class="description-header">
					{{$goal->daily_bolus_total}} <small>{{$_base_units['insulin']}}</small>
				</h5>
				<span class="description-text"><small>UK. BOLUS/DAN</small></span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="description-block">
				<h5 class="description-header">
					{{$goal->tdd}} <small>{{$_base_units['insulin']}}</small>
				</h5>
				<span class="description-text"><small>TDD</small></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4 border-right">
			<div class="description-block">
				<h5 class="description-header">
					{{$goal->min_daily_excercise}} <small>{{$_base_units['activity_length']}}</small>
				</h5>
				<span class="description-text"><small>MIN AKTIVNOST/DAN</small></span>
			</div>
		</div>
		<div class="col-sm-4 border-right">
			<div class="description-block">
				<h5 class="description-header">
					{{$goal->min_daily_steps}} <small>koraci</small>
				</h5>
				<span class="description-text"><small>MIN KRETANJE/DAN</small></span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="description-block">
				<h5 class="description-header">
					{{$goal->weight}} <small>{{$_base_units['weight']}}</small>
				</h5>
				<span class="description-text"><small>TJELESNA MASA</small></span>
			</div>
		</div>
	</div>
</div>