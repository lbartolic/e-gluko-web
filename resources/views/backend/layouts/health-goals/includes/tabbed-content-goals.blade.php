<div class="nav-tabs-custom _margin-b-0">
	<ul class="nav nav-tabs">
		@foreach($healthGoals as $goalKey => $goalValue)
			<li class="{{ ($loop->first) ? 'active' : '' }}">
				<a href="#logs-goals-{{ $goalValue->id }}-collapse" data-toggle="tab">
					{{ $goalValue->goal_from_ts->format(config('application.time.date_formats.default_date')) }}
				</a>
			</li>
		@endforeach
	</ul>
	<div class="tab-content">
		@foreach($healthGoals as $goalKey => $goalValue)
			<div class="tab-pane {{ ($loop->first) ? 'active' : '' }}" id="logs-goals-{{ $goalValue->id }}-collapse">
				@include('backend.layouts.health-goals.includes.goal-values', [
					'goal' => $goalValue
				])
			</div>
		@endforeach
	</div>
</div>