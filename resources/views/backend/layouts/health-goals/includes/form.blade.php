<div class="row">
    <div class="col-xs-3 col-sm-12 col-md-3">
        <div class="form-group {{ ($errors->has('max_a1c')) ? 'has-error' : '' }}">
            <label>MAX A1C [{{$_base_units['a1c']}}]</label>
            {{ Form::number('max_a1c', (old('max_a1c')) ? old('max_a1c') : (($lastGoal) ? $lastGoal->max_a1c : ''), ['class' => 'form-control input-lg', 'min' => 0, 'step' => 0.1]) }}
        </div>
    </div>
    <div class="col-xs-3 col-sm-12 col-md-3">
        <div class="form-group {{ ($errors->has('min_bg_before_meal')) ? 'has-error' : '' }}">
            <label>MIN prije obroka [{{$_base_units['bg']}}]</label>
            {{ Form::number('min_bg_before_meal', (old('min_bg_before_meal')) ? old('min_bg_before_meal') : (($lastGoal) ? $lastGoal->min_bg_before_meal : ''), ['class' => 'form-control input-lg', 'min' => 0, 'step' => 0.1]) }}
        </div>
    </div>
    <div class="col-xs-3 col-sm-12 col-md-3">
        <div class="form-group {{ ($errors->has('max_bg_before_meal')) ? 'has-error' : '' }}">
            <label>MAX prije obroka [{{$_base_units['bg']}}]</label>
            {{ Form::number('max_bg_before_meal', (old('max_bg_before_meal')) ? old('max_bg_before_meal') : (($lastGoal) ? $lastGoal->max_bg_before_meal : ''), ['class' => 'form-control input-lg', 'min' => 0, 'step' => 0.1]) }}
        </div>
    </div>
    <div class="col-xs-3 col-sm-12 col-md-3">
        <div class="form-group {{ ($errors->has('max_bg_after_meal')) ? 'has-error' : '' }}">
            <label>MAX poslije obroka [{{$_base_units['bg']}}]</label>
            {{ Form::number('max_bg_after_meal', (old('max_bg_after_meal')) ? old('max_bg_after_meal') : (($lastGoal) ? $lastGoal->max_bg_after_meal : ''), ['class' => 'form-control input-lg', 'min' => 0, 'step' => 0.1]) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-3 col-sm-12 col-md-3">
        <div class="form-group {{ ($errors->has('daily_basal_total')) ? 'has-error' : '' }}">
            <label>Uk. bazalni/dan [{{$_base_units['insulin']}}]</label>
            {{ Form::number('daily_basal_total', (old('daily_basal_total')) ? old('daily_basal_total') : (($lastGoal) ? $lastGoal->daily_basal_total : ''), ['class' => 'form-control input-lg', 'min' => 0]) }}
        </div>
    </div>
    <div class="col-xs-3 col-sm-12 col-md-3">
        <div class="form-group {{ ($errors->has('daily_bolus_total')) ? 'has-error' : '' }}">
            <label>Uk. bolus/dan [{{$_base_units['insulin']}}]</label>
            {{ Form::number('daily_bolus_total', (old('daily_bolus_total')) ? old('daily_bolus_total') : (($lastGoal) ? $lastGoal->daily_bolus_total : ''), ['class' => 'form-control input-lg', 'min' => 0]) }}
        </div>
    </div>
    <div class="col-xs-3 col-sm-12 col-md-3">
        <div class="form-group {{ ($errors->has('tdd')) ? 'has-error' : '' }}">
            <label>TDD [{{$_base_units['insulin']}}]</label>
            {{ Form::number('tdd', (old('tdd')) ? old('tdd') : (($lastGoal) ? $lastGoal->tdd : ''), ['class' => 'form-control input-lg', 'min' => 0]) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-3 col-sm-12 col-md-3">
        <div class="form-group {{ ($errors->has('min_daily_excercise')) ? 'has-error' : '' }}">
            <label>MIN aktivnost/dan [{{$_base_units['activity_length']}}]</label>
            {{ Form::number('min_daily_excercise', (old('min_daily_steps')) ? old('min_daily_excercise') : (($lastGoal) ? $lastGoal->min_daily_excercise : ''), ['class' => 'form-control input-lg', 'min' => 0]) }}
        </div>
    </div>
    <div class="col-xs-3 col-sm-12 col-md-3">
        <div class="form-group {{ ($errors->has('min_daily_steps')) ? 'has-error' : '' }}">
            <label>MIN kretanje/dan [koraci]</label>
            {{ Form::number('min_daily_steps', (old('min_daily_steps')) ? old('min_daily_steps') : (($lastGoal) ? $lastGoal->min_daily_steps : ''), ['class' => 'form-control input-lg', 'min' => 0]) }}
        </div>
    </div>
    <div class="col-xs-3 col-sm-12 col-md-3">
        <div class="form-group {{ ($errors->has('weight')) ? 'has-error' : '' }}">
            <label>Tjelesna masa [{{$_base_units['weight']}}]</label>
            {{ Form::number('weight', (old('weight')) ? old('weight') : (($lastGoal) ? $lastGoal->weight : ''), ['class' => 'form-control input-lg', 'min' => 0]) }}
        </div>
    </div>
    <div class="col-xs-3 col-sm-12 col-md-3">
        <div class="form-group {{ ($errors->has('goal_from_ts')) ? 'has-error' : '' }}">
            <label>Datum od</label>
            <div class='input-group'>
                {{ Form::text('goal_from_ts', (old('goal_from_ts')) ? old('goal_from_ts') : null, ['class' => 'form-control input-lg _datetimepicker-date', 'id' => 'dp-goal-from']) }}
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
</div>