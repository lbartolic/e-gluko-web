@extends ('backend.layouts.app')

@section ('title', 'Pacijenti')

@section('after-styles')
@endsection

@section('page-header')
<h1>
	Ciljne vrijednosti
	<small>popis svih ciljnih vrijednosti</small>
</h1>
@endsection

@section('content')
@if($lastGoal)
<div class="callout callout-info _inline-block">
    <p><i class="icon fa _fa fa-info"></i> Posljednje vrijednosti postavljene <strong>{{$lastGoal->goal_from_formatted}}</strong></p>
</div>
@else
<div class="callout callout-danger _inline-block">
    <p><i class="icon fa _fa fa-exclamation-triangle"></i> Ciljne vrijednosti nisu postavljene, potrebno ih je postaviti.</p>
</div>
@endif
@if($goals->count() > 0)
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
            <tbody>
                <tr class="_colspan-row">
                    <th colspan="3">Glukoza u krvi (GUK)</th>
                    <th colspan="3">Inzulin</th>
                    <th colspan="5"></th>
                </tr>
                <tr>
                    <th>MAX A1C</th>
                    <th>Raspon prije obroka</th>
                    <th>MAX poslije obroka</th>
                    <th>Uk. bazalni/dan</th>
                    <th>Uk. bolus/dan</th>
                    <th>TDD</th>
                    <th>MIN aktivnost/dan</th>
                    <th>MIN kretanje/dan</th>
                    <th>Tjelesna masa</th>
                    <th>Datum od</th>
                    <th></th>
                </tr>
                @foreach($goals as $goal)
                <tr>
                    <td>{{$goal->max_a1c}} <small>{{$_base_units['a1c']}}</small></td>
                    <td>{{$goal->min_bg_before_meal}} - {{$goal->max_bg_before_meal}} <small>{{$_base_units['bg']}}</small></td>
                    <td>{{$goal->max_bg_after_meal}} <small>{{$_base_units['bg']}}</td>
                    <td>{{$goal->daily_basal_total}} <small>{{$_base_units['insulin']}}</small></td>
                    <td>{{$goal->daily_bolus_total}} <small>{{$_base_units['insulin']}}</small></td>
                    <td>{{$goal->tdd}} <small>{{$_base_units['insulin']}}</small></td>
                    <td>{{$goal->min_daily_excercise}} <small>{{$_base_units['activity_length']}}</small></td>
                    <td>{{$goal->min_daily_steps}} <small>koraci</small></td>
                    <td>{{$goal->weight}} <small>{{$_base_units['weight']}}</small></td>
                    <td style="text-transform: lowercase;">{{$goal->goal_from_ts->formatLocalized("%d.%m.%Y. (%a)")}}</td>
                    <td>
                        @role(1)
                        <form method="POST" action="{{ route('admin.health-goals.destroy', ['patient' => $user->id, 'health_goal' => $goal->id]) }}" onsubmit="return confirm('Potvrdite brisanje ovih ciljnih vrijednosti.');">
                            <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-close"></i></button>
                            <input name="_method" type="hidden" value="DELETE">
                            {{ csrf_field() }}
                        </form>
                        @endauth
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
@endif
@role(1)
<div class="row">
    <div class="col-sm-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Dodavanje novih ciljnih vrijednosti</h3>
            </div>
            <div class="box-body">
                {{ Form::open(['method' => 'POST', 'route' => ['admin.health-goals.store', $user->id]]) }}
                @include('backend.layouts.health-goals.includes.form', [
                    'lastGoal' => $lastGoal
                ])
                <div class="form-group clearfix no-margin">
                    <button type="submit" class="btn btn-lg btn-primary pull-right">Spremi</button>
                </div>
                {{ Form::close() }}
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
@endauth
@endsection

@section('after-scripts')
<script>
	$(document).ready(function() {
		$('._datetimepicker-date').datetimepicker({
	        format: eGlukoData.def_date_format,
	        allowInputToggle: true,
	        locale: 'hr'
	    });
	});
</script>
@endsection
