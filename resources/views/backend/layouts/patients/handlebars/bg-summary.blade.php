<script id="bg-summary-hb" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-lg-6 _dotted-divider-r">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="_section-title text-center _margin-t-0">Sva mjerenja</h3>
                    <div id="bg-summary-total-chart" class="_margin-b-10" width="100%"></div>
                    <div class="_margin-b-10">
                        <div class="label label-info _margin-r-6">
                            <b>@{{total_logs}}</b> mjerenja ukupno
                        </div>
                        <div class="label label-info">
                            <b>@{{avg_logs_per_day}}</b> mjerenja/dan
                        </div>
                    </div>
                    <div class="box box-solid _box-clean">
                        <div class="box-header with-border">
                            <h3 class="box-title">Očitavanja</h3>
                        </div>
                        <div class="box-body">
                            <div class="_stats-box">
                                <div class="row">
                                    <a href="#" class="_a-clear">
                                        <div class="col-sm-4 border-right">
                                            <div class="description-block">
                                                <h5 class="description-header _relative">
                                                    @{{avg_value.value}}
                                                    <i class="@{{getBgIndicator avg_value.desc}}"></i>
                                                    <div class="_desc-extra">
                                                        <small>mmol/l</small>
                                                    </div>
                                                </h5>
                                                <span class="description-text"><small>SREDNJA VRIJEDNOST</small></span>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="_a-clear">
                                        <div class="col-sm-4 border-right">
                                            <div class="description-block">
                                                <h5 class="description-header _relative">
                                                    @{{min_value.log.value}}
                                                    <i class="@{{getBgIndicator min_value.desc}}"></i>
                                                    <div class="_desc-extra">
                                                        <small>mmol/l</small>
                                                    </div>
                                                </h5>
                                                <span class="description-text"><small>NAJNIŽA VRIJEDNOST</small></span>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="_a-clear">
                                        <div class="col-sm-4">
                                            <div class="description-block">
                                                <h5 class="description-header _relative">
                                                    @{{max_value.log.value}}
                                                    <i class="@{{getBgIndicator max_value.desc}}"></i>
                                                    <div class="_desc-extra">
                                                        <small>mmol/l</small>
                                                    </div>
                                                </h5>
                                                <span class="description-text"><small>NAJVIŠA VRIJEDNOST</small></span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h3 class="_section-title text-center _margin-t-0">Prije/poslije obroka</h3>
            <h5 class="text-uppercase _margin-b-6">Prije</h5>
            <div id="bg-summary-before-chart" class="" width="100%"></div>
            <div class="_margin-b-6">
                <div class="label label-info">
                    <b>@{{before_after.before_count}}</b> mjerenja
                </div>
            </div>
            <div class="box box-solid _box-clean">
                <div class="box-body">
                    <div class="_stats-box">
                        <div class="row">
                            <a href="#" class="_a-clear">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header _relative">
                                            @{{before_after.avg_before.value}}
                                            <i class="@{{getBgIndicator before_after.avg_before.desc}}"></i>
                                            <div class="_desc-extra">
                                                <small>mmol/l</small>
                                            </div>
                                        </h5>
                                        <span class="description-text"><small>SREDNJA VRIJEDNOST</small></span>
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="_a-clear">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header _relative">
                                            @{{before_after.min_before.log.value}}
                                            <i class="@{{getBgIndicator before_after.min_before.desc}}"></i>
                                            <div class="_desc-extra">
                                                <small>mmol/l</small>
                                            </div>
                                        </h5>
                                        <span class="description-text"><small>NAJNIŽA VRIJEDNOST</small></span>
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="_a-clear">
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <h5 class="description-header _relative">
                                            @{{before_after.max_before.log.value}}
                                            <i class="@{{getBgIndicator before_after.max_before.desc}}"></i>
                                            <div class="_desc-extra">
                                                <small>mmol/l</small>
                                            </div>
                                        </h5>
                                        <span class="description-text"><small>NAJVIŠA VRIJEDNOST</small></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <h5 class="text-uppercase _margin-b-6">Poslije</h5>
            <div id="bg-summary-after-chart" class="" width="100%"></div>
            <div class="_margin-b-6">
                <div class="label label-info">
                    <b>@{{before_after.after_count}}</b> mjerenja
                </div>
            </div>
            <div class="box box-solid _box-clean">
                <div class="box-body">
                    <div class="_stats-box">
                        <div class="row">
                            <a href="#" class="_a-clear">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header _relative">
                                            @{{before_after.avg_after.value}}
                                            <i class="@{{getBgIndicator before_after.avg_after.desc}}"></i>
                                            <div class="_desc-extra">
                                                <small>mmol/l</small>
                                            </div>
                                        </h5>
                                        <span class="description-text"><small>SREDNJA VRIJEDNOST</small></span>
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="_a-clear">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header _relative">
                                            @{{before_after.min_after.log.value}}
                                            <i class="@{{getBgIndicator before_after.min_after.desc}}"></i>
                                            <div class="_desc-extra">
                                                <small>mmol/l</small>
                                            </div>
                                        </h5>
                                        <span class="description-text"><small>NAJNIŽA VRIJEDNOST</small></span>
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="_a-clear">
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <h5 class="description-header _relative">
                                            @{{before_after.max_after.log.value}}
                                            <i class="@{{getBgIndicator before_after.max_after.desc}}"></i>
                                            <div class="_desc-extra">
                                                <small>mmol/l</small>
                                            </div>
                                        </h5>
                                        <span class="description-text"><small>NAJVIŠA VRIJEDNOST</small></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>