@extends ('backend.layouts.app')

@section ('title', 'Pacijenti')

@section('after-styles')
@endsection

@section('page-header')
<h1>
	Pacijenti
	<small>popis svih pacijenata</small>
</h1>
@endsection

@section('content')
<div class="row">
	<div class="col-md-4 col-lg-3">
		<div class="box box-primary">
			@role(1)
			<a href="{{ route('admin.patients.edit', ['patient' => $patient]) }}" class="btn btn-xs btn-default pull-right _margin-r-6 _margin-t-6" data-toggle="tooltip" data-original-title="Uredi">
				<i class="fa fa-edit"></i> Uredi
			</a>
			@endauth
			<div class="box-body box-profile">
				<div class="_profile-pic-holder">
					<img class="img-responsive img-circle" src="{{ ($patient->patientInfo->sex == 0) ? '/img/male.jpg' : '/img/female.jpg' }}">
				</div>
				<h3 class="profile-username text-center" style="margin-bottom: 0;">{{$patient->full_name}}</h3>
				<p class="text-muted text-center">
					@if($patient->patientInfo->diabetes_type)
					<span class="_margin-r-6">Dijabetes tip {{$patient->patientInfo->diabetes_type}}</span> <span class="badge bg-{{config('application.colors.diabetes_type.' . $patient->patientInfo->diabetes_type)}}"><i class="fa _fa fa-tag"></i> DT{{$patient->patientInfo->diabetes_type}}</span>
					@else

					@endif
				</p>
				<table class="table table-striped _margin-t-10 _margin-b-10">
					<tr>
						<td class="_fw-700">Datum rođ.</td>
						<td>
							<div class="pull-right">
								<span class="_block">{{$patient->patientInfo->birth_date_formatted}}</span>
								<div class="pull-right">
									<span class="label bg-gray">{{$patient->patientInfo->years}} god.</span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="_fw-700">Dijabetes od</td>
						<td>
							<div class="pull-right">
								<span class="_block">{{$patient->patientInfo->diabetes_from_formatted}}</span>
								<div class="pull-right">
									<span class="label bg-gray">{{$patient->patientInfo->yearsDiabetes}} god.</span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="_fw-700">Spol</td>
						<td>
							<span class="pull-right">{{$patient->patientInfo->sex_char}}</span>
						</td>
					</tr>
					<tr>
						<td class="_fw-700">Visina</td>
						<td>
							<span class="pull-right">{{$patient->patientInfo->height}}</span>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Uređaji</h3>
			</div>
			<div class="box-body">
				<strong class="text-uppercase"><i class="fa _fa fa-wifi"></i> Glukometar</strong>
				@if($glucometer)
				<table class="table _margin-t-10 _margin-b-10">
					<tbody><tr>
						<td class="_fw-700">Naziv</td>
						<td>
							<div class="pull-right">
								<span class="_block">{{ $glucometer->name }}</span>
							</div>
						</td>
					</tr>
					<tr style="background: #dbf7e6;">
						<td class="_fw-700">Zadnja sink.</td>
						<td>
							<div class="pull-right">
								<span class="_block">{{ $glucometer->last_sync_ts->format(config('application.time.date_formats.default_date_time')) }}</span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="_fw-700">Postavljen</td>
						<td>
							<div class="pull-right">
								<span class="_block">{{ $glucometer->created_at->format(config('application.time.date_formats.default_date')) }}</span>
							</div>
						</td>
					</tr>
				</tbody></table>
				@endif
                <hr>
				<strong class="text-uppercase _margin-t-6"><i class="fa _fa fa-wifi"></i> Fitbit</strong>
				@if($fitbit)
				<table class="table _margin-t-10 _margin-b-10">
					<tbody><tr>
						<td class="_fw-700">Naziv</td>
						<td>
							<div class="pull-right">
								<span class="_block">{{ $fitbit->device_version }}</span>
							</div>
						</td>
					</tr>
					<tr style="background: #dbf7e6;">
						<td class="_fw-700">Zadnja sink.</td>
						<td>
							<div class="pull-right">
								<span class="_block">{{ $fitbit->last_sync_time->format(config('application.time.date_formats.default_date_time')) }}</span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="_fw-700">Postavljen</td>
						<td>
							<div class="pull-right">
								<span class="_block">{{ $fitbit->created_at->format(config('application.time.date_formats.default_date')) }}</span>
							</div>
						</td>
					</tr>
				</tbody></table>
				@else
					@role(2)
					<a class="btn btn-default btn-block _margin-t-6" href="{{ route('admin.fitbit.authorize') }}" target="_blank">Dodaj uređaj</a>
					@endauth
				@endif
				@if(session('fitbit_success')) 
				<div class="alert alert-success alert-dismissible">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                <i class="icon fa fa-check"></i> Uređaj uspješno postavljen.
              	</div>
				@endif
			</div>
		</div>
	</div>
	<div class="col-md-8 col-lg-9">
		<div class="row" style="display: flex; position: relative;">
			<div class="col-sm-12">
				<!-- Custom Tabs -->
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_cv" data-toggle="tab"><i class="fa _fa fa-trophy"></i> Ciljne vrijednosti</a></li>
						<li class="pull-right"><a href="{{ route('admin.health-goals.index', [$patient->id]) }}" style="padding: 5px;"><button class="btn btn-primary btn-sm">Vidi više</button></a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_cv">
							<div class="callout callout-info _inline-block">
								<p><i class="icon fa _fa fa-info"></i> Posljednje vrijednosti postavljene <strong>{{$lastGoal->goal_from_formatted}}</strong></p>
							</div>
							<div class="box box-solid _box-clean _margin-b-0">
								@include('backend.layouts.health-goals.includes.goal-values', [
                                    'goal' => $lastGoal
                                ])
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Posljednja mjerenja</h3>
                        <div class="box-tools">
                            <a href="{{ route('admin.health-logs.logs.show', [$patient->id]) }}"><button class="btn btn-primary btn-sm">Vidi više</button></a>
                        </div>
                    </div>
                    <div class="box-body">
                        <ul class="timeline _logs-timeline">
                            @foreach($logs['data'] as $dataKey => $dataValue)
                                <li class="time-label">
                                    <span class="bg-navy">
                                        {{ \Carbon\Carbon::parse($dataValue['date'])->formatLocalized(config('application.time.locale_date_formats.default_day_date')) }}
                                    </span>
                                </li>
                                @include('backend.layouts.health-logs.includes.log-list', [
                                    'logs' => $dataValue['logs']
                                ])
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-primary" id="bg-summary" data-patient="{{ $patient->id }}">
					<div class="box-header with-border">
						<h3 class="box-title">Pregled stanja</h3>
						<div class="box-tools">
							<div class="form-group _date-range-pickers">
								<div class='input-group input-group-sm date' id='gluc-summ-date-from'>
									<input type='text' class="form-control" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<div class="_pickers-divider pull-left">do</div>
								<div class='input-group input-group-sm date' id='gluc-summ-date-to'>
									<input type='text' class="form-control" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="box-body no-padding">
						<div class="nav-tabs-custom _margin-b-0">
							<ul class="nav nav-tabs _flex">
								<li class="active"><a href="#overview-tab-glucose" data-toggle="tab">
									Glukoza u krvi
								</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="overview-tab-glucose">
									@include('backend.layouts.patients.handlebars.bg-summary')
									<div id="bg-summary-hb-holder"></div>
								</div>
								<div class="tab-pane" id="overview-tab-insulin">
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('backend.layouts.health-logs.includes.modal-new-log')
@endsection


@section('after-scripts')
<script>
	$(document).ready(function() {
		setupGlucoseSummaryUI(true);
	});
</script>
@endsection
