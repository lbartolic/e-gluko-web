@extends ('backend.layouts.app')

@section ('title', 'Pacijenti')

@section('after-styles')
@endsection

@section('page-header')
<h1>
	Pacijenti
	<small>popis svih pacijenata</small>
</h1>
@endsection

@section('content')
<div class="box">
	<div class="box-body no-padding">
		<table class="table">
			<tbody>
                <tr>
                    <th colspan="5"></th>
                    <th class="border-left text-center" colspan="3">AVG vrijednosti zadnjih 7 dana</th>
                </tr>
				<tr>
					<th></th>
					<th>Pacijent</th>
					<th>Datum rođenja</th>
					<th>Dijabetes od</th>
					<th class="text-center">Zadnje mjerenje</th>
					<th class="border-left text-center">
                        <div>Kretanje <small>(dn.)</small></div>
                        <div>Kalorije <small>(dn.)</small></div>
                    </th>
					<th class="text-center">
						<div>GUK</div>
                        <div>Bolus/bazalni <small>(dn.)</small></div>
					</th>
				</tr>
				@foreach($patients as $patient)
				<tr>
					<td>
						<a class="btn btn-app" href="{{route('admin.patients.show', $patient->id)}}">
							<i class="fa fa-folder-open"></i> Prikaži
						</a>
					</td>
					<td>
                        <div><strong>{{$patient->first_name}}</strong></div>
                        <div><strong>{{$patient->last_name}}</strong></div>
						<div style="margin-top: 4px;"><span class="badge bg-{{config('application.colors.diabetes_type.' . $patient->patientInfo->diabetes_type)}}"><i class="fa _fa fa-tag"></i> DT{{$patient->patientInfo->diabetes_type}}</span></div>
					</td>
					<td>
						<div><span>{{$patient->patientInfo->birth_date_ts->format(config('application.time.date_formats.default_date'))}}</span></div>
						<div><small class="text-muted">({{ $patient->patientInfo->years }} god.)</small></div>
					</td>
					<td>
						<div><span>{{$patient->patientInfo->diabetes_from_ts->format(config('application.time.date_formats.default_date'))}}</span></div>
						<div><small class="text-muted">({{$patient->patientInfo->yearsDiabetes}} god.)</small></div>
					</td>
					<td class="text-center">
                        @if($patient->healthLogs->first() != null)
                        <div><span><i class="fa _fa fa-calendar"></i> {{ $patient->healthLogs->first()->log_ts->formatLocalized(config('application.time.locale_date_formats.default_day_date')) }}</span></div>
						<div><span><i class="fa _fa fa-clock-o"></i> {{ $patient->healthLogs->first()->log_ts->format(config('application.time.date_formats.default_time')) }}</span></div>
                        <div><small class="text-muted"><b>{{ $patient->healthLogs->first()->logType->type_name }}</b></small></div>
                        @else
                        <small class="text-muted">nema mjerenja</small>
                        @endif
					</td>
					<td class="border-left text-center">
                        @if($patient->stats['step']['total_logs'] != null || $patient->stats['calorie']['total_logs'] != null)
                            @if($patient->stats['step']['total_logs'] != null)
                                <big>
                                    <strong>@include('backend.layouts.health-logs.includes.step-indicator', ['level' => $patient->stats['step']['daily_avg_steps']["desc"]])
                                    {{ $patient->stats['step']['daily_avg_steps']['value'] }}</strong> <small>koraci</small>
                                </big>
                            @else
                                -
                            @endif
                            <div style="margin-bottom: 4px;"></div>
                            @if($patient->stats['calorie']['total_logs'] != null)
                                <big>
                                    <strong>{{ $patient->stats['calorie']['daily_avg_calories']['value'] }}</strong> <small>{{ $_base_units['calories'] }}</small>
                                </big>
                            @else
                                -
                            @endif
                            <div><small>MJERENO DANA: <strong>{{ $patient->stats['calorie']['logged_days'] }}</strong></small></div>
                        @else
                        <small class="text-muted">nema mjerenja</small>
                        @endif
					</td>
					<td class="text-center">
                        @if($patient->stats['glucose']['total_logs'] > 0 || $patient->stats['insulin']['logged_days'] > 0)
                            @if($patient->stats['glucose']['total_logs'] > 0)
                                <big>
                                    <strong>@include('backend.layouts.health-logs.includes.bg-indicator', ['bgLevel' => $patient->stats['glucose']['avg_value']["desc"]])
                                    {{ $patient->stats['glucose']['avg_value']['value'] or '-' }}</strong> <small>{{ $_base_units['bg'] }}</small>
                                </big>
                                <div><small>UK. OČITAVANJA: <strong>{{ $patient->stats['glucose']['total_logs'] }}</strong></small></div>
                            @else
                                -
                            @endif
                            <div style="margin-bottom: 4px;"></div>
                            @if($patient->stats['insulin']['daily_avg_units_bolus'] != null)
                                <big>
                                    <strong>@include('backend.layouts.health-logs.includes.insulin-indicator', ['doseLevel' => $patient->stats['insulin']['daily_avg_units_basal']["desc"]])
                                    {{ $patient->stats['insulin']['daily_avg_units_bolus']['value'] }}</strong> <small>{{ $_base_units['insulin'] }}</small>
                                </big>
                            @else
                                -
                            @endif
                            /
                            @if($patient->stats['insulin']['daily_avg_units_bolus'] != null)
                                <big>
                                    <strong>@include('backend.layouts.health-logs.includes.insulin-indicator', ['doseLevel' => $patient->stats['insulin']['daily_avg_units_bolus']["desc"]])
                                    {{ $patient->stats['insulin']['daily_avg_units_bolus']['value'] }}</strong> <small>{{ $_base_units['insulin'] }}</small>
                                </big>
                            @else
                                -
                            @endif
                            <div><small>UK. DOZIRANJA: <strong>{{ $patient->stats['insulin']['total_logs_bolus'] }} / {{ $patient->stats['insulin']['total_logs_basal'] }}</strong></small></div>
                        @else
                        <small class="text-muted">nema mjerenja</small>
                        @endif
					</td>
				</tr>
				@endforeach
			</tbody></table>
		</div>
		<!-- /.box-body -->
	</div>
	@endsection

	@section('after-scripts')
	@endsection
