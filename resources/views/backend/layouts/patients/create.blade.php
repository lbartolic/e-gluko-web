@extends ('backend.layouts.app')

@section ('title', 'Pacijenti')

@section('after-styles')
@endsection

@section('page-header')
<h1>
	Pacijenti
	<small>kreiranje novog</small>
</h1>
@endsection

@section('content')
<div class="row">
	<div class="col-md-8 col-lg-9">
        @if (isset($patient))
            {{ Form::model($patient, ['method' => 'PATCH', 'route' => ['admin.patients.update', $patient->id]]) }}
        @else
        {{ Form::open(['method' => 'POST', 'route' => ['admin.patients.store']]) }}
        @endif
            @include('backend.layouts.patients.includes.form')
        {{ Form::close() }}
	</div>
</div>
@endsection

@section('after-scripts')
<script>
	$(document).ready(function() {
		$('._datetimepicker-date').datetimepicker({
	        format: eGlukoData.def_date_format,
	        allowInputToggle: true,
	        locale: 'hr'
	    });
        
        @if (isset($patient))
            $('#dp-birth-date').data("DateTimePicker").date(moment('{{ $patient->patientInfo->birth_date_ts }}', eGlukoData.db_timestamp_format));
            $('#dp-diabetes-from').data("DateTimePicker").date(moment('{{ $patient->patientInfo->diabetes_from_ts }}', eGlukoData.db_timestamp_format));
        @endif
	});
</script>
@endsection
