<li class="{{ active_class(Active::checkUriPattern('admin/patients/*')) }} treeview">
	<a href="{{ route('admin.patients.show', [$user->id]) }}">
		<i class="fa fa-user"></i>
		<span>{{ $user->name }}</span>
	</a>

	<ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/patients/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/patients/*'), 'display: block;') }}">
		<li class="{{ active_class(Active::checkUriPattern('admin/patients/'.$user->id)) }}">
			<a href="{{ route('admin.patients.show', [$user->id]) }}">
				<i class="fa"></i>
				<span>Pregled</span>
			</a>
		</li>
		<li class="{{ active_class(Active::checkUriPattern('admin/patients/'.$user->id.'/health-logs/trends*')) }}">
			<a href="{{ route('admin.health-logs.trends.show', [$user->id, 'period']) }}">
				<i class="fa fa-area-chart"></i>
				<span>Trendovi</span>
			</a>
		</li>
		<li class="{{ active_class(Active::checkUriPattern('admin/patients/'.$user->id.'/health-logs/logs*')) }}">
			<a href="{{ route('admin.health-logs.logs.show', [$user->id]) }}">
				<i class="fa fa-list-ul"></i>
				<span>Zapisi i izvještaji</span>
			</a>
		</li>
		<li class="{{ active_class(Active::checkUriPattern('admin/patients/'.$user->id.'/health-logs/stats*')) }}">
			<a href="{{ route('admin.health-logs.stats.show', [$user->id]) }}">
				<i class="fa fa-table"></i>
				<span>Statistika</span>
			</a>
		</li>
		<li class="{{ active_class(Active::checkUriPattern('admin/patients/'.$user->id.'/health-goals')) }}">
			<a href="{{ route('admin.health-goals.index', [$user->id]) }}">
				<i class="fa fa-dot-circle-o"></i>
				<span>Ciljne vrijednosti</span>
			</a>
		</li>
	</ul>
</li>