<div class="row">
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Općeniti podaci</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-4 col-sm-12 col-md-4">
                        <div class="form-group {{ ($errors->has('first_name')) ? 'has-error' : '' }}">
                            <label>Ime</label>
                            {{ Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'ime']) }}
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-12 col-md-4">
                        <div class="form-group {{ ($errors->has('last_name')) ? 'has-error' : '' }}">
                            <label>Prezime</label>
                            {{ Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'prezime']) }}
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-12 col-md-4">
                        <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
                            <label>Email</label>
                            {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'email']) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-12 col-md-6">
                        <div class="form-group {{ ($errors->has('password')) ? 'has-error' : '' }}">
                            <label>Lozinka</label>
                            {{ Form::text('password', (!isset($patient)) ? str_random(8) : '', ['class' => 'form-control', 'placeholder' => 'lozinka']) }}
                            @if (isset($patient))
                            <small><i class="fa _fa fa-info-circle"></i> Ostaviti prazno ako se lozinka ne mijenja</small>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-12 col-md-6">
                        <div class="form-group {{ ($errors->has('status')) ? 'has-error' : '' }}">
                            <label style="display: block;">Status</label>
                            <div class="radio _inline-block _margin-r-15">
                                <label>
                                    {{ Form::radio('status', '1', true) }}
                                    Aktiviran
                                </label>
                            </div>
                            <div class="radio _inline-block">
                                <label>
                                    {{ Form::radio('status', '0') }}
                                    Neaktiviran
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Dodatni podaci</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-4 col-sm-12 col-md-4">
                        <div class="form-group {{ ($errors->has('patientInfo.birth_date_ts')) ? 'has-error' : '' }}">
                            <label>Datum rođenja</label>
                            <div class='input-group'>
                                {{ Form::text('patientInfo[birth_date_ts]', null, ['class' => 'form-control _datetimepicker-date', 'id' => 'dp-birth-date', 'placeholder' => 'datum rođenja']) }}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-12 col-md-4">
                        <div class="form-group {{ ($errors->has('patientInfo.height')) ? 'has-error' : '' }}">
                            <label>Visina</label>
                            {{ Form::number('patientInfo[height]', null, ['class' => 'form-control', 'placeholder' => 'visina']) }}
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-12 col-md-4">
                        <div class="form-group {{ ($errors->has('patientInfo.sex')) ? 'has-error' : '' }}">
                            <label style="display: block;">Spol</label>
                            <div class="radio _inline-block _margin-r-15">
                                <label>
                                    {{ Form::radio('patientInfo[sex]', '0', true) }}
                                    Muško
                                </label>
                            </div>
                            <div class="radio _inline-block">
                                <label>
                                    {{ Form::radio('patientInfo[sex]', '1') }}
                                    Žensko
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 col-sm-12 col-md-4">
                        <div class="form-group {{ ($errors->has('patientInfo.diabetes_from_ts')) ? 'has-error' : '' }}">
                            <label>Dijabetes od</label>
                            <div class='input-group'>
                                {{ Form::text('patientInfo[diabetes_from_ts]', null, ['class' => 'form-control _datetimepicker-date', 'id' => 'dp-diabetes-from', 'placeholder' => 'dijabetes od']) }}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-12 col-md-4">
                        <div class="form-group {{ ($errors->has('patientInfo.diabetes_type')) ? 'has-error' : '' }}">
                            <label style="display: block;">Tip dijabetesa</label>
                            <div class="radio _inline-block _margin-r-15">
                                <label>
                                    {{ Form::radio('patientInfo[diabetes_type]', '1', true) }}
                                    DT1
                                </label>
                            </div>
                            <div class="radio _inline-block">
                                <label>
                                    {{ Form::radio('patientInfo[diabetes_type]', '2') }}
                                    DT2
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-group clearfix no-margin">
    <button type="submit" class="btn btn-lg btn-primary pull-right">Spremi</button>
</div>