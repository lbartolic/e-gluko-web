<header class="main-header">

    <a href="{{ route('frontend.index') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
           {{ substr(app_name(), 0, 1) }}
        </span>

        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            {{ app_name() }}
        </span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <div class="_navbar-inner">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">{{ trans('labels.general.toggle_navigation') }}</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ access()->user()->picture }}" class="user-image" alt="User Avatar"/>
                            <span class="hidden-xs">{{ access()->user()->full_name }}</span>
                        </a>

                        <ul class="dropdown-menu" style="border: 0 !important; background: #605ba8; box-shadow: 0 0 15px rgba(0,0,0,0.25) !important;">
                            <li class="user-header" style="height: 135px;">
                                <img src="{{ access()->user()->picture }}" class="img-circle" alt="User Avatar"  style="width: 60px; height: 60px;"/>
                                <p>
                                    {{ access()->user()->full_name }} - {{ implode(", ", access()->user()->roles->pluck('name')->toArray()) }}
                                    <small>Registriran: {{ access()->user()->created_at->format("m/d/Y") }}</small>
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="{!! route('frontend.auth.logout') !!}" class="btn btn-danger btn-flat">
                                        <i class="fa fa-sign-out"></i>
                                        Odjava
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-custom-menu -->
        </div>
    </nav>
</header>
