var mix = require('laravel-mix');
var WebpackRTLPlugin = require('webpack-rtl-plugin');
var webpack = require('webpack');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/* webpack only for vendor files (only temporary) */

 mix.sass('resources/assets/sass/frontend/app.scss', 'public/css/frontend.css')
 .sass('resources/assets/sass/backend/app.scss', 'public/css/dashboard/vendor.css');
 //mix.sass('resources/assets/sass/app/backend/app.scss', 'public/css/dashboard/app.css')
 mix.js([
    'resources/assets/js/frontend/app.js',
    'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
    'resources/assets/js/plugins.js'
    ], 'public/js/frontend.js')
 .js([
    'resources/assets/js/backend/app.js',
    'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
    'resources/assets/js/plugins.js',
    'node_modules/jquery-slimscroll/jquery.slimscroll.min.js'
    ], 'public/js/backend.js')
 .webpackConfig({
    plugins: [
        new WebpackRTLPlugin('/css/[name].rtl.css')
    ]
});

 if(mix.inProduction){
    mix.version();
}