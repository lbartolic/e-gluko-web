/* laravel elixir only for app files (only temporary) */

var elixir = require('laravel-elixir');

elixir(function(mix) {
	// backend sass
	mix.sass([
		"./resources/assets/sass/app/backend/app.scss"
	], "./resources/assets/sass/app/backend/app_compiled.css");

	// frontend sass

	// backend styles bundle
	mix.styles([
	    "./resources/assets/lib/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css",
	    "./resources/assets/sass/app/backend/app_compiled.css"
	], 'public/css/dashboard/app/app.css');

	// frontend styles bundle
	/*mix.styles([
	    "./resources/assets/sass/frontend/app_compiled.css"
	], 'public/css/frontend.css');*/

	// backend scripts bundle
	mix.scripts([
    	'./node_modules/highcharts/highstock.js',
    	'./node_modules/highcharts/highcharts-more.js',
    	'./node_modules/underscore/underscore-min.js',
    	'./node_modules/moment/min/moment-with-locales.min.js',
    	'./node_modules/underscore/underscore-min.js',
    	'./node_modules/handlebars/dist/handlebars.min.js',
	    "./resources/assets/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js",
    	'./resources/assets/js/backend/app/charts.js',
    	'./resources/assets/js/backend/app/stats.js',
    	'./resources/assets/js/backend/app/app.js'
	], 'public/js/dashboard/app/backend.js');

	// frontend scripts bundle
	/*mix.scripts([
    	'./resources/assets/js/frontend.js'
	], 'public/js/frontend.js');*/

	mix.version([
         "public/css/dashboard/app/app.css",
         "public/js/dashboard/app/backend.js"
     ]);
});